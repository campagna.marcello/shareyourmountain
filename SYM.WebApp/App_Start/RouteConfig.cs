﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SYM.WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("Mountain/Details/{*pathInfo}");
            routes.IgnoreRoute("Route/Details/{*pathInfo}");

            routes.MapRoute(
                        name: "Index",
                        url: "sitemap.xml",
                        defaults: new { controller = "Sitemap", action = "Index" }
            );

            routes.MapRoute(
                       name: "Index2",
                       url: "sitemaproutes.xml",
                       defaults: new { controller = "Sitemap", action = "Index2" }
           );

            routes.MapRoute(
               name: "MountainDetailWithCulture",
               url: "{culture}/Mountain/{id}-{name}",
               defaults: new { culture = "it", controller = "Mountain", action = "Details" },
               constraints: new { culture = "it|en|de|es|fr" }
            );

            routes.MapRoute(
               name: "MountainDetail",
               url: "Mountain/{id}-{name}",
               defaults: new { culture = "it", controller = "Mountain", action = "Details" }
            );

            routes.MapRoute(
              name: "RouteDetailWithCulture",
              url: "{culture}/Route/{id}-{name}",
              defaults: new { culture = "it", controller = "Route", action = "Details" },
              constraints: new { culture = "it|en|de|es|fr" }
            );

            routes.MapRoute(
              name: "RouteDetail",
              url: "Route/{id}-{name}",
              defaults: new { culture = "it", controller = "Route", action = "Details" }
            );

            routes.MapRoute(
              name: "PublicProfileWithCulture",
              url: "Community/Profile/{id}",
              defaults: new { culture = "it", controller = "Community", action = "Profile" },
              constraints: new { culture = "it|en|de|es|fr" }
            );

            routes.MapRoute(
              name: "PublicProfile",
              url: "Community/Profile/{id}",
              defaults: new { culture = "it", controller = "Community", action = "Profile" }
            );

            routes.MapRoute(
                   name: "DefaultWithCulture",
                   url: "{culture}/{controller}/{action}/{id}",
                   defaults: new { culture = "it", controller = "Home", action = "Index", id = UrlParameter.Optional },
                   constraints: new { culture = "it|en|de|es|fr" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { culture = "it", controller = "Home", action = "Index", id = UrlParameter.Optional },
                constraints: new { culture = "it|en|de|es|fr" }
            );
        }
    }
}
