﻿using SYM.WebApp.Filters;
using System.Web.Mvc;

namespace SYM.WebApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LocalizationAttribute("it"), 0);
            //TODO: Add manage log in this method to track any exception
            filters.Add(new HandleAntiforgeryTokenErrorAttribute() { ExceptionType = typeof(HttpAntiForgeryException) });
        }
    }
}
