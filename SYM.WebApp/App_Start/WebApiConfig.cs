﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SYM.WebApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            //Add a new route template, the {action} parameter names the action method on the controller. 
            //With this style of routing, use attributes to specify the allowed HTTP methods. (RPC-style API)
            config.Routes.MapHttpRoute(
                name: "ActionApi",
                routeTemplate: "api/{controller}/{action}"
            );
        }
    }
}
