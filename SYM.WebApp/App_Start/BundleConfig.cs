﻿using System.Web;
using System.Web.Optimization;

namespace SYM.WebApp
{
    public class BundleConfig
    {

            public static void RegisterBundles(BundleCollection bundles)
        {

            #if DEBUG
                        BundleTable.EnableOptimizations = false;
            #else
                                            BundleTable.EnableOptimizations = true;
            #endif

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            #region Metronic

            #region Metronic_CSS
            bundles.Add(new StyleBundle("~/content/themes/m1/global/css").Include(
                        "~/Content/themes/m1/assets/global/plugins/bootstrap/css/bootstrap.css",
                        "~/Content/themes/m1/assets/global/plugins/uniform/css/uniform.default.css",
                        "~/Content/themes/m1/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css",
                        "~/Content/themes/m1/assets/global/plugins/morris/morris.css", //PAGE LEVEL PLUGIN STYLES
                        "~/Content/themes/m1/assets/global/plugins/select2/css/select2.css", //PAGE LEVEL PLUGIN STYLES
                        "~/Content/themes/m1/assets/global/plugins/select2/css/select2-bootstrap.css", //PAGE LEVEL PLUGIN STYLES
                        "~/Content/themes/m1/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css", //PAGE LEVEL PLUGIN STYLES
                        "~/Content/themes/m1/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css", //PAGE LEVEL PLUGIN STYLES,
                        "~/Content/themes/m1/assets/global/plugins/dropzone/css/dropzone.css", //PAGE LEVEL PLUGIN STYLES,
                        "~/Content/themes/m1/assets/global/css/components-rounded.css", //GLOBAL STYLES
                        "~/Content/themes/m1/assets/global/css/plugins.css", //GLOBAL STYLES
                        "~/Content/themes/m1/assets/global/plugins/bootstrap-toastr/toastr.css",
                        "~/Content/themes/m1/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css",
                        "~/Content/assets/plugins/flat-weather/css/flatWeatherPlugin.css",
                        "~/Content/themes/m1/assets/global/plugins/fancybox/source/jquery.fancybox.css"
                        ));

            bundles.Add(new StyleBundle("~/content/themes/m1/assets/layouts/4/css").Include(
                       "~/Content/themes/m1/assets/layouts/layout4/css/layout.css",
                       "~/Content/themes/m1/assets/layouts/layout4/css/themes/light.css",
                       "~/Content/themes/m1/assets/layouts/layout4/css/custom.css",
                       "~/Content/themes/m1/assets/layouts/layout4/css/icon-activities-custom.css",
                       "~/Content/themes/m1/assets/layouts/layout4/css/iconizr-svg-sprite.css",
                       "~/Content/themes/m1/assets/layouts/layout4/css/iconizr-svg-sprite-2016.css",
                       "~/Content/themes/m1/assets/layouts/layout4/css/iconizr-svg-sprite-2016-22.css"));

            //CSS 4 Login Page
            bundles.Add(new StyleBundle("~/content/themes/m1/assets/global/login/css").Include(
                       "~/Content/themes/m1/assets/global/plugins/bootstrap/css/bootstrap.css",
                       "~/Content/themes/m1/assets/global/plugins/uniform/css/uniform.default.css",
                       "~/Content/themes/m1/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.css",                     
                       "~/Content/themes/m1/assets/global/plugins/select2/css/select2.css",
                       "~/Content/themes/m1/assets/global/plugins/select2/css/select2-bootstrap.css",
                       "~/Content/themes/m1/assets/global/css/components-rounded.css",
                       "~/Content/themes/m1/assets/global/css/plugins.css",
                       "~/Content/themes/m1/assets/pages/css/login2.css"
                       ));
            #endregion Metronic_CSS

            #region Metronic_JAVASCRIPT
            //Common for all Template Metronic
            bundles.Add(new ScriptBundle("~/content/themes/m1/assets/core/js").Include(
                       "~/Content/themes/m1/assets/global/plugins/jquery.js",
                       "~/Content/themes/m1/assets/global/plugins/bootstrap/js/bootstrap.js",
                       "~/Content/themes/m1/assets/global/plugins/js.cookie.js",
                       "~/Content/themes/m1/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js",
                       "~/Content/themes/m1/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.js",
                       "~/Content/themes/m1/assets/global/plugins/jquery.blockui.js",
                       "~/Content/themes/m1/assets/global/plugins/uniform/jquery.uniform.js",
                       "~/Content/themes/m1/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.js",
                       "~/Content/themes/m1/assets/global/plugins/select2/js/select2.full.js", //Page Level Plugin
                       "~/Content/themes/m1/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js", //Page Level Plugin   
                       "~/Content/themes/m1/assets/global/plugins/bootstrap-select/js/bootstrap-select.js",
                       "~/Content/themes/m1/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js",
                       "~/Content/themes/m1/assets/global/plugins/bootstrap-toastr/toastr.js",
                       "~/Content/themes/m1/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
                       "~/Content/themes/m1/assets/global/plugins/fancybox/source/jquery.fancybox.js"
                       ));

            //For Only Login Metronic
            bundles.Add(new ScriptBundle("~/content/themes/m1/assets/login/js").Include(
                        "~/Content/themes/m1/assets/global/plugins/jquery-validation/js/jquery.validate.js",
                        "~/Content/themes/m1/assets/global/plugins/jquery-validation/js/additional-methods.js",
                        "~/Content/themes/m1/assets/global/plugins/backstretch/jquery.backstretch.js",
                        "~/Content/themes/m1/assets/global/scripts/app.js"
                       ));

            bundles.Add(new ScriptBundle("~/content/themes/m1/assets/plugins/pagedetails/js").Include(
                      "~/Content/themes/m1/assets/global/plugins/jquery-imagefill.js",
                      "~/Content/themes/m1/assets/global/plugins/imagesloaded.pkgd.min.js",
                      "~/Content/themes/m1/assets/global/plugins/counterup/jquery.waypoints.min.js",
                      "~/Content/themes/m1/assets/global/plugins/counterup/jquery.counterup.min.js",
                      "~/Content/assets/plugins/flat-weather/js/jquery.flatWeatherPlugin.js",
                      "~/Content/assets/plugins/tiles-grid/js/jquery.finaltilesgallery.js",
                      "~/Content/assets/plugins/tiles-grid/js/lightbox2.js",
                      "~/Content/assets/plugins/owlcarousel2/owl.carousel.js",
                      "~/Content/themes/m1/assets/global/plugins/dropzone/dropzone.js"
                       ));

            bundles.Add(new ScriptBundle("~/content/themes/m1/assets/layouts/js").Include(
                        "~/scripts/custom/select2_routeDD.js",
                        "~/Content/themes/m1/assets/layouts/layout4/scripts/layout.js",
                        "~/Content/themes/m1/assets/layouts/layout4/scripts/demo.js"
                        ));

            //METRONIC JS 4 STATS
            bundles.Add(new ScriptBundle("~/content/themes/m1/assets/plugins/stats/js").Include(
                        "~/Content/themes/m1/assets/global/plugins/amcharts/amcharts/amcharts.js",
                        "~/Content/themes/m1/assets/global/plugins/amcharts/amcharts/serial.js",
                        "~/Content/themes/m1/assets/global/plugins/amcharts/amcharts/themes/light.js",
                        "~/Content/themes/m1/assets/global/plugins/amcharts/amcharts/themes/patterns.js",
                        "~/Content/themes/m1/assets/global/plugins/amcharts/amcharts/themes/chalk.js",
                        "~/Content/themes/m1/assets/global/plugins/amcharts/ammap/ammap.js",
                        "~/Content/themes/m1/assets/global/plugins/amcharts/ammap/maps/js/worldHigh.js"
                       ));
            #endregion

            #endregion

            #region Home
            bundles.Add(new StyleBundle("~/content/themes/home/css").Include(
                       "~/Content/themes/sleek/css/bootstrap.css",
                       "~/Content/themes/sleek/js/slick/slick.css",
                       "~/Content/themes/sleek/js/flex-slider/flexslider.css",
                       "~/Content/themes/sleek/js/owl-carousel/owl.carousel.css",
                       "~/Content/themes/sleek/js/owl-carousel/owl.theme.css",
                       "~/Content/themes/sleek/js/owl-carousel/owl.transitions.css",
                       "~/Content/themes/sleek/css/prettyphoto.css",
                       "~/Content/themes/sleek/css/style.css",
                       "~/Content/themes/sleek/css/jquery-ui.css",
                       "~/Content/themes/sleek/css/twitter.css",
                       "~/Content/themes/sleek/css/colors/skin-green5.css"
               ));

            bundles.Add(new ScriptBundle("~/content/themes/home/js").Include(
                        "~/Content/themes/sleek/js/jquery.js",
                        "~/Content/themes/sleek/js/bootstrap.js",
                        "~/Content/themes/sleek/js/jquery.easing.js",
                        "~/Content/themes/sleek/js/flex-slider/jquery.flexslider.js",
                        "~/Content/themes/sleek/js/owl-carousel/owl.carousel.js",
                        "~/Content/themes/sleek/js/slick/slick.js",
                        "~/Content/themes/sleek/js/jquery.prettyphoto.js",
                        "~/Content/themes/sleek/js/contact.js",
                        "~/Content/themes/sleek/js/typeahead.bundle.js",
                        "~/Content/themes/m1/assets/global/plugins/backstretch/jquery.backstretch.js",
                        "~/Content/assets/plugins/typed/typed.js",
                        "~/Content/assets/plugins/owlcarousel2/owl.carousel.min.js"
            ));

            bundles.Add(new ScriptBundle("~/content/themes/home/js_script").Include(
                        "~/Content/themes/sleek/js/mc/main.js",
                        "~/Content/themes/sleek/js/gmaps.js",
                        "~/Content/themes/sleek/js/home.js"));
            #endregion
        }
    }
}
