CREATE DATABASE  IF NOT EXISTS `sym.core` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sym.core`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: sym.core
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `act_activity`
--

DROP TABLE IF EXISTS `act_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `act_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(45) DEFAULT NULL,
  `activity_description` varchar(45) DEFAULT NULL,
  `activity_parent_id` int(11) DEFAULT NULL,
  `activity_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_activity`
--

LOCK TABLES `act_activity` WRITE;
/*!40000 ALTER TABLE `act_activity` DISABLE KEYS */;
INSERT INTO `act_activity` VALUES (1,'Alpinismo',NULL,0,'2014-09-07 11:51:56'),(2,'Free Climbing',NULL,0,'2014-09-07 11:51:56'),(3,'Boulder',NULL,2,'2014-09-07 11:51:56'),(4,'Falesia',NULL,2,'2014-09-07 11:51:56'),(5,'Vie Lunghe',NULL,2,'2014-09-07 11:51:56'),(6,'Trekking',NULL,0,'2014-09-07 11:51:56'),(7,'Vie Ferrate',NULL,0,'2014-09-07 11:51:56'),(8,'Neve',NULL,0,'2014-09-07 11:51:56'),(9,'Scialpismo',NULL,8,'2014-09-07 11:51:56'),(10,'Sci di fondo',NULL,8,'2014-09-07 11:51:56'),(11,'Ciaspolate',NULL,8,'2014-09-07 11:51:56'),(12,'Ghiaccio e Misto',NULL,1,'2015-01-06 19:51:58');
/*!40000 ALTER TABLE `act_activity` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-31 14:27:36
