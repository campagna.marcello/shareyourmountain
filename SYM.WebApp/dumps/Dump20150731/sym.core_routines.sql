CREATE DATABASE  IF NOT EXISTS `sym.core` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sym.core`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: sym.core
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `v_route_autocomplete`
--

DROP TABLE IF EXISTS `v_route_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_route_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_route_autocomplete` (
  `id` tinyint NOT NULL,
  `title` tinyint NOT NULL,
  `country_id` tinyint NOT NULL,
  `country_code` tinyint NOT NULL,
  `region_id` tinyint NOT NULL,
  `region_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_mountain_autocomplete`
--

DROP TABLE IF EXISTS `v_mountain_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_mountain_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_mountain_autocomplete` (
  `id` tinyint NOT NULL,
  `mntn_name` tinyint NOT NULL,
  `mntn_title` tinyint NOT NULL,
  `country_id` tinyint NOT NULL,
  `country_code` tinyint NOT NULL,
  `region_id` tinyint NOT NULL,
  `region_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_geocity_autocomplete`
--

DROP TABLE IF EXISTS `v_geocity_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_geocity_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_geocity_autocomplete` (
  `id` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `region` tinyint NOT NULL,
  `country` tinyint NOT NULL,
  `lat` tinyint NOT NULL,
  `lng` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_route_autocomplete`
--

/*!50001 DROP TABLE IF EXISTS `v_route_autocomplete`*/;
/*!50001 DROP VIEW IF EXISTS `v_route_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_route_autocomplete` AS select `route`.`id` AS `id`,`route`.`route_title` AS `title`,`route`.`country_id` AS `country_id`,`geo_region`.`country_code` AS `country_code`,coalesce(`geo_region`.`id`,0) AS `region_id`,coalesce(`geo_region`.`region_name`,'') AS `region_name` from ((`route` join `mountain` on((`route`.`mountain_id` = `mountain`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mountain_autocomplete`
--

/*!50001 DROP TABLE IF EXISTS `v_mountain_autocomplete`*/;
/*!50001 DROP VIEW IF EXISTS `v_mountain_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_mountain_autocomplete` AS select `mntncountry`.`mntn_id` AS `id`,`mntncountry`.`mntn_name` AS `mntn_name`,concat(`mntncountry`.`mntn_name`,' - ',`mountain`.`mntn_altitude`,' m.') AS `mntn_title`,`mntncountry`.`country_id` AS `country_id`,`mntncountry`.`country_code` AS `country_code`,coalesce(`mntnregion`.`region_id`,0) AS `region_id`,coalesce(`mntnregion`.`region_name`,'') AS `region_name` from ((`ass_mountaincountry` `mntncountry` join `mountain` on((`mntncountry`.`mntn_id` = `mountain`.`id`))) left join `ass_mountainregion` `mntnregion` on((`mntnregion`.`mntn_id` = `mntncountry`.`mntn_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_geocity_autocomplete`
--

/*!50001 DROP TABLE IF EXISTS `v_geocity_autocomplete`*/;
/*!50001 DROP VIEW IF EXISTS `v_geocity_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_geocity_autocomplete` AS select distinct `geo_city`.`id` AS `id`,`geo_city`.`city_name` AS `city`,`geo_region`.`region_name` AS `region`,`geo_city`.`country_code` AS `country`,`geo_city`.`city_latitude` AS `lat`,`geo_city`.`city_longitude` AS `lng` from (`geo_city` join `geo_region` on(((`geo_city`.`region_code` = `geo_region`.`region_code`) and (`geo_city`.`country_code` = `geo_region`.`country_code`)))) where (`geo_city`.`country_code` in ('IT','CH','DE','FR','ES')) order by `geo_city`.`city_name` limit 100 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping routines for database 'sym.core'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-31 14:27:37
