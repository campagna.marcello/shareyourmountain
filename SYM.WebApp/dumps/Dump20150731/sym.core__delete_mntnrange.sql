CREATE DATABASE  IF NOT EXISTS `sym.core` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sym.core`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: sym.core
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `_delete_mntnrange`
--

DROP TABLE IF EXISTS `_delete_mntnrange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_delete_mntnrange` (
  `mntnRange_id` int(11) NOT NULL AUTO_INCREMENT,
  `mntnRange_name` varchar(100) DEFAULT NULL,
  `mntnRange_parent_id` int(11) DEFAULT NULL,
  `mntnRange_description` varchar(45) DEFAULT NULL,
  `mntnRange_cardinal_point` varchar(2) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `mntnRange_dateins` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mntnRange_id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COMMENT='Per le Alpi ho utlizzato la classificazione SOUISA';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_delete_mntnrange`
--

LOCK TABLES `_delete_mntnrange` WRITE;
/*!40000 ALTER TABLE `_delete_mntnrange` DISABLE KEYS */;
INSERT INTO `_delete_mntnrange` VALUES (1,'Alpi',0,NULL,NULL,'it/',NULL),(2,'Appennini',0,NULL,NULL,'it',NULL),(3,'Pirenei',0,NULL,NULL,'es/fr',NULL),(4,'Appennino Ligure',2,NULL,NULL,'it',NULL),(5,'Appennino Tosco-emiliano',2,NULL,NULL,'it',NULL),(6,'Appennino Umbro-marchigiano',2,NULL,NULL,'it',NULL),(7,'Appennino Abruzzese',2,NULL,NULL,'it',NULL),(8,'Appennino Sannita',2,NULL,NULL,'it',NULL),(9,'Appennino Lucano',2,NULL,NULL,'it',NULL),(10,'Appennino Campano',2,NULL,NULL,'it',NULL),(11,'Appennino Calabro',2,NULL,NULL,'it',NULL),(12,'Appennino Siculo',2,NULL,NULL,'it',NULL),(13,'Alpi Liguri',1,NULL,'SO','it/fr',NULL),(14,'Alpi Marittime e Prealpi di Nizza',1,NULL,'SO','it/fr/mc',NULL),(15,'Alpi e Prealpi di Provenza',1,NULL,'SO','fr',NULL),(16,'Alpi Cozie',1,NULL,'SO','it/fr',NULL),(17,'Alpi del Delfinato',1,NULL,'SO','fr',NULL),(18,'Prealpi del Delfinato',1,NULL,'SO','fr',NULL),(19,'Alpi Graie',1,NULL,'NO','it/fr/ch',NULL),(20,'Prealpi di Savoia',1,NULL,'NO','fr/ch',NULL),(21,'Alpi Pennine',1,NULL,'NO','it/ch',NULL),(22,'Alpi Lepontine',1,NULL,'NO','it/ch',NULL),(23,'Prealpi Luganesi',1,NULL,'NO','it/ch',NULL),(24,'Alpi bernesi',1,NULL,'NO','ch',NULL),(25,'Alpi Glaronesi',1,NULL,'NO','ch',NULL),(26,'Prealpi svizzere',1,NULL,'NO','ch',NULL),(27,'Alpi Retiche occidentali',1,NULL,NULL,'it/ch/at',NULL),(28,'Alpi Retiche orientali',1,NULL,NULL,'it/at',NULL),(29,'Alpi dei Tauri occidentali',1,NULL,NULL,'it/at',NULL),(30,'Alpi dei Tauri orientali',1,NULL,NULL,'at',NULL),(31,'Alpi di Stiria e Carinzia',1,NULL,NULL,'at',NULL),(32,'Prealpi di Stiria',1,NULL,NULL,'at/slo/h',NULL),(33,'Alpi Calcaree Nordtirolesi',1,NULL,'NE','at/d',NULL),(34,'Alpi Bavaresi',1,NULL,'NE','at/d',NULL),(35,'Alpi Scistose Tirolesi',1,NULL,'NE','at',NULL),(36,'Alpi Settentrionali Salisburghesi',1,NULL,'NE','at/d',NULL),(37,'Alpi del Salzkammergut e dell\'Alta Austria',1,NULL,'NE','at',NULL),(38,'Alpi Settentrionali di Stiria',1,NULL,'NE','at',NULL),(39,'Alpi della Bassa Austria',1,NULL,'NE','at',NULL),(40,'Alpi Retiche meridionali',1,NULL,'SE','it',NULL),(41,'Alpi Orobie e Prealpi Bergamasche',1,NULL,'SE','it',NULL),(42,'Prealpi Bresciane e Gardesane',1,NULL,'SE','it',NULL),(43,'Dolomiti',1,NULL,'SE','it',NULL),(44,'Prealpi Venete',1,NULL,'SE','it',NULL),(45,'Alpi Carniche e della Gail',1,NULL,'SE','it/at',NULL),(46,'Alpi e Prealpi Giulie',1,NULL,'SE','it/slo',NULL),(47,'Alpi di Carinzia e di Slovenia',1,NULL,'SE','it/slo/at',NULL),(48,'Prealpi Slovene',1,NULL,'SE','at/slo',NULL),(49,'Prealpi Liguri',13,NULL,NULL,'it',NULL),(50,'Alpi del Marguareis',13,NULL,NULL,'it/fr',NULL),(51,'Alpi Marittime ',14,NULL,NULL,'it/fr',NULL),(52,'Prealpi di Nizza',14,NULL,NULL,'it/fr',NULL),(53,'Alpi di Provenza',15,NULL,NULL,'fr',NULL),(54,'Prealpi di Digne',15,NULL,NULL,'fr',NULL),(55,'Prealpi di Grasse',15,NULL,NULL,'fr',NULL),(56,'Prealpi di Vaucluse',15,NULL,NULL,'fr',NULL),(57,'Alpi del Monviso',16,NULL,NULL,'fr/it',NULL),(58,'Alpi del Monginevro',16,NULL,NULL,'fr/it',NULL),(59,'Alpi del Moncenisio',16,NULL,NULL,'fr/it',NULL),(60,'Alpi delle Grandes Rousses e delle Aiguilles d\'Averes',17,NULL,NULL,'fr',NULL),(61,'Catena di Belledonne',17,NULL,NULL,'fr',NULL),(62,'Massiccio des Ecrins',17,NULL,NULL,'fr',NULL),(63,'Massiccio del Taillefer',17,NULL,NULL,'fr',NULL),(64,'Massiccio del Champsaur',17,NULL,NULL,'fr',NULL),(65,'Massiccio dell\'Embrunais',17,NULL,NULL,'fr',NULL),(66,'Monti orientali di Gap',17,NULL,NULL,'fr',NULL),(67,'Prealpi del Devoluy',18,NULL,NULL,'fr',NULL),(68,'Prealpi occidentali di Gap',18,NULL,NULL,'fr',NULL),(69,'Prealpi del Vercors',18,NULL,NULL,'fr',NULL),(70,'Prealpi del Diois',18,NULL,NULL,'fr',NULL),(71,'Prealpi delle Baronnies',18,NULL,NULL,'fr',NULL),(72,'Alpi di Lanzo e dell\'Alta Moriana',19,NULL,NULL,'fr/it',NULL),(73,'Alpi della Vanoise e del Grand Arc',19,NULL,NULL,'fr',NULL),(74,'Alpi della Grande Sassière e del Rutor',19,NULL,NULL,'fr/it',NULL),(75,'Alpi del Gran Paradiso',19,NULL,NULL,'it',NULL),(76,'Alpi del Monte Bianco',19,NULL,NULL,'it/fr/ch',NULL),(77,'Alpi del Beaufortain',19,NULL,NULL,'fr',NULL),(78,'Catena delle Aiguilles Rouges',20,NULL,NULL,'fr',NULL),(79,'Prealpi del Giffre',20,NULL,NULL,'fr/ch',NULL),(80,'Prealpi dello Sciablese',20,NULL,NULL,'fr/ch',NULL),(81,'Prealpi dei Bornes',20,NULL,NULL,'fr',NULL),(82,'Prealpi dei Bauges',20,NULL,NULL,'fr',NULL),(83,'Prealpi della Chartreuse',20,NULL,NULL,'fr',NULL),(84,'Alpi del Grand Combin',21,NULL,NULL,'it/ch',NULL),(85,'Alpi del Weisshorn e del Cervino',21,NULL,NULL,'it/ch',NULL),(86,'Alpi del Monte Rosa',21,NULL,NULL,'it/ch',NULL),(87,'Alpi Biellesi e Cusiane',21,NULL,NULL,'it',NULL),(88,'Alpi del Mischabel e del Weissmies',21,NULL,NULL,'ch/it',NULL),(89,'Alpi del Monte Leone e del San Gottardo',22,NULL,NULL,'ch/it',NULL),(90,'Alpi Ticinesi e del Verbano',22,NULL,NULL,'ch/it',NULL),(91,'Alpi dell\'Adula',22,NULL,NULL,'ch/it',NULL),(92,'Prealpi Comasche',23,NULL,NULL,'ch/it',NULL),(93,'Prealpi Varesine',23,NULL,NULL,'ch/it',NULL),(94,'Alpi Urane',24,NULL,NULL,'ch',NULL),(95,'Alpi di Vaud',24,NULL,NULL,'ch',NULL),(96,'Alpi Urano-Glaronesi',25,NULL,NULL,'ch',NULL),(97,'Alpi Glaronesi iss',25,NULL,NULL,'ch',NULL),(98,'Prealpi di Vaud e Friburgo',26,NULL,NULL,'ch',NULL),(99,'Prealpi Bernesi',26,NULL,NULL,'ch',NULL),(100,'Prealpi di Lucerna e di Untervaldo',26,NULL,NULL,'ch',NULL),(101,'Prealpi di Svitto e di Uri',26,NULL,NULL,'ch',NULL),(102,'Prealpi di Appenzello e di San Gallo',26,NULL,NULL,'ch',NULL),(103,'Alpi Bernesi - sottosezione',24,NULL,NULL,'ch',NULL),(104,'Alpi del Platta',27,NULL,NULL,'ch/it',NULL),(105,'Alpi dell\'Albula',27,NULL,NULL,'ch',NULL),(106,'Alpi del Bernina',27,NULL,NULL,'ch/it',NULL),(107,'Alpi di Livigno',27,NULL,NULL,'ch/it',NULL),(108,'Alpi della Val Müstair',27,NULL,NULL,'ch/it/au',NULL),(109,'Alpi del Silvretta, del Samnaun e del Verwall',27,NULL,NULL,'ch/au',NULL),(110,'Alpi del Plessur',27,NULL,NULL,'ch',NULL),(111,'Catena del Rätikon',27,NULL,NULL,'ch/au/',NULL),(112,'Alpi Venoste',28,NULL,NULL,'it/au',NULL),(113,'Alpi dello Stubai',28,NULL,NULL,'it/au',NULL),(114,'Alpi Sarentine',28,NULL,NULL,'it',NULL),(115,'Alpi dell\'Ortles',40,NULL,NULL,'it',NULL),(116,'Alpi della Val di Non',40,NULL,NULL,'it',NULL),(117,'Alpi dell\'Adamello e della Presanella',40,NULL,NULL,'it',NULL),(118,'Dolomiti di Brenta',40,NULL,NULL,'it',NULL),(119,'Alpi Orobie',41,NULL,NULL,'it',NULL),(120,'Prealpi Bergamasche',41,NULL,NULL,'it',NULL);
/*!40000 ALTER TABLE `_delete_mntnrange` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-31 14:27:33
