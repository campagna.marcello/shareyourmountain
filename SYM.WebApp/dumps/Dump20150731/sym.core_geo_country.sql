CREATE DATABASE  IF NOT EXISTS `sym.core` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sym.core`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: sym.core
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `geo_country`
--

DROP TABLE IF EXISTS `geo_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geo_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) DEFAULT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geo_country`
--

LOCK TABLES `geo_country` WRITE;
/*!40000 ALTER TABLE `geo_country` DISABLE KEYS */;
INSERT INTO `geo_country` VALUES (1,'Afghanistan','AF'),(2,'Albania','AL'),(3,'Algeria','DZ'),(4,'Andorra','AD'),(5,'Angola','AO'),(6,'Anguilla','AI'),(7,'Antartide','AQ'),(8,'Antigua e Barbuda','AG'),(9,'Antille Olandesi','AN'),(10,'Arabia Saudita','SA'),(11,'Argentina','AR'),(12,'Armenia','AM'),(13,'Aruba','AW'),(14,'Australia','AU'),(15,'Austria','AT'),(16,'Azerbaigian','AZ'),(17,'Bahamas','BS'),(18,'Bahrein','BH'),(19,'Bangladesh','BD'),(20,'Barbados','BB'),(21,'Belgio','BE'),(22,'Belize','BZ'),(23,'Benin','BJ'),(24,'Bermuda','BM'),(25,'Bhutan','BT'),(26,'Bielorussia','BY'),(27,'Bolivia','BO'),(28,'Bosnia Erzegovina','BA'),(29,'Botswana','BW'),(30,'Brasile','BR'),(31,'Brunei','BN'),(32,'Bulgaria','BG'),(33,'Burkina Faso','BF'),(34,'Burundi','BI'),(35,'Cambogia','KH'),(36,'Camerun','CM'),(37,'Canada','CA'),(38,'Capo Verde','CV'),(39,'Ciad','TD'),(40,'Cile','CL'),(41,'Cina','CN'),(42,'Cipro','CY'),(43,'Colombia','CO'),(44,'Comore','KM'),(45,'Congo','CG'),(46,'Corea del Nord','KP'),(47,'Corea del Sud','KR'),(48,'Costa Rica','CR'),(49,'Costa d’Avorio','CI'),(50,'Croazia','HR'),(51,'Cuba','CU'),(52,'Danimarca','DK'),(53,'Dominica','DM'),(54,'Ecuador','EC'),(55,'Egitto','EG'),(56,'El Salvador','SV'),(57,'Emirati Arabi Uniti','AE'),(58,'Eritrea','ER'),(59,'Estonia','EE'),(60,'Etiopia','ET'),(61,'Federazione Russa','RU'),(62,'Figi','FJ'),(63,'Filippine','PH'),(64,'Finlandia','FI'),(65,'Francia','FR'),(66,'Gabon','GA'),(67,'Gambia','GM'),(68,'Georgia','GE'),(69,'Georgia del Sud e Isole Sandwich del Sud','GS'),(70,'Germania','DE'),(71,'Ghana','GH'),(72,'Giamaica','JM'),(73,'Giappone','JP'),(74,'Gibilterra','GI'),(75,'Gibuti','DJ'),(76,'Giordania','JO'),(77,'Grecia','GR'),(78,'Grenada','GD'),(79,'Groenlandia','GL'),(80,'Guadalupa','GP'),(81,'Guam','GU'),(82,'Guatemala','GT'),(83,'Guernsey','GG'),(84,'Guiana Francese','GF'),(85,'Guinea','GN'),(86,'Guinea Equatoriale','GQ'),(87,'Guinea-Bissau','GW'),(88,'Guyana','GY'),(89,'Haiti','HT'),(90,'Honduras','HN'),(91,'India','IN'),(92,'Indonesia','ID'),(93,'Iran','IR'),(94,'Iraq','IQ'),(95,'Irlanda','IE'),(96,'Islanda','IS'),(97,'Isola Bouvet','BV'),(98,'Isola Norfolk','NF'),(99,'Isola di Christmas','CX'),(100,'Isola di Man','IM'),(101,'Isole Aland','AX'),(102,'Isole Cayman','KY'),(103,'Isole Cocos','CC'),(104,'Isole Cook','CK'),(105,'Isole Falkland','FK'),(106,'Isole Faroe','FO'),(107,'Isole Heard ed Isole McDonald','HM'),(108,'Isole Marianne Settentrionali','MP'),(109,'Isole Marshall','MH'),(110,'Isole Minori lontane dagli Stati Uniti','UM'),(111,'Isole Solomon','SB'),(112,'Isole Turks e Caicos','TC'),(113,'Isole Vergini Americane','VI'),(114,'Isole Vergini Britanniche','VG'),(115,'Israele','IL'),(116,'Italia','IT'),(117,'Jersey','JE'),(118,'Kazakistan','KZ'),(119,'Kenya','KE'),(120,'Kirghizistan','KG'),(121,'Kiribati','KI'),(122,'Kuwait','KW'),(123,'Laos','LA'),(124,'Lesotho','LS'),(125,'Lettonia','LV'),(126,'Libano','LB'),(127,'Liberia','LR'),(128,'Libia','LY'),(129,'Liechtenstein','LI'),(130,'Lituania','LT'),(131,'Lussemburgo','LU'),(132,'Madagascar','MG'),(133,'Malawi','MW'),(134,'Maldive','MV'),(135,'Malesia','MY'),(136,'Mali','ML'),(137,'Malta','MT'),(138,'Marocco','MA'),(139,'Martinica','MQ'),(140,'Mauritania','MR'),(141,'Mauritius','MU'),(142,'Mayotte','YT'),(143,'Messico','MX'),(144,'Micronesia','FM'),(145,'Moldavia','MD'),(146,'Monaco','MC'),(147,'Mongolia','MN'),(148,'Montenegro','ME'),(149,'Montserrat','MS'),(150,'Mozambico','MZ'),(151,'Myanmar','MM'),(152,'Namibia','NA'),(153,'Nauru','NR'),(154,'Nepal','NP'),(155,'Nicaragua','NI'),(156,'Niger','NE'),(157,'Nigeria','NG'),(158,'Niue','NU'),(159,'Norvegia','NO'),(160,'Nuova Caledonia','NC'),(161,'Nuova Zelanda','NZ'),(162,'Oman','OM'),(163,'Paesi Bassi','NL'),(164,'Pakistan','PK'),(165,'Palau','PW'),(166,'Palestina','PS'),(167,'Panama','PA'),(168,'Papua Nuova Guinea','PG'),(169,'Paraguay','PY'),(170,'Perù','PE'),(171,'Pitcairn','PN'),(172,'Polinesia Francese','PF'),(173,'Polonia','PL'),(174,'Portogallo','PT'),(175,'Portorico','PR'),(176,'Qatar','QA'),(177,'Regione Amministrativa Speciale di Hong Kong della Repubblica Popolare Cinese','HK'),(178,'Regione Amministrativa Speciale di Macao della Repubblica Popolare Cinese','MO'),(179,'Regno Unito','GB'),(180,'Repubblica Ceca','CZ'),(181,'Repubblica Centrafricana','CF'),(182,'Repubblica Democratica del Congo','CD'),(183,'Repubblica Dominicana','DO'),(184,'Repubblica di Macedonia','MK'),(185,'Romania','RO'),(186,'Ruanda','RW'),(187,'Réunion','RE'),(188,'Sahara Occidentale','EH'),(189,'Saint Kitts e Nevis','KN'),(190,'Saint Lucia','LC'),(191,'Saint Pierre e Miquelon','PM'),(192,'Saint Vincent e Grenadines','VC'),(193,'Samoa','WS'),(194,'Samoa Americane','AS'),(195,'San Bartolomeo','BL'),(196,'San Marino','SM'),(197,'Sant’Elena','SH'),(198,'Sao Tomé e Príncipe','ST'),(199,'Senegal','SN'),(200,'Serbia','RS'),(201,'Serbia e Montenegro','CS'),(202,'Seychelles','SC'),(203,'Sierra Leone','SL'),(204,'Singapore','SG'),(205,'Siria','SY'),(206,'Slovacchia','SK'),(207,'Slovenia','SI'),(208,'Somalia','SO'),(209,'Spagna','ES'),(210,'Sri Lanka','LK'),(211,'Stati Uniti','US'),(212,'Sudafrica','ZA'),(213,'Sudan','SD'),(214,'Suriname','SR'),(215,'Svalbard e Jan Mayen','SJ'),(216,'Svezia','SE'),(217,'Svizzera','CH'),(218,'Swaziland','SZ'),(219,'Tagikistan','TJ'),(220,'Tailandia','TH'),(221,'Taiwan','TW'),(222,'Tanzania','TZ'),(223,'Territori australi francesi','TF'),(224,'Territorio Britannico dell’Oceano Indiano','IO'),(225,'Timor Est','TL'),(226,'Togo','TG'),(227,'Tokelau','TK'),(228,'Tonga','TO'),(229,'Trinidad e Tobago','TT'),(230,'Tunisia','TN'),(231,'Turchia','TR'),(232,'Turkmenistan','TM'),(233,'Tuvalu','TV'),(234,'Ucraina','UA'),(235,'Uganda','UG'),(236,'Ungheria','HU'),(237,'Uruguay','UY'),(238,'Uzbekistan','UZ'),(239,'Vanuatu','VU'),(240,'Vaticano','VA'),(241,'Venezuela','VE'),(242,'Vietnam','VN'),(243,'Wallis e Futuna','WF'),(244,'Yemen','YE'),(245,'Zambia','ZM'),(246,'Zimbabwe','ZW'),(247,'regione non valida o sconosciuta','ZZ');
/*!40000 ALTER TABLE `geo_country` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-31 14:27:34
