CREATE DATABASE  IF NOT EXISTS `sym.core` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sym.core`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: sym.core
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `_delete_ck_outdoor_activity`
--

DROP TABLE IF EXISTS `_delete_ck_outdoor_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_delete_ck_outdoor_activity` (
  `oa_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `oa_name` varchar(100) DEFAULT NULL,
  `oa_slug` varchar(100) DEFAULT NULL,
  `oa_parent` smallint(6) DEFAULT NULL,
  `oa_description` text,
  PRIMARY KEY (`oa_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Elenco di tutte le attività sportive outdoor gestite nella p';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_delete_ck_outdoor_activity`
--

LOCK TABLES `_delete_ck_outdoor_activity` WRITE;
/*!40000 ALTER TABLE `_delete_ck_outdoor_activity` DISABLE KEYS */;
INSERT INTO `_delete_ck_outdoor_activity` VALUES (1,'Alpinismo','alpinismo',0,NULL),(2,'Free Climbing','free-climbing',0,NULL),(3,'Boulder','boulder',2,NULL),(4,'Falesia','falesia',2,NULL),(5,'Vie Lunghe','vie-lunghe',2,NULL),(6,'Trekking','trekking',0,NULL),(7,'Vie Ferrate','vie-ferrate',0,NULL),(8,'Neve','neve',0,NULL),(9,'Scialpismo','scialpinismo',8,NULL),(10,'Sci di fondo','sci-di-fondo',8,NULL),(11,'Ciaspolate','ciaspolate',8,NULL);
/*!40000 ALTER TABLE `_delete_ck_outdoor_activity` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-31 14:27:37
