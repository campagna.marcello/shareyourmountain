CREATE DATABASE  IF NOT EXISTS `sym.core` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sym.core`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: sym.core
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `valley`
--

DROP TABLE IF EXISTS `valley`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valley` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valley_name` varchar(45) DEFAULT NULL,
  `valley_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valley_dateMod` timestamp NULL DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `valley_parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_valley_region_id_idx` (`region_id`),
  KEY `fk_valley_country_id_idx` (`country_id`),
  CONSTRAINT `fk_valley_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_valley_region_id` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valley`
--

LOCK TABLES `valley` WRITE;
/*!40000 ALTER TABLE `valley` DISABLE KEYS */;
INSERT INTO `valley` VALUES (1,'Val di Mello','2015-01-26 22:26:38',NULL,116,1736,NULL),(2,'Valsassina','2015-01-26 22:26:38',NULL,116,1736,NULL),(3,'Valchiavenna','2015-01-26 22:30:26',NULL,116,1736,NULL),(4,'Val Badia','2015-01-26 22:30:26',NULL,116,1744,NULL),(5,'Valtellina','2015-01-26 22:30:26',NULL,116,1736,NULL),(6,'Val Masino','2015-01-31 17:58:27',NULL,116,1736,NULL),(7,'Valle dei Ratti','2015-01-31 17:58:27',NULL,116,1736,NULL),(8,'Val Codera','2015-01-31 17:58:27',NULL,116,1736,NULL),(9,'Valmalenco','2015-01-31 17:58:27',NULL,116,1736,NULL),(10,'Valle Bova','2015-03-02 21:48:05',NULL,116,1736,NULL),(11,'Valle Albigna','2015-06-06 12:12:14',NULL,217,540,NULL),(12,'Val Bregaglia','2015-06-06 15:32:57',NULL,217,540,NULL),(13,'Valle Maggia','2015-06-06 22:25:56',NULL,217,551,NULL);
/*!40000 ALTER TABLE `valley` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-31 14:27:35
