CREATE DATABASE  IF NOT EXISTS `sym.core` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sym.core`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: sym.core
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mntn_mountainrange`
--

DROP TABLE IF EXISTS `mntn_mountainrange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mntn_mountainrange` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mntnRange_name` varchar(100) DEFAULT NULL,
  `mntnRange_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mntnRange_dateMod` timestamp NULL DEFAULT NULL,
  `mntnRange_parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mntnRange_name_UNIQUE` (`mntnRange_name`),
  KEY `fk_mntnRange_mntnRange_id_idx` (`mntnRange_parentId`),
  CONSTRAINT `fk_mntnRange_mntnRange_id` FOREIGN KEY (`mntnRange_parentId`) REFERENCES `mntn_mountainrange` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8 COMMENT='Informazioni prelevate dalla classificazione SOIUSA. Le catene montuose registrate sono la Sezione e SottoSezione.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mntn_mountainrange`
--

LOCK TABLES `mntn_mountainrange` WRITE;
/*!40000 ALTER TABLE `mntn_mountainrange` DISABLE KEYS */;
INSERT INTO `mntn_mountainrange` VALUES (1,'Alpi','2014-11-04 22:55:36',NULL,NULL),(2,'Appennini','2014-11-04 22:55:36',NULL,NULL),(3,'Pirenei','2014-11-04 22:55:36',NULL,NULL),(4,'Appennino Ligure','2014-11-04 22:55:36',NULL,2),(5,'Appennino Tosco-emiliano','2014-11-04 22:55:36',NULL,2),(6,'Appennino Umbro-marchigiano','2014-11-04 22:55:36',NULL,2),(7,'Appennino Abruzzese','2014-11-04 22:55:36',NULL,2),(8,'Appennino Sannita','2014-11-04 22:55:36',NULL,2),(9,'Appennino Lucano','2014-11-04 22:55:36',NULL,2),(10,'Appennino Campano','2014-11-04 22:55:36',NULL,2),(11,'Appennino Calabro','2014-11-04 22:55:36',NULL,2),(12,'Appennino Siculo','2014-11-04 22:55:36',NULL,2),(13,'Alpi Liguri','2014-11-04 22:55:36',NULL,1),(14,'Alpi Marittime e Prealpi di Nizza','2014-11-04 22:55:36',NULL,1),(15,'Alpi e Prealpi di Provenza','2014-11-04 22:55:36',NULL,1),(16,'Alpi Cozie','2014-11-04 22:55:36',NULL,1),(17,'Alpi del Delfinato','2014-11-04 22:55:36',NULL,1),(18,'Prealpi del Delfinato','2014-11-04 22:55:36',NULL,1),(19,'Alpi Graie','2014-11-04 22:55:36',NULL,1),(20,'Prealpi di Savoia','2014-11-04 22:55:36',NULL,1),(21,'Alpi Pennine','2014-11-04 22:55:36',NULL,1),(22,'Alpi Lepontine','2014-11-04 22:55:36',NULL,1),(23,'Prealpi Luganesi','2014-11-04 22:55:36',NULL,1),(24,'Alpi bernesi','2014-11-04 22:55:36',NULL,1),(25,'Alpi Glaronesi','2014-11-04 22:55:36',NULL,1),(26,'Prealpi svizzere','2014-11-04 22:55:36',NULL,1),(27,'Alpi Retiche occidentali','2014-11-04 22:55:36',NULL,1),(28,'Alpi Retiche orientali','2014-11-04 22:55:36',NULL,1),(29,'Alpi dei Tauri occidentali','2014-11-04 22:55:36',NULL,1),(30,'Alpi dei Tauri orientali','2014-11-04 22:55:36',NULL,1),(31,'Alpi di Stiria e Carinzia','2014-11-04 22:55:36',NULL,1),(32,'Prealpi di Stiria','2014-11-04 22:55:36',NULL,1),(33,'Alpi Calcaree Nordtirolesi','2014-11-04 22:55:36',NULL,1),(34,'Alpi Bavaresi','2014-11-04 22:55:36',NULL,1),(35,'Alpi Scistose Tirolesi','2014-11-04 22:55:36',NULL,1),(36,'Alpi Settentrionali Salisburghesi','2014-11-04 22:55:36',NULL,1),(37,'Alpi del Salzkammergut e dell\'Alta Austria','2014-11-04 22:55:36',NULL,1),(38,'Alpi Settentrionali di Stiria','2014-11-04 22:55:36',NULL,1),(39,'Alpi della Bassa Austria','2014-11-04 22:55:36',NULL,1),(40,'Alpi Retiche meridionali','2014-11-04 22:55:36',NULL,1),(41,'Alpi Orobie e Prealpi Bergamasche','2014-11-04 22:55:36',NULL,1),(42,'Prealpi Bresciane e Gardesane','2014-11-04 22:55:36',NULL,1),(43,'Dolomiti','2014-11-04 22:55:36',NULL,1),(44,'Prealpi Venete','2014-11-04 22:55:36',NULL,1),(45,'Alpi Carniche e della Gail','2014-11-04 22:55:36',NULL,1),(46,'Alpi e Prealpi Giulie','2014-11-04 22:55:36',NULL,1),(47,'Alpi di Carinzia e di Slovenia','2014-11-04 22:55:36',NULL,1),(48,'Prealpi Slovene','2014-11-04 22:55:36',NULL,1),(49,'Prealpi Liguri','2014-11-04 22:55:36',NULL,13),(50,'Alpi del Marguareis','2014-11-04 22:55:36',NULL,13),(51,'Alpi Marittime ','2014-11-04 22:55:36',NULL,14),(52,'Prealpi di Nizza','2014-11-04 22:55:36',NULL,14),(53,'Alpi di Provenza','2014-11-04 22:55:36',NULL,15),(54,'Prealpi di Digne','2014-11-04 22:55:36',NULL,15),(55,'Prealpi di Grasse','2014-11-04 22:55:36',NULL,15),(56,'Prealpi di Vaucluse','2014-11-04 22:55:36',NULL,15),(57,'Alpi del Monviso','2014-11-04 22:55:36',NULL,16),(58,'Alpi del Monginevro','2014-11-04 22:55:36',NULL,16),(59,'Alpi del Moncenisio','2014-11-04 22:55:36',NULL,16),(60,'Alpi delle Grandes Rousses e delle Aiguilles d\'Averes','2014-11-04 22:55:36',NULL,17),(61,'Catena di Belledonne','2014-11-04 22:55:36',NULL,17),(62,'Massiccio des Ecrins','2014-11-04 22:55:36',NULL,17),(63,'Massiccio del Taillefer','2014-11-04 22:55:36',NULL,17),(64,'Massiccio del Champsaur','2014-11-04 22:55:36',NULL,17),(65,'Massiccio dell\'Embrunais','2014-11-04 22:55:36',NULL,17),(66,'Monti orientali di Gap','2014-11-04 22:55:36',NULL,17),(67,'Prealpi del Devoluy','2014-11-04 22:55:36',NULL,18),(68,'Prealpi occidentali di Gap','2014-11-04 22:55:36',NULL,18),(69,'Prealpi del Vercors','2014-11-04 22:55:36',NULL,18),(70,'Prealpi del Diois','2014-11-04 22:55:36',NULL,18),(71,'Prealpi delle Baronnies','2014-11-04 22:55:36',NULL,18),(72,'Alpi di Lanzo e dell\'Alta Moriana','2014-11-04 22:55:36',NULL,19),(73,'Alpi della Vanoise e del Grand Arc','2014-11-04 22:55:36',NULL,19),(74,'Alpi della Grande Sassière e del Rutor','2014-11-04 22:55:36',NULL,19),(75,'Alpi del Gran Paradiso','2014-11-04 22:55:36',NULL,19),(76,'Alpi del Monte Bianco','2014-11-04 22:55:36',NULL,19),(77,'Alpi del Beaufortain','2014-11-04 22:55:36',NULL,19),(78,'Catena delle Aiguilles Rouges','2014-11-04 22:55:36',NULL,20),(79,'Prealpi del Giffre','2014-11-04 22:55:36',NULL,20),(80,'Prealpi dello Sciablese','2014-11-04 22:55:36',NULL,20),(81,'Prealpi dei Bornes','2014-11-04 22:55:36',NULL,20),(82,'Prealpi dei Bauges','2014-11-04 22:55:36',NULL,20),(83,'Prealpi della Chartreuse','2014-11-04 22:55:36',NULL,20),(84,'Alpi del Grand Combin','2014-11-04 22:55:36',NULL,21),(85,'Alpi del Weisshorn e del Cervino','2014-11-04 22:55:36',NULL,21),(86,'Alpi del Monte Rosa','2014-11-04 22:55:36',NULL,21),(87,'Alpi Biellesi e Cusiane','2014-11-04 22:55:36',NULL,21),(88,'Alpi del Mischabel e del Weissmies','2014-11-04 22:55:36',NULL,21),(89,'Alpi del Monte Leone e del San Gottardo','2014-11-04 22:55:36',NULL,22),(90,'Alpi Ticinesi e del Verbano','2014-11-04 22:55:36',NULL,22),(91,'Alpi dell\'Adula','2014-11-04 22:55:36',NULL,22),(92,'Prealpi Comasche','2014-11-04 22:55:36',NULL,23),(93,'Prealpi Varesine','2014-11-04 22:55:36',NULL,23),(94,'Alpi Urane','2014-11-04 22:55:36',NULL,24),(95,'Alpi di Vaud','2014-11-04 22:55:36',NULL,24),(96,'Alpi Urano-Glaronesi','2014-11-04 22:55:36',NULL,25),(97,'Alpi Glaronesi iss','2014-11-04 22:55:36',NULL,25),(98,'Prealpi di Vaud e Friburgo','2014-11-04 22:55:36',NULL,26),(99,'Prealpi Bernesi','2014-11-04 22:55:36',NULL,26),(100,'Prealpi di Lucerna e di Untervaldo','2014-11-04 22:55:36',NULL,26),(101,'Prealpi di Svitto e di Uri','2014-11-04 22:55:36',NULL,26),(102,'Prealpi di Appenzello e di San Gallo','2014-11-04 22:55:36',NULL,26),(103,'Alpi Bernesi - sottosezione','2014-11-04 22:55:36',NULL,24),(104,'Alpi del Platta','2014-11-04 22:55:36',NULL,27),(105,'Alpi dell\'Albula','2014-11-04 22:55:36',NULL,27),(106,'Alpi del Bernina','2014-11-04 22:55:36',NULL,27),(107,'Alpi di Livigno','2014-11-04 22:55:36',NULL,27),(108,'Alpi della Val Müstair','2014-11-04 22:55:36',NULL,27),(109,'Alpi del Silvretta, del Samnaun e del Verwall','2014-11-04 22:55:36',NULL,27),(110,'Alpi del Plessur','2014-11-04 22:55:36',NULL,27),(111,'Catena del Rätikon','2014-11-04 22:55:36',NULL,27),(112,'Alpi Venoste','2014-11-04 22:55:36',NULL,28),(113,'Alpi dello Stubai','2014-11-04 22:55:36',NULL,28),(114,'Alpi Sarentine','2014-11-04 22:55:36',NULL,28),(115,'Alpi dell\'Ortles','2014-11-04 22:55:36',NULL,40),(116,'Alpi della Val di Non','2014-11-04 22:55:36',NULL,40),(117,'Alpi dell\'Adamello e della Presanella','2014-11-04 22:55:36',NULL,40),(118,'Dolomiti di Brenta','2014-11-04 22:55:36',NULL,40),(119,'Alpi Orobie','2014-11-04 22:55:36',NULL,41),(120,'Prealpi Bergamasche','2014-11-04 22:55:36',NULL,41),(121,'Prealpi Bresciane','2014-10-25 13:00:58',NULL,42),(122,'Prealpi Gardesane','2014-10-25 13:00:58',NULL,42),(123,'Dolomiti di Sesto, di Braies e d\'Ampezzo','2014-10-25 13:00:58',NULL,43),(124,'Dolomiti di Zoldo','2014-10-25 13:00:58',NULL,43),(125,'Dolomiti di Gardena e di Fassa','2014-10-25 13:00:58',NULL,43),(126,'Dolomiti di Feltre e delle Pale di San Martino','2014-10-25 13:00:58',NULL,43),(127,'Dolomiti di Fiemme','2014-10-25 13:00:58',NULL,43),(128,'Prealpi Vicentine','2014-10-25 13:00:58',NULL,44),(129,'Prealpi Bellunesi','2014-10-25 13:00:58',NULL,44),(130,'Alpi Carniche','2014-10-25 13:00:58',NULL,45),(131,'Alpi della Gail','2014-10-25 13:00:58',NULL,45),(132,'Prealpi Carniche','2014-10-25 13:00:58',NULL,45),(133,'Alpi Giulie','2014-10-25 13:00:58',NULL,46),(134,'Prealpi Giulie','2014-10-25 13:00:58',NULL,46),(135,'Caravanche','2014-10-25 13:00:58',NULL,47),(136,'Alpi di Kamnik e della Savinja','2014-10-25 13:00:58',NULL,48),(137,'Prealpi Slovene occidentali','2014-10-25 13:00:58',NULL,49),(138,'Prealpi Slovene orientali','2014-10-25 13:00:58',NULL,49),(139,'Prealpi Slovene nord-orientali','2014-10-25 13:00:58',NULL,49);
/*!40000 ALTER TABLE `mntn_mountainrange` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-31 14:27:34
