var FormDropzone = function (idHtml, _maxFiles, _url) {


    return {
        //main function to initiate the module
        init: function (idHtml, _maxFiles, _url) {

            var options = {
                url: _url,
                dictDefaultMessage: "Drop images here to upload...",
                maxFiles: _maxFiles,
                acceptedFiles: "image/*",
                init: function () {
                    this.on("addedfile", function (file) {
                        var removeButton = Dropzone.createElement("<a href='javascript:;'' class='btn red btn-sm btn-block'>Remove</a>");
                        var _this = this;
                        removeButton.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            _this.removeFile(file);
                        });
                        file.previewElement.appendChild(removeButton);
                    });
                }
            }
            var myDropzone = new Dropzone(idHtml, options);
        }
    };
}();