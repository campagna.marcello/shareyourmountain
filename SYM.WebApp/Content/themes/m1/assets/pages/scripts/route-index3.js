﻿//Add script for autocomplete
var bestTopos = new Bloodhound({
    limit: 3,
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('route_title'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: '/api/autocomplete/GetTopos',
        prepare: function (query, settings) {
            settings.type = "POST";
            settings.contentType = "application/json; charset=UTF-8";
            settings.data = JSON.stringify({ "p_term": $('#route_name').val(), "p_countryid": $("#country").val(), "p_regionid": $("#region").val() });
            return settings;
        },
        onError: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to retrieve topos.');
        },
        onSuccess: function (response) {
            return response;
        },
        transform: function (response) {
            return response;
        }

    }
});

var bestMountains = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: '/api/autocomplete/GetMountains',
        prepare: function (query, settings) {
            settings.type = "POST";
            settings.contentType = "application/json; charset=UTF-8";
            settings.data = JSON.stringify({ "p_term": $('#route_name').val(), "p_countryid": $("#country").val(), "p_regionid": $("#region").val() });
            return settings;
        },
        onError: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to retrieve mountains.');
        },
        onSuccess: function (response) {
            return response;
        },
        transform: function (response) {
            return response;
        }

    }
});

var bestValley = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: '/api/autocomplete/GetValleys',
        prepare: function (query, settings) {
            settings.type = "POST";
            settings.contentType = "application/json; charset=UTF-8";
            settings.data = JSON.stringify({ "p_term": $('#route_name').val(), "p_countryid": $("#country").val(), "p_regionid": $("#region").val() });
            return settings;
        },
        onError: function (xhr, ajaxOptions, thrownError) {
            alert('Failed to retrieve mountains.');
        },
        onSuccess: function (response) {
            return response;
        },
        transform: function (response) {
            return response;
        }
    }
});

function RouteIndex_autocomplete() {

    $("#route_name").typeahead({
        hint: true,
        highlight: true,
        minLength: 2
    },
    {
        name: 'name',
        display: 'name',
        limit: 3,
        source: bestMountains,
        templates: {
            header: '<h3 class="group-name">Mountain</h3>',
            suggestion: function (data) { return '<div>' + data.name + ' <small>' + data.altitude + 'm.</small></div><div><small>' + data.range + '</small></div>'; }
        }
    },
    {
        name: 'name',
        display: 'name',
        limit: 3,
        source: bestValley,
        templates: {
            header: '<h3 class="group-name">Valley</h3>',
            suggestion: function (data) { return '<div>' + data.name + '</div>'; }
        }
    },
    {
        name: 'route_title',
        display: 'route_title',
        source: bestTopos,
        templates: {
            header: '<h3 class="group-name">Topos</h3>'
        }
    }).bind("typeahead:selected", function (obj, datum, name) {
        if (datum.type == "m") {
            $("#mountain").val(datum.id);
            $("#valley").val("");
            $("#route").val("");
        }
        else if (datum.type == "v") {
            $("#valley").val(datum.id);
            $("#mountain").val("");
            $("#route").val("");
        }
        else if (datum.type == "t") {
            $("#valley").val("");
            $("#mountain").val("");
            $("#route").val(datum.id);
        }
        else {
            //$('#form_search').attr('action', "@(Url.Action("Details", "Route"))" + "/" + datum.id); //this works
            //$("#topos_id").val(datum.id);
        }
    });

};

jQuery(document).ready(function () {
    RouteIndex_autocomplete();
});