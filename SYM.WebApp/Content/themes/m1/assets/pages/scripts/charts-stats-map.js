var ChartsAmcharts = function() {

    var initChartSample10 = function() {
        /*
            although ammap has methos like getAreaCenterLatitude and getAreaCenterLongitude,
            they are not suitable in quite a lot of cases as the center of some countries
            is even outside the country itself (like US, because of Alaska and Hawaii)
            That's why wehave the coordinates stored here
        */

        var latlong = {};

        latlong["CH"] = {
            "latitude": 47,
            "longitude": 8
        };
        latlong["FR"] = {
            "latitude": 46,
            "longitude": 2
        };
        latlong["IT"] = {
            "latitude": 42.8333,
            "longitude": 12.8333
        };
        latlong["IT_CO"] = {
            "latitude": 45.808060,
            "longitude": 9.085176
        };
        latlong["IT_LOM"] = {
            "latitude": 45.666,
            "longitude": 9.964
        };
        latlong["IT_PMN"] = {
            "latitude": 45.277,
            "longitude": 7.921
        };
        latlong["IT_VNT"] = {
            "latitude": 45.744,
            "longitude": 11.862
        };
        latlong["IT_TRN"] = {
            "latitude": 46.0664228,
            "longitude": 11.1257601
        };
        latlong["IT_VDA"] = {
            "latitude": 45.7278,
            "longitude": 7.3705
        };
        latlong["IT_FVG"] = {
            "latitude": 46.0000,
            "longitude": 13.0000
        };
        latlong["IT_ABR"] = {
            "latitude": 42.3488992,
            "longitude": 13.3981375
        };
        latlong["IT_LGR"] = {
            "latitude": 44.2278,
            "longitude": 8.7836
        };
        latlong["IT_EML"] = {
            "latitude": 44.444,
            "longitude": 10.975
        };
        latlong["IT_TSC"] = {
            "latitude": 43.4586541, 
            "longitude": 11.1389204
        };
        latlong["IT_MRC"] = {
            "latitude": 43.3458388,
            "longitude": 13.1415872
        };

        var mapData = [{
            "code": "FR",
            "name": "France",
            "value": 7,
            "color": "#4f5dd8"
        }, {
            "code": "IT_LOM",
            "name": "montagne in Lombardia",
            "value": 2380,
            "color": "#8cdbb5"
        },{
            "code": "IT_LOM",
            "name": "Lombardia",
            "value": 982,
            "color": "#d8854f"
        }, {
            "code": "IT_PMN",
            "name": "montagne in Piemonte",
            "value": 1598,
            "color": "#8cdbb5"
        }, {
            "code": "IT_PMN",
            "name": "Piemonte",
            "value": 426,
            "color": "#d8854f"
        },{
            "code": "IT_VNT",
            "name": "montagne in Veneto",
            "value": 860,
            "color": "#8cdbb5"
        }, {
            "code": "IT_VNT",
            "name": "Veneto",
            "value": 449,
            "color": "#d8854f"
        }, {
            "code": "IT_TRN",
            "name": "montagne in Trentino-Alto Adige",
            "value": 2010,
            "color": "#8cdbb5"
        }, {
            "code": "IT_TRN",
            "name": "Trentino-Alto Adige",
            "value": 537,
            "color": "#d8854f"
        }, {
            "code": "IT_VDA",
            "name": "montagne in Valle D'Aosta",
            "value": 636,
            "color": "#8cdbb5"
        }, {
            "code": "IT_VDA",
            "name": "Valle D'Aosta",
            "value": 166,
            "color": "#d8854f"
        }, {
            "code": "IT_FVG",
            "name": "montagne in Friuli-Venezia Giulia",
            "value": 1205,
            "color": "#8cdbb5"
        }, {
            "code": "IT_FVG",
            "name": "Friuli-Venezia Giulia",
            "value": 110,
            "color": "#d8854f"
        }, {
            "code": "IT_ABR",
            "name": "montagne in Abruzzo",
            "value": 368,
            "color": "#8cdbb5"
        }, {
            "code": "IT_ABR",
            "name": "Abruzzo",
            "value": 88,
            "color": "#d8854f"
        }, {
            "code": "IT_LGR",
            "name": "montagne in Liguria",
            "value": 1002,
            "color": "#8cdbb5"
        }, {
            "code": "IT_MRC",
            "name": "montagne in Marche",
            "value": 509,
            "color": "#8cdbb5"
        }, {
            "code": "IT_LGR",
            "name": "Liguria",
            "value": 32,
            "color": "#d8854f"
        }, {
            "code": "IT_EML",
            "name": "montagne in Emilia Romagna",
            "value": 788,
            "color": "#8cdbb5"
        }, {
            "code": "IT_EML",
            "name": "Emilia Romagna",
            "value": 14,
            "color": "#d8854f"
        }, {
            "code": "IT_TSC",
            "name": "Toscana",
            "value": 42,
            "color": "#d8854f"
        }, {
            "code": "CH",
            "name": "montagne in Swiss",
            "value": 841,
            "color": "#8cdbb5"
        }, {
            "code": "CH",
            "name": "Swiss",
            "value": 70,
            "color": "#d84f5d"
        }];


        var map;
        var minBulletSize = 5;
        var maxBulletSize = 30;
        var min = Infinity;
        var max = -Infinity;


        // get min and max values
        for (var i = 0; i < mapData.length; i++) {
            var value = mapData[i].value;
            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
        }

        // build map
        AmCharts.ready(function() {
            AmCharts.theme = AmCharts.themes.light;
            map = new AmCharts.AmMap();
            map.pathToImages = App.getGlobalPluginsPath() + "amcharts/ammap/images/",

            map.fontFamily = 'Open Sans';
            map.fontSize = '13';
            map.color = '#888';
            
            map.addTitle("Mountains and routes distribution", 16, "#578ebe");
            map.addTitle("source: ShareYourMountain.com", 11, "#578ebe");
            map.areasSettings = {
                unlistedAreasColor: "#000000",
                unlistedAreasAlpha: 0.1
            };
            map.export = {
                enabled: true
            };
            map.imagesSettings.balloonText = "<span style='font-size:14px;'><b>[[title]]</b> - n. [[value]]</span>";

            var dataProvider = {
                mapVar: AmCharts.maps.worldHigh,
                images: []
            }

            // create circle for each country
            for (var i = 0; i < mapData.length; i++) {
                var dataItem = mapData[i];
                var value = dataItem.value;
                // calculate size of a bubble
                var size = (value - min) / (max - min) * (maxBulletSize - minBulletSize) + minBulletSize;
                if (size < minBulletSize) {
                    size = minBulletSize;
                }
                var id = dataItem.code;

                dataProvider.images.push({
                    type: "circle",
                    width: size,
                    height: size,
                    color: dataItem.color,
                    selectedColor: "#d8a74f",
                    longitude: latlong[id].longitude,
                    latitude: latlong[id].latitude,
                    title: dataItem.name,
                    value: value
                });
            }

            map.dataProvider = dataProvider;

            map.write("chart_10");
            map.zoomToLongLat(10, 12.8333, 40.8333, false);
        });

        $('#chart_10').closest('.portlet').find('.fullscreen').click(function() {
            map.invalidateSize();
        });
    }

    return {
        //main function to initiate the module

        init: function() {
            initChartSample10();
        }

    };

}();

jQuery(document).ready(function() {    
   ChartsAmcharts.init(); 
});