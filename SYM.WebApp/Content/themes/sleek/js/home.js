var ComponentsBootstrapSelect = function () {

    var handleBootstrapSelect = function () {
        $('#activity').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check',
            noneSelectedText: 'Attivit&#224; (6)'
        });

    }

    return {
        //main function to initiate the module
        init: function () {
            handleBootstrapSelect();
        }
    };

}();

//Main Section
jQuery(document).ready(function () {
    ComponentsBootstrapSelect.init();

    //Check the Resolution of Screen && Set Programatically the height for Intro
    var height = window.innerHeight;
    var largeHeader = document.getElementById("intro");
    largeHeader.style.height = height + 'px';
    largeHeader = document.getElementById("map");
    height = window.innerHeight - 65;
    largeHeader.style.height = height + 'px';

    // Fix Header during the scroll
    $(window).scroll(function () {
        var value = $(this).scrollTop();
        if (value > 350)
            $("header").css("background", "#1abc9c").css("padding", "0px 0px 13px");
        else
            $("header").css("background", "transparent").css("padding", "20px 0px 20px");
    });
});

function smoothScroll_Sym(offset,classLink) {

    $(classLink).on('click', function (e) {
        e.preventDefault();

        var target = this.hash,
            $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - offset
        }, 1500, 'easeInOutExpo', function () {
            window.location.hash = '1' + target;
        });
    });

}

//Search Autocomplete & Carosel
jQuery(document).ready(function () {

    var bestTopos = new Bloodhound({
        limit: 3,
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('route_title'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/api/autocomplete/GetTopos',
            prepare: function (query, settings) {
                settings.type = "POST";
                settings.contentType = "application/json; charset=UTF-8";
                settings.data = JSON.stringify({ "p_term": $('#meta').val(), "p_countryid": $("#country").val(), "p_regionid": $("#region").val() });
                return settings;
            },
            onError: function (xhr, ajaxOptions, thrownError) {
                alert('Failed to retrieve topos.');
            },
            onSuccess: function (response) {
                return response;
            },
            transform: function (response) {
                return response;
            }

        }
    });

    var bestMountains = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/api/autocomplete/GetMountains',
            prepare: function (query, settings) {
                settings.type = "POST";
                settings.contentType = "application/json; charset=UTF-8";
                settings.data = JSON.stringify({ "p_term": $('#meta').val(), "p_countryid": $("#country").val(), "p_regionid": $("#region").val() });
                return settings;
            },
            onError: function (xhr, ajaxOptions, thrownError) {
                alert('Failed to retrieve mountains.');
            },
            onSuccess: function (response) {
                return response;
            },
            transform: function (response) {
                return response;
            }

        }
    });

    var bestValley = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/api/autocomplete/GetValleys',
            prepare: function (query, settings) {
                settings.type = "POST";
                settings.contentType = "application/json; charset=UTF-8";
                settings.data = JSON.stringify({ "p_term": $('#meta').val(), "p_countryid": $("#country").val(), "p_regionid": $("#region").val() });
                return settings;
            },
            onError: function (xhr, ajaxOptions, thrownError) {
                alert('Failed to retrieve mountains.');
            },
            onSuccess: function (response) {
                return response;
            },
            transform: function (response) {
                return response;
            }
        }
    });

    var regions = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/data/cache.region_prefetch.json'
    });

    var countries = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/data/cache.country_prefetch_it.json'
    });

    $("#meta").typeahead({
        hint: true,
        highlight: true,
        minLength: 2
    },
    {
        name: 'name',
        limit: 3,
        display: 'name',
        source: bestMountains,
        templates: {
            suggestion: function (data) { return '<div>' + data.name + ' <small>' + data.altitude + 'm.</small></div><div><small>' + data.range + '</small></div>'; }
        }
    },
    {
        name: 'name',
        display: 'name',
        limit: 2,
        source: bestValley,
        templates: {
            suggestion: function (data) { return '<div>' + data.name + '</div>'; }
        }
    },
    {
        name: 'route_title',
        display: 'route_title',
        source: bestTopos
    },
    {
    name: 'name',
    display: 'name',
    source: countries,
    templates: {
        suggestion: function (data) {
            return '<div><img width="16" src="/Content/themes/sleek/images/country/' + data.id + '.png"> ' + data.name + '</div>';
            }
        }
    },{
        name: 'name',
        display: 'name',
        source: regions,
        templates: {
            suggestion: function (data) {
                return '<div><img width="16" src="/Content/themes/sleek/images/region/' + data.country_id + '_' + data.id + '.png"> ' + data.name + ' (' + data.country_code + ')</div>';
            }
        }
    }).bind("typeahead:selected", function (obj, datum, name) {
        if (datum.type == "m") {
            $('#form_search').attr('action', "/Mountain/" + datum.id + "-tempname"); //this works
            $("#mountain").val(datum.id);
            $("#topos").val("");
            $("#metaDisable").val("false");
        } else if (datum.type == "r") {
            $('#form_search').attr('action', "/Route/Index3"); //Region
            $("#region").val(datum.id);
            $("#country").val(datum.country_id);
            $("#topos").val("");
            $("#metaDisable").val("true");
        } else if (datum.type == "c") {
            $('#form_search').attr('action', "/Route/Index3"); //Country
            $("#region").val("");
            $("#country").val(datum.id);
            $("#topos").val("");
            $("#metaDisable").val("true");
        } else if (datum.type == "t") {
            $('#form_search').attr('action', "/Route/" + datum.id + "-topos"); //Topos
            $("#topos").val(datum.id);
            $("#region").val("");
            $("#metaDisable").val("false");
        }
    });

    $("#suggest").click(function (e) {
        e.preventDefault();
        $('#form_search').attr('action', "/Route/Index3");
        $("#orderby").val("1");
        $('#form_search').submit();
    });

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        mouseDrag: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            800: {
                items: 3,
                nav: false,
                loop: false
            }
        }
    })

});

//Map Section
var map;

function loadResults(data) {
    console.log("Inside loadResults");

    var items, markers_data = [];
    var item;

    if (data.length > 0) {
        items = data;

        for (var i = 0; i < items.length; i++) {
            item = items[i];
            var icon;
            if (item.lat != undefined && item.lng != undefined) {

                var textRoutes = "";

                if (item.done == true) {
                    icon = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png';
                }
                else if (item.route != '0') {
                    icon = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle_highlight.png';
                    textRoutes = "<br/>Itinerari: " + item.route
                }
                else if (item.name != '')
                    icon = '/Content/images/placemark_circle_grey_s.png';
                else
                    icon = 'http://maps.google.com/mapfiles/kml/pal2/icon13.png';

                markers_data.push({
                    lat: item.lat,
                    lng: item.lng,
                    title: item.name + " - " + item.altitude + " m.",
                    altitude: item.altitude,
                    icon: {
                        size: new google.maps.Size(32, 32),
                        url: icon
                    }, infoWindow: {
                        content: "<i class='fa fa-spinner fa-spin fa-lg' style='color: #FFA46B;' title='Loading...'></i> Loading..."
                    },
                    infoWindow_Ajax: '/Mountain/InfoWindow?id=' + item.id
                });
            }
        }
    }

    map.addMarkers(markers_data);
}

function initMapNew(lat, lng, user) {

    map = new GMaps({
        el: "#map",
        lat: lat,
        lng: lng,
        zoom: 11,
        zoomControl: true,
        scrollwheel: false,
        controls: true,
        zoomControlOpt: {
            style: "BIG",
            position: "TOP_LEFT"
        },
        panControl: true,
        streetViewControl: false,
        mapTypeControl: false,
        overviewMapControl: false,
        dragend: function (event) {
            var showby = $('#pac-showby option:selected')[0].value;
            LoadMountainsInGmaps(event.getCenter().lat(), event.getCenter().lng(), showby, user);
        },
        styles: [
            { featureType: 'water', stylers: [{ color: '#03C9A9' }, { visibility: 'on' }] },
            { featureType: 'road', stylers: [{ saturation: -100 }, { lightness: 45 }] },
            { featureType: 'road.highway', stylers: [{ visibility: 'simplified' }] },
            { featureType: 'road.arterial', elementType: 'labels.icon', stylers: [{ visibility: 'off' }] },
            { featureType: 'administrative', elementType: 'labels.text.fill', stylers: [{ color: '#444444' }] },
            { featureType: 'transit', stylers: [{ visibility: 'off' }] },
            { featureType: 'poi', stylers: [{ visibility: 'off' }] }
        ]
    });

    //FIXED MARKER
    map.addMarker({
        lat: 45.751573,
        lng: 8.810881,
        animation: 'DROP',
        label: {
            text: '3D',
            color: 'white',
            fontWeight: 'bold'
        },
        title: "Free climbing Castronno",
        infoWindow: {
            content: "<img width='150' src='http://asd3dclimbing.altervista.org/assets/images/3Dclimbing.png'><br/><div style='text-align:center;margin-top:5px;'><a target='_blank' href='http://3dclimbing.it/'>3dclimbing.it</a></div>"
        }
    });

    //New search function of interest city
    map.addControlById({
        position: 'TOP_LEFT',
        id: 'pac-input'
    });

    map.addControlById({
        position: 'TOP_LEFT',
        id: 'pac-showby'
    });

    var options = {
        types: ['(cities)']
    };
    var input = (document.getElementById('pac-input'));
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    autocomplete.bindTo('bounds', map);

    //The Autocomplete Function with binding to GMAP POI
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
        var showby = $('#pac-showby option:selected')[0].value;
        LoadMountainsInGmaps(place.geometry.location.lat(), place.geometry.location.lng(), 2, user);
        map.setCenter(place.geometry.location.lat(), place.geometry.location.lng());
    });

    //Ajax call to update the nearest mountains on the map.
    //var showby = $('#pac-showby option:selected')[0].index;
    LoadMountainsInGmaps(lat, lng, 0, user);

    //Init Select DropDown after add the control
    //$("#pac-showby").selectpicker({
    //    iconBase: 'fa',
    //    tickIcon: 'fa-check',
    //    noneSelectedText: "Mostra se ...",
    //});
}

//New Function after refactoring
function LoadMountainsInGmaps(latitude, longitude, show, user) {
    $.ajax({
        type: "POST",
        url: "/api/autocomplete/GetNearestMountainByLocation",
        data: '{p_mountainId:"0",p_lat:"' + latitude + '", p_long:"' + longitude + '",p_showby:"' + show + '",p_userId:"' + user + '"}',
        contentType: "application/json; charset=utf-8",
        cache: false,
        success: function (response) {
            console.log(latitude + " -  " + longitude + " - " + user);
            loadResults(response);
        },
        error: function () {
            console.log("Error gets mountains by location.");
        }
    });
}
