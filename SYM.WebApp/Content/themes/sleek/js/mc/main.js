// Mailchimp Newsletter
$(document).ready(function() {
	$('#invite').ketchup().submit(function() {
	if ($(this).ketchup('isValid')) {
	var action = $(this).attr('action');
	$.ajax({
	url: action,
	type: 'POST',
	data: {
		email: $('#address').val(),
		fname: $('#fname').val(),
		lname: $('#lname').val()
	},
	success: function(data){
	$('#result_nl').html(data).css('color', 'white');
	},
	error: function() {
	$('#result_nl').html('Sorry, an error occurred.').css('color', 'white');
	}
	});
	}
	return false;
	});
});
