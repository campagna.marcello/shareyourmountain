$(document).ready(function($) {
    "use strict";

    var height = window.innerHeight;
    //Set Programatically the height for Intro
    //var largeHeader = document.getElementById("database");
    //largeHeader.style.height = height + 'px';

    // Anchor Smooth Scroll
    $('body').on('click', '.page-scroll', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    // Screenshots Slider
    $('.shots-slider').slick({
        dots: true,
        arrows: false,
        speed: 300,
        centerMode: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });

    // Prettyphoto
    $("a[class^='prettyPhoto']").prettyPhoto({
        theme: 'pp_default'
    });

    $("header").css("background", "#1abc9c").css("padding", "0px 0px 13px");

    // Handles counterup plugin wrapper
    var handleCounterup = function () {
        if (!$().counterUp) {
            return;
        }

        $("[data-counter='counterup']").counterUp({
            delay: 10,
            time: 1000
        });
    };
    handleCounterup(); // handle counterup instances

});