﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace SYM.SearchEngine.MvcApp.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString MenuLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string url, string icon)
        {
            var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");

            // Create span tag builder
            var span = new TagBuilder("span");
            span.MergeAttribute("class", "title");
            span.InnerHtml = linkText;

            // Create a tag builder
            var i = new TagBuilder("i");
            i.MergeAttribute("class", icon);

            // Create a tag builder
            var a = new TagBuilder("a");
            a.MergeAttribute("href",url);
            a.InnerHtml = i.ToString(TagRenderMode.Normal) + span.ToString(TagRenderMode.Normal);

            var builder = new TagBuilder("li")
            {
                InnerHtml = a.ToString(TagRenderMode.Normal)
            };

            builder.MergeAttribute("id", controllerName+actionName);

            if (controllerName.ToLower() == currentController.ToLower() && actionName.ToLower() == currentAction.ToLower())
                builder.AddCssClass("active");

            return new MvcHtmlString(builder.ToString());
        }

        public static MvcHtmlString SubMenuLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName)
        {
            var currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            var currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller");

            var builder = new TagBuilder("li")
            {
                InnerHtml = htmlHelper.ActionLink(linkText, actionName, controllerName).ToHtmlString()
            };

            if (controllerName == currentController && actionName == currentAction)
                builder.AddCssClass("active");

            return new MvcHtmlString(builder.ToString());
        }

        public static MvcHtmlString DropDownListBootstrap( string name, IEnumerable<SelectListItemCustom> selectList, string countselected = "2",bool multiple = true)
        {          
            var select = new TagBuilder("select");

            var options = "";
            TagBuilder option;

            foreach (var item in selectList)
            {
                option = new TagBuilder("option");
                if (item.Selected) option.MergeAttribute("selected", "selected");
                option.MergeAttribute("value", item.Value.ToString());
                if(!String.IsNullOrEmpty(item.SubText)) option.MergeAttribute("data-subtext", item.SubText.ToString());
                option.SetInnerText(item.Text);
                options += option.ToString(TagRenderMode.Normal) + "\n";
            }

            select.MergeAttribute("class", "bs-select form-control");
            //select.MergeAttribute("data-style", "btn-success");
            select.MergeAttribute("id", name);
            select.MergeAttribute("name", name);
            if(multiple) 
                select.MergeAttribute("multiple", "multiple");
            select.MergeAttribute("data-show-subtext", "true");
            select.MergeAttribute("data-selected-text-format", "count>" + countselected);
        
            select.InnerHtml = options;

            return new MvcHtmlString(select.ToString(TagRenderMode.Normal));
        }

        public static string Truncate(this string s, int length)
        {
            if (s.Length > length) return s.Substring(0, length) + ".";
            return s;
        }

 
        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
            #if DEBUG
                  return true;
            #else
                  return false;
            #endif
        }
    }

    public static class RouteHelper {

        public static string DistanceFormat(int length)
        {
            string s = null;

            if (length >= 1000)
            {
                double length_km = (double)length / 1000;
                s = Math.Round(length_km, 2).ToString() + " km.";
            }
            else
                s = length.ToString() + " m.";

            return s;
        }

        /// <summary>
        /// Return the full route facing
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string GetFullFacing(string code) {

            string face;

            switch (code)
            {
                case "N":
                    face = "Nord";
                    break;
                case "S":
                    face = "Sud";
                    break;
                case "E":
                    face = "Est";
                    break;
                case "O":
                case "W":
                    face = "Ovest";
                    break;
                case "NE":
                case "EN":
                case "N-E":
                case "E-N":
                    face = "Nord Est";
                    break;
                case "WN":
                case "NW":
                case "W-N":
                case "N-W":
                    face = "Nord Ovest";
                    break;
                case "SE":
                case "ES":
                case "S-E":
                case "E-S":
                    face = "Sud Est";
                    break;
                case "SW":
                case "WS":
                case "S-W":
                case "W-S":
                    face = "Sud Ovest";
                    break;
                default:
                    face = code;
                    break;
            }

            return face;
        }
    }
}