﻿using System;

namespace SYM.SearchEngine.MvcApp.Helpers
{
    // Summary:
    //     Represents the selected item in an instance of the System.Web.Mvc.SelectList
    //     class.
    public class SelectListItemCustom
    {
        // Summary:
        //     Initializes a new instance of the System.Web.Mvc.SelectListItem class.
        //public SelectListItemCustom();
        // Summary:
        //     Gets or sets a value that indicates whether this System.Web.Mvc.SelectListItem
        //     is selected.
        //
        // Returns:
        //     true if the item is selected; otherwise, false.
        public bool Selected { get; set; }
        //
        // Summary:
        //     Gets or sets the text of the selected item.
        //
        // Returns:
        //     The text.
        public string Text { get; set; }
        //
        // Summary:
        //     Gets or sets the text of the selected item.
        //
        // Returns:
        //     The Subtext.
        public string SubText { get; set; }
        //
        // Summary:
        //     Gets or sets the value of the selected item.
        //
        // Returns:
        //     The value.
        public string Value { get; set; }
    }
}