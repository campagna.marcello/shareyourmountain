﻿(function (MyApp, $, undefined) {
    'use strict';

    MyApp.Name = 'ShareYourMountain';

    MyApp.init = function () {
        $.fn.select2.defaults.set("theme", "bootstrap");
    };

    MyApp.render = function () {
    };

    function format(state) {
        if (!state.id) return state.text; // optgroup
        return state.text + " (" + state.country + ")";
    }

    function formatMountain(state) {
        if (!state.id) return state.text; // optgroup
        return state.text + " - " + state.altitude + " m.";
    }

    MyApp.Select2Region = function (lookupCode, urlFetch, placeholder) {
        $(lookupCode).select2({
            width: "off",
            minimumInputLength: 2,
            quietMillis: 250,
            allowClear: true,
            placeholder: placeholder,
            templateResult: format,
            ajax: {
                url: urlFetch,
                dataType: 'json',
                type: "POST",
                data: function (params) {
                     return {
                        p_term: params.term // search term
                    };
                },
                params: { // extra parameters that will be passed to ajax
                    contentType: 'application/json; charset=UTF-8'
                },
                processResults: function (data, page) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.id,
                                text: item.name,
                                country: item.country_code,
                                country_id: item.country_id
                            };
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (m) { return m; }
        });
    };

    MyApp.Select2Lookup = function (lookupCode, urlFetch, placeholder) {
        $(lookupCode).select2({
            width: "off",
            minimumInputLength: 2,
            quietMillis: 250,
            allowClear: true,
            placeholder: placeholder,
            ajax: {
                url: urlFetch,
                dataType: 'json',
                type: "POST",
                data: function (params) {
                    return {
                        p_term: params.term, // search term
                        p_countryid: $("#country").val(),
                        p_regionid: $("#region").val()
                    };
                },
                params: { // extra parameters that will be passed to ajax
                    contentType: 'application/json; charset=UTF-8'
                },
                processResults: function (data, page) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.id,
                                text: item.name
                            };
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (m) { return m; }
        });
    };

    //Verify is already used
    MyApp.Select2LookupSym = function (lookupCode, urlFetch, countryCode, regionCode ,placeholder) {
        $(lookupCode).select2({
            width: "off",
            minimumInputLength: 2,
            quietMillis: 300,
            allowClear: true,
            placeholder: placeholder,
            ajax: {
                url: urlFetch,
                dataType: 'json',
                type: "POST",
                data: function (params) {
                    return {
                        p_term: params.term, // search term
                        p_countryid: $(countryCode).val(),
                        p_regionid: $(regionCode).val()
                    };
                },
                params: { // extra parameters that will be passed to ajax
                    contentType: 'application/json; charset=UTF-8'
                },
                processResults: function (data, page) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.id,
                                text: item.name
                            };
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (m) { return m; }
        });
    };
    //New
    MyApp.Select2LookupMountain = function (lookupCode, urlFetch, countryid, regionid) {
        $(lookupCode).select2({
            width: "off",
            minimumInputLength: 2,
            quietMillis: 200,
            allowClear: true,
            placeholder: "Select mountain...",
            templateResult: formatMountain,
            ajax: {
                url: urlFetch,
                dataType: 'json',
                type: "POST",
                data: function (params) {
                    return {
                        p_term: params.term, // search term
                        p_countryid: $(countryid).val(),
                        p_regionid: $(regionid).val()
                    };
                },
                params: { // extra parameters that will be passed to ajax
                    contentType: 'application/json; charset=UTF-8'
                },
                processResults: function (data, page) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.id,
                                text: item.name,
                                altitude: item.altitude
                            };
                        })
                    };
                },
                cache: true
            },
            escapeMarkup: function (m) { return m; }
        });
    };

}(window.MyApp = window.MyApp || {}, jQuery));