﻿//TODO:create CLASS and private Method
function SetAdventure(mntn, route, done, text) {

    $.ajax({
        type: "POST",
        url: "/api/Adventure/Set",
        data: '{p_mountain:"' + mntn + '",p_route:"' + route + '",p_done:' + done + '}',
        contentType: "application/json; charset=utf-8",
        cache: false,
        success: function (response) {
            if (!response) {
                UpdateUI(mntn, route, done, text);
                return;
            }
        },
        error: function () {
            console.log("Error counter");
        }
    });
};

function UpdateUI(mntn, route, done, text) {

    var msg;

    if (mntn != "0" && done == true) {
        msg = "<i class='fa fa-thumbs-o-up'></i> Montagna già scalata !!!";
    }
    else if (mntn != "0" && done == false) {
        msg = "Montagna è nella tua lista <i class='fa fa-heart'></i>";
    }
    else if (route != "0" && done == true) {
        msg = "<i class='fa fa-thumbs-o-up'></i> Itineario già scalato !!!";
    }
    else if (route != "0" && done == false) {
        msg = "Itinerario aggiunto alla lista <i class='fa fa-heart'></i>";
    }

    //TODO: Check logic when you don't have climb the mountain or add it to the list....
    if (done == true) {
        $('#logAdventure').removeClass('btn-success').addClass('yellow-gold sbold').html(msg);
        toastr.warning(msg);
    }
    else if (done == false) {
        $('#logAdventure').removeClass('btn-success').addClass('yellow-gold btn-outline sbold').html(msg);
        toastr.warning(msg);
    }
};