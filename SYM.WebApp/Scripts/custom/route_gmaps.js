﻿//Init
var iconBase = 'http://maps.google.com/mapfiles/kml/pal2/';
var bounds = new google.maps.LatLngBounds();
var icons = {
    car: { icon: iconBase + 'icon47.png' },
    meta: { icon: iconBase + 'icon13.png' },
    parking: { icon: 'http://www.gstatic.com/mapspro/images/stock/1453-trans-parking.png' }
};
var map;
var mountain = { name: "", lat: "", lng: "" };
var city = { name: "", lat: "", lng: "" };
var trackgps = { name: "", url: "" };
var parking = { name: "", lat: "", lng: "" };
var destination;

//Function
var mapGeocoding = function (id, mountain, city, parking, trackgps) {

    map = new GMaps({
        div: id,
        lat: mountain.lat,
        lng: mountain.lng,
        streetViewControl: false,
        mapTypeControl: true,
        overviewMapControl: false,
        mapTypeControlOptions: {
            mapTypeIds: ["roadmap", "satellite", "osm"]
        }
    });
    map.addMapType("osm", {
        getTileUrl: function (coord, zoom) {
            return "https://a.tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
        },
        tileSize: new google.maps.Size(256, 256),
        name: "OpenStreetMap",
        maxZoom: 18
    });
    map.setMapTypeId("osm");

    //If exist object City or Parking    
    if (parking.hasOwnProperty("name") && parking.name != "") {
        //Parking
        position = new google.maps.LatLng(parking.lat, parking.lng);
        bounds.extend(position);
        map.getElevations({
            locations: [[parking.lat, parking.lng]],
            callback: function (result, status) {
                if (status == google.maps.ElevationStatus.OK) {
                    map.addMarker({
                        lat: parking.lat,
                        lng: parking.lng,
                        icon: icons["parking"].icon,
                        title: 'Start from' + parking.name + ' - ' + Math.round(result[0].elevation) + ' m.'
                    });
                }
            }
        });
    }
    else if (city.hasOwnProperty("name") && city.name != "") {
        map.addMarker({
            lat: city.lat,
            lng: city.lng,
            icon: icons["meta"].icon,
            title: 'Località di partenza: ' + city.name
        });
        position = new google.maps.LatLng(city.lat, city.lng);
        bounds.extend(position);
    }

    //If exist TrackGps
    if (trackgps.hasOwnProperty("url") && trackgps.url != "") {
        infoWindow = new google.maps.InfoWindow({});
        map.loadFromKML({
            url: trackgps.url,
            suppressInfoWindows: true,
            events: {
                click: function (point) {
                    infoWindow.setContent(point.featureData.infoWindowHtml);
                    infoWindow.setPosition(point.latLng);
                    infoWindow.open(map.map);
                }
            }
        });
    }
    
    //CHECK LAT VALUE
    if (parseFloat(mountain.lat) > 0) {
        map.addMarker({
            lat: mountain.lat,
            lng: mountain.lng,
            title: mountain.name,
            label: {
                text: 'M',
                color: 'white',
                fontWeight: 'bold'
            }
        });
        var position = new google.maps.LatLng(mountain.lat, mountain.lng);
        bounds.extend(position);
    }

    map.fitBounds(bounds);
    //map.setZoom(12);
}

//Init Search Function
var handleAction = function () {
    var text = $.trim($('#gmap_geocoding_address').val());
    GMaps.geocode({
        address: text,
        callback: function (results, status) {
            if (status == 'OK') {
                var latlng = results[0].geometry.location;
                map.addMarker({
                    lat: latlng.lat(),
                    lng: latlng.lng(),
                    icon: icons["car"].icon
                });
                var position = new google.maps.LatLng(latlng.lat(), latlng.lng());
                bounds.extend(position);
                map.fitBounds(bounds);
                App.scrollTo($(id));
                map.drawRoute({
                    origin: [latlng.lat(), latlng.lng()],
                    destination: [destination.lat, destination.lng],
                    travelMode: 'driving',
                    strokeColor: 'blue',
                    strokeOpacity: 0.6,
                    strokeWeight: 6,
                    callback: function (route) {                     
                        if (route.legs.length > 0) {
                            var km = route.legs[0].distance.text;
                            var hours = route.legs[0].duration.text;
                            //console.log(km);
                            $('#routingDistance').html(km);
                            $('#routingDuration').html(hours);
                        }
                    }
                });
            }
        }
    });
}

$('#gmap_geocoding_btn').click(function (e) {
    handleAction(destination);
});

$("#gmap_geocoding_address").keypress(function (e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    if (keycode == '13') {
        e.preventDefault();
        handleAction();
    }
});