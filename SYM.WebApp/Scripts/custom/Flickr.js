﻿function GalleryFlickrThumb(mountain, mountainID, lat, long, divID) {
    
    var temp = mountain.replace(" ","+");
    var lat = lat;
    var lng = long;

    var url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=6003e225dbb6e722fcd004fdd7e01d9a&text=" + temp + "&format=json&nojsoncallback=1&lat=" + lat + "lon=" + lng + "&sort=interestingness-desc&per_page=40&extras=owner_name,tags,viewsdate_taken";
    var src, datasrc;
    var title, subtitle, social;
    var count = 0;
    social = '<div class="ftg-social"><a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a><a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a><a href="#" data-social="google-plus"><i class="fa fa-google"></i></a><a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a></div>';

jQuery.getJSON(url + "", function (data) {

    count = data.photos.total;
    if (count > 0) {
        App.startPageLoading({ animate: true });
        jQuery.each(data.photos.photo, function (i, item) {
            src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
            datasrc = "http://farm" + item.farm + ".static.flickr.com/" + item.server + "/" + item.id + "_" + item.secret;
            var img = jQuery("<img />").attr("class", "item").attr("src", src).attr("data-src", datasrc + "_s.jpg");
            var tileinner = jQuery("<a class='tile-inner' />").attr("data-title", item.title + " - " + item.ownername).attr("data-href", datasrc + ".jpg").append(img);//.append('<span class="hover"><i class="icon fa fa-search"></i></span>');
            var tile = jQuery("<div class='tile'/>").append(tileinner);

            tile.appendTo(divID + " > .ftg-items");

            if (i >= 30 || i == count - 1) {
                App.stopPageLoading();
                $(divID).finalTilesGallery({
                    gridSize: 10,
                    allowEnlargement: true,
                    margin: 5,
                    minTileWidth: 67
                });
                return false;
            }
        });
    }
});

}

function GalleryFlickr(mountain, mntnId, lat, long, divID) {

    var temp = mountain.replace(" ", "+");
    var lat = lat;
    var lng = long;

    var url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=6003e225dbb6e722fcd004fdd7e01d9a&text=" + temp + "&format=json&nojsoncallback=1&lat=" + lat + "lon=" + lng + "&sort=interestingness-desc&per_page=100&extras=owner_name,tags,viewsdate_taken";
    var src, datasrc;
    var title, subtitle, social;
    social = '<div class="ftg-social"><a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a><a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a><a href="#" data-social="google-plus"><i class="fa fa-google"></i></a><a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a></div>';
    console.log(url);
    jQuery.getJSON(url + "", function (data) {
        count = data.photos.total;
        if (count > 0) {
            App.startPageLoading({ animate: true });
            jQuery.each(data.photos.photo, function (i, item) {
                src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
                title = '<span class="title">' + item.title.substring(0, 40) + '</span>';
                //subtitle = '<span class="subtitle">' + item.tags.substring(0, 25) + '</span>';
                datasrc = "http://farm" + item.farm + ".static.flickr.com/" + item.server + "/" + item.id + "_" + item.secret;
                var img = jQuery("<img />").attr("class", "item").attr("src", src).attr("data-src", datasrc + ".jpg");
                var tileinner = jQuery("<a class='tile-inner change-cover' />").attr("data-title", item.title + " - " + item.ownername).attr("data-lightbox", "gallery").attr("href", datasrc + "_b.jpg").append(img).append(title);//append(subtitle);//.append('<span class="hover"><i class="icon fa fa-search"></i></span>');
                var tile = jQuery("<div class='tile'/>").append(tileinner).append(social);
                tile.appendTo(divID + " > .ftg-items");
                if (i >= 45 || i == count - 1) {
                    App.stopPageLoading();
                    $(divID).finalTilesGallery({
                        gridSize: 10,
                        allowEnlargement: true
                    });
                    if(mntnId != 0){
                        $(".change-cover").click(function () {
                            var value = $(this).attr('href').replace("_b.jpg",".jpg");
                            $.ajax({
                                type: "POST",
                                url: "/Mountain/AddImage",
                                data: '{p_mntn: "' + mntnId + '",p_url:"' + value + '"}',
                                contentType: "application/json; charset=utf-8",
                                cache: false,
                                success: function (response) {
                                },
                                error: function () {
                                    console.log("error ajax change-cover");
                                }
                            });
                        });
                    }
                    return false;
                }
            });
        }
    });

}

//Function to get the images load by an user
function GalleryProfile(userId, divID, imagesToShow) {

    var url = "/api/Image/GetImagesByUserId?Id="+userId;
    var src, datasrc;
    var title, subtitle, social;
    social = '<div class="ftg-social"><a href="#" data-social="twitter"><i class="fa fa-twitter"></i></a><a href="#" data-social="facebook"><i class="fa fa-facebook"></i></a><a href="#" data-social="google-plus"><i class="fa fa-google"></i></a><a href="#" data-social="pinterest"><i class="fa fa-pinterest"></i></a></div>';
    console.log(url);
    jQuery.getJSON(url + "", function (data) {
        count = data.length;
        if (count > 0) {
            App.startPageLoading({ animate: true });

            jQuery.each(data, function (i, item) {
                src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
                title = '<span class="title">' + item.title.substring(0, 40) + '</span>';
                datasrc = "http://res.cloudinary.com/shareyourmountain/image/upload/w_400/" + item.image_url;
                var img = jQuery("<img />").attr("class", "item").attr("src", src).attr("data-src", datasrc);
                var tileinner = jQuery("<a class='tile-inner change-cover' />").attr("data-title", item.title).append(img).append(title);
                var tile = jQuery("<div class='tile'/>").append(tileinner).append(social);
                tile.appendTo(divID + " > .ftg-items");
                if (i >= imagesToShow || i == count - 1) {
                    App.stopPageLoading();
                    $(divID).finalTilesGallery({
                        gridSize: 10,
                        allowEnlargement: true
                    });
                    return false;
                }
            });
        }
    });

}