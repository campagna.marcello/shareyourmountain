﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SYM.DataAccessLayer;
using NLog;
using SYM.BusinessLayer.Services;
using System.Text;

namespace SYM.WebApp.ControllersApi
{
    public class SyncController : ApiController
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public HttpResponseMessage UserStats()
        {
            HttpResponseMessage response = null;

            try { 
                    
                UserService userService = new UserService();
  
                int routes = 0, peaks = 0, uploads = 0, routeImg = 0, mntnImg = 0;
            
                //Loop for all users
                var users = _context.user_profile.ToList();

                foreach (user_profile user in users) {
                    routes = _context.user_wishlist.Where(a => a.user_id == user.profile_id && a.route_id != null && a.wl_done == true).ToList().Count();
                    peaks = _context.user_wishlist.Where(a => a.user_id == user.profile_id && a.mountain_id != null && a.wl_done == true).Select(grp => grp.mountain_id).Distinct().ToList().Count;
                    routeImg = _context.route_image.Where(a => a.user_id == user.profile_id).ToList().Count();
                    mntnImg = _context.mntn_image.Where(a => a.user_id == user.profile_id).ToList().Count();
                    uploads = routeImg + mntnImg;

                    //Set Stats;
                    userService.SetStats(user.profile_id.GetValueOrDefault(), routes, peaks, uploads);
                }

                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent("", Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return response;
            }

            //logger.Debug("Task complete");
            return response;
        }

        [HttpGet]
        public HttpResponseMessage RankingFullUpdate()
        {
            HttpResponseMessage response = null;
            RouteService RS = new RouteService();

            try
            {
                var routes = _context.route.Where(a => a.route_status == true).ToList();
                foreach (route r in routes)
                {
                    int newscore = RS.SetRanking(r.id);
                    logger.Trace("Ranking update: " + r.id + "-" +r.route_name + " score " + newscore.ToString());
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return response;
            }

            logger.Debug("RankingFullUpdate -> Task complete");
            return response;
        }

        [HttpGet]
        public HttpResponseMessage RouteToFix()
        {
            HttpResponseMessage response = null;
            RouteService RS = new RouteService();

            try
            {
                var routes = _context.v_sync_routetofix.ToList();
                foreach (v_sync_routetofix r in routes)
                {
                    RS.UpdateMountainData(r.route_id, r.mntn_rangeId.GetValueOrDefault(), r.mntn_groupId.GetValueOrDefault());
                    RS.SetRanking(r.route_id);
                    logger.Trace("Route Fixed: " + r.route_id + " / Mountain data " + r.mntn_rangeId.GetValueOrDefault() +  " - "  + r.mntn_groupId.GetValueOrDefault());
                }
                response = new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                logger.Error("RouteToFix -> " + ex.Message);
                response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                return response;
            }

            logger.Debug("RouteToFix -> Task complete");
            return response;
        }
    }
}
