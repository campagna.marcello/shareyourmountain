﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SYM.DataAccessLayer;
using Newtonsoft.Json;
using System.Text;
using NLog;

namespace SYM.SearchEngine.MvcApp.ControllersApi.Autocomplete
{
    public class Parameters
    {
        public string p_term { get; set; }
        public string p_countryId { get; set; }
        public string p_regionId { get; set; }
        public string p_rangeId { get; set; }
        public string p_mountainId { get; set; }
        public string p_valleyId { get; set; }
        public string p_lat { get; set; }
        public string p_long { get; set; }
        public string p_showby { get; set; }
        public string p_userId { get; set; }
    }

    public class AutocompleteController : ApiController
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        //api/autocomplete/GetMountains
        [HttpPost]
        public HttpResponseMessage GetMountains(Parameters parameters)
        {

            string p_term = parameters.p_term;
            string p_countryid = parameters.p_countryId;
            string p_regionid = parameters.p_regionId;
            HttpResponseMessage response = null;

            try
            {
                int country_id = String.IsNullOrEmpty(p_countryid) ? 0 : Convert.ToInt32(p_countryid);
                int region_id = String.IsNullOrEmpty(p_regionid) ? 0 : Convert.ToInt32(p_regionid);
                //Check if Region is FILL :D
                var result3 = _context.v_mountain_autocomplete.Where(s => s.name.ToLower().Contains(p_term.ToLower()) && (region_id == 0 || s.region_id == region_id) && s.country_id == country_id).Select(w => new { w.id, w.name, w.altitude, range = w.range_name, type = "m" }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result3);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return response;
        }

        //NEW api/autocomplete/GetMountain
        [HttpPost]
        public HttpResponseMessage GetMountain(Parameters parameters)
        {
            int id = String.IsNullOrEmpty(parameters.p_mountainId) ? 0 : Convert.ToInt32(parameters.p_mountainId);
            HttpResponseMessage response = null;

            try
            {
                var result3 = _context.mountain.Where(s => s.id == id).Select(w => new { w.id, name = w.mntn_name, altitude = w.mntn_altitude, lat = w.mntn_latitude, lng = w.mntn_longitude, range = w.mntn_mountainrange.mntnRange_name }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result3);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return response;
        }

        //api/autocomplete/GetMountainsByRange
        [HttpPost]
        [System.Web.Mvc.OutputCache(Duration = 3600, VaryByParam = "*")]
        public HttpResponseMessage GetMountainByRange(Parameters parameters)
        {

            string p_rangeid = parameters.p_rangeId;
            HttpResponseMessage response = null;

            try
            {
                int range_id = String.IsNullOrEmpty(p_rangeid) ? 0 : Convert.ToInt32(p_rangeid);
                var result3 = _context.v_mountain4map.Where(s => s.range_id == range_id).Select(w => new { w.id, w.name, w.lat, w.lng, altitude = w.altitude, route = w.route }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result3);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return response;
        }

        [HttpPost]
        //[System.Web.Mvc.OutputCache(Duration = 3600, VaryByParam = "*")]

        public HttpResponseMessage GetNearestMountainByLocation(Parameters parameters)
        {

            int mntnIdToExclude = Convert.ToInt32(parameters.p_mountainId);
            int userId = Convert.ToInt32(parameters.p_userId);
            int altitude = 0;
            string lat = parameters.p_lat.Replace(',','.');
            string lng = parameters.p_long.Replace(',', '.');
            string showby = parameters.p_showby;
            bool flagRouteCounter = false;

            if (showby == "true")
                flagRouteCounter = true;
            else if (showby == "1500")
                altitude = 1500;
            else if (showby == "2500")
                altitude = 2500;
            else if (showby == "3500")
                altitude = 3500;

            HttpResponseMessage response = null;

            try
            {
                var sql = @"CALL `sp_geo_nearestMountains`({0} ,{1} ,{2} ,{3} ,{4}, {5} )";
                var result3 = _context.Database.SqlQuery<nearestMountains>(sql, new object[] { lat, lng, mntnIdToExclude, altitude, flagRouteCounter, userId }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result3);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return response;
        }

        [HttpPost]
        public HttpResponseMessage GetTopos(Parameters parameters)
        {

            string p_term = parameters.p_term;
            string p_countryid = parameters.p_countryId;
            string p_regionid = parameters.p_regionId;
            HttpResponseMessage response = null;
            try
            {
                int country_id = String.IsNullOrEmpty(p_countryid) ? 0 : Convert.ToInt32(p_countryid);
                int region_id = String.IsNullOrEmpty(p_regionid) ? 0 : Convert.ToInt32(p_regionid);

                var result3 = _context.v_route_autocomplete.Where(s => s.route_title.ToLower().Contains(p_term.ToLower()) && (region_id == 0 || s.region_id == region_id) && s.country_id == country_id).Select(w => new { w.id, w.route_title, type = "t" }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result3);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return response;
        }

        [HttpPost]
        public HttpResponseMessage GetRegion(Parameters parameters)
        {
            string p_term = parameters.p_term;
            string p_countryid = parameters.p_countryId;
            string p_regionid = parameters.p_regionId;
            HttpResponseMessage response = null;
            try
            {
                int country_id = String.IsNullOrEmpty(p_countryid) ? 0 : Convert.ToInt32(p_countryid);
                var result = _context.v_georegion_autocomplete.ToList();
                var result3 = result.Where(s => s.name.ToLower().Contains(p_term.ToLower()) && (s.country_id == country_id || (country_id == 0))).Select(w => new { w.id, name = w.name, country_code = w.country_code, w.country_id }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result3);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return response;
        }

        [HttpPost]
        public HttpResponseMessage GetCity(Parameters parameters)
        {
            string p_term = parameters.p_term;
            string p_countryid = parameters.p_countryId;
            string p_regionid = parameters.p_regionId;
            HttpResponseMessage response = null;
            try
            {
                int country_id = Convert.ToInt32(p_countryid);
                int region_id = Convert.ToInt32(p_regionid);
                var result = _context.v_geocity_autocomplete.Where(s => s.city.ToLower().Contains(p_term.ToLower()) && s.regionid == region_id && (0 == country_id || s.countryid == country_id)).Select(w => new { w.id, name = w.city, w.country, w.lat, w.lng }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return response;
        }

        [HttpPost]
        public HttpResponseMessage GetMntnRange(Parameters parameters)
        {
            string p_term = parameters.p_term;
            string p_countryid = parameters.p_countryId;
            string p_regionid = parameters.p_regionId;
            HttpResponseMessage response = null;
            try
            {
                var result = _context.mntn_mountainrange.Where(s => s.mntnRange_name.ToLower().Contains(p_term.ToLower())).Select(w => new { w.id, name = w.mntnRange_name }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return response;
        }

        [HttpPost]
        public HttpResponseMessage GetMntnGroup(Parameters parameters)
        {
            string p_term = parameters.p_term;
            string p_countryid = parameters.p_countryId;
            string p_regionid = parameters.p_regionId;
            HttpResponseMessage response = null;
            try
            {
                var result = _context.mntn_mountaingroup.Where(s => s.mntnGroup_name.ToLower().Contains(p_term.ToLower())).Select(w => new { w.id, name = w.mntnGroup_name }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return response;
        }

        [HttpPost]
        public HttpResponseMessage GetValleys(Parameters parameters)
        {
            string p_term = parameters.p_term;
            string p_countryid = parameters.p_countryId;
            HttpResponseMessage response = null;
            try
            {
                int country_id = Convert.ToInt32(p_countryid);
                int region_id = String.IsNullOrEmpty(parameters.p_regionId) ? 0 : Convert.ToInt32(parameters.p_regionId);
                var result = _context.valley.Where(s => s.valley_name.ToLower().Contains(p_term.ToLower()) && (region_id == 0 || s.region_id == region_id) && s.country_id == country_id).Select(w => new { w.id, name = w.valley_name, type = "v" }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return response;
        }

        [HttpPost]
        public HttpResponseMessage GetValley(Parameters parameters)
        {
            int id = String.IsNullOrEmpty(parameters.p_valleyId) ? 0 : Convert.ToInt32(parameters.p_valleyId);
            HttpResponseMessage response = null;

            try
            {
                var result3 = _context.valley.Where(s => s.id == id).Select(w => new { w.id, name = w.valley_name, lat = w.valley_lat, lng = w.valley_lng }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result3);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return response;
        }

        [HttpPost]
        public HttpResponseMessage GetParking(Parameters parameters)
        {
            string p_term = parameters.p_term;
            string p_countryid = parameters.p_countryId;
            string p_regionid = parameters.p_regionId;
            HttpResponseMessage response = null;
            try
            {
                int country_id = Convert.ToInt32(p_countryid);
                int region_id = Convert.ToInt32(p_regionid);
                var result = _context.parking.Where(s => s.parking_name.ToLower().Contains(p_term.ToLower()) && s.region_id == region_id && (0 == country_id || s.country_id == country_id)).Select(w => new { w.id, name = w.parking_name, country = w.country_id, lat = w.parking_lat, lng = w.parking_lng }).ToList();
                string jsonResult = JsonConvert.SerializeObject(result);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return response;
        }

    }

    public class nearestMountains
    {
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<float> lat { get; set; }
        public Nullable<float> lng { get; set; }
        public int altitude { get; set; }
        public int km { get; set; }
        public int route { get; set; }
        public bool done { get; set; }
    }
}
