﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using SYM.DataAccessLayer;
using SYM.BusinessLayer.Services;
using NLog;
using System.Text;

namespace SYM.WebApp.ControllersApi
{
    public class Parameters
    {
        public string p_regionCode { get; set; }
        public string p_countryCode { get; set; }
        public string p_url_mntnSource { get; set; }
        public string p_url_mntnToFix { get; set; }
    }

    public class MountainController : ApiController
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        [HttpPost]
        /// <summary>
        /// Update the mountain categorization comparing it with another peak (Mountain Rage & Group)
        /// </summary>
        /// <param name="mntnIdSource"></param>
        /// <param name="mntnIdToFix"></param>
        /// <returns></returns>
        public HttpResponseMessage UpdateMissingCategorization(Parameters parameters)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.NotAcceptable); ;
            MountainService MS = new MountainService();
            int mountainIdSource = 0, mountainIdToFix = 0;
            bool valid = false;

            //TODO: ADD AUDIT

            try
            {
                if (!String.IsNullOrEmpty(parameters.p_url_mntnSource) && !String.IsNullOrEmpty(parameters.p_url_mntnToFix))
                {
                    mountainIdSource = GetMountainIdByURL(parameters.p_url_mntnSource);
                    mountainIdToFix = GetMountainIdByURL(parameters.p_url_mntnToFix);

                    valid = MS.UpdateMissingCategorization(mountainIdSource, mountainIdToFix);

                    logger.Trace("MountainUpdateMissingData ->" + valid.ToString() + " = " + mountainIdSource.ToString() + "/" + mountainIdToFix.ToString());

                    if (valid)
                        response = new HttpResponseMessage(HttpStatusCode.OK);
                    else {
                        response = new HttpResponseMessage(HttpStatusCode.NotAcceptable);
                        response.Content = new StringContent("Errore Durante l'aggiornamento dei dati");
                    }
                }
                else
                    response.Content = new StringContent("Parametri non valizzati");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                response = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            return response;
        }

        /// <summary>
        /// Add in automatic country and region by coordinates using the google api
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage UpdateMissingGeoData(int id)
        {
            HttpResponseMessage response = null;
            MountainService MS = new MountainService();
            string message = null;

            try
            {
                message = MS.UpdateMissingGeoData(id);
                response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(message, Encoding.UTF8, "application/json");
                logger.Debug(message);
            }
            catch (Exception ex)
            {
                logger.Error("AddGeoData -> " + ex.Message);
                response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                return response;
            }

            return response;
        }

        internal int GetMountainIdByURL(string url) {
            var urlS = new Uri(url.Replace("/it", "").Replace("/en", ""));
            return Convert.ToInt32(urlS.Segments[2].Split('-')[0]);
        }
   
    }

}
