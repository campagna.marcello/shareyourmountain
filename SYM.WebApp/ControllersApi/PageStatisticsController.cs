﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using SYM.DataAccessLayer;
using System.Text;
using Newtonsoft.Json;

namespace SYM.SearchEngine.MvcApp.ControllersApi.PageStatistics
{
    public class Parameters
    {
        public string p_id{ get; set; }
        public string p_type { get; set; }
    }
    
    public class PageStatisticsController : ApiController
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        enum TypePage
        {
            Route,
            Mountain,
            Camp
        };

        [HttpPost]
        public HttpResponseMessage GetCounter(Parameters parameters)
        {
            HttpResponseMessage response = null;
            page_statistics obj;
            Int16 type = 0;
            int p_id = Convert.ToInt32(parameters.p_id);
            string p_type = parameters.p_type;

            switch (p_type)
            {
                case "m":
                    type = 1;
                    break;
                case "r":
                    type = 2;
                    break;
                case "c":
                    type = 3;
                    break;
                default:
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            try
            {
                obj = PageExist(p_id, type);
                if(obj == null)
                {
                    obj = new page_statistics();
                    obj.page_countview = 0;
                    obj.page_dateMod = DateTime.Now;
                }

                var data = new { countview = (int)obj.page_countview, lastview = @String.Format("{0:dd/MM/yyyy hh:mm}", obj.page_dateMod) };
                string jsonResult = JsonConvert.SerializeObject(data, Formatting.Indented);
                response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch
            {
                //logger.Error(ex.Message);
                response = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            
            return response;
        }

        [HttpPost]
        public HttpResponseMessage SetCounter(Parameters parameters)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            page_statistics obj;
            Int16 type = 0;
            int p_id = Convert.ToInt32(parameters.p_id);
            string p_type = parameters.p_type;

            switch (p_type)
            {
                case "m":
                    type = 1;
                    break;
                case "r":
                    type = 2;
                    break;
                case "c":
                    type = 3;
                    break;
                default:
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            obj = PageExist(p_id, type);
            try
            {
                if (obj != null)
                {
                    //Increment & Update Date
                    obj.page_countview = obj.page_countview + 1;
                    obj.page_dateMod = DateTime.Now;
                }
                else {
                    //Create new record
                    obj = new page_statistics();
                    obj.page_id = p_id;
                    obj.page_type = type;
                    obj.page_countview = 1;
                    obj.page_dateMod = DateTime.Now;
                    _context.page_statistics.Add(obj);        
                }
                _context.SaveChanges();

                var data = new { countview = (int)obj.page_countview, lastview = @String.Format("{0:dd/MM/yyyy hh:mm}", obj.page_dateMod) };
                string jsonResult = JsonConvert.SerializeObject(data, Formatting.Indented);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch(Exception ex)
            {
                response = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
   
            return response;
        }

        private page_statistics PageExist(int id, Int16 type)
        {
            page_statistics result = null;
            
            var list = _context.page_statistics.Where(a => a.page_type == type && a.page_id == id).ToList();
            if (list.Count > 0)
                result = list.First();
            return result;
        }
    }
}
