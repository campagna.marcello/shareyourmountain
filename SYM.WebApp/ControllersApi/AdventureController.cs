﻿using NLog;
using SYM.DataAccessLayer;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;
using System.Collections.Generic;
using SYM.BusinessLayer.Services;
using Newtonsoft.Json;
using System.Text;
using SYM.WebApp.Models;

namespace SYM.SearchEngine.MvcApp.ControllersApi
{
    public class Parameters
    {
        public string p_mountain { get; set; }
        public string p_route { get; set; }
        public string p_hut { get; set; }
        public string p_type { get; set; } //CHeck if is possibile to remove this column
        public string p_done { get; set; }
        public string p_profileid { get; set; }
    }

    public class AdventureController : ApiController
    {

        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();
        UserService userService = new UserService();

        [HttpPost]
        public HttpResponseMessage Set(Parameters parameters)
        {
            HttpResponseMessage response = null;
            AdventureService AS = new AdventureService();

            int mountain = 0, route = 0, hut = 0, profileID = 0;
            bool? done = false;

            try
            {

                if (!String.IsNullOrEmpty(parameters.p_mountain))
                    mountain = Convert.ToInt32(parameters.p_mountain);
                if (!String.IsNullOrEmpty(parameters.p_route))
                    route = Convert.ToInt32(parameters.p_route);
                if (!String.IsNullOrEmpty(parameters.p_hut))
                    hut = Convert.ToInt32(parameters.p_hut);
                if (!String.IsNullOrEmpty(parameters.p_done))
                    done = Convert.ToBoolean(parameters.p_done);
                //AUTH
                if (userService.GetUserByProfileId(parameters.p_profileid) != null)
                    profileID = Convert.ToInt32(parameters.p_profileid);
                else
                    profileID = GetProfileId();

                bool result = AS.Set(profileID, mountain, route, hut, done);
                response = new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " Set ->" + profileID + "," + mountain + "," + route + "," + hut);
                response = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            return response;
        }

        [HttpPost]
        public HttpResponseMessage Remove(Parameters parameters)
        {
            HttpResponseMessage response = null;
            AdventureService AS = new AdventureService();
            user_wishlist obj;
            int mountain = 0, route = 0, hut = 0, profileID = 0;

            try
            {
                if (!String.IsNullOrEmpty(parameters.p_mountain))
                    mountain = Convert.ToInt32(parameters.p_mountain);
                if (!String.IsNullOrEmpty(parameters.p_route))
                    route = Convert.ToInt32(parameters.p_route);
                if (!String.IsNullOrEmpty(parameters.p_hut))
                    hut = Convert.ToInt32(parameters.p_hut);
                //AUTH
                if (userService.GetUserByProfileId(parameters.p_profileid) != null)
                    profileID = Convert.ToInt32(parameters.p_profileid);
                else
                    profileID = GetProfileId();

                obj = AS.GetWishlist(profileID, mountain, route, hut);

                if (obj != null)
                {
                    obj = _context.user_wishlist.Find(obj.id);
                    _context.user_wishlist.Remove(obj);
                    _context.SaveChanges();
                    response = new HttpResponseMessage(HttpStatusCode.OK);
                    logger.Info("Adventure - Removed OK -> " + profileID + "," + mountain + "," + route + "," + hut);
                }
                else {
                    response = new HttpResponseMessage(HttpStatusCode.NoContent);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " Removed KO ->" + profileID + "," + mountain + "," + route + "," + hut);
                response = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            return response;
        }

        [HttpPost]
        public HttpResponseMessage Get(Parameters parameters)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK); ;
            AdventureService AS = new AdventureService();
            user_wishlist obj;
            int mountain = 0, route = 0, hut = 0, profileID = 0;

            try
            {
                if (!String.IsNullOrEmpty(parameters.p_mountain))
                    mountain = Convert.ToInt32(parameters.p_mountain);
                if (!String.IsNullOrEmpty(parameters.p_route))
                    route = Convert.ToInt32(parameters.p_route);
                if (!String.IsNullOrEmpty(parameters.p_hut))
                    hut = Convert.ToInt32(parameters.p_hut);
                //AUTH
                if (userService.GetUserByProfileId(parameters.p_profileid) != null)
                    profileID = Convert.ToInt32(parameters.p_profileid);
                else
                    profileID = GetProfileId();

                obj = AS.GetWishlist(profileID, mountain, route, hut);

                if (obj != null && profileID > 0)
                {
                    var data = new { mountain = obj.mountain_id, route = obj.route_id, done = (bool)obj.wl_done, };
                    string jsonResult = JsonConvert.SerializeObject(data, Formatting.Indented);
                    response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
                }
                else {
                    response = new HttpResponseMessage(HttpStatusCode.NoContent);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " Get(profileId,mountain,route,hut) ->" + profileID + "," + mountain + "," + route + "," + hut);
                response = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetMountainsForAmcharts(string userId)
        {
            HttpResponseMessage response = null;

            var user = userService.GetUserByUserId(new Guid(userId));

            try
            {
                MapDataViewModel mapData = new MapDataViewModel();

                mapData.map = "worldHigh";
                mapData.zoomLevel = 18;
                mapData.zoomLatitude = (double)user.geo_city.city_latitude;
                mapData.zoomLongitude = (double)user.geo_city.city_longitude;

                List<Area> areas = new List<Area>();

                Area areaIT = new Area();
                areaIT.title = "Italy";
                areaIT.id = "IT";
                areaIT.color = "#2ab4c0";
                areaIT.customData = "1957"; //?
                areaIT.groupId = "before2004"; //?
                areas.Add(areaIT);

                Area areaCH = new Area();
                areaCH.title = "Swiss";
                areaCH.id = "CH";
                areaCH.color = "#44b6ae";
                areaCH.customData = "1957"; //?
                areaCH.groupId = "before2004"; //?
                areas.Add(areaCH);

                mapData.areas = areas;

                List<Line> lines = new List<Line>();
                foreach (var adventure in _context.user_wishlist.Where(a => a.user_id == user.profile_id && a.mountain_id != null && a.wl_done == true).ToList())
                {
                    Line line = new Line();
                    line.latitudes = new List<double> { mapData.zoomLatitude, (double)adventure.mountain.mntn_latitude };
                    line.longitudes = new List<double> { mapData.zoomLongitude, (double)adventure.mountain.mntn_longitude };
                    lines.Add(line);
                }
                mapData.lines = lines;

                List<Image> images = new List<Image>();
                Image imageMntn;

                //MAKER MAIN
                Image image1 = new Image();
                image1.id = user.geo_city.city_name.ToLower();
                image1.svgPath = @"M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
                image1.title = user.geo_city.city_name;
                image1.latitude = mapData.zoomLatitude;
                image1.longitude = mapData.zoomLongitude;
                image1.scale = 1.5;
                image1.color = "#ffa500";
                images.Add(image1);

                //MAKERS ADV
                foreach (var adventure in _context.user_wishlist.Where(a => a.user_id == user.profile_id && a.mountain_id != null && a.wl_done == true).ToList())
                {
                    imageMntn = new Image();
                    imageMntn.svgPath = @"M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
                    imageMntn.title = "<span><b>"+ adventure.mountain.mntn_name + "</b> - " + adventure.mountain.mntn_altitude + " m.</ span >";
                    imageMntn.latitude = (double)adventure.mountain.mntn_latitude;
                    imageMntn.longitude = (double)adventure.mountain.mntn_longitude;
                    imageMntn.scale = 0.5;
                    images.Add(imageMntn);

                }

                mapData.images = images;

                string jsonResult = JsonConvert.SerializeObject(mapData);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                response = this.Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return response;
        }

        //TODO -- esternalizzare.....

        //Return the id using the session context or the profile parameter

        /// <summary>
        /// Get ProfileId By HttpContext
        /// </summary>
        /// <returns></returns>
        private int GetProfileId()
        {
            ClaimsIdentity identity;
            int id = 0;
            try
            {
                    identity = (ClaimsIdentity)User.Identity;
                    IEnumerable<Claim> claims = identity.Claims.ToList();
                    id = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);
            }
            catch (Exception ex)
            {
                return 0;
            }

            return id;
        }

    }
}
