﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using SYM.DataAccessLayer;
using NLog;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using SYM.BusinessLayer.Services;

namespace SYM.WebApp.ControllersApi
{

    public class ImageController : ApiController
    {

        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();
        UserService userService = new UserService();

        // GET: api/Gallery
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Gallery/5
        public string Get(int id)
        {
            return "value";
        }

        // GET: api/Gallery/5
        [HttpGet]
        public HttpResponseMessage Gallery(int page)
        {
            HttpResponseMessage response = null;
            try
            {
                var model = _context.v_feed_images.ToList().OrderBy(s => s.days).Select(w => new { w.id, w.title, w.image_url, w.profile_firstName, w.profile_LastName, w.type, w.activity_id, w.user_id }).ToList();
                string jsonResult = JsonConvert.SerializeObject(model);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetImagesByUserId(string Id)
        {
            HttpResponseMessage response = null;
            try
            {
                var user = userService.GetUserByUserId(new Guid(Id));

                var model = _context.v_feed_images.Where(a => a.user_id == user.profile_id).ToList().OrderBy(s => s.days).Select(w => new { w.id, w.title, w.image_url, w.profile_firstName, w.profile_LastName, w.type, w.activity_id, w.user_id }).ToList();
                string jsonResult = JsonConvert.SerializeObject(model);
                response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return response;
        }

        //// POST: api/Gallery
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/Gallery/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Gallery/5
        //public void Delete(int id)
        //{
        //}
    }
}
