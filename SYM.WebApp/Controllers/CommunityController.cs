﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Threading.Tasks;
using SYM.DataAccessLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SYM.BusinessLayer;
using SYM.BusinessLayer.Services;
using SYM.SearchEngine.MvcApp.Helpers;
using SYM.WebApp.Models;
using NLog;
using System.Web;
using System.IO;
using System.Security.Claims;
using CloudinaryDotNet.Actions;
using System.Web.Configuration;
using PagedList;

namespace SYM.WebApp.Controllers
{
    public class CommunityController : Controller
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();
        UserService userService = new UserService();

        [OutputCache(Duration = 60)]
        public ActionResult Index()
        {
            try
            {
                var users = _context.v_community.ToList().OrderByDescending(a => a.image);
                if (users == null)
                {
                    return this.HttpNotFound();
                }
                return View(users);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return View();
            }
        }

        [HttpGet]
        [OutputCache(Duration = 60, VaryByCustom = "id")]
        public ActionResult Profile(string id)
        {
            List<int> activityArray = null;
            try
            {
                user_profile user = userService.GetUserByUserId(new Guid(id));
                if (user == null) {
                    logger.Error("User NOT Found " + id);
                    return this.HttpNotFound();
                }
                //Init ViewBag
                GetProfileCounter(user.profile_id.GetValueOrDefault());

                return View(user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return View();
            }
        }

        private void GetProfileCounter(int profileId)
        {
            try
            {
                int routeImg = _context.route_image.Where(a => a.user_id == profileId).ToList().Count();
                int mntnImg = _context.mntn_image.Where(a => a.user_id == profileId).ToList().Count();

                ViewBag.Routes = _context.user_wishlist.Where(a => a.user_id == profileId && a.route_id != null && a.wl_done == true).ToList().Count();
                ViewBag.Peaks = _context.user_wishlist.Where(a => a.user_id == profileId && a.mountain_id != null && a.wl_done == true).Select(grp => grp.mountain_id).Distinct().ToList().Count;
                ViewBag.Uploads = routeImg + mntnImg;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }


    }
}