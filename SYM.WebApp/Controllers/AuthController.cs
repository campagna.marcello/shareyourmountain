﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Script.Serialization;
using SYM.DataAccessLayer;
using NLog;
using SYM.BusinessLayer;
using System.Net.Mail;
using System.Net;



namespace SYM.SearchEngine.MvcApp.Controllers
{
    //public class AuthController : Controller
    //{
    //    Entities _context = new Entities();
    //    Logger logger = LogManager.GetCurrentClassLogger();

    //    [AllowAnonymous]
    //    public ActionResult Login()
    //    {
    //        HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
    //        // var ek = cookie.Value;
    //        try
    //        {
    //            //some times no cookie in browser
    //            //JavaScriptSerializer js = new JavaScriptSerializer();
    //            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
    //            ////string data = ticket.UserData;
    //            //LoginModel model = js.Deserialize<LoginModel>(ticket.UserData);
    //            //if (MyAuthentication.Login(model.UserName, model.Password) == true)
    //            //{
    //            //    RedirectToAction("Index", "Home");
    //            //}
    //        }
    //        catch
    //        {

    //        } 
            
    //        return View();
    //    }

    //    //
    //    // POST: /Account/Login

    //    [HttpPost]
    //    [AllowAnonymous]
    //    public ActionResult Login(FormCollection collection, string returnUrl)
    //    {
    //        user valideUser = null;
    //        string username;
    //        string password;
    //        string ip = this.Request.UserHostAddress;

    //        if (collection != null || collection.AllKeys.Length > 0)
    //        {
    //            username = collection["username"].ToString();
    //            password = collection["password"].ToString();
    //            string encPassword = EncryptUtil.Encrypt(password, true);

    //            valideUser = _context.user.FirstOrDefault(a => a.user_userName == username && a.user_password == encPassword);

    //            if (valideUser == null)
    //            {
    //                ViewBag.ErrorMsg = "Username doesn’t match with the password. Please try again, or request a new password.";
    //                ViewBag.Result = "warning";
    //                logger.Log(LogLevel.Warn, "Error Login: " + username + " -> " + ip);
    //            }
    //            else if (valideUser.user_status == false)
    //            {
    //                ViewBag.ErrorMsg = "Account isn't confirmed. Please check your email and click the activation link.";
    //                ViewBag.Result = "warning";
    //                logger.Log(LogLevel.Warn, "Login Inactive: " + username + " -> " + ip);
    //            }
    //            else if (valideUser != null)
    //            {
    //                CreateAuthenticationTicket(Convert.ToInt32(valideUser.id));
    //                valideUser.user_dateLogin = DateTime.Now;
    //                _context.SaveChanges();
    //                //logger.Log(LogLevel.Debug, "Login: " + username + " -> " + ip);
    //                //Login Success
    //                return RedirectToAction("Index", "Home");
    //            }               
    //        }

    //        return View();
    //    }

    //    [HttpGet]
    //    [AllowAnonymous]
    //    public ActionResult ConfirmationEmail(string id,string encrypt)
    //    {
    //        user valideUser = null;
    //        int userId = Convert.ToInt32(id);

    //        if (!String.IsNullOrEmpty(id) && encrypt == "7368617265796f75726d6f756e7461696e")
    //        {
    //            valideUser = _context.user.FirstOrDefault(a => a.id == userId && a.user_status == false);
    //            if (valideUser != null)
    //            {
    //                CreateAuthenticationTicket(Convert.ToInt32(valideUser.id));
    //                valideUser.user_dateMod = DateTime.Now;
    //                valideUser.user_dateLogin = DateTime.Now;
    //                valideUser.user_status = true;
    //                _context.SaveChanges();
    //                //Login Success
    //                return RedirectToAction("Index", "Home");
    //            }
    //            else
    //            {
    //                ViewBag.ErrorMsg = "Email already confirm or an error during the request. Please send an email to support@shareyourmountain.com";
    //                ViewBag.Result = "warning";
    //                logger.Log(LogLevel.Warn, "Error during the confirmation process of user " + id );
    //            }
    //        }

    //        return View("Login");
    //    }

    //    [HttpPost]
    //    [AllowAnonymous]
    //    public ActionResult Create(FormCollection collection)
    //    {
    //        user _user = new user();
    //        string encPassword;
    //        string email, username = null;
    //        string ip = this.Request.UserHostAddress;
    //        ViewBag.Result = "";
    //        ViewBag.ErrorMsg = "";

    //        if (collection != null || collection.AllKeys.Length > 0)
    //        {
    //            try
    //            {
    //                username = collection["username"].ToString();
    //                email = collection["email"].ToString();
                    
    //                if (UserExist(email, username)) {
    //                    ViewBag.ErrorMsg = "Username or Email already exists. Please try again.";
    //                    ViewBag.Result = "warning";
    //                }
    //                else { 
    //                    _user.user_userName = username;
    //                    _user.user_displayName = _user.user_firstName + ' ' + _user.user_lastName;
    //                    _user.user_firstName = collection["firstname"].ToString();
    //                    _user.user_lastName = collection["lastname"].ToString();
    //                    _user.user_email = email;
    //                    encPassword = EncryptUtil.Encrypt(collection["password"].ToString(), true);
    //                    _user.user_password = encPassword;
    //                    _user.country_id = Convert.ToInt32(collection["country"].ToString());
    //                    _user.region_id = Convert.ToInt32(collection["region"].ToString());
    //                    _user.user_status = false;
    //                    _context.user.Add(_user);
    //                    _context.SaveChanges();
    //                    //Set variable Result
    //                    ViewBag.Result = "success";
    //                    ViewBag.Username = username;

    //                    var filePath = Server.MapPath(@"/Content/templateMail/UserConfirm.html");
    //                    string message = Mailer.GetBody(filePath, "", username, _user.id.ToString());
    //                    Mailer.SendMail(email, "info@shareyourmountain.com", "Welcome to ShareYourMountain! Confirm your Email", message);

    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                ViewBag.ErrorMsg = ex.Message;
    //                ViewBag.Result = "error";
    //                logger.Log(LogLevel.Error, "Create: " + ex.Message + " -> " + ip);
    //            }
    //        }

    //        return View("Login");
    //    }
    
    //    public void CreateAuthenticationTicket(int user_id)
    //    {
    //        string encTicket = null;
    //        string userData = null;
    //        var authUser = _context.user.First(u => u.id == user_id);
            
    //        CustomPrincipalSerializedModel serializeModel = new CustomPrincipalSerializedModel();
    //        serializeModel.Id = Convert.ToInt32(authUser.id);
    //        serializeModel.FirstName = authUser.user_firstName;
    //        serializeModel.LastName = authUser.user_lastName;
    //        serializeModel.RegionId = Convert.ToInt32(authUser.region_id);
    //        serializeModel.Region = authUser.geo_region.region_name;

    //        //serializeModel.Profile = authUser.PROFILE.NAME;
    //        JavaScriptSerializer serializer = new JavaScriptSerializer();
    //        userData = serializer.Serialize(serializeModel);

    //        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, authUser.user_userName, DateTime.Now, DateTime.Now.AddHours(4), true, userData);
    //        FormsAuthentication.SetAuthCookie(authUser.user_userName, true); // <- true/false
    //        encTicket = FormsAuthentication.Encrypt(authTicket);
    //        HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);

    //        Response.Cookies.Add(faCookie);
    //    }

    //    [HttpPost]
    //    [AllowAnonymous]
    //    public ActionResult ForgotPassword(FormCollection collection)
    //    {
    //        user _user = null;
    //        string password,encPassword;
    //        string email = null;
    //        string ip = this.Request.UserHostAddress;
    //        ViewBag.Result = "";
    //        ViewBag.ErrorMsg = "";

    //        if (collection != null || collection.AllKeys.Length == 1)
    //        {
    //            try
    //            {
    //                email = collection["email"].ToString();

    //                if (!EmailExist(email))
    //                {
    //                    ViewBag.ErrorMsg = "Email doesn't exists. Please try again.";
    //                    ViewBag.Result = "warning";
    //                }
    //                else
    //                {
    //                    _user = _context.user.Where(a => a.user_email == email).First();
    //                    password = EncryptUtil.RandomString(8);
    //                    encPassword = EncryptUtil.Encrypt(password, true);
    //                    _user.user_password = encPassword;
    //                    _user.user_dateMod = DateTime.Now;
    //                    _context.SaveChanges();
    //                    //Set variable Result
    //                    ViewBag.Result = "success";

    //                    var filePath = Server.MapPath(@"/Content/templateMail/ForgetPassword.html");
    //                    string message = Mailer.GetBody(filePath, "", _user.user_userName , password);
    //                    Mailer.SendMail(email, "info@shareyourmountain.com", "Notify by ShareYourMountain! Reset Password", message);
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                ViewBag.ErrorMsg = ex.Message;
    //                ViewBag.Result = "error";
    //                logger.Log(LogLevel.Error, "Forgot Password: " + ex.Message + " -> " + ip);
    //            }
    //        }

    //        return View("Login");
    //    }

    //    //
    //    // POST: /Account/LogOff

    //    [HttpPost]
    //    [ValidateAntiForgeryToken]
    //    public ActionResult LogOff()
    //    {
    //        //WebSecurity.Logout();
    //        FormsAuthentication.SignOut();
    //        return RedirectToAction("Login", "Auth");
    //    }
        
    //    public ActionResult Logout()
    //    {
    //        FormsAuthentication.SignOut();
    //        return RedirectToAction("Login", "Auth");
    //    }

    //    private bool UserExist(string email, string username) {
    //        bool value = false;
    //        var user = _context.user.Where(a => a.user_email.Contains(email) || a.user_userName == username).ToList();
    //        if (user.Count() > 0)
    //            value = true;

    //        return value;   
    //    }

    //    private bool EmailExist(string email)
    //    {
    //        bool value = false;
    //        var user = _context.user.Where(a => a.user_email == email).ToList();
    //        if (user.Count() > 0)
    //            value = true;

    //        return value;
    //    }

    //}
}
