﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SYM.DataAccessLayer;
using SYM.SearchEngine.MvcApp.Helpers;
using SYM.WebApp.Models;
using NLog;

namespace SYM.WebApp.Controllers
{
    public class FeedController : Controller
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        [OutputCache(Duration = 15)]
        public PartialViewResult _Routes()
        {
            IEnumerable<v_feed_route> model = null;
            try
            {
                 model = _context.v_feed_route.ToList().OrderBy(s => s.day).Take(10);            
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return PartialView("~/Views/Feed/Partial/_Routes.cshtml", model);
        }

        [OutputCache(Duration = 60)]
        public PartialViewResult _Users()
        {
            IEnumerable<aspnetusers> model = null;
            try
            {
                model = _context.aspnetusers.ToList().OrderByDescending(s => s.dateIns).Take(10);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return PartialView("~/Views/Feed/Partial/_Users.cshtml", model);
        }

        [OutputCache(Duration = 15)]
        public PartialViewResult _Stats()
        {
            IEnumerable<v_feed_stats> model = null;
            try
            {
                model = _context.v_feed_stats.ToList().OrderByDescending(s => s.page_countview).Take(10);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return PartialView("~/Views/Feed/Partial/_Stats.cshtml", model);
        }

        [OutputCache(Duration = 15)]
        public PartialViewResult _Images()
        {
            IEnumerable<v_feed_images> model = null;
            try
            {
                model = _context.v_feed_images.ToList().OrderBy(s => s.days);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return PartialView("~/Views/Feed/Partial/_Images.cshtml", model);
        }
    }
}
