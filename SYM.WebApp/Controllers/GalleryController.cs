﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SYM.DataAccessLayer;
using NLog;
using PagedList;

namespace SYM.WebApp.Controllers
{
    public class GalleryController : Controller
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Gallery
        [HttpGet]
        [OutputCache(Duration = 60)]
        public ActionResult Index(int? page)
        {
            IEnumerable<v_feed_images> model = null;
            try
            {
                model = _context.v_feed_images.ToList().OrderBy(s => s.days);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            int pageSize = 20;
            int pageNumber = (page ?? 1);
            return View(model.ToPagedList(pageNumber, pageSize));
        }

        [HttpGet]
        [OutputCache(Duration = 60)]
        public ActionResult _ImagesList(int? page)
        {
            IEnumerable<v_feed_images> model = null;
            try
            {
                model = _context.v_feed_images.ToList().OrderBy(s => s.days);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            int pageSize = 20;
            int pageNumber = (page ?? 1);
            ViewBag.blockNumber = pageNumber;
            return PartialView("~/Views/Gallery/Partial/_ImagesList.cshtml",model.ToPagedList(pageNumber, pageSize));
        }
    }
}
