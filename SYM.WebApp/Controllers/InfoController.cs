﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SYM.DataAccessLayer;
using PagedList;
using NLog;
using SYM.BusinessLayer.Services;

namespace SYM.SearchEngine.MvcApp.Controllers
{
    public class InfoController : Controller
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        // GET: /Stats/
        [OutputCache(Duration = 600)]
        public ActionResult Statistics()
        {
            //TODO: Singleton?
            InfoService infoService = new InfoService();
            var summaryStats = infoService.GetSummaryStats();

            ViewBag.counterMntn = summaryStats.counterMntn.ToString();
            ViewBag.counterMntnRange = summaryStats.counterMntnRange.ToString();
            ViewBag.counterMntnGroup = summaryStats.counterMntnGroup.ToString();
            ViewBag.counterRoute = summaryStats.counterRoutes.ToString();

            var source = _context.v_report_route4source.ToList();

            return View(source);
        }

        [OutputCache(Duration = 3600)]
        public ActionResult Partners()
        {
            //var source = _context.v_report_route4source.ToList();
            return View();
        }

        [OutputCache(Duration = 600)]
        public ActionResult _Mountain4Region()
        {
            IEnumerable<v_report_mountain4region> model = null;
            try
            {
                model = _context.v_report_mountain4region.ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return PartialView("~/Views/Info/Partial/_Mountain4Region.cshtml",model);
        }

    }
}
