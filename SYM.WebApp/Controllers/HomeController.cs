﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace SYM.WebApp.Controllers
{
    public class HomeController : Controller
    {
        [OutputCache(Duration = 600, VaryByCustom = "User")]
        public ActionResult Index()
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims.ToList();
            if (claims.FirstOrDefault(x => x.Type == "profileid") != null)
                ViewBag.ProfileId = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);
            else
                ViewBag.ProfileId = 0;

            ViewBag.ImageIntro = GetImageRandom();

            return View();
        }

        [OutputCache(Duration = 600)]
        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult SendMessage(FormCollection collection)
        {
            string subject, email, name, message;

            if (collection.Count > 0)
            {
                subject = String.IsNullOrEmpty(collection["subject"]) ? "" : collection["subject"];
                email = String.IsNullOrEmpty(collection["email"]) ? "" : collection["email"];
                name = String.IsNullOrEmpty(collection["name"]) ? "" : collection["name"];
                message = String.IsNullOrEmpty(collection["message"]) ? "" : collection["message"];

                //TODO: Revision
                //Mailer.SendMail(SYM.SearchEngine.MvcApp.Properties.Settings.Default.email_Info, email, "Keep in Touch - Richiesta Informazioni", message);

            }

            return PartialView("~/Views/Home/Partial/_ContactForm.cshtml");
        }

        public ActionResult ChangeLang(string lang)
        {
            string lang_old = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            string absoluteUrl = "";
            var obj = HttpContext.Request.UrlReferrer;

            if (lang_old == lang)
                return Redirect(obj.AbsoluteUri);

            //Set Cookie
            var langCookie = new HttpCookie("_culture", lang) { HttpOnly = true };
            Response.AppendCookie(langCookie);

            if (obj.AbsoluteUri.Contains("/" + lang_old + "/"))
                absoluteUrl = obj.AbsoluteUri.Replace("/" + lang_old + "/", "/" + lang + "/");
            else
                absoluteUrl = obj.AbsoluteUri.Replace(obj.AbsolutePath, "/" + lang + obj.AbsolutePath);

            return Redirect(absoluteUrl);
        }

        //Create Function JS and remove the logic server side
        private static string GetImageRandom()
        {
            List<string> images = new List<string>();

            images.Add("photo-6-low.jpg");
            images.Add("photo-3-low.jpg");
            images.Add("photo-9-low.jpg");
            images.Add("photo-13-min.jpg");
            images.Add("photo-14-min.jpg");
            images.Add("photo-15-min.jpg");
            images.Add("photo-16-min.jpg");
            images.Add("photo-11-min.jpg");

            Random rnd = new Random();
            int r = rnd.Next(images.Count);

            return (string)images[r];
        }
    }
}