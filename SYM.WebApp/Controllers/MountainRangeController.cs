﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SYM.DataAccessLayer;
using PagedList;

namespace SYM.SearchEngine.MvcApp.Controllers
{
    public class MountainRangeController : Controller
    {
        public Entities _context = new Entities();

        //
        // GET: /MountainGroup/

        //public ActionResult Index()
        //{
        //    var mountainGroup = _context.mntn_mountainrange.ToList();
        //    return View(mountainGroup);
        //}

        public ActionResult Index(FormCollection collection, string sortOrder, int? page)
        {
            IEnumerable<mntn_mountainrange> mountains;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            ViewBag.Range = String.IsNullOrEmpty(collection["range"]) ? "" : collection["range"];
            ViewBag.Country = String.IsNullOrEmpty(collection["country"]) ? null : collection["country"];

            //Init Param
            string range = ViewBag.Range;
           
            if (collection == null || collection.AllKeys.Length == 0)
                mountains = _context.mntn_mountainrange.ToList();
            else
            {
                mountains = _context.mntn_mountainrange.ToList();

                if (!String.IsNullOrEmpty(range))
                    mountains = mountains.Where(a => a.mntnRange_name == null || a.mntnRange_name.ToLower().Contains(range.ToLower()));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    mountains = mountains.OrderByDescending(s => s.mntnRange_name);
                    break;
                default:  // Name ascending 
                    mountains = mountains.OrderBy(s => s.mntnRange_name);
                    break;
            }

            //return View(mountains);
            int pageSize = 15;
            int pageNumber = (page ?? 1);
            return View(mountains.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /MountainRange/Details/5

        public ActionResult Details(int id)
        {
            var mountainRange = _context.mntn_mountainrange.Find(id);
            if (mountainRange == null)
            {
                return HttpNotFound();
            }
            return View(mountainRange); 
        }

        //
        // GET: /MountainRange/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MountainRange/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MountainRange/Edit/5

        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //
        // POST: /MountainRange/Edit/5

        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /MountainRange/Delete/5

        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        ////
        //// POST: /MountainRange/Delete/5

        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
