﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SYM.DataAccessLayer;
using PagedList;

namespace SYM.SearchEngine.MvcApp.Controllers
{
    public class MountainGroupController : Controller
    {
        public Entities _context = new Entities();
        
        //
        // GET: /MountainGroup/

        //public ActionResult Index()
        //{
        //    var mountainGroup = _context.mntn_mountaingroup.ToList();
        //    return View(mountainGroup);
        //}

        public ActionResult Index(FormCollection collection, string sortOrder, int? page)
        {
            IEnumerable<mntn_mountaingroup> mountains;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.Group = String.IsNullOrEmpty(collection["group"]) ? "" : collection["group"];
            ViewBag.Range = String.IsNullOrEmpty(collection["range"]) ? "" : collection["range"];
            ViewBag.Country = String.IsNullOrEmpty(collection["country"]) ? null : collection["country"];

            //Init Param
            string country = ViewBag.Country;
            string group = ViewBag.Group;
            string range = ViewBag.Range;

            if (collection == null || collection.AllKeys.Length == 0)
                mountains = _context.mntn_mountaingroup.ToList();
            else
            {
                mountains = _context.mntn_mountaingroup.ToList();

                if (!String.IsNullOrEmpty(range))
                    mountains = mountains.Where(a => a.mntn_mountainrange != null && a.mntn_mountainrange.mntnRange_name.ToLower().Contains(range.ToLower()));
                if (!String.IsNullOrEmpty(group))
                    mountains = mountains.Where(a => a.mntnGroup_name != null && a.mntnGroup_name.ToLower().Contains(group.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    mountains = mountains.OrderByDescending(s => s.mntnGroup_name);
                    break;
                default:  // Name ascending 
                    mountains = mountains.OrderBy(s => s.mntnGroup_name);
                    break;
            }


            //return View(mountains);
            int pageSize = 15;
            int pageNumber = (page ?? 1);
            return View(mountains.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /MountainGroup/Details/5

        public ActionResult Details(int id)
        {
            var mountainGroup = _context.mntn_mountaingroup.Find(id);
            if (mountainGroup == null)
            {
                return HttpNotFound();
            }
            return View(mountainGroup); 
        }

        //
        // GET: /MountainGroup/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MountainGroup/Create

        [HttpPost]
        public ActionResult Create(mntn_mountaingroup group)
        {
            try
            {
                // TODO: Add insert logic here
                _context.mntn_mountaingroup.Add(group);
                _context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MountainGroup/Edit/5

        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        ////
        //// POST: /MountainGroup/Edit/5

        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /MountainGroup/Delete/5
        //[Authorize]
        //public ActionResult Delete(int id)
        //{
        //    var group = _context.mntn_mountaingroup.Find(id);
        //    _context.mntn_mountaingroup.Remove(group);
        //    _context.SaveChanges();

        //    return RedirectToAction("Index");
        //}

        //
        // POST: /MountainGroup/Delete/5

        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        string tmp = collection["groupId"].ToString();
        //        var group = _context.mntn_mountaingroup.Find(id);
        //        _context.mntn_mountaingroup.Remove(group);
        //        _context.SaveChanges();

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

    }
}
