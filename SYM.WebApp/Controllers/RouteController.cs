﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;
using System.IO;
using SYM.DataAccessLayer;
using SYM.SearchEngine.MvcApp.Helpers;
using SYM.BusinessLayer;
using SYM.BusinessLayer.Services;
using CloudinaryDotNet.Actions;
using System.Web.Configuration;
using PagedList;
using NLog;
using System.Threading.Tasks;

namespace SYM.SearchEngine.MvcApp.Controllers
{
    public class RouteController : Controller
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        //
        // GET: /Route/
        [Authorize]
        [OutputCache(Duration = 600, VaryByParam = "*", VaryByCustom = "User")]
        public ActionResult Index(FormCollection collection, string sortOrder, int? page, string currentFilter, string _route, string _mountain, string _country, string _region_text, string _mntn_text, string _act, string _source)
        {
            try
            {
                IEnumerable<route> routes = null;
                string meta = null;

                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims.ToList();
                string profile_regionID = claims.FirstOrDefault(x => x.Type == "regionid").Value;
                string profile_countryID = claims.FirstOrDefault(x => x.Type == "countryid").Value;
                string profile_region = claims.FirstOrDefault(x => x.Type == ClaimTypes.StateOrProvince).Value;

                #region ViewBagInit
                if (collection.Count > 0)
                {
                    ViewBag.Mountain = String.IsNullOrEmpty(collection["mountain"]) ? null : collection["mountain"];
                    ViewBag.Mntn_text = String.IsNullOrEmpty(collection["mntn_text"]) ? null : collection["mntn_text"];
                    ViewBag.Range = String.IsNullOrEmpty(collection["range"]) ? "" : collection["range"];
                    ViewBag.Route_text = String.IsNullOrEmpty(collection["route_name"]) ? "" : collection["route_name"];
                    ViewBag.Route = String.IsNullOrEmpty(collection["route"]) ? null : collection["route"];
                    ViewBag.Country = String.IsNullOrEmpty(collection["country"]) ? null : collection["country"];
                    ViewBag.Region = String.IsNullOrEmpty(collection["region"]) ? null : collection["region"];
                    ViewBag.Activity = String.IsNullOrEmpty(collection["activity"]) ? "" : collection["activity"];
                    ViewBag.Region_text = String.IsNullOrEmpty(collection["region"]) ? "" : _context.geo_region.Find(Convert.ToInt32((string)ViewBag.Region)).region_name;
                    ViewBag.Sort = String.IsNullOrEmpty(collection["orderby"]) ? null : collection["orderby"];
                    meta = collection["meta"];

                    if (!String.IsNullOrEmpty(meta)) ViewBag.Route_text = meta;
                }
                else if (page >= 2)
                {
                    ViewBag.Region = String.IsNullOrEmpty(currentFilter) ? "0" : currentFilter;
                    ViewBag.Region_text = String.IsNullOrEmpty(_region_text) ? "" : _region_text;
                    ViewBag.Route_text = String.IsNullOrEmpty(_route) ? ViewBag.Route : _route;
                    ViewBag.Mountain = String.IsNullOrEmpty(_mountain) ? ViewBag.Mountain : _mountain;
                    ViewBag.Mntn_text = String.IsNullOrEmpty(_mntn_text) ? ViewBag.Mntn_text : _mntn_text;
                    ViewBag.Country = String.IsNullOrEmpty(_country) ? profile_countryID : _country;
                    ViewBag.Activity = String.IsNullOrEmpty(_act) ? "" : _act;
                    ViewBag.Source = String.IsNullOrEmpty(_source) ? "0" : _source;
                    ViewBag.Sort = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
                }
                else if (Request.QueryString.HasKeys()) //If exists parameters I don't initialize some variables.
                {
                    ViewBag.Region = String.IsNullOrEmpty(currentFilter) ? "0" : currentFilter;
                    ViewBag.Region_text = String.IsNullOrEmpty(_region_text) ? "" : _region_text;
                    ViewBag.Route_text = String.IsNullOrEmpty(_route) ? ViewBag.Route : _route;
                    ViewBag.Mountain = String.IsNullOrEmpty(_mountain) ? ViewBag.Mountain : _mountain;
                    ViewBag.Mntn_text = String.IsNullOrEmpty(_mntn_text) ? ViewBag.Mntn_text : _mntn_text;
                    ViewBag.Country = String.IsNullOrEmpty(_country) ? "0" : _country;
                    ViewBag.Activity = String.IsNullOrEmpty(_act) ? "" : _act;
                    ViewBag.Source = String.IsNullOrEmpty(_source) ? "0" : _source;
                    ViewBag.Sort = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
                }
                else
                {
                    ViewBag.Region = String.IsNullOrEmpty(currentFilter) ? profile_regionID : currentFilter;
                    ViewBag.Region_text = String.IsNullOrEmpty(_region_text) ? profile_region : _region_text;
                    ViewBag.Route_text = String.IsNullOrEmpty(_route) ? ViewBag.Route : _route;
                    ViewBag.Mountain = String.IsNullOrEmpty(_mountain) ? ViewBag.Mountain : _mountain;
                    ViewBag.Mntn_text = String.IsNullOrEmpty(_mntn_text) ? ViewBag.Mntn_text : _mntn_text;
                    ViewBag.Country = String.IsNullOrEmpty(_country) ? profile_countryID : _country;
                    ViewBag.Activity = String.IsNullOrEmpty(_act) ? "" : _act;
                    ViewBag.Source = String.IsNullOrEmpty(_source) ? "0" : _source;
                    ViewBag.Sort = String.IsNullOrEmpty(sortOrder) ? "" : sortOrder;
                }
                #endregion

                //Init Param
                string route = ViewBag.Route_text;
                int region = Convert.ToInt32((string)ViewBag.Region);
                int mountain = Convert.ToInt32((string)ViewBag.Mountain);
                int country = Convert.ToInt32((string)ViewBag.Country);
                int routeid = Convert.ToInt32((string)ViewBag.Route);
                int sourceid = Convert.ToInt32((string)ViewBag.Source);
                int valley = Convert.ToInt32((string)ViewBag.Valley);
                string tmp = ViewBag.Activity;
                List<int> activityArray = String.IsNullOrEmpty(tmp) ? null : tmp.Split(',').Select(Int32.Parse).ToList();

                //Init DropDown
                InitDropDownActivities(activityArray);

                if (routeid > 0)
                    return RedirectToAction("Details/" + routeid.ToString());

                //BL to extract data from database
                routes = GetRoutesFromDAL(region, country, mountain, valley, activityArray, route, meta, sourceid, (string)ViewBag.Sort);
                ViewBag.Count = routes.Count();

                int pageSize = 20;
                int pageNumber = (page ?? 1);
                return View(routes.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return View();
            }
        }

        //
        // GET: /Route/
        [OutputCache(Duration = 600, VaryByParam = "*", VaryByCustom = "User")]
        public ActionResult Index3(FormCollection collection, string sortOrder, int? page, string _reg, string _r, string _m, string _c, string _rtxt, string _mtxt, string _v, string _act)
        {
            IEnumerable<v_route_index> routes = null;
            string profile_regionID, profile_countryID, profile_region;

            //TODO: create Static Helper & Singleton 
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                IEnumerable<Claim> claims = identity.Claims.ToList();
                profile_regionID = claims.FirstOrDefault(x => x.Type == "regionid").Value;
                profile_countryID = claims.FirstOrDefault(x => x.Type == "countryid").Value;
                profile_region = claims.FirstOrDefault(x => x.Type == ClaimTypes.StateOrProvince).Value;
            }
            else {
                profile_regionID = "1736";
                profile_countryID = "116";
                profile_region = "Lombardia";
            }

            #region ViewBagInit
            if (collection.Count > 0)
            {
                ViewBag.Mountain = String.IsNullOrEmpty(collection["mountain"]) ? null : collection["mountain"];
                ViewBag.Valley = String.IsNullOrEmpty(collection["valley"]) ? null : collection["valley"];
                ViewBag.Mntn_text = String.IsNullOrEmpty(collection["mntn_text"]) ? null : collection["mntn_text"];
                ViewBag.Meta = String.IsNullOrEmpty(collection["meta"]) ? null : collection["meta"];
                ViewBag.Range = String.IsNullOrEmpty(collection["range"]) ? "" : collection["range"];
                ViewBag.Route_text = String.IsNullOrEmpty(collection["route_name"]) ? "" : collection["route_name"];
                ViewBag.Route = String.IsNullOrEmpty(collection["route"]) ? null : collection["route"];
                ViewBag.Country = String.IsNullOrEmpty(collection["country"]) ? null : collection["country"];
                ViewBag.Region = String.IsNullOrEmpty(collection["region"]) ? null : collection["region"];
                ViewBag.Region_text = String.IsNullOrEmpty(collection["region"]) ? "" : _context.geo_region.Find(Convert.ToInt32((string)ViewBag.Region)).region_name;
                ViewBag.orderbySelected = String.IsNullOrEmpty(collection["orderby"]) ? null : collection["orderby"];
                ViewBag.Activity = String.IsNullOrEmpty(collection["activity"]) ? "" : collection["activity"];
                ViewBag.MetaDisable = String.IsNullOrEmpty(collection["metaDisable"]) ? "" : collection["metaDisable"];
            }
            else if (page >= 2)
            {
                ViewBag.Route_text = String.IsNullOrEmpty(_r) ? ViewBag.Route : _r;
                ViewBag.Mountain = String.IsNullOrEmpty(_m) ? ViewBag.Mountain : _m;
                ViewBag.Valley = String.IsNullOrEmpty(_v) ? ViewBag.Valley : _v;
                ViewBag.Mntn_text = String.IsNullOrEmpty(_mtxt) ? ViewBag.Mntn_text : _mtxt;
                ViewBag.Region = String.IsNullOrEmpty(_reg) ? "0" : _reg;
                ViewBag.Region_text = String.IsNullOrEmpty(_rtxt) ? "" : _rtxt;
                ViewBag.Country = String.IsNullOrEmpty(_c) ? profile_countryID : _c;
                ViewBag.Activity = String.IsNullOrEmpty(_act) ? "" : _act;
                ViewBag.orderbySelected = String.IsNullOrEmpty(sortOrder) ? null : sortOrder;
            }
            else if (Request.QueryString.HasKeys()) //If exists parameters I don't initialize some variables.
            {
                ViewBag.Region = String.IsNullOrEmpty(_reg) ? "0" : _reg;
                ViewBag.Region_text = String.IsNullOrEmpty(_rtxt) ? "" : _rtxt;
                ViewBag.Route_text = String.IsNullOrEmpty(_r) ? ViewBag.Route : _r;
                ViewBag.Mountain = String.IsNullOrEmpty(_m) ? ViewBag.Mountain : _m;
                ViewBag.Mntn_text = String.IsNullOrEmpty(_mtxt) ? ViewBag.Mntn_text : _mtxt;
                ViewBag.Country = String.IsNullOrEmpty(_c) ? "0" : _c;
                ViewBag.Activity = String.IsNullOrEmpty(_act) ? "" : _act;
                ViewBag.orderbySelected = String.IsNullOrEmpty(sortOrder) ? null : sortOrder;
            }
            else
            {
                ViewBag.Region = String.IsNullOrEmpty(_reg) ? profile_regionID : _reg;
                ViewBag.Region_text = String.IsNullOrEmpty(_rtxt) ? profile_region : _rtxt;
                ViewBag.Route_text = String.IsNullOrEmpty(_r) ? ViewBag.Route : _r;
                ViewBag.Mountain = String.IsNullOrEmpty(_m) ? ViewBag.Mountain : _m;
                ViewBag.Mntn_text = String.IsNullOrEmpty(_mtxt) ? ViewBag.Mntn_text : _mtxt;
                ViewBag.Country = String.IsNullOrEmpty(_c) ? profile_countryID : _c;
                ViewBag.Activity = String.IsNullOrEmpty(_act) ? "" : _act;
                ViewBag.Valley = String.IsNullOrEmpty(_v) ? ViewBag.Valley : _v;
                ViewBag.orderbySelected = String.IsNullOrEmpty(sortOrder) ? null : sortOrder;
            }
            #endregion

            //Init Param
            string route = ViewBag.Route_text;
            int routeid = Convert.ToInt32((string)ViewBag.Route);
            int region = Convert.ToInt32((string)ViewBag.Region);
            int mountain = Convert.ToInt32((string)ViewBag.Mountain);
            int country = Convert.ToInt32((string)ViewBag.Country);
            int valley = Convert.ToInt32((string)ViewBag.Valley);

            string tmp = ViewBag.Activity;
            List<int> activityArray = String.IsNullOrEmpty(tmp) ? null : tmp.Split(',').Select(Int32.Parse).ToList();

            //Init DropDown
            InitDropDownActivities(activityArray);
            InitDropDownSort((string)ViewBag.orderbySelected);

            if (routeid > 0)
                return RedirectToAction("Details/" + routeid.ToString());

            string txt;
            if (!String.IsNullOrEmpty((string)ViewBag.Meta) && ViewBag.MetaDisable != "true")
                txt = (string)ViewBag.Meta;
            else
                txt = route;

            //BL to extract data from database
            RouteService obj = new RouteService();
            routes = obj.GetRoutesSummary(txt, region, country, mountain, valley, activityArray, 0, (string)ViewBag.orderbySelected);
            ViewBag.Count = routes.Count();

            int pageSize = 12;
            int pageNumber = (page ?? 1);
            return View(routes.ToPagedList(pageNumber, pageSize));
        }

        private IEnumerable<route> GetRoutesFromDAL(int region, int country, int mountain, int valley, List<int> activityArray, string route_txt, string meta, int sourceid, string sort)
        {
            IEnumerable<route> routes = null;
            string txt;

            try
            {
                //First check Region Or Country and the mountain
                if (region > 0)
                    routes = _context.route.Where(a => a.region_id == region).ToList();
                else if (country > 0)
                    routes = _context.route.Where(a => a.country_id == country).ToList();
                else if (mountain > 0)
                    routes = _context.route.Where(a => a.mountain_id == mountain).ToList();  //If I want all results about routes of a mountain I MUST to have this possibility
                else if (valley > 0)
                    routes = _context.route.Where(a => a.valley_id == valley).ToList();

                if (mountain > 0)
                    routes = routes.Where(a => a.mountain_id == mountain);

                if (activityArray != null && activityArray.Count() > 0)
                    routes = routes.Where(a => activityArray.Contains(Convert.ToInt32(a.activity_id)));

                if (!String.IsNullOrEmpty(meta))
                    txt = meta;
                else
                    txt = route_txt;

                if (!String.IsNullOrEmpty(txt) && mountain == 0)
                    routes = routes.Where(a => a.route_title.ToLower().Contains(txt.ToLower().Trim()) || a.route_name.ToLower().Contains(txt.ToLower().Trim()));

                //TODO: IMprove this check considering all records associated
                if (sourceid > 0)
                    routes = _context.route.Where(a => a.country_id == country && a.route_webinfo.FirstOrDefault().source_id == sourceid).ToList();

                switch (sort)
                {
                    case "1":
                        routes = routes.Where(c => c.route_status == true).OrderByDescending(s => s.route_ranking);
                        break;
                    case "2":
                        routes = routes.Where(c => c.route_status == true).OrderByDescending(s => s.route_altitude);
                        break;
                    case "3":
                        routes = routes.Where(c => c.route_status == true).OrderByDescending(s => s.route_dateIns);
                        break;
                    default:  // Name ascending 
                        routes = routes.Where(c => c.route_status == true).OrderByDescending(s => s.route_dateIns);
                        break;
                }

                return routes;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }

        }

        [OutputCache(Duration = 600, VaryByParam = "id")]
        public PartialViewResult _PartialIndex(int id)
        {
            IEnumerable<route> routes = _context.route.Where(c => c.mountain_id == id && c.route_status == true).ToList();

            ViewBag.Mountain = id;

            int count = routes.Count();

            if (count == 0)
                return PartialView("_PartialNoRoute");
            else
                return PartialView(routes.ToPagedList(1, count));
        }

        //
        // GET: /Route/Details/5
        [OutputCache(Duration = 2400, VaryByParam = "id")]
        public ActionResult Details(int id)
        {
            var route = _context.route.Find(id);
            user_profile user = null;
            if (route == null)
            {
                return this.HttpNotFound();
            }

            if (route.user_id != null)
            {
                user = _context.user_profile.Where(a => a.profile_id == route.user_id).FirstOrDefault();
                ViewBag.UserName = user.aspnetusers.UserName;
                ViewBag.UserId = user.UserId;
            }

            //TODO EXTERNALIZE THE FUNCTION ESTIMANTE....
            if ((route.route_length > 0) && route.route_height != null)
                ViewBag.Hours = EstimateTime(route.route_length.GetValueOrDefault(), Convert.ToInt32(route.route_height), route.activity_id, route.route_pitchAsc, 3.75);

            var page = _context.page_statistics.Where(a => a.page_type == 2 && a.page_id == id).FirstOrDefault();
            ViewBag.CountViews = page != null ? page.page_countview : 0;
            ViewBag.LastViews = page != null ? @String.Format("{0:dd/MM/yyyy hh:mm}", page.page_dateMod) : "";

            return View(route);
        }

        [Authorize]
        public ActionResult Create(int? id)
        {
            mountain _mountain = new mountain();
            route _route = new route();
            geo_region _region = new geo_region();

            try
            {
                if (id != null)
                {
                    _mountain = _context.mountain.Find(id);
                    _route.mountain_id = _mountain.id;
                    _route.mountain = _mountain;
                    _route.route_altitude = _mountain.mntn_altitude;
                    _route.mntnRange_id = _mountain.mntnRange_id;
                    _route.mntnGroup_id = _mountain.mntnGroup_id;
                    _route.country_id = _mountain.ass_mountaincountry.First().country_id;
                    _route.region_id = _mountain.ass_mountainregion.First().region_id;
                    //Find Region
                    _route.geo_region = _mountain.ass_mountainregion.First().geo_region;
                }
                else
                    _route = null;

                InitDropDownList(_route);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return View(_route);
        }

        //
        // POST: /Route/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                //Get User Id from session
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims.ToList();
                int profileId = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);

                route _route = new route();
                route_webinfo _route_webinfo = new route_webinfo();

                string mountain_id = String.IsNullOrEmpty(collection["routeMntn"]) ? "" : collection["routeMntn"];
                string country_id = String.IsNullOrEmpty(collection["routeCountry"]) ? "" : collection["routeCountry"];
                string route_title = String.IsNullOrEmpty(collection["route_title"]) ? "" : collection["route_title"];
                string route_name = String.IsNullOrEmpty(collection["route_name"]) ? "" : collection["route_name"];
                string valley_id = String.IsNullOrEmpty(collection["routeValley"]) ? "" : collection["routeValley"];
                string city_id = String.IsNullOrEmpty(collection["routeCity"]) ? "" : collection["routeCity"];
                string region_id = String.IsNullOrEmpty(collection["routeRegion"]) ? "" : collection["routeRegion"];
                string activity = String.IsNullOrEmpty(collection["routeActivity"]) ? "" : collection["routeActivity"];
                string mountainranges_id = String.IsNullOrEmpty(collection["routeMntnRange"]) ? "" : collection["routeMntnRange"];
                string mountaingroups_id = String.IsNullOrEmpty(collection["routeMntnGroup"]) ? "" : collection["routeMntnGroup"];
                string length = String.IsNullOrEmpty(collection["route_length"]) ? "" : collection["route_length"];
                string height = String.IsNullOrEmpty(collection["route_height"]) ? "" : collection["route_height"];
                string altitude = String.IsNullOrEmpty(collection["route_altitude"]) ? "" : collection["route_altitude"];
                string route_facing = String.IsNullOrEmpty(collection["route_facing"]) ? "" : collection["route_facing"];
                string difficulty = String.IsNullOrEmpty(collection["difficulty_grade"]) ? "" : collection["difficulty_grade"];
                string wi_url = String.IsNullOrEmpty(collection["wi_url"]) ? "" : collection["wi_url"];
                string route_firstAscDate = String.IsNullOrEmpty(collection["route_firstAscDate"]) ? "" : collection["route_firstAscDate"];
                string route_firstAscClimbers = String.IsNullOrEmpty(collection["route_firstAscClimbers"]) ? "" : collection["route_firstAscClimbers"];
                string route_pitchAsc = String.IsNullOrEmpty(collection["route_pitchAsc"]) ? "" : collection["route_pitchAsc"];
                //New Paramters
                string city_txt = String.IsNullOrEmpty(collection["routeCity"]) ? "" : collection["routeCity"];
                string city_name = String.IsNullOrEmpty(collection["startCity"]) ? "" : collection["startCity"];
                string city_region = String.IsNullOrEmpty(collection["startRegion"]) ? "" : collection["startRegion"];
                string city_country = String.IsNullOrEmpty(collection["startCountry"]) ? "" : collection["startCountry"];
                string city_lat = String.IsNullOrEmpty(collection["startlat"]) ? "" : collection["startlat"];
                string city_lng = String.IsNullOrEmpty(collection["startlng"]) ? "" : collection["startlng"];

                //Check client side if the field is required
                _route.route_title = route_title;
                _route.route_name = route_name;
                _route.route_altitude = 0;

                if (!String.IsNullOrEmpty(mountain_id))
                {
                    _route.mountain_id = Convert.ToInt32(mountain_id);
                    var mntn = _context.mountain.Find(_route.mountain_id);
                    _route.mntnRange_id = mntn.mntnRange_id;
                    _route.mntnGroup_id = mntn.mntnGroup_id;
                    _route.route_altitude = mntn.mntn_altitude;
                }
                else
                {
                    if (!String.IsNullOrEmpty(mountainranges_id)) _route.mntnRange_id = Convert.ToInt32(mountainranges_id);
                    if (!String.IsNullOrEmpty(mountaingroups_id)) _route.mntnGroup_id = Convert.ToInt32(mountaingroups_id);
                }

                if (!String.IsNullOrEmpty(region_id))
                {
                    _route.region_id = Convert.ToInt32(region_id);
                    _route.country_id = _context.geo_region.Find(_route.region_id).country_id;
                }
                else if (String.IsNullOrEmpty(country_id))
                {
                    _route.country_id = Convert.ToInt32(country_id);
                }

                if (!String.IsNullOrEmpty(altitude))
                    _route.route_altitude = Convert.ToInt16(altitude);

                if (!String.IsNullOrEmpty(length))
                    _route.route_length = Convert.ToInt32(length);
                else
                    _route.route_length = 0;

                if (!String.IsNullOrEmpty(height))
                    _route.route_height = Convert.ToInt16(height);

                if (!String.IsNullOrEmpty(difficulty))
                    _route.difficulty_grade = difficulty.ToUpper();
                else
                    _route.difficulty_grade = "";

                if (!String.IsNullOrEmpty(activity))
                    _route.activity_id = Convert.ToInt32(activity);
                else
                    _route.activity_id = 0;

                if (!String.IsNullOrEmpty(route_facing))
                    _route.route_facing = route_facing;

                if (!String.IsNullOrEmpty(route_pitchAsc))
                    _route.route_pitchAsc = Convert.ToInt16(route_pitchAsc);

                if (!String.IsNullOrEmpty(route_firstAscClimbers))
                    _route.route_firstAscClimbers = route_firstAscClimbers;

                if (!String.IsNullOrEmpty(valley_id))
                    _route.valley_id = Convert.ToInt32(valley_id);

                //SMY 2.0  - new feature
                var geoService = new GeographicService();
                var region = geoService.GetRegionByName(city_region, city_country);
                geo_city city = null;
                if (region != null)
                    city = geoService.GetCity(city_name, region, float.Parse(city_lat.Replace(".", ",")), float.Parse(city_lng.Replace(".", ",")));

                _route.city_id = city.id;

                _route.route_status = true;
                _route.user_id = profileId;

                //NEW ValidationRoute
                if (ValidationRoute(_route))
                {
                    _context.route.Add(_route);
                    _context.SaveChanges();

                    //Add Route Web Info
                    AddWebInfo(wi_url, _route.id);

                    //Last Step - Update Score Route
                    RouteService RS = new RouteService();
                    RS.SetRanking(_route.id);

                    return RedirectToAction(_route.id + "-new");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Route/_PartialEdit/5
        [Authorize]
        public PartialViewResult _UploadImages(int? id)
        {
            try
            {
                var route = _context.route.Find(id);
                if (route == null)
                    return null;

                if (route.mountain != null)
                    ViewBag.Title = route.mountain.mntn_name + " - " + route.route_name;
                else if (route.valley != null)
                    ViewBag.Title = route.valley.valley_name + " - " + route.route_name;
                else
                    ViewBag.Title = route.route_name;

                return PartialView("~/Views/Route/Partial/_UploadImages.cshtml", route);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return null;
            }
        }

        //
        // GET: /Route/_PartialEdit/5
        [Authorize]
        public PartialViewResult _UploadImage()
        {
            return PartialView("~/Views/Route/Partial/_UploadImage.cshtml");
        }

        public ActionResult SaveUploadedFile(int id)
        {
            DateTime startTime = DateTime.Now;
            bool isSavedSuccessfully = true;
            string imageName = "";
            string valley = "",
                   mountain = "",
                   activity = "",
                   name = "";

            var route = _context.route.Find(id);
            if (route == null || route.route_status == false)
                return Json(new { ErrorCode = 1000, Message = "Route NOT Exists" });

            //TODO: create Static Helper & Singleton 
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims.ToList();
            int profileID = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);

            //Configure TAGS
            if (route.valley != null) valley = route.valley.valley_name;
            if (route.mountain != null) mountain = route.mountain.mntn_name;
            activity = route.act_activity.activity_name;
            name = route.route_name;
            string tags = activity + "," + mountain + "," + name + "," + valley;

            CloudinaryIntegration cloudinary = new CloudinaryIntegration(
                                                    WebConfigurationManager.AppSettings["cloud_string"],
                                                    WebConfigurationManager.AppSettings["cloud_key"],
                                                    WebConfigurationManager.AppSettings["cloud_secret"]);

            try
            {
                foreach (string req in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[req];

                    imageName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {

                        var directory = new DirectoryInfo(string.Format("{0}Images\\routes_upload", Server.MapPath(@"\")));
                        var path = Path.Combine(directory.ToString(), imageName);
                        file.SaveAs(path);

                        route_image ri = new route_image();
                        ri.route_id = route.id;
                        ri.image_filename = imageName;
                        ri.user_id = profileID;
                        ri.image_cdn = true;
                        _context.route_image.Add(ri);
                        _context.SaveChanges();

                        logger.Debug("Befoore to upload image on Cloudinary -> sec. " + DateTime.Now.Subtract(startTime).TotalSeconds.ToString());
                        Task.Run(() => UploadImageOnCloudinaryAsync(startTime, route, tags, cloudinary, path, ri));
                    }

                }

            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
                logger.Error(ex.Message);
            }

            if (isSavedSuccessfully) //Update climbing log
            {
                //This is important because there is the assertion that the user has done the picture
                AdventureService AS = new AdventureService();
                AS.Set(profileID, route.mountain_id.GetValueOrDefault(), route.id, 0, true);
                return Json(new { ErrorCode = 0, Message = imageName });
            }
            else
                return Json(new { ErrorCode = 1000, Message = "Error in saving file" + imageName });
        }

        private void UploadImageOnCloudinaryAsync(DateTime startTime, route route, string tags, CloudinaryIntegration cloudinary, string path, route_image ri)
        {
            try
            {
                ImageUploadResult result = cloudinary.Upload(@"routes/" + route.id.ToString(), tags.Replace(" ", ""), path);
                DateTime endTime = DateTime.Now;
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    ri = _context.route_image.Find(ri.id);
                    ri.image_url = result.PublicId + ".jpg";
                    _context.SaveChanges();
                    endTime = DateTime.Now;
                    logger.Debug("Upload image Total -> sec. " + endTime.Subtract(startTime).TotalSeconds.ToString());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        //
        // GET: /Route/Edit/5
        [Authorize]
        public ActionResult Edit(int id)
        {
            try
            {
                var route = _context.route.Find(id);
                if (route == null)
                {
                    return this.HttpNotFound();
                }

                InitDropDownList(route);

                return View(route);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return View();
            }
        }

        //
        // POST: /Route/Edit/5
        // TODO: Aggiornamento Region
        [HttpPost]
        [Authorize]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var route = _context.route.Find(id);
                if (route == null)
                {
                    logger.Error("No route Found");
                    return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
                }

                string mountain_id = String.IsNullOrEmpty(collection["routeMntn"]) ? "" : collection["routeMntn"];
                string route_title = String.IsNullOrEmpty(collection["route_title"]) ? "" : collection["route_title"];
                string route_name = String.IsNullOrEmpty(collection["route_name"]) ? "" : collection["route_name"];
                string valley_id = String.IsNullOrEmpty(collection["routeValley"]) ? "" : collection["routeValley"];
                string city_id = String.IsNullOrEmpty(collection["routeCity"]) ? "" : collection["routeCity"];
                string region_id = String.IsNullOrEmpty(collection["routeRegion"]) ? "" : collection["routeRegion"];
                string length = String.IsNullOrEmpty(collection["route_length"]) ? "" : collection["route_length"];
                string height = String.IsNullOrEmpty(collection["route_height"]) ? "" : collection["route_height"];
                string altitude = String.IsNullOrEmpty(collection["route_altitude"]) ? "" : collection["route_altitude"];
                string activity = String.IsNullOrEmpty(collection["routeActivity"]) ? "" : collection["routeActivity"];
                string mountainranges_id = String.IsNullOrEmpty(collection["routeMntnRange"]) ? "" : collection["routeMntnRange"];
                string mountaingroups_id = String.IsNullOrEmpty(collection["routeMntnGroup"]) ? "" : collection["routeMntnGroup"];
                string difficulty = String.IsNullOrEmpty(collection["difficulty_grade"]) ? "" : collection["difficulty_grade"];
                string route_firstAscDate = String.IsNullOrEmpty(collection["route_firstAscDate"]) ? "" : collection["route_firstAscDate"];
                string route_firstAscClimbers = String.IsNullOrEmpty(collection["route_firstAscClimbers"]) ? "" : collection["route_firstAscClimbers"];
                string route_pitchAsc = String.IsNullOrEmpty(collection["route_pitchAsc"]) ? "" : collection["route_pitchAsc"];
                string wi_url = String.IsNullOrEmpty(collection["wi_url"]) ? "" : collection["wi_url"];
                //New Paramters
                string city_txt = String.IsNullOrEmpty(collection["routeCity"]) ? "" : collection["routeCity"];
                string city_name = String.IsNullOrEmpty(collection["startCity"]) ? "" : collection["startCity"];
                string city_region = String.IsNullOrEmpty(collection["startRegion"]) ? "" : collection["startRegion"];
                string city_country = String.IsNullOrEmpty(collection["startCountry"]) ? "" : collection["startCountry"];
                string city_lat = String.IsNullOrEmpty(collection["startlat"]) ? "" : collection["startlat"];
                string city_lng = String.IsNullOrEmpty(collection["startlng"]) ? "" : collection["startlng"];


                route.route_title = route_title;
                route.route_name = route_name;
                route.route_dateMod = DateTime.Now;

                if (!String.IsNullOrEmpty(valley_id))
                {
                    route.valley_id = Convert.ToInt32(valley_id);
                }
                else
                    route.valley_id = null;

                if (!String.IsNullOrEmpty(mountain_id))
                {
                    //If exist the mountain i can set also the Mountain Range & Group
                    route.mountain_id = Convert.ToInt32(mountain_id);
                    var mntn = _context.mountain.Find(route.mountain_id);
                    route.mntnRange_id = mntn.mntnRange_id;
                    route.mntnGroup_id = mntn.mntnGroup_id;
                }
                else
                {

                    if (!String.IsNullOrEmpty(mountainranges_id)) route.mntnRange_id = Convert.ToInt32(mountainranges_id);
                    if (!String.IsNullOrEmpty(mountaingroups_id)) route.mntnGroup_id = Convert.ToInt32(mountaingroups_id);

                }

                if (!String.IsNullOrEmpty(region_id))
                    route.region_id = Convert.ToInt32(region_id);
                else
                    route.region_id = 0;

                if (!String.IsNullOrEmpty(altitude))
                    route.route_altitude = Convert.ToInt16(altitude);
                else
                    route.route_altitude = 0;

                if (!String.IsNullOrEmpty(length))
                    route.route_length = Convert.ToInt32(length);
                else
                    route.route_length = 0;

                if (!String.IsNullOrEmpty(height))
                    route.route_height = Convert.ToInt16(height);

                if (!String.IsNullOrEmpty(difficulty))
                    route.difficulty_grade = difficulty;
                else
                    route.difficulty_grade = "";

                if (!String.IsNullOrEmpty(activity))
                    route.activity_id = Convert.ToInt32(activity);
                else
                    route.activity_id = 0;

                //SMY 2.0  - new feature
                var geoService = new GeographicService();
                var region = geoService.GetRegionByName(city_region, city_country);
                geo_city city = null;
                if (region != null)
                    city = geoService.GetCity(city_name, region, float.Parse(city_lat.Replace(".", ",")), float.Parse(city_lng.Replace(".", ",")));

                route.city_id = city.id;

                if (!String.IsNullOrEmpty(route_pitchAsc))
                    route.route_pitchAsc = Convert.ToInt16(route_pitchAsc);

                if (!String.IsNullOrEmpty(route_firstAscClimbers))
                    route.route_firstAscClimbers = route_firstAscClimbers;

                if (route.route_webinfo.Count == 0 && wi_url != "")
                    AddWebInfo(wi_url, id);

                _context.SaveChanges();

                string t = Request.UrlReferrer.ToString();

                //Update Score Route
                RouteService RS = new RouteService();
                RS.SetRanking(id);

                return RedirectToAction(route.id + "-" + route.route_name.Replace(" ", "").Replace("-", "_").Replace(".", ""));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddUrl(FormCollection collection)
        {

            if (collection.Count > 0 && !String.IsNullOrEmpty(collection["wi_url"]))
            {
                string url = String.IsNullOrEmpty(collection["wi_url"]) ? null : collection["wi_url"];
                string id = String.IsNullOrEmpty(collection["route"]) ? null : collection["route"];
                AddWebInfo(url, Convert.ToInt32(id));
            }

            return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);

        }

        [OutputCache(Duration = 60, VaryByParam = "id")]
        public ActionResult _WidgetSocial(int id)
        {
            ViewBag.id = id;
            return PartialView("~/Views/Route/Partial/_WidgetSocial.cshtml");
        }

        private double EstimateTime(int distance, int height, int? activity, int? pitches, double km4hour)
        {

            double serialization = ((distance / 1000) + (height / 100));
            double hours = serialization / km4hour;

            if (activity == 17 && pitches != null)
                hours = hours + (0.33 * pitches.GetValueOrDefault());

            return Math.Round(hours, 2);
        }

        private bool AddWebInfo(string _url, int routeId)
        {

            route_webinfo _route_webinfo = new route_webinfo();

            //TODO: create Static Helper & Singleton 
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims.ToList();
            int profileID = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);

            //Add Route Web Info
            try
            {
                int sourceId = 0;
                Uri convertedUri = new Uri(_url);
                SourceController s = new SourceController();
                sourceId = s.FindByDomainUrl(convertedUri.Host);
                if (sourceId == 0)
                {
                    sourceId = s.Create(convertedUri.Host, "http://" + convertedUri.Host);
                }
                _route_webinfo.route_id = routeId;
                _route_webinfo.source_id = sourceId;
                _route_webinfo.wi_url = convertedUri.PathAndQuery;
                _route_webinfo.language_code = "it";
                _route_webinfo.user_id = profileID;
                _context.route_webinfo.Add(_route_webinfo);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return false;
            }

            return true;
        }

        private bool ValidationRoute(route routeTmp)
        {

            bool valide = true;

            if (String.IsNullOrEmpty(routeTmp.route_name))
                valide = false;
            else if (routeTmp.city_id == null)
                valide = false;

            return valide;
        }

        #region InitDrop

        //Externaliyyare in ua classe l'oggetto itemR con i valori di default in casa is empty
        private void InitDropDownList(route itemR)
        {
            try
            {
                int? defaultRange = 1;
                int defaultR = 1736; //Lombardia
                int? defaultA = 0;
                int? defaultM = 1;
                int? defaultG = 0;
                int? defaultV = 0;
                int? defaultC = 1;
                int defaultCountry = 116; //IT
                //string defaultRegionCode = "";

                if (itemR != null)
                {
                    defaultRange = itemR.mntnRange_id;
                    defaultR = itemR.region_id;
                    defaultA = itemR.activity_id;
                    defaultG = itemR.mntnGroup_id;
                    defaultM = itemR.mountain_id;
                    defaultV = itemR.valley_id;
                    defaultC = itemR.city_id;
                    defaultCountry = itemR.country_id;
                    //defaultRegionCode = itemR.geo_region.region_code;
                }

                //Mountain Range x Nazione?
                //IEnumerable<mntn_mountainrange> mountainrange = _context.mntn_mountainrange.ToList();
                //IEnumerable<SelectListItem> mountainranges = from value in _context.mntn_mountainrange.OrderBy(bs => bs.id).ToList()
                //                                             select new SelectListItem
                //                                             {
                //                                                 Text = value.mntnRange_name,
                //                                                 Value = value.id.ToString(),
                //                                                 Selected = (value.id == defaultRange)
                //                                             };

                //ViewBag.routeMntnRange = mountainranges;

                //Mountain Group x Nazione?
                //IEnumerable<mntn_mountaingroup> mountaingroup = _context.mntn_mountaingroup.ToList();
                //IEnumerable<SelectListItem> mountaingroups = from value in _context.mntn_mountaingroup.OrderBy(bs => bs.id).ToList()
                //                                             .OrderBy(bs => bs.mntnGroup_name).ToList()
                //                                             select new SelectListItem
                //                                             {
                //                                                 Text = value.mntnGroup_name,
                //                                                 Value = value.id.ToString(),
                //                                                 Selected = (value.id == defaultG)
                //                                             };
                //ViewBag.routeMntnGroup = mountaingroups;

                //Mountain Region
                IEnumerable<SelectListItem> regions = from value in _context.geo_region.Where(bs => bs.country_id == defaultCountry).OrderBy(bs => bs.id).ToList()
                                                      select new SelectListItem
                                                      {
                                                          Text = value.region_name,
                                                          Value = value.id.ToString(),
                                                          Selected = (value.id == defaultR)
                                                      };
                ViewBag.routeRegion = regions;

                //Il campo dovrebbe filtrare nel seguente ordine Nazione/Regione o Catena/Gruppo
                //IEnumerable<mountain> mountain = .ToList();
                IEnumerable<SelectListItem> mountains = from value in _context.mountain.Where(bs => bs.ass_mountainregion.Select(i => i.region_id).Contains(defaultR))
                                                            .OrderBy(bs => bs.mntn_name).ToList()
                                                        select new SelectListItem
                                                        {
                                                            Text = value.mntn_name,
                                                            Value = value.id.ToString(),
                                                            Selected = (value.id == defaultM)
                                                        };
                if (mountains.Count() == 0)
                {
                    mountains = from value in _context.mountain.Where(bs => bs.ass_mountainregion.Select(i => i.country_id).Contains(defaultCountry))
                                .OrderBy(bs => bs.mntn_name).ToList()
                                select new SelectListItem
                                {
                                    Text = value.mntn_name,
                                    Value = value.id.ToString(),
                                    Selected = (value.id == defaultM)
                                };
                }

                ViewBag.routeMntn = mountains;

                IEnumerable<act_activity> activity = _context.act_activity.Where(a => a.activity_status == true).ToList();
                IEnumerable<SelectListItem> activities = from value in activity.OrderBy(bs => bs.id).ToList()
                                                         select new SelectListItem
                                                         {
                                                             Text = value.activity_name,
                                                             Value = value.id.ToString(),
                                                             Selected = (value.id == defaultA)
                                                         };
                ViewBag.routeActivity = activities;

                IEnumerable<SelectListItem> valleyList = from value in _context.valley.Where(bs => bs.country_id == defaultCountry).OrderBy(bs => bs.id).ToList()
                                                         select new SelectListItem
                                                         {
                                                             Text = value.valley_name,
                                                             Value = value.id.ToString(),
                                                             Selected = (value.id == defaultV)
                                                         };
                ViewBag.routeValley = valleyList;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        private void InitDropDownCountry(int countryid)
        {
            try
            {
                IEnumerable<SelectListItem> countries = new List<SelectListItem>()
                {
                    new SelectListItem() {Text="IT", Value="116", Selected = (116 == countryid)},
                    new SelectListItem() { Text="CH", Value="217", Selected = (217 == countryid)}
                    //new SelectListItem() { Text="FR", Value="65", Selected = (65 == countryid)}
                };

                ViewBag.country = countries;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        private void InitDropDownActivities(List<int> activityArray)
        {
            try
            {

                IEnumerable<act_activity> activity = _context.act_activity.Where(a => a.activity_status == true).ToList();
                IEnumerable<SelectListItemCustom> activities = from value in activity.OrderBy(bs => bs.activity_order).ToList()
                                                               select new SelectListItemCustom
                                                               {
                                                                   Text = value.activity_name,
                                                                   Value = value.id.ToString(),
                                                                   SubText = value.activity_description,
                                                                   Selected = activityArray == null ? false : (activityArray.Contains(value.id))
                                                               };
                ViewBag.activityDD = activities;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        private void InitDropDownSort(string sortid)
        {
            try
            {

                IEnumerable<SelectListItem> sort = new List<SelectListItem>()
                {
                    new SelectListItem() {Text="", Value="", Selected = ("" == sortid)},
                    new SelectListItem() {Text="per Punteggio", Value="1", Selected = ("1" == sortid)},
                    new SelectListItem() { Text="per Altitudine", Value="2", Selected = ("2"  == sortid)},
                    new SelectListItem() { Text="per Aggiornamento", Value="3", Selected = ("3" == sortid)},
                    new SelectListItem() { Text="per Preferiti", Value="4", Selected = ("4" == sortid)}
                };

                ViewBag.orderby = sort;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        #endregion
    }
}
