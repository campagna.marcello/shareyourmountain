﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SYM.BusinessLayer.Services;

namespace SYM.SearchEngine.MvcApp.Controllers
{
    public class ProjectController : Controller
    {
        //
        // GET: /Project/
        [OutputCache(Duration = 600)]
        public ActionResult Index()
        {
             //TODO: Singleton?
            InfoService infoService = new InfoService();
            var summaryStats = infoService.GetSummaryStats();

            ViewBag.counterMntn = summaryStats.counterMntn.ToString();
            ViewBag.counterMntnRange = summaryStats.counterMntnRange.ToString();
            ViewBag.counterMntnGroup = summaryStats.counterMntnGroup.ToString();
            ViewBag.counterRoute = summaryStats.counterRoutes.ToString();

            return View();
        }

    }
}
