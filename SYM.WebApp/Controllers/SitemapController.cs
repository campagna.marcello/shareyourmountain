﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SYM.DataAccessLayer;
using SimpleMvcSitemap;
using NLog;
using System.Globalization;

namespace SYM.SearchEngine.MvcApp.Controllers
{
    public class SitemapController : Controller
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        //Site Map with main pages and mountains List
        [OutputCache(Duration = 3600)]
        public ActionResult Index()
        {
            List<SitemapNode> nodes = new List<SitemapNode>
            {
                new SitemapNode(Url.Action("Index","Home")){
                    ChangeFrequency = ChangeFrequency.Weekly,
                    LastModificationDate = DateTime.UtcNow,
                    Priority = 0.9M
                },

                new SitemapNode(Url.Action("Index","Project")){
                    ChangeFrequency = ChangeFrequency.Weekly,
                    LastModificationDate = DateTime.UtcNow,
                    Priority = 0.5M
                },

                //other nodes
                new SitemapNode(Url.Action("Contact","Home")){
                    ChangeFrequency = ChangeFrequency.Weekly,
                    LastModificationDate = DateTime.UtcNow,
                    Priority = 0.5M
                },

                //Statistics
                new SitemapNode(Url.Action("Statistics","Info")){
                    ChangeFrequency = ChangeFrequency.Weekly,
                    LastModificationDate = DateTime.UtcNow,
                    Priority = 0.7M
                },

                //Gallery
                new SitemapNode(Url.Action("Index","Gallery")){
                    ChangeFrequency = ChangeFrequency.Weekly,
                    LastModificationDate = DateTime.UtcNow,
                    Priority = 0.8M
                },

                //Book SHop
                new SitemapNode(Url.Action("Book","Shop")){
                    ChangeFrequency = ChangeFrequency.Weekly,
                    LastModificationDate = DateTime.UtcNow,
                    Priority = 0.8M
                }
            };

            //Introduce algoritm to change dinamically the priority using the number of routes.
            var mountains = _context.mountain.Where(a=> a.mntn_status == true).ToList();
            foreach (mountain item in mountains)
            {
                //Set PRIORITY
                int count = item.route.Count();
                string priority = "0.";
                if (count >= 10)
                    priority = "1";
                else
                    priority = "0." + count.ToString();

                //TODO: regural expression to replace all not normal char
                nodes.Add(new SitemapNode(Url.Action("Details", "Mountain").Replace("Details", "") + item.id + "-" + Uri.EscapeDataString(item.mntn_name.Replace(" ", "").Replace("-", "_").Replace("&", "").Replace("/", "").Replace(".", "")))
                {
                    ChangeFrequency = ChangeFrequency.Daily,
                    LastModificationDate = DateTime.UtcNow,
                    Priority = decimal.Parse(priority,CultureInfo.InvariantCulture)
                });

            }

            return new SitemapProvider().CreateSitemap(HttpContext, nodes);
        }

        //Site Map with routes List
        [OutputCache(Duration = 3600)]
        public ActionResult Index2()
        {
            List<SitemapNode> nodes = new List<SitemapNode>();

            var route = _context.route.Where(a=> a.route_status == true).ToList();
            foreach (route r in route)
            {
                //Set PRIORITY
                int ranking = 0;
                if(r.route_ranking != null)
                    ranking = Convert.ToInt32(r.route_ranking);

                string priority = "0." + ranking.ToString();
                string prefix = "";

                if (r.mountain != null)
                {
                    prefix = r.mountain.mntn_name;
                }
                else if (r.valley_id != null)
                {
                    prefix = r.valley.valley_name;
                }

                if (!String.IsNullOrEmpty(prefix))
                {
                    //TODO: regural expression to replace all not normal char
                    nodes.Add(new SitemapNode(Url.Action("Details", "Route").Replace("Details", "") + r.id + "-" + prefix.Replace(" ", "").Replace("-", "_").Replace("&", "").Replace("/", "").Replace(".", "") + "_" + Uri.EscapeDataString(r.route_name.Replace(" ", "").Replace("-", "_").Replace("&", "").Replace("/", "").Replace(".", "")))
                    {
                        ChangeFrequency = ChangeFrequency.Daily,
                        LastModificationDate = DateTime.UtcNow,
                        Priority = decimal.Parse(priority, CultureInfo.InvariantCulture)
                    });
                }

            }

            return new SitemapProvider().CreateSitemap(HttpContext, nodes);
        }
    }
}
