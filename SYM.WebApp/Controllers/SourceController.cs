﻿using NLog;
using SYM.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SYM.SearchEngine.MvcApp.Controllers
{
    public class SourceController : Controller
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        #region actionsController

        //
        // GET: /Source/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Source/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Source/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Source/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Source/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Source/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Source/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Source/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        #endregion actionsController

        public int Create(string name, string domainUrl)
        {
            try
            {
                source _source = new source();
                _source.source_name = name;
                _source.source_url = domainUrl;
                _context.source.Add(_source);
                _context.SaveChanges();
                return _source.id;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return 0;
            }
        }

        /// <summary>Find if exist the Domain Url and get it id.</summary>
        /// <param name="domainUrl">url of domain (ex. http://domainurl.com or domanin.com)</param>
        public int FindByDomainUrl(string domainUrl)
        {

            int value = 0;
            var source = _context.source.Where(a => a.source_url.Contains(domainUrl));
            if (source.Count() == 1)
                value = source.First().id;

            return value;

        }

    }
}
