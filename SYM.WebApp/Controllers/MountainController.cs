﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web;
using System.IO;
using System.Security.Claims;
using SYM.DataAccessLayer;
using SYM.BusinessLayer;
using SYM.BusinessLayer.Services;
using CloudinaryDotNet.Actions;
using System.Web.Configuration;
using PagedList;
using NLog;

namespace SYM.SearchEngine.MvcApp.Controllers
{
    public class MountainController : Controller
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();

        // GET: /Mountain/
        //[Authorize]
        [OutputCache(Duration = 600, VaryByParam = "*", VaryByCustom = "User")]
        public ActionResult Index(FormCollection collection, string sortOrder, int? page, string _mntn, string _r, string _mntng, string _mntnr, string _country, string _region_text, string _routes)
        {
            try
            {
                IEnumerable<v_mountain_index> mountains;
                string profile_regionID, profile_countryID, profile_region;

                //TODO: create Static Helper & Singleton 
                var identity = (ClaimsIdentity)User.Identity;
                if (identity.IsAuthenticated)
                {
                    IEnumerable<Claim> claims = identity.Claims.ToList();
                    profile_regionID = claims.FirstOrDefault(x => x.Type == "regionid").Value;
                    profile_countryID = claims.FirstOrDefault(x => x.Type == "countryid").Value;
                    profile_region = claims.FirstOrDefault(x => x.Type == ClaimTypes.StateOrProvince).Value;
                }
                else {
                    profile_regionID = "1736";
                    profile_countryID = "116";
                    profile_region = "Lombardia";
                }

                #region ViewBagInit
                ViewBag.CurrentSort = sortOrder;
                //TODO: Uniformare nome variabili
                if (collection.Count > 0)
                {
                    ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "name";
                    ViewBag.AltitudeSortParm = sortOrder == "altitude" ? "altitude_desc" : "altitude";
                    ViewBag.Mountain = String.IsNullOrEmpty(collection["mntn_name"]) ? collection["meta"] : collection["mntn_name"];
                    ViewBag.Range = String.IsNullOrEmpty(collection["range"]) ? "" : collection["range"];
                    ViewBag.Group = String.IsNullOrEmpty(collection["group"]) ? "" : collection["group"];
                    ViewBag.Range_text = String.IsNullOrEmpty(collection["range_text"]) ? "" : collection["range_text"];
                    ViewBag.Group_text = String.IsNullOrEmpty(collection["group_text"]) ? "" : collection["group_text"];
                    ViewBag.Country = String.IsNullOrEmpty(collection["country"]) ? null : collection["country"];
                    ViewBag.Region = String.IsNullOrEmpty(collection["region"]) ? null : collection["region"];
                    ViewBag.Region_text = String.IsNullOrEmpty(collection["region"]) ? "" : _context.geo_region.Find(Convert.ToInt32((string)ViewBag.Region)).region_name;
                    ViewBag.Routes = String.IsNullOrEmpty(collection["routes"]) ? "" : collection["routes"];
                }
                else if (page >= 2)
                {
                    //New condition to recognize when some parameters all null
                    ViewBag.CurrentSort = sortOrder;
                    ViewBag.Mountain = String.IsNullOrEmpty(_mntn) ? "" : _mntn;
                    ViewBag.Range = String.IsNullOrEmpty(_mntnr) ? "" : _mntnr;
                    ViewBag.Group = String.IsNullOrEmpty(_mntng) ? "" : _mntng;
                    ViewBag.Country = String.IsNullOrEmpty(_country) ? null : _country;
                    ViewBag.Region = String.IsNullOrEmpty(_r) ? null : _r;
                    ViewBag.Region_text = String.IsNullOrEmpty(_r) ? "" : _context.geo_region.Find(Convert.ToInt32((string)ViewBag.Region)).region_name;
                    ViewBag.Routes = String.IsNullOrEmpty(_routes) ? "" : _routes;
                }
                else {
                    ViewBag.CurrentSort = sortOrder;
                    ViewBag.Mountain = String.IsNullOrEmpty(_mntn) ? "" : _mntn;
                    ViewBag.Range = String.IsNullOrEmpty(_mntnr) ? "" : _mntnr;
                    ViewBag.Group = String.IsNullOrEmpty(_mntng) ? "" : _mntng;
                    ViewBag.Region_text = String.IsNullOrEmpty(_region_text) ? profile_region : _region_text;
                    ViewBag.Region = String.IsNullOrEmpty(_r) ? profile_regionID : _r;
                    ViewBag.Country = String.IsNullOrEmpty(_country) ? profile_countryID : _country;
                }
                #endregion

                //Init Param
                int country = Convert.ToInt32((string)ViewBag.Country);
                string mountainName = ViewBag.Mountain;
                int range = String.IsNullOrEmpty(ViewBag.Range) ? 0 : Convert.ToInt32(ViewBag.Range);
                int group = String.IsNullOrEmpty(ViewBag.Group) ? 0 : Convert.ToInt32(ViewBag.Group);
                int region = String.IsNullOrEmpty(ViewBag.Region) ? 0 : Convert.ToInt32(ViewBag.Region);
                string altitude = String.IsNullOrEmpty(collection["altitude"]) ? null : collection["altitude"];
                bool routes = String.IsNullOrEmpty(ViewBag.Routes) ? false : Convert.ToBoolean(ViewBag.Routes);

                if (region > 0)
                    mountains = _context.v_mountain_index.Where(a => a.region_id == region).ToList();
                else if (country > 0)
                    mountains = _context.v_mountain_index.Where(a => a.country_id == country).ToList();
                else
                    mountains = _context.v_mountain_index.ToList();

                if (!String.IsNullOrEmpty(mountainName))
                    mountains = mountains.Where(a => a.mntn_name.ToLower().Contains(mountainName.ToLower().Trim()));
                if (group > 0)
                    mountains = mountains.Where(a => a.mntnGroup_id != null && a.mntnGroup_id == group);
                if (range > 0)
                    mountains = mountains.Where(a => a.mntnRange_id != null && a.mntnRange_id == range);
                if (routes)
                    mountains = mountains.Where(a => a.count > 0);
                //NOT USED
                //if (!String.IsNullOrEmpty(altitude))
                //    mountains = mountains.Where(a => a.mntn_altitude >= Convert.ToInt32(altitude));

                switch (sortOrder)
                {
                    case "name_desc":
                        mountains = mountains.OrderByDescending(s => s.mntn_name);
                        break;
                    case "altitude_desc":
                        mountains = mountains.OrderByDescending(s => s.mntn_altitude);
                        break;
                    case "range_desc":
                        mountains = mountains.OrderByDescending(s => s.mntnRange_id);
                        break;
                    default:  // Name ascending 
                        mountains = mountains.OrderByDescending(s => s.count);
                        break;
                }

                //IMPORTANT: New instruction to create a new viewmodel to distinct and avoid duplicate when the user not filter the region
                mountains = mountains.GroupBy(s => new { s.id, s.mntn_name, s.mntn_altitude, s.mntnRange_name, s.mntnGroup_name, s.wikipedia, s.coordinates, s.count })
                    .Select(cc => new v_mountain_index() { id = cc.First().id, mntn_name = cc.First().mntn_name, mntn_altitude = cc.First().mntn_altitude, region_id = 0, country_id = 0, mntnRange_id = 0, mntnGroup_id = 0, mntnRange_name = cc.First().mntnRange_name, mntnGroup_name = cc.First().mntnGroup_name, wikipedia = cc.First().wikipedia, coordinates = cc.First().coordinates, count = cc.First().count }).ToList();
                ViewBag.Count = mountains.Count();

                int pageSize = 10;
                int pageNumber = (page ?? 1);
                return View(mountains.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return View("Index");
            }
        }

        [OutputCache(Duration = 600, VaryByParam = "*", VaryByCustom = "User")]
        public ActionResult Search(FormCollection collection, string sortOrder, int? page, string _mntn, string _r, string _country)
        {
            IEnumerable<v_mountain_index> mountains;
            string profile_regionID, profile_countryID, profile_region;

            //TODO: create Static Helper & Singleton 
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                IEnumerable<Claim> claims = identity.Claims.ToList();
                profile_regionID = claims.FirstOrDefault(x => x.Type == "regionid").Value;
                profile_countryID = claims.FirstOrDefault(x => x.Type == "countryid").Value;
                profile_region = claims.FirstOrDefault(x => x.Type == ClaimTypes.StateOrProvince).Value;
            }
            else {
                profile_countryID = "116";
            }

            #region ViewBagInit
            ViewBag.CurrentSort = sortOrder;

            if (collection.Count > 0)
            {
                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "name";
                ViewBag.AltitudeSortParm = sortOrder == "altitude" ? "altitude_desc" : "altitude";
                ViewBag.Mountain = String.IsNullOrEmpty(collection["mntn_name"]) ? "" : collection["mntn_name"];
                ViewBag.Country = String.IsNullOrEmpty(collection["country"]) ? profile_countryID : collection["country"];
            }
            else if (page >= 2)
            {
                ViewBag.CurrentSort = sortOrder;
                ViewBag.Mountain = String.IsNullOrEmpty(_mntn) ? "" : _mntn;
                ViewBag.Country = String.IsNullOrEmpty(_country) ? profile_countryID : _country;
            }
            else {
                ViewBag.CurrentSort = sortOrder;
                ViewBag.Mountain = String.IsNullOrEmpty(_mntn) ? "" : _mntn;
                ViewBag.Country = String.IsNullOrEmpty(_country) ? profile_countryID : _country;
            }
            #endregion

            //Init Param
            int country = Convert.ToInt32((string)ViewBag.Country);
            geo_country obj = _context.geo_country.Where(a => a.id == country).First();
            ViewBag.CountryCode = obj.country_code;

            string textSearch = (String)ViewBag.Mountain.Trim().ToLower();
            string altitude = String.IsNullOrEmpty(collection["altitude"]) ? null : collection["altitude"];

            if (country > 0 && !String.IsNullOrEmpty(textSearch))
                mountains = _context.v_mountain_index.Where(a => a.country_id == country && a.mntn_name.ToLower().Contains(textSearch.ToLower().Trim())).ToList();
            else if (country > 0)
                mountains = _context.v_mountain_index.Where(a => a.country_id == country).ToList();
            else
                mountains = _context.v_mountain_index.ToList();

            if(mountains.Count() == 0) //IF NO result with name we search the keyword in mountain range & group
                mountains = _context.v_mountain_index.Where(a => a.country_id == country && (a.mntnRange_name.ToLower().Contains(textSearch) || a.mntnGroup_name.ToLower().Contains(textSearch))).ToList();

            switch (sortOrder)
            {
                case "name_desc":
                    mountains = mountains.OrderByDescending(s => s.mntn_name);
                    break;
                case "altitude_desc":
                    mountains = mountains.OrderByDescending(s => s.mntn_altitude);
                    break;
                default:  // Name ascending 
                    mountains = mountains.OrderByDescending(s => s.count);
                    break;
            }

            //IMPORTANT: New instruction to create a new viewmodel to distinct and avoid duplicate when the user not filter the region
            mountains = mountains.GroupBy(s => new { s.id, s.mntn_name, s.mntn_altitude, s.mntnRange_name, s.mntnGroup_name, s.wikipedia, s.coordinates, s.count, s.lat, s.lng, s.countview, s.lastview })
                .Select(cc => new v_mountain_index() { id = cc.First().id, mntn_name = cc.First().mntn_name, mntn_altitude = cc.First().mntn_altitude, region_id = 0, country_id = 0, mntnRange_id = 0, mntnGroup_id = 0, mntnRange_name = cc.First().mntnRange_name, mntnGroup_name = cc.First().mntnGroup_name, wikipedia = cc.First().wikipedia, coordinates = cc.First().coordinates, count = cc.First().count, lat = cc.First().lat, lng = cc.First().lng, countview = cc.First().countview, lastview = cc.First().lastview }).ToList();
            ViewBag.Count = mountains.Count();

            logger.Trace("Search keyword - " + textSearch + " - n° " + mountains.Count().ToString());

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(mountains.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Mountain/Details/5
        [OutputCache(Duration = 2400, VaryByParam = "id", VaryByCustom = "User")]
        public ActionResult Details(int id)
        {
            var mountain = _context.mountain.Find(id);
            if (mountain == null || mountain.mntn_status == false)
            {
                return HttpNotFound();
            }

            var page = _context.page_statistics.Where(a => a.page_type == 1 && a.page_id == id).FirstOrDefault();
            ViewBag.CountViews = page != null ? page.page_countview : 0;
            ViewBag.LastViews = page != null ? @String.Format("{0:dd/MM/yyyy hh:mm}", page.page_dateMod) : "";
            ViewBag.Images = _context.mntn_image.Where(a => a.mountain_id == id).ToList().Count;

            var routeService = new RouteService();
            ViewBag.MountainActivities =  routeService.GetMountainActivities(id);

            return View(mountain);
        }

        // GET: /Mountain/Details/5 ?????
        [OutputCache(Duration = 600, VaryByParam = "id")]
        public PartialViewResult InfoWindow(int id)
        {
            var mountain = _context.mountain.Find(id);
            if (mountain == null || mountain.mntn_status == false)
            {
                return PartialView();
            }
            return PartialView("~/Views/Mountain/Partial/_InfoWindow.cshtml", mountain);
        }

        [HttpPost]
        public ActionResult SaveUploadedFile(HttpPostedFileBase file, int id, string cover, string mountain_log, string imageDescr)
        {
            try
            {
                AdventureService AS = new AdventureService();
                string fName = "";
                string mountain = "", mntnRange = "";
                var mntn = _context.mountain.Find(id);

                if (mntn == null || mntn.mntn_status == false || file == null)
                {
                    return RedirectToAction("Index");
                }

                //****TODO: create Static Helper & Singleton ****
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims.ToList();
                int profileID = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);

                //Configure TAGS
                if (mntn != null) mountain = mntn.mntn_name;
                if (mntn.mntn_mountainrange != null) mntnRange = mntn.mntn_mountainrange.mntnRange_name;
                string tags = mountain + "," + mntnRange;


                CloudinaryIntegration cloudinary = new CloudinaryIntegration(WebConfigurationManager.AppSettings["cloud_string"], WebConfigurationManager.AppSettings["cloud_key"], WebConfigurationManager.AppSettings["cloud_secret"]);

                var allowedExtensions = new[] { ".jpg", ".png", ".gif", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!allowedExtensions.Contains(extension.ToLower()))
                {
                    ViewBag.Message = "The extension is not correct.";
                }
                else if (file != null && file.ContentLength > 0)
                {
                    //Save file content goes here
                    fName = file.FileName;
                    var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\mountains_upload", Server.MapPath(@"\")));
                    var fileName1 = Path.GetFileName(file.FileName);
                    var path = string.Format("{0}\\{1}", originalDirectory.ToString(), file.FileName);
                    file.SaveAs(path);

                    //ID to register in database
                    ImageUploadResult result = cloudinary.Upload(@"mountains/" + mntn.id.ToString(), tags.Replace(" ", ""), path);

                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        mntn_image m = new mntn_image();
                        m.mountain_id = mntn.id;
                        m.image_url = result.PublicId + ".jpg";
                        m.image_filename = fileName1;
                        m.image_cover = Convert.ToBoolean(cover);
                        m.image_descr = imageDescr;
                        m.image_cdn = true;
                        m.user_id = profileID;
                        _context.mntn_image.Add(m);
                        _context.SaveChanges();
                    }
                }

                //New method to set the mountain log after image uplaod
                if (!String.IsNullOrEmpty(mountain_log))
                {
                    bool? done = Convert.ToBoolean(mountain_log);
                    AS.Set(profileID, id, 0, 0, done);
                }

            }
            catch (Exception ex)
            {
                //isSavedSuccessfully = false;
                logger.Error(ex.Message);
            }

            return RedirectToAction("Details/" + id.ToString());

        }

        // GET: /Mountain/Details/5
        [OutputCache(Duration = 600, VaryByParam = "id")]
        public ActionResult Map(int id)
        {
            var mountain = _context.mountain.Find(id);
            if (mountain == null)
            {
                return HttpNotFound();
            }
            return View(mountain);
        }

        //Action for Modal
        [Authorize]
        [OutputCache(Duration = 600)]
        public PartialViewResult _Create()
        {
            InitDropDownList(116);
            return PartialView("~/Views/Mountain/Partial/_Create.cshtml");
        }

        //
        // POST: /Mountain/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                mountain _mountain = new mountain();
                ass_mountainregion _ass_mountainregion = new ass_mountainregion();
                ass_mountaincountry _ass_mountaincountry = new ass_mountaincountry();

                string name = String.IsNullOrEmpty(collection["mntn_name"]) ? "" : collection["mntn_name"];
                string altitude = String.IsNullOrEmpty(collection["altitude"]) ? "" : collection["altitude"];
                string latitude = String.IsNullOrEmpty(collection["latitude"]) ? "" : collection["latitude"];
                string longitude = String.IsNullOrEmpty(collection["longitude"]) ? "" : collection["longitude"];
                string range_id = String.IsNullOrEmpty(collection["mntRange"]) ? "" : collection["mntRange"];
                string group_id = String.IsNullOrEmpty(collection["mntGroup"]) ? "" : collection["mntGroup"];
                string countries = String.IsNullOrEmpty(collection["countries"]) ? "" : collection["countries"];
                string region_id = String.IsNullOrEmpty(collection["region"]) ? "" : collection["region"];

                //Create new MOUNTAIN
                //Errore nell'inserimento di 0 per le catene e gruppi montuosi
                _mountain.mntn_name = name;
                _mountain.mntn_altitude = Convert.ToInt16(altitude);
                if (!String.IsNullOrEmpty(range_id)) _mountain.mntnRange_id = Convert.ToInt32(range_id);
                if (!String.IsNullOrEmpty(group_id)) _mountain.mntnGroup_id = Convert.ToInt32(group_id);
                _mountain.mntn_latitude = float.Parse(latitude.Replace(".", ","));
                _mountain.mntn_longitude = float.Parse(longitude.Replace(".", ","));
                _mountain.mntn_dateIns = DateTime.Now;
                _mountain.mntn_status = true;
                _context.mountain.Add(_mountain);
                _context.SaveChanges();

                //Create record for ASS_COUNTRY = 116 Italia 217 Svizzera;
                //Todo Loop
                int id = _mountain.id;
                geo_region regionTmp;
                geo_country countryTmp;
                countryTmp = _context.geo_country.Find(Convert.ToInt32(countries));
                try
                {
                    _ass_mountaincountry.country_id = countryTmp.id;
                    _ass_mountaincountry.mntn_id = id;
                    _ass_mountaincountry.country_code = countryTmp.country_code;
                    _context.ass_mountaincountry.Add(_ass_mountaincountry);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    logger.Error("Mountain Create ->" + ex.Message);
                }

                //Create record for ASS_REGION 
                regionTmp = _context.geo_region.Find(Convert.ToInt32(region_id));
                _ass_mountainregion.country_id = countryTmp.id;
                _ass_mountainregion.mntn_id = id;
                _ass_mountainregion.region_name = regionTmp.region_name;
                _ass_mountainregion.region_id = Convert.ToInt32(region_id);

                _context.ass_mountainregion.Add(_ass_mountainregion);
                _context.SaveChanges();

                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
            }
            catch (Exception ex)
            {
                logger.Error("Mountain Create ->" + ex.Message);
                return View("Index");
            }
        }

        // GET: /Mountain/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            logger.Info("DISABLE mountain ID " + id.ToString());
            try
            {
                mountain c = _context.mountain.Find(id);
                c.mntn_dateMod = DateTime.Now;
                c.mntn_status = false;
                c.mntn_name = c.mntn_name + " (ToDelete)";
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("Delete Failed " + ex.InnerException.InnerException.ToString());
            }
            return RedirectToAction("Index");

        }

        //
        // POST: /Mountain/Delete/5

        [HttpPost]
        [Authorize]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddImage(string p_mntn, string p_url)
        {
            int mountain_id = Convert.ToInt32(p_mntn);
            mntn_image _image;
            string url = p_url;
            bool result = true;

            try
            {
                _image = this.FindByImageUrl(p_url);
                if (_image == null)
                {
                    mntn_image _temp = new mntn_image();
                    _temp.image_url = p_url;
                    _temp.mountain_id = Convert.ToInt32(p_mntn);
                    _temp.image_dateIns = DateTime.Now;
                    _temp.image_cover = false;
                    _context.mntn_image.Add(_temp);
                    _context.SaveChanges();
                }
                else {
                    //Mod with increment click count
                    _image.image_click = _image.image_click + 1;
                    _image.image_dateMod = DateTime.Now;
                    _context.SaveChanges();
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = false;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>Find if exist the Domain Url and get it id.</summary>
        /// <param name="domainUrl">url of domain (ex. http://domainurl.com or domanin.com)</param>
        public mntn_image FindByImageUrl(string url)
        {
            mntn_image _image = null;
            var checkList = _context.mntn_image.Where(a => a.image_url.Contains(url)).ToList();
            if (checkList.Count > 0)
                _image = checkList.First();

            return _image;
        }

        [OutputCache(Duration = 600)]
        public ActionResult _UploadImage(int id = 0)
        {
            return PartialView("~/Views/Mountain/Partial/_UploadImage.cshtml");
        }

        [OutputCache(Duration = 60)]
        public ActionResult _WidgetSocial(int id)
        {
            ViewBag.id = id;
            return PartialView("~/Views/Mountain/Partial/_WidgetSocial.cshtml");
        }

        private void InitDropDownCountry(int countryid)
        {
            try
            {
                //IEnumerable<mntn_mountainrange> mountainrange = _context.mntn_mountainrange.ToList();
                IEnumerable<SelectListItem> countries = new List<SelectListItem>()
                {
                    new SelectListItem() { Text="IT", Value="116", Selected = (116 == countryid)},
                    new SelectListItem() { Text="CH", Value="217", Selected = (217 == countryid)},
                    new SelectListItem() { Text="FR", Value="65", Selected = (65 == countryid)}
                };

                ViewBag.country = countries;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        //Todo: passare parametri di default associati al profilo o alla pagina
        private void InitDropDownList(int country_id, int region_id = 1736)
        {
            try
            {
                //Mountain Region
                //IEnumerable<geo_region> region = _context.geo_region.ToList();
                IEnumerable<SelectListItem> regions = from value in _context.geo_region.Where(bs => bs.country_id == country_id).OrderBy(bs => bs.id).ToList()
                                                      select new SelectListItem
                                                      {
                                                          Text = value.region_name,
                                                          Value = value.id.ToString(),
                                                          Selected = (value.id == region_id)
                                                      };
                ViewBag.regions = regions;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

    }
}
