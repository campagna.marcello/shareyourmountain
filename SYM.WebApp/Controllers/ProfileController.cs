﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Threading.Tasks;
using SYM.DataAccessLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SYM.BusinessLayer;
using SYM.BusinessLayer.Services;
using SYM.SearchEngine.MvcApp.Helpers;
using SYM.WebApp.Models;
using NLog;
using System.Web;
using System.IO;
using System.Security.Claims;
using CloudinaryDotNet.Actions;
using System.Web.Configuration;
using PagedList;

namespace SYM.WebApp
{
    [Authorize]
    public class ProfileController : Controller
    {
        private Entities _context = new Entities();
        Logger logger = LogManager.GetCurrentClassLogger();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        #region Helper
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }
        #endregion

        public ActionResult Index()
        {
            List<int> activityArray = null;
            try
            {
                user_profile user = GetUserProfile();
                if (user == null)
                    return this.HttpNotFound();

                //Init ViewBag
                string activities = user.profile_activities;
                if (!String.IsNullOrEmpty(activities))
                    activityArray = activities.Split(',').Select(Int32.Parse).ToList();
                InitDropDownActivities(activityArray);
                InitDropDownRoles(user.role_id.ToString());
                LoadDropDownList();

                return View(user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return View();
            }
        }

        public ActionResult AdventureList()
        {
            try
            {
                user_profile user = GetUserProfile();
                if (user == null)
                    return this.HttpNotFound();

                return View(user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return View();
            }
        }

        [OutputCache(Duration = 60, VaryByCustom = "User")]
        public ActionResult Gallery()
        {
            IEnumerable<v_feed_images> model = null;
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims.ToList();
                int profileID = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);
                model = _context.v_feed_images.Where(a => a.user_id == profileID).ToList().OrderBy(s => s.days);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            int pageSize = 40;
            return View(model.ToPagedList(1, pageSize));
        }

        #region partialview
        public PartialViewResult _MountainsList()
        {
            int profileID = 0;
            try
            {
                //TODO: create Static Helper & Singleton 
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims.ToList();
                profileID = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);
                var model = _context.user_wishlist.Where(a => a.user_id == profileID && a.mountain_id != null && a.route_id == null).OrderByDescending(a => a.wl_done).ToList();
                return PartialView("~/Views/Profile/Partial/_MountainsList.cshtml", model);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " / " + profileID.ToString());
                return PartialView("~/Views/Profile/Partial/_MountainsList.cshtml");
            }
        }

        public PartialViewResult _RouteList()
        {
            int profileID = 0;
            try
            {
                //TODO: create Static Helper & Singleton 
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims.ToList();
                profileID = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);
                var model = _context.user_wishlist.Where(a => a.user_id == profileID && a.route_id != null).OrderByDescending(a => a.wl_done).ToList();
                return PartialView("~/Views/Profile/Partial/_RouteList.cshtml", model);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " / " + profileID.ToString());
                return PartialView("~/Views/Profile/Partial/_RouteList.cshtml");
            }
        }

        public PartialViewResult _WishList()
        {
            int profileID = 0;
            try
            {
                IEnumerable<SelectListItem> ratingList = new List<SelectListItem>()
                {
                    new SelectListItem() {Text="", Value=""},
                    new SelectListItem() {Text="1", Value="1"},
                    new SelectListItem() {Text="2", Value="2"},
                    new SelectListItem() {Text="3", Value="3"},
                    new SelectListItem() {Text="4", Value="4"}
                };

                ViewBag.rating = ratingList;

                //TODO: create Static Helper & Singleton 
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims.ToList();
                profileID = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);
                var model = _context.user_wishlist.Where(a => a.user_id == profileID && a.wl_done == true).OrderByDescending(a => a.wl_dateAscent).ThenBy(n => n.wl_done).ToList();
                return PartialView("~/Views/Profile/Partial/_WishList.cshtml", model);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " / " + profileID.ToString());
                return PartialView("~/Views/Profile/Partial/_WishList.cshtml");
            }
        }

        [OutputCache(Duration = 60, VaryByCustom = "User")]
        public PartialViewResult _Widget()
        {
            try
            {
                //TODO: create Static Helper & Singleton 
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims.ToList();
                int profileID = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);
                user_profile tmp = _context.user_profile.Find(identity.GetUserId());

                int routeImg = _context.route_image.Where(a => a.user_id == profileID).ToList().Count();
                int mntnImg = _context.mntn_image.Where(a => a.user_id == profileID).ToList().Count();

                ViewBag.Routes = _context.user_wishlist.Where(a => a.user_id == profileID && a.route_id != null && a.wl_done == true).ToList().Count();
                ViewBag.Peaks = _context.user_wishlist.Where(a => a.user_id == profileID && a.mountain_id != null && a.wl_done == true).Select(grp => grp.mountain_id).Distinct().ToList().Count;
                ViewBag.Uploads = routeImg + mntnImg;
                ViewBag.Url = tmp.profile_website;

                UserService US = new UserService();
                US.SetStats(profileID, ViewBag.Routes, ViewBag.Peaks, ViewBag.Uploads, 0);

                return PartialView("~/Views/Profile/Partial/_ProfileSideBarWidget.cshtml");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return PartialView("~/Views/Profile/Partial/_ProfileSideBarWidget.cshtml");
            }
        }

        public PartialViewResult _EditAdventure(int id)
        {
            int profileID = 0;
            try
            {
                //TODO: create Static Helper & Singleton 
                var identity = (ClaimsIdentity)User.Identity;
                IEnumerable<Claim> claims = identity.Claims.ToList();
                profileID = Convert.ToInt32(claims.FirstOrDefault(x => x.Type == "profileid").Value);
                user_wishlist model = _context.user_wishlist.Where(a => a.user_id == profileID && a.id == id).First();
                InitDropDownScore(model.wl_score.GetValueOrDefault());
                return PartialView("~/Views/Profile/Partial/_ModalEditAdventure.cshtml", model);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " / " + profileID.ToString());
                return PartialView("~/Views/Profile/Partial/_ModalEditAdventure.cshtml");
            }
        }

        #endregion partialview

        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            try
            {
                user_profile user = GetUserProfile();
                if (user == null)
                    return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);

                string firstname = String.IsNullOrEmpty(collection["firstname"]) ? "" : collection["firstname"];
                string lastname = String.IsNullOrEmpty(collection["lastname"]) ? "" : collection["lastname"];
                string email = String.IsNullOrEmpty(collection["email"]) ? "" : collection["email"];
                string region_id = String.IsNullOrEmpty(collection["regions"]) ? "" : collection["regions"];
                string city_id = String.IsNullOrEmpty(collection["city"]) ? "" : collection["city"];
                string about = String.IsNullOrEmpty(collection["about"]) ? "" : collection["about"];
                string website = String.IsNullOrEmpty(collection["website"]) ? "" : collection["website"];
                string activities = String.IsNullOrEmpty(collection["tags_interests"]) ? "" : collection["tags_interests"];
                string role = String.IsNullOrEmpty(collection["role"]) ? "" : collection["role"];
                List<int> activityArray = String.IsNullOrEmpty(activities) ? null : activities.Split(',').Select(Int32.Parse).ToList();

                //If exist the mountain i can set also the Mountain Range & Group
                if (!String.IsNullOrEmpty(firstname))
                    user.profile_firstName = firstname;
                if (!String.IsNullOrEmpty(lastname))
                    user.profile_lastName = lastname;
                if (!String.IsNullOrEmpty(region_id))
                {
                    user.region_id = Convert.ToInt32(region_id);
                    geo_region obj = _context.geo_region.Find(user.region_id);
                    user.country_id = obj.country_id;
                }
                if (!String.IsNullOrEmpty(city_id))
                    user.city_id = Convert.ToInt32(city_id);
                user.profile_bio = about;
                if (!String.IsNullOrEmpty(website))
                    user.profile_website = website;
                if (!String.IsNullOrEmpty(role))
                    user.role_id = Convert.ToInt32(role);

                user.profile_activities = activities;
                user.profile_dateMod = DateTime.Now;

                _context.SaveChanges();

                string t = Request.UrlReferrer.ToString();
                //Init DropDown
                LoadDropDownList();
                InitDropDownActivities(activityArray);
                InitDropDownRoles(role);

                return View("Index", user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                return PartialView("~/Views/Profile/Partial/_EditAccount.cshtml");
            }

        }

        [HttpPost]
        public ActionResult EditAdventure(FormCollection collection)
        {
            int profileID = 0;
            DateTime dateClimb;
            try
            {
                user_profile user = GetUserProfile();
                if (user == null)
                    return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);

                int id = Convert.ToInt32(collection["id"]);
                short? score = Convert.ToInt16(collection["score"].ToString());

                var wishlist = _context.user_wishlist.Find(id);
                wishlist.wl_score = score;
                if (!String.IsNullOrEmpty(collection["date"]))
                {
                    dateClimb = DateTime.Parse(collection["date"].ToString());
                    if(dateClimb > DateTime.Parse("1970-01-01"))
                        wishlist.wl_dateAscent = dateClimb;
                }
                if (!String.IsNullOrEmpty(collection["done"]))
                {
                    bool? done = Convert.ToBoolean(collection["done"]);
                    wishlist.wl_done = done;
                }
                wishlist.wl_dateMod = DateTime.Now;
                _context.SaveChanges();

                return View("AdventureList", user);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " / " + profileID.ToString());
                return PartialView("~/Views/Profile/Partial/_ModalEditAdventure.cshtml");
            }
        }

        [HttpPost]
        // TODO: Fix the problem with the Ajax Form and take present to save the name of image in table withut use the GUID ID 
        public ActionResult UploadImage(HttpPostedFileBase file)
        {
            string path = null;
            user_profile profile = GetUserProfile();
            if (profile == null)
                return this.HttpNotFound();

            CloudinaryIntegration cloudinary = new CloudinaryIntegration(WebConfigurationManager.AppSettings["cloud_string"], WebConfigurationManager.AppSettings["cloud_key"], WebConfigurationManager.AppSettings["cloud_secret"]);

            try
            {
                string fName = "";
                var allowedExtensions = new[] { ".jpg", ".png", ".gif", ".jpeg" };
                var extension = Path.GetExtension(file.FileName);
                if (!allowedExtensions.Contains(extension))
                {
                    ViewBag.Message = "The extension is not correct.";
                }
                else if (file != null && file.ContentLength > 0)
                {
                    //Save file content goes here
                    fName = file.FileName;
                    var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\avatar", Server.MapPath(@"\")));
                    var fileName1 = Path.GetFileName(file.FileName);
                    path = string.Format("{0}\\{1}", originalDirectory.ToString(), file.FileName);
                    file.SaveAs(path);

                    //ID to register in database
                    ImageUploadResult result = cloudinary.Upload(@"Avatar", "", path);

                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        profile.profile_image = result.PublicId + ".jpg";
                        profile.profile_dateMod = DateTime.Now;
                        _context.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                ViewBag.Message = "You have not specified a file.";
                logger.Error(ex.Message);
            }

            return RedirectToAction("Index");
        }

        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<PartialViewResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView("~/Views/Profile/Partial/_EditPassword.cshtml", model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                ViewBag.dateMod = DateTime.Now;
                return PartialView("~/Views/Profile/Partial/_EditPassword.cshtml", model);
            }
            AddErrors(result);
            return PartialView("~/Views/Profile/Partial/_EditPassword.cshtml", model);
        }

        #region InitDrop
        private void LoadDropDownList()
        {
            try
            {
                int? defaultA = 1;
                //int? defaultC = 1;

                //Mountain Region
                //IEnumerable<geo_region> region = _context.geo_region.ToList();
                //IEnumerable<SelectListItem> regions = from value in region.Where(bs => bs.country_id == 116).OrderBy(bs => bs.id).ToList()
                //                                      select new SelectListItem
                //                                      {
                //                                          Text = value.region_name,
                //                                          Value = value.id.ToString(),
                //                                          Selected = (value.id == region_id)
                //                                      };
                //ViewBag.regions = regions;

                IEnumerable<act_activity> activity = _context.act_activity.ToList();
                IEnumerable<SelectListItem> activities = from value in activity.OrderBy(bs => bs.id).ToList()
                                                         select new SelectListItem
                                                         {
                                                             Text = value.activity_name,
                                                             Value = value.id.ToString(),
                                                             Selected = (value.id == defaultA)
                                                         };
                ViewBag.activity_id = activities;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        private void InitDropDownActivities(List<int> activityArray)
        {
            try
            {

                IEnumerable<act_activity> activity = _context.act_activity.Where(a => a.activity_status == true).ToList();
                IEnumerable<SelectListItemCustom> activities = from value in activity.OrderBy(a => a.activity_order).ToList()
                                                               select new SelectListItemCustom
                                                               {
                                                                   Text = value.activity_name,
                                                                   Value = value.id.ToString(),
                                                                   SubText = value.activity_description,
                                                                   Selected = activityArray == null ? false : (activityArray.Contains(value.id))
                                                               };
                ViewBag.Activity = activities;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        private void InitDropDownRoles(string roleid)
        {
            try
            {

                IEnumerable<user_role> role = _context.user_role.ToList();
                IEnumerable<SelectListItemCustom> roles = from value in role.OrderBy(bs => bs.id).ToList()
                                                          select new SelectListItemCustom
                                                          {
                                                              Text = value.role_name,
                                                              Value = value.id.ToString(),
                                                              SubText = value.role_desc,
                                                              Selected = roleid == value.id.ToString() ? true : false
                                                          };
                ViewBag.Role = roles;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        private void InitDropDownScore(short score)
        {
            try
            {
                IEnumerable<SelectListItemCustom> scoreList = new List<SelectListItemCustom>()
                {
                    new SelectListItemCustom() { Text="Pessimo", Value="0", Selected = (0 == score)},
                    new SelectListItemCustom() { Text="Discreto", Value="1", Selected = (1 == score)},
                    new SelectListItemCustom() { Text="Buono", Value="2", Selected = (2 == score)},
                    new SelectListItemCustom() { Text="Ottimo", Value="3", Selected = (3 == score)},
                    new SelectListItemCustom() { Text="Eccezionale", Value="4", Selected = (4 == score)}
                };

                ViewBag.Score = scoreList;

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }
        #endregion

        private user_profile GetUserProfile()
        {
            string guid = User.Identity.GetUserId();
            var user = _context.user_profile.Find(guid);
            if (user == null)
            {
                logger.Error("No user Found");
            }
            return user;
        }

    }

}
