﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SYM.DataAccessLayer;
using PagedList;

namespace SYM.SearchEngine.MvcApp.Controllers
{
    public class ValleyController : Controller
    {
        public Entities _context = new Entities();
        
        //
        // GET: /Valley/

        public ActionResult Index(int? page)
        {
            IEnumerable<valley> valleis;
            valleis = _context.valley.ToList();

            //return View(mountains);
            int pageSize = 15;
            int pageNumber = (page ?? 1);
            return View(valleis.ToPagedList(pageNumber, pageSize)); 
        }

        //
        // GET: /Valley/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Valley/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Valley/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Valley/Edit/5

        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        ////
        //// POST: /Valley/Edit/5

        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /Valley/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Valley/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
