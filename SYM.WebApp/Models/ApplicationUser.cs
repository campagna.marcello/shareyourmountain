﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using MySql.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using SYM.DataAccessLayer;
using System;

namespace SYM.WebApp.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        private Entities _context = new Entities();

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType            
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            try
            {   
                // Add custom user claims here & Great Way to simplify access to the profile info using the application session
                IEnumerable<Claim> claims = userIdentity.Claims.ToList();
                string userId = claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;
                user_profile profile = _context.user_profile.Find(userId);
                userIdentity.AddClaim(new Claim(ClaimTypes.Country, profile.geo_country.country_name));
                userIdentity.AddClaim(new Claim(ClaimTypes.StateOrProvince, profile.geo_region.region_name));
                userIdentity.AddClaim(new Claim("countryid", profile.country_id.ToString()));
                userIdentity.AddClaim(new Claim("regionid", profile.region_id.ToString()));
                userIdentity.AddClaim(new Claim("profileid", profile.profile_id.ToString()));
                // End custom user claims here

                //Add log access in stats to understand the last login....
                user_stats stats = _context.user_stats.Where(a => a.user_id == profile.profile_id).FirstOrDefault();
                if (stats != null) {
                    stats.stats_lastLogin = DateTime.Now;
                    _context.SaveChanges();     
                }
            }
            catch (Exception ex) {
                return userIdentity;
            }

            return userIdentity;
        }
    }
}
