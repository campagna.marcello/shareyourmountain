﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SYM.WebApp.Models
{
    public class Line
    {
        public List<double> latitudes { get; set; }
        public List<double> longitudes { get; set; }
    }

    public class Image
    {
        public string id { get; set; }
        public string svgPath { get; set; }
        public string title { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public double scale { get; set; }
        public string color { get; set; }
    }

    public class Area {
        public string id { get; set; }
        public string title { get; set; }
        public string groupId { get; set; }
        public string customData { get; set; }
        public string color { get; set; }
    }

    public class MapDataViewModel
    {
        public string map { get; set; }
        public double zoomLevel { get; set; }
        public double zoomLongitude { get; set; }
        public double zoomLatitude { get; set; }
        public List<Area> areas { get; set; }
        public List<Line> lines { get; set; }
        public List<Image> images { get; set; }
    }
}