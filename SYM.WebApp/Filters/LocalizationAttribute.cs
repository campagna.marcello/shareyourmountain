﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace SYM.WebApp.Filters
{
    public class LocalizationAttribute : ActionFilterAttribute
    {
        private string _DefaultLanguage = "it";

        public LocalizationAttribute(string defaultLanguage)
        {
            _DefaultLanguage = defaultLanguage;
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(_DefaultLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(_DefaultLanguage);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string lang = (string)filterContext.RouteData.Values["culture"] ?? _DefaultLanguage;
            if (!String.IsNullOrEmpty(lang))
            {
                try
                {
                    
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);
                    Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
                }
                catch (Exception e)
                {
                    throw new NotSupportedException(String.Format("ERROR: Invalid language code '{0}'.", lang));
                }
            }
        }
    }
}