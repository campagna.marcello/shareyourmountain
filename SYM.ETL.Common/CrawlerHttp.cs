﻿using Dufenergy.WebCrawlerUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SYM.Common
{
    public class CrawlerHttp
    {
        WebCrawlerUtility objCrawler;

        /// <summary>
        /// Format: json, jsonfm, xml, xmlfm
        /// </summary>
        public enum Format
        {
            /// <summary>json</summary>
            json,
            /// <summary>jsonfm</summary>
            jsonfm,
            /// <summary>xml</summary>
            xml,
            /// <summary>xmlfm</summary>
            xmlfm
        };

        public CrawlerHttp() {
            objCrawler = new WebCrawlerUtility();
        }
        
        ///<summary>
        /// Performe generic HHTP Query
        /// Parameter: bool certificate, string uri, string method, string post_data, string referer, string cookie
        /// Return: HttpWebResponse
        ///</summary>
        public HttpWebResponse Request_QueryApi(string url, string referer, string _host = "it.wikipedia.org")
        {
            HttpWebResponse response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                request.Referer = referer; //"it.wikipedia.com";
                request.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)";
                request.Headers.Set(HttpRequestHeader.AcceptEncoding, "gzip");
                request.KeepAlive = true;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Method = "GET";
                request.AutomaticDecompression = DecompressionMethods.GZip;
                request.Host = _host;

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError) response = (HttpWebResponse)e.Response;
                else return null;
            }
            catch (Exception e)
            {
                if (response != null) response.Close();
                return null;
            }

            return response;
        }    

        ///<summary>
        /// Save Reponse Body Http/https in file
        /// <param name="response">HttpWebResponse</param>
        /// <param name="targetPathDirectory"></param>
        /// <param name="fileName"></param>
        /// Return: bool
        ///</summary>
        public string SaveHttpResponseBody(HttpWebResponse response, string targetPathDirectory, string fileName, bool overwrite = true)
        {
            string pathFile = Path.Combine(@targetPathDirectory, fileName);

            if (response.StatusCode != HttpStatusCode.OK) 
                return null;

            if (!File.Exists(pathFile))
	        {
                try
                {
                    using (Stream output = File.OpenWrite(pathFile))
                    {
                        using (Stream input = response.GetResponseStream())
                        {
                            input.CopyTo(output);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return pathFile;
        }

        ///<summary>
        /// Get cookie value from Http/https response
        /// <param name="response">HttpWebResponse</param>
        /// <param name="Name Cookie">Cookie name to find in object response. If cookie is not found the value returned is null.</param>
        /// Return: string
        ///</summary>
        public string GetCookieValue(HttpWebResponse response, string nameCookie)
        {

            string tmp = null;
            string s = response.Headers["Set-Cookie"].ToString();

            string[] multipleCookies = s.Split(new char[] { ';', ',' });
            foreach (string word in multipleCookies)
            {
                //Console.WriteLine(word.ToString());
                if (word.IndexOf(nameCookie + "=") == 0)
                    tmp = word.Substring((nameCookie + "=").Length);
            }

            return tmp;
        }
    }
}
