﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SYM.Common
{
    public static class Utility
    {
        public static string ReadFile(string path, string fileName, bool ReadLastLine)
        {
            string tmp;

            try
            {
                if (!ReadLastLine)
                    tmp = System.IO.File.ReadAllText(Path.Combine(path, fileName));
                else
                    tmp = File.ReadLines(Path.Combine(path, fileName)).Last();
            }
            catch (Exception ex)
            {
                Console.WriteLine("File Not FOund " + ex.Message.ToString());
                tmp = "";
            }
            return tmp;
        }

        public static string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
    }
}
