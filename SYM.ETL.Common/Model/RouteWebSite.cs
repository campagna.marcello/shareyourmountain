﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SYM.Common.Model
{
    public class RouteWebSite
    {
        public string name { get; set; }
        public string tempoSalita { get; set; }
        public string imageUrl { get; set; }
        public string imageDesc { get; set; }
        public string difficolta { get; set; }
        public string primiSalitori { get; set; }
        public string esposizione { get; set; }
        public string montagna { get; set; }
        public string catena { get; set; }
        public string url { get; set; }
        public string dislivello { get; set; }
    }
}