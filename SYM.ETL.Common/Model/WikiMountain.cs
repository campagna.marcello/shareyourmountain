﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SYM.ModelWikipedia
{
    //Modello dei dati presenti nel file xml restituito da Wikipedia
    //TODO: Utilizzare la Data Annotations
    public class WikiMountain
    {
        public string name { get; set; }
        public string nameIT { get; set; }
        public string nameDE { get; set; }
        public string immagine { get; set; }
        public List<string> country { get; set; }
        public List<string> region { get; set; }
        public int altezza { get; set; }
        public float latitudine { get; set; }
        public float longitudine { get; set; }
        public string sezione { get; set; }
        public string sottosezione { get; set; }
        public string gruppo { get; set; }
        public string descrizione { get; set; }
        public string dataprimasalita { get; set; }
        public string primisalitori { get; set; }
        public bool valid { get; set; }
        public int WikipageId { get; set; }
        public string thumbnail { get; set; }
        public string urlResource { get; set; }

    }
}
