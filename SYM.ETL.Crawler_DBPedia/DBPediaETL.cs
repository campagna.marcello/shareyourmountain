﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using SYM;
using System.Xml;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using SYM.ModelWikipedia;
using SYM.Common;
using SYM.DataAccessLayer;
using Dufenergy.SystemFileUtility;

namespace SYM.Crawler_DBPedia
{

    //***************************************************
    // ETL (Extract, transform, load) - > Business Logic
    //***************************************************

    class DBPediaETL
    {
        private Logger logger;
        private string pathTempFolder;

        //Init
        public DBPediaETL(string _pathTempFolder)
        {
            pathTempFolder = _pathTempFolder;
            logger = LogManager.GetCurrentClassLogger();
        }

        private WikiMountain RequestMountainToDBPedia(string word)
        {
            WikiMountain obj = null;
            string pathTempFile = null;

            //replace with Upper Case 4 the first Letter
            word = word.Replace("*", "").Replace(" ","_");
            try
            {
                CrawlerHttp objApi = new CrawlerHttp();
                string url = "http://it.dbpedia.org/resource/" + word.Replace("*", "") + "/html?output=application%2Fld%2Bjson";
                HttpWebResponse response = objApi.Request_QueryApi(url, "it.dbpedia.org", "it.dbpedia.org");

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    pathTempFile = objApi.SaveHttpResponseBody(response, pathTempFolder, word.Replace("*", "").Replace("/", "") + ".dat");
                    obj = SerializeJsonToWikiObject(pathTempFile);
                    Console.WriteLine("\n >> Serialization Done...");
                }
                response.Close();
                return obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                logger.Error("Http Request for montain " + word + " : " + ex.Message);
                return obj;
            }
        }

        ///<summary>
        /// Check if exist mountain is already exist in final table mountain
        ///<param name="name">Mounatin name in temporary table ETl</param>
        ///</summary>
        //private bool ExistMountain(string name, int elevation = 0)
        //{
        //    EntitiesETL _context = new EntitiesETL;
        //    int num = _context.mountain.Where(a => a.mntn_name.ToLower() == name.ToLower()).Count();
        //    if (num > 0) 
        //        return true;
        //    else 
        //        return false;
        //}

        ///<summary>
        /// Check if exist mountain is already exist in final table mountain
        ///<param name="name">Mounatin name in temporary table ETl</param>
        ///</summary>
        private bool ExistEtlMountain(string name, int elevation = 0)
        {
            EntitiesETL _context = new EntitiesETL();
            int num = _context.etl_mountain.Where(a => a.mountain_name.ToLower() == name.ToLower() && (a.mountain_altitude == elevation || elevation == 0)).Count();
            if (num > 0)
                return true;
            else
                return false;
        }

        //Crawling from DBPEDIA.IT using mountain list in temp table
        public void SaveMountainsFromDBPedia()
        {

            EntitiesETL _context = new EntitiesETL();
            WikiMountain obj = null;

            var mountains = _context.etl_mountain.Where(a => a.osm == 1 && a.mountain_range_id == null && a.status == false && a.region_id_str.Contains("1735")).ToList();
            Console.WriteLine("Mountains to analyze ... " + mountains.Count);

            //Download data from wikipedia if necessary
            foreach (var m in mountains)
            {
                Console.WriteLine(m.mountain_name);
                obj = this.RequestMountainToDBPedia(m.mountain_name.ToString());
                updateEtlMountain(obj, m.mountain_name);
            }

            //Log Execution in table
            Console.WriteLine("Update Complete Mountains!!!");
        }

        //Crawling from Wikipedia using name string
        public void SaveMountainFromDBPedia(string mountain)
        {
            WikiMountain obj = null;
            obj = this.RequestMountainToDBPedia(mountain.ToString());
            if (!this.ExistEtlMountain(mountain))
            {
                obj = this.RequestMountainToDBPedia(mountain.ToString());
                if (obj.valid != false)
                    addEtlMountain(obj, mountain.ToString());
            }

        }

        //public void UpdateMountainsFromDBPedia() {

        //    Entities _context = new Entities();
        //    WikiMountain obj = null;

        //    try
        //    {
        //    var mountains = _context.mountain.Where(a => a.mntn_dbpedia.Count == 0).OrderByDescending(a => a.id).ToList();
        //    Console.WriteLine("Mountains to process ... " + mountains.Count);

        //    //Download data from wikipedia if necessary
        //    foreach (var m in mountains)
        //    {
        //        Console.WriteLine(m.mntn_name);

        //        obj = this.RequestMountainToDBPedia(m.mntn_name.ToString());
        //        if (obj.valid != false)
        //            insertMntnDBPedia(obj, m.mntn_name.ToString(),m.id);
                
        //    }

        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("Error during the download & update process of mountains data..." + e.Message);
        //    }

        
        //}

        #region ETL_Database
        public void addEtlMountain(WikiMountain tmp, string mntn)
        {

            try
            {
                EntitiesETL _context = new EntitiesETL();

                if (tmp.sottosezione == null) tmp.sottosezione = "";
                if (tmp.gruppo == null) tmp.gruppo = "";

                etl_mountain mnt = new etl_mountain
                {
                    mountain_name = mntn,
                    mountain_latitude = tmp.latitudine,
                    mountain_longitude = tmp.longitudine,
                    mountain_altitude = tmp.altezza,
                    mountain_range = tmp.sottosezione.Replace("resource:","").Replace("_"," "),
                    mountain_group = tmp.gruppo.Replace("resource:","").Replace("_"," "),
                    country_code = (tmp.country == null) ? "" : string.Join(",", tmp.country.ToArray()),
                    region_name = string.Join(",", tmp.region.ToArray()),
                    mountain_date = tmp.dataprimasalita,
                    mountain_latitude_str = tmp.latitudine.ToString(),
                    mountain_longitude_str = tmp.longitudine.ToString(),
                    @abstract = tmp.descrizione,
                    thumbnail = tmp.thumbnail,
                    wikipedia_pageid = tmp.WikipageId,
                    status = false
                };
                
                _context.etl_mountain.Add(mnt);

                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Insert" + ex.Message);
                logger.Error("Add EtlMountain into table >> " + mntn);
            }

        }

        private void updateEtlMountain(WikiMountain tmp, string mntn)
        {

            try
            {
                EntitiesETL _context = new EntitiesETL();
                etl_mountain etl = _context.etl_mountain.Where(a => a.mountain_name == mntn.Replace("_", " ")).First();

                if (tmp == null || String.IsNullOrEmpty(tmp.name) || String.IsNullOrEmpty(tmp.sottosezione) || String.IsNullOrEmpty(tmp.gruppo))
                {
                    etl.status = true;
                    etl.mountain_description = etl.mountain_description + " - check but data not found.";
                    _context.SaveChanges();
                    return;
                }
                else
                {
                    etl.mountain_group = tmp.gruppo;
                    //etl.mountain_latitude = tmp.latitudine;
                    //etl.mountain_longitude = tmp.longitudine;
                    etl.mountain_altitude = tmp.altezza;
                    etl.mountain_range = tmp.sottosezione;
                    etl.country_code = string.Join(",", tmp.country.ToArray());
                    etl.region_name = string.Join(",", tmp.region.ToArray());
                    etl.mountain_date = tmp.dataprimasalita;
                    etl.@abstract = tmp.descrizione;
                    etl.thumbnail = tmp.thumbnail;
                    etl.wikipedia_pageid = tmp.WikipageId;
                    etl.status = true;
                    _context.SaveChanges();
                }
                   

               

                

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Update" + ex.Message);
                logger.Error("UpdateEtlMountain into table >> " + tmp.name);
            }

        }

        //private void insertMntnDBPedia(WikiMountain tmp, string mntn, int id)
        //{

        //    try
        //    {
        //        if (String.IsNullOrEmpty(tmp.descrizione))
        //            return;

        //        Entities _context = new Entities();

        //        mntn_dbpedia dbpedia = new mntn_dbpedia();
        //        dbpedia.dbpedia_id = mntn.Replace("*", "").Replace(" ", "_");
        //        dbpedia.dbpedia_abstract = tmp.descrizione;
        //        dbpedia.mountain_id = id;
        //        dbpedia.dbpedia_wikipageId = tmp.WikipageId;
        //        dbpedia.dbpedia_url = tmp.urlResource;
        //        dbpedia.language_id = 1;

        //        _context.mntn_dbpedia.Add(dbpedia);
        //        _context.SaveChanges();

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error during the insert in mntn_dbpedia... " + ex.Message);
        //        logger.Error("Error during the insert in mntn_dbpedia...  >> " + tmp.name);
        //    }

        //}

        //public void add_AssCountryMountain(int id, string name, int country_id, string code)
        //{

        //    try
        //    {
        //        Entities _context = new Entities();

        //        ass_mountaincountry ass_mnt = new ass_mountaincountry
        //        {
        //            mntn_id = id,
        //            mntn_name = name,
        //            country_id = country_id,
        //            country_code = code
        //        };

        //        //Salvo i dati se è presente almeno il nome della montagna
        //        if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(code))
        //            _context.ass_mountaincountry.Add(ass_mnt);

        //        _context.SaveChanges();

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error Insert" + ex.Message);
        //        logger.Error("ÂddEtlMountain into table >> " + name);
        //    }

        //}

        //public void add_AssRegionMountain(int id, string nameRegion, int country_id, int regionId)
        //{

        //    try
        //    {
        //        Entities _context = new Entities();

        //        ass_mountainregion ass_mnt = new ass_mountainregion
        //        {
        //            mntn_id = id,
        //            region_id = regionId,
        //            region_name = nameRegion,
        //            country_id = country_id
        //        };

        //        //Salvo i dati se è presente almeno il nome della montagna
        //        if (!String.IsNullOrEmpty(nameRegion))
        //            _context.ass_mountainregion.Add(ass_mnt);

        //        _context.SaveChanges();

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error Insert" + ex.Message);
        //        logger.Error("ÂddEtlMountain into table >> " + nameRegion);
        //    }

        //}

        //***TODO:*** Normalizate data from ETL TABLE to FINAL TABLE
        //***TODO:*** Normalization data releated to Region
        //public void NormalizationCountryEtlMountain()
        //{
        //    try
        //    {

        //        Entities _context = new Entities();
        //        //Recuper la lista delle montagne con l'associazione dell'id
        //        var etlMountain = _context.C_etl_mountain.Where(a => a.mntn_id_final != null && a.region_id == 1744).ToList();

        //        foreach (var mnt in etlMountain)
        //        {
        //            string[] country = mnt.country_code.Split(',');

        //            foreach (string c in country)
        //            {
        //                Console.WriteLine(c + " " + mnt.mountain_name);
        //                //Check if exist in table ass mountain vs country
        //                int i = _context.ass_mountaincountry.Where(a => a.country_code.ToUpper() == c.ToUpper() && a.mntn_id == mnt.mntn_id_final).Count();
        //                int idmnt = mnt.mntn_id_final ?? default(int);

        //                if (i == 0)
        //                    this.add_AssCountryMountain(idmnt, mnt.mountain_name, getCountryId(c), c.ToUpper());
        //            }

        //            //Implementare tabella di mappatura dei codici di Wikipedia con le regione Salvate nella tabella apposita
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error("Error: " + ex.Message);

        //    }

        //}

        //***TODO:*** Normalizate data from ETL TABLE to FINAL TABLE
        //Normalization data releated to Region
        //public void NormalizationRegionEtlMountain()
        //{

        //    Entities _context = new Entities();
        //    //Recuper la lista delle montagne con l'associazione dell'id
        //    var etlMountain = _context.C_etl_mountain.Where(a => a.mntn_id_final != null && a.region_id == 1744).ToList();

        //    foreach (var mnt in etlMountain)
        //    {
        //        //gestione dei casi particolari con [[distretto...
        //        string[] region = mnt.region_name.Split('[')[0].Split(',');

        //        foreach (string c in region)
        //        {
        //            try
        //            {
        //                //Try Region Data with JOIN
        //                Console.WriteLine(c.Replace("{{", "").Replace("}}", "") + " " + mnt.mountain_name + " ");
        //                geo_region re = null;

        //                if (c.Replace("{{", "").Replace("}}", "") == "IT-TAA" || c.Replace("{{", "").Replace("}}", "") == "IT-VEN")
        //                    re = _context.geo_region.Where(a => a.region_wikipedia == c.Replace("{{", "").Replace("}}", "")).First();

        //                if (re == null)
        //                {
        //                    //logger.Error("Region not found in table Geo_Region:");
        //                    //Skip item
        //                    continue;
        //                }

        //                Console.WriteLine(">>" + re.region_name);
        //                //Check if exist in table ass mountain vs region
        //                int i = _context.ass_mountainregion.Where(a => a.region_id == re.id && a.mntn_id == mnt.mntn_id_final).Count();
        //                int idmnt = mnt.mntn_id_final ?? default(int);
        //                int idcountry = re.country_id;

        //                //add region in ass table
        //                if (i == 0)
        //                    this.add_AssRegionMountain(idmnt, re.region_name, getCountryId(re.country_code), re.id);
        //            }
        //            catch (Exception ex)
        //            {
        //                //logger.Error("Region not found in table Geo_Region: " + ex.Message);
        //                //Skip item
        //                continue;
        //            }
        //        }

        //    }


        //}
        #endregion

        private int getCountryId(string code)
        {

            int id = 0;

            switch (code.ToUpper())
            {
                case "IT":
                    id = 116;
                    break;
                case "CH":
                    id = 217;
                    break;
                case "FR":
                    id = 65;
                    break;
                case "DE":
                    id = 70;
                    break;
                case "AT":
                    id = 15;
                    break;
                case "SI":
                    id = 207;
                    break;
                case "LI":
                    id = 129;
                    break;
            }

            return id;
        }

        ///<summary>
        /// Parsing JSON Output in object WikiMountain
        ///<param name="ListOfEntry"></param>
        ///<param name="filePath"></param>
        ///<param name="typeEvent">Planned or Unplanned</param>
        ///</summary>
        public WikiMountain SerializeJsonToWikiObject(string pathFileToLoad)
        {
            XmlDocument doc = new XmlDocument();
            WikiMountain objectWiki = new WikiMountain();
            objectWiki.region = new List<string>();
            objectWiki.country = new List<string>();
            objectWiki.valid = true;

            StreamReader streamReader = new StreamReader(@pathFileToLoad);
            string json = streamReader.ReadToEnd();
            streamReader.Close();

            try
            {
                DBPedia_Mountain node = JsonConvert.DeserializeObject<DBPedia_Mountain>(json, new JsonSerializerSettings
                {
                    Error = (sender, args) =>
                    {
                        Console.WriteLine(args.ErrorContext.Error.Message);
                        args.ErrorContext.Handled = true;
                    }
                });

                if (node.Graph == null)
                    //Console.WriteLine("Object DBPedia is null ...check the name of the mountain");
                    objectWiki.valid = false;
                else
                {
                    Console.WriteLine("Object DBPedia Exist");
                    //Assign the istance of WikiMountain after the revision of class with new properties...

                    objectWiki.altezza = Convert.ToInt32(node.Graph.First().PropertyAltezza);
                    if (node.Graph.First().Abstract != null && node.Graph.First().Abstract.Language == "it") 
                        objectWiki.descrizione = node.Graph.First().Abstract.Value;
                    objectWiki.gruppo = node.Graph.First().Gruppo;
                    objectWiki.sottosezione = node.Graph.First().AlpsSubsection.Replace("resource:", "");
                    objectWiki.sezione = node.Graph.First().Sezione;
                    objectWiki.dataprimasalita = node.Graph.First().Dataprimasalita;
                    objectWiki.WikipageId = node.Graph.First().OntologyWikiPageID;
                    objectWiki.country = node.Graph.First().OntologyIso31661Code;
                    objectWiki.thumbnail = node.Graph.First().Thumbnail;
                    objectWiki.latitudine = node.Graph.First().PropertyLatitudineD;
                    objectWiki.longitudine = node.Graph.First().PropertyLongitudineD;
                    objectWiki.urlResource = node.Graph.First().WasDerivedFrom;
                    objectWiki.name = node.Graph.First().Id.Replace("resource:", "");
                }
                           
            }
            catch (Exception e)
            {
                logger.Error("Parsing Json " + e.Message);
                objectWiki.valid = false;
                return null;
            }

            return objectWiki;
        }
    }
}
