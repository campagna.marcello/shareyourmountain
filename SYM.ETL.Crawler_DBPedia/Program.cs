﻿using Dufenergy.SystemFileUtility;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SYM.Crawler_DBPedia
{
    class Program
    {

        #region DeclareVariables
        static private string pathFolderTemp;
        static private string pathFolderLog;
        static private SystemFileUtility obj;
        static private Logger logger;
        #endregion

        static void Main(string[] args)
        {

            string mountainTmp = null;

            //Init
            obj = SystemFileUtility.Instance;
            logger = LogManager.GetCurrentClassLogger();

            Console.WriteLine("***************************************************");
            Console.WriteLine("SHAREYOURMOUNTAIN - Crawler 4 IT.DBPedia.ORG");
            Console.WriteLine("Template Project");
            Console.WriteLine("***************************************************\n");

            //Setting main directories of console application
            pathFolderTemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ETL.Crawler_DBPedia.Properties.Settings.Default.tempFolder);
            pathFolderLog = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ETL.Crawler_DBPedia.Properties.Settings.Default.logFolder);

            List<String> listFolders = new List<String>();
            listFolders.Add(pathFolderTemp);
            listFolders.Add(pathFolderLog);

            foreach (string pathFolder in listFolders)
            {
                bool isExists = System.IO.Directory.Exists(pathFolder);
                if (!isExists)
                    System.IO.Directory.CreateDirectory(pathFolder);
            }

            //TODO: Create Folder
            logger.Debug("START LOG");

            //************************************************
            //Console.WriteLine("\n>> Init Console Parameters");
            //***********************************************
            var options = new Options();
            int type_task = 0;

            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                if (!String.IsNullOrEmpty(options.mountain))
                {
                    logger.Info(">> Init Parameter");
                    mountainTmp = options.mountain;
                    type_task = options.type;
                }
                else
                {
                    Console.WriteLine("\n\n>> Parametri Assenti");
                    logger.Info(">> Parametri Assenti");
                }
            }

            //BL CRAWLING
            //***************************
            //CRAWLING WEB SITE
            //***************************
            DBPediaETL DBPediaEtl = new DBPediaETL(pathFolderTemp);
           
            try
            {

                switch (type_task)
                {
                    case 1:
                        Console.WriteLine(">> Download data of ONE Mountain in temporary table- " + mountainTmp);
                        if (!String.IsNullOrEmpty(mountainTmp))
                            DBPediaEtl.SaveMountainFromDBPedia(mountainTmp);
                        break;
                    case 2:
                        Console.WriteLine(">> Download data of List of MountainS in temporary table");
                        DBPediaEtl.SaveMountainsFromDBPedia();
                        break;
                    //case 3:
                    //    Console.WriteLine(">> Download & Update existing Mountains");
                    //    DBPediaEtl.UpdateMountainsFromDBPedia();
                    //    break;
                    //case 4:
                    //    Console.WriteLine(">> Normalization data of Region (ass_mountain_region).");
                    //    DBPediaEtl.NormalizationRegionEtlMountain();
                    //    break;
                }

            }
            catch (Exception ex)
            {
                //Last error with resource:Ulrichshorn
                logger.Error(ex.Message);
                throw;
            }

            Console.WriteLine("\n\n>> END");
            logger.Info("END LOG");
        }
    }
}
