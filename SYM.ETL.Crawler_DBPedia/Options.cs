﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace SYM.Crawler_DBPedia
{
    class Options
    {
        //[Option('v', null, HelpText = "Print details during execution.")]
        //public bool Verbose { get; set; }

        [Option("type", Required = true, HelpText = "Type of Crawling")]
        public int type { get; set; }

        //[Option("dateTo", Required = false, HelpText = "Date To")]
        //public string dateTo { get; set; }

        [Option("mountain", Required = false, HelpText = "Mountain Name")]
        public string mountain { get; set; }

        //[Option("folderOut", Required = false, HelpText = "Destination path for files zip/unzip")]
        //public string folderOut { get; set; }

        [Option("unzip", DefaultValue = "false", Required = false, HelpText = "zip/unzip file xxx_AGGR_xxx.zip")]
        public string unzip { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            // this without using CommandLine.Text
            //  or using HelpText.AutoBuild
            var usage = new StringBuilder();
            usage.AppendLine("");
            usage.AppendLine("Quickstart Application 1.0");
            usage.AppendLine("Read user manual for usage instructions...");
            return usage.ToString();
        }
    }
}
