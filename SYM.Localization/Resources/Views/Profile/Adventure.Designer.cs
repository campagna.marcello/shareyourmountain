﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SYM.Localization.Resources.Views.Profile {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Adventure {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Adventure() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SYM.Localization.Resources.Views.Profile.Adventure", typeof(Adventure).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Altitude.
        /// </summary>
        public static string Altitude {
            get {
                return ResourceManager.GetString("Altitude", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ascent.
        /// </summary>
        public static string Ascent {
            get {
                return ResourceManager.GetString("Ascent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date Ascent.
        /// </summary>
        public static string DateAscent {
            get {
                return ResourceManager.GetString("DateAscent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Done.
        /// </summary>
        public static string Done {
            get {
                return ResourceManager.GetString("Done", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Adventure.
        /// </summary>
        public static string EditTitle {
            get {
                return ResourceManager.GetString("EditTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grade.
        /// </summary>
        public static string Grade {
            get {
                return ResourceManager.GetString("Grade", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mountain.
        /// </summary>
        public static string Mountain {
            get {
                return ResourceManager.GetString("Mountain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mountains.
        /// </summary>
        public static string Mountains {
            get {
                return ResourceManager.GetString("Mountains", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Range.
        /// </summary>
        public static string Range {
            get {
                return ResourceManager.GetString("Range", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ranking.
        /// </summary>
        public static string Ranking {
            get {
                return ResourceManager.GetString("Ranking", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Region.
        /// </summary>
        public static string Region {
            get {
                return ResourceManager.GetString("Region", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Route.
        /// </summary>
        public static string Route {
            get {
                return ResourceManager.GetString("Route", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Routes.
        /// </summary>
        public static string Routes {
            get {
                return ResourceManager.GetString("Routes", resourceCulture);
            }
        }
    }
}
