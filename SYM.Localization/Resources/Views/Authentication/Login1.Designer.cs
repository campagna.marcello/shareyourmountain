﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SYM.Localization.Resources.Views.Authentication {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Login {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Login() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SYM.Localization.Resources.Views.Authentication.Login", typeof(Login).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back.
        /// </summary>
        public static string Back {
            get {
                return ResourceManager.GetString("Back", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Country.
        /// </summary>
        public static string Country {
            get {
                return ResourceManager.GetString("Country", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Don&apos;t have an account yet ?.
        /// </summary>
        public static string DontHaveAccount {
            get {
                return ResourceManager.GetString("DontHaveAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your account details below:.
        /// </summary>
        public static string EnterAccountDetails {
            get {
                return ResourceManager.GetString("EnterAccountDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your e-mail to reset it..
        /// </summary>
        public static string EnterEmailToReset {
            get {
                return ResourceManager.GetString("EnterEmailToReset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter your personal details below:.
        /// </summary>
        public static string EnterPersonalDetails {
            get {
                return ResourceManager.GetString("EnterPersonalDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Name.
        /// </summary>
        public static string FirstName {
            get {
                return ResourceManager.GetString("FirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Forgot Password?.
        /// </summary>
        public static string ForgotPassword {
            get {
                return ResourceManager.GetString("ForgotPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Name.
        /// </summary>
        public static string LastName {
            get {
                return ResourceManager.GetString("LastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create an account.
        /// </summary>
        public static string NewAccount {
            get {
                return ResourceManager.GetString("NewAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Or login with.
        /// </summary>
        public static string OrLoginWith {
            get {
                return ResourceManager.GetString("OrLoginWith", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Region.
        /// </summary>
        public static string Region {
            get {
                return ResourceManager.GetString("Region", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remember me.
        /// </summary>
        public static string RememberMe {
            get {
                return ResourceManager.GetString("RememberMe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reset.
        /// </summary>
        public static string Reset {
            get {
                return ResourceManager.GetString("Reset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reset Password.
        /// </summary>
        public static string ResetPassword {
            get {
                return ResourceManager.GetString("ResetPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Re-type Your Password.
        /// </summary>
        public static string ReTypePassword {
            get {
                return ResourceManager.GetString("ReTypePassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send Request.
        /// </summary>
        public static string SendRequest {
            get {
                return ResourceManager.GetString("SendRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sign Up in 45 seconds..
        /// </summary>
        public static string SignUp {
            get {
                return ResourceManager.GetString("SignUp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submit.
        /// </summary>
        public static string Submit {
            get {
                return ResourceManager.GetString("Submit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beta.
        /// </summary>
        public static string SubTitle {
            get {
                return ResourceManager.GetString("SubTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Welcome..
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
    }
}
