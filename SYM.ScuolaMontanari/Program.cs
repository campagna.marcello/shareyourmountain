﻿using HtmlAgilityPack;
using SYM.DataAccessLayer;
using SYM.Common;
using SYM.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SYM.ScuolaMontanari
{
    class Program
    {
        #region DeclareVariables
        static private string pathFolderTemp;
        static private string pathFolderLog;
        static EntitiesETL _context = new EntitiesETL();
        #endregion
        
        static void Main(string[] args)
        {

            Console.WriteLine("***************************************************");
            Console.WriteLine("SHAREYOURMOUNTAIN - Get Images From WebSites");
            Console.WriteLine("Template Project");
            Console.WriteLine("***************************************************\n");

            //Setting main directories of console application
            pathFolderTemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp");

            ParsingHtmlToGetImage(Path.Combine(pathFolderTemp,"scuolamontanari.html"));

        }

        private static void ParsingHtmlToGetImage(string filePath)
        {
            //List<Itinerario> listOfEntry = new List<Itinerario>();

            

            try
            {
                HtmlDocument doc = new HtmlDocument();

                doc.Load(@filePath);

                //Filtro non funziona
                List<HtmlNode> list = doc.DocumentNode.SelectNodes("//tr").ToList();

                foreach (HtmlNode node in list)
                {

                    RouteWebSite obj = new RouteWebSite();
                   
                    int i = 1;
                    foreach (HtmlNode column in node.ChildNodes.ToList())
                    {

                        if (column.Name != "td") 
                            continue;

                        //Console.WriteLine(">> " + column.InnerHtml);
                        switch (i)
                        {
                            case 1:
                                Console.WriteLine("catena: " + column.InnerHtml.Trim());
                                obj.catena = column.InnerHtml.Trim();
                                break;
                            case 2:
                                Console.WriteLine("Montagna: " + column.InnerHtml.Trim());
                                obj.montagna = column.InnerHtml.Trim();
                                break;
                            case 3:                             
                                string[] words = column.InnerHtml.Split('#');
                                Console.WriteLine("Url: " + words[0].ToString());
                                obj.url = words[0].ToString().Trim();
                                break;
                            case 4:
                                Console.WriteLine("Route: " + column.InnerHtml.Trim());
                                obj.name = column.InnerHtml.Trim();
                                break;
                            case 5:
                                Console.WriteLine("Difficoltà: " + column.InnerHtml.Trim());
                                obj.difficolta = column.InnerHtml.Trim();
                                break;
                            case 6:
                                Console.WriteLine("Dislivello: " + column.InnerHtml.Replace("m","").Trim());
                                obj.dislivello = column.InnerHtml.Replace("m", "").Trim();
                                break;
                            case 7:
                                Console.WriteLine("Ripetizione: " + column.InnerHtml.Trim());
                                if (obj.url != "../arrampiCAI/relazioni/nuovo.pdf" && obj.montagna != "Pilastri Bandiarac")
                                { 
                                    AddRouteTemp(obj);
                                }
                                break;
                        }

                        i++;
                    }
                }

                //Console.WriteLine(obj.imageUrl);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message + "\r\n" + e.StackTrace);
                //return null;
            }

            //return null;
        }

        private static bool AddRouteTemp(RouteWebSite tmp) {

            etl_route_temp objroute = new etl_route_temp();
            objroute.source_id = 18;
            objroute.route_name = tmp.name;
            objroute.mountain_name = tmp.montagna;
            objroute.route_url_report = tmp.url;
            objroute.mountain_group_name = tmp.catena;
            objroute.route_difference = Convert.ToInt32(tmp.dislivello);
            objroute.difficulty_str = tmp.difficolta;
            objroute.status = 1;

            _context.etl_route_temp.Add(objroute);
            _context.SaveChanges();

            return true;
        }
    }
}
