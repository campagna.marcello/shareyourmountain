﻿//using Dufenergy.SystemFileUtility;
using HtmlAgilityPack;
using NLog;
using SYM.Common;
using SYM.DataAccessLayer;
using SYM.GetImagesFromWebSites.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SYM.GetImagesFromWebSites
{
    class Program
    {
        #region DeclareVariables
        static private string pathFolderTemp;
        static private string pathFolderLog;
        static private Logger logger;
        static EntitiesETL _context = new EntitiesETL();
        #endregion

        static void Main(string[] args)
        {
            //Init
            //var obj = SystemFileUtility.Instance;
            logger = LogManager.GetCurrentClassLogger();

            Console.WriteLine("***************************************************");
            Console.WriteLine("SHAREYOURMOUNTAIN - Get Images From WebSites");
            Console.WriteLine("Template Project");
            Console.WriteLine("***************************************************\n");

            //Setting main directories of console application
            pathFolderTemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ETL.GetImagesFromWebSites.Properties.Settings.Default.tempFolder);
            pathFolderLog = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ETL.GetImagesFromWebSites.Properties.Settings.Default.logFolder);

            List<String> listFolders = new List<String>();
            listFolders.Add(pathFolderTemp);
            listFolders.Add(pathFolderLog);

            foreach (string pathFolder in listFolders)
            {
                bool isExists = System.IO.Directory.Exists(pathFolder);
                if (!isExists)
                    System.IO.Directory.CreateDirectory(pathFolder);
            }

            //TODO: Create Folder
            logger.Debug("START LOG");

            //************************************************
            //Console.WriteLine("\n>> Init Console Parameters");
            //***********************************************
            var options = new Options();
            int type_task = 0;

            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {
                if (!String.IsNullOrEmpty(options.mountain))
                {
                    logger.Info(">> Init Parameter");
                    //mountainTmp = options.mountain;
                    type_task = options.type;
                }
                else
                {
                    Console.WriteLine("\n\n>> Parametri Assenti");
                    logger.Info(">> Parametri Assenti");
                }
            }

            //BL CRAWLING
            //Load file and PArsing node of file xml.
            //EntitiesETL _context = new EntitiesETL();
            List<v_route_url> objects = _context.v_route_url.Where(a => a.source_id == 1).ToList();
            Console.WriteLine("N° mountains to import: " + objects.Count());

            //Check list and use object if name is not empty and altitude > 600
            foreach (v_route_url m in objects)
            {
                Console.WriteLine(m.route_url);
                string file = RequestMountainToDBPedia(m.route_url, "www.scuolaguidodellatorre.it", m.route_id.ToString());
                RouteWebSite result = ParsingHtmlToGetImage(file);

                //Insert data in temporary table
                int exist = ExistRouteImage(m.route_url,1);

                //Add Object if info has been extracted by the page and the record doesn't exist in the table
                if (exist == 0 && result != null)
                {
                    //DownloadImages in filesystem and update record into table
                    string[] words = result.imageUrl.Split('/');
                    string localFilename = Path.Combine(pathFolderTemp, m.route_id.ToString() + "_"+ m.source_id.ToString() +"_" +words[words.Length-1]);
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile("http://www.scuolaguidodellatorre.it/relazioni/"+result.imageUrl.Replace("./",""), localFilename);
                    }

                    etl_route_image objrouteImg = new etl_route_image();
                    objrouteImg.route_id = m.route_id;
                    objrouteImg.image_url = "http://www.scuolaguidodellatorre.it/relazioni/" + result.imageUrl.Replace("./", "");
                    objrouteImg.image_cover = true;
                    objrouteImg.source_id = 1;
                    objrouteImg.image_descr = result.imageDesc;
                    objrouteImg.image_filename = m.route_id.ToString() + "_" + m.source_id.ToString() + "_" + words[words.Length-1];
                    _context.etl_route_image.Add(objrouteImg);
                    _context.SaveChanges();
                }

            }

            //***************************
            //CRAWLING WEB SITE
            //***************************
        }

        //VIEW To Etract url of each routes inside the table and execute the crawler to get the images include
        private void GetUrl(){
        }

        private void ParsingHml() { 
        }

        ///<summary>
        /// Serialize Table Html in Object List to convert it in file xml
        ///<param name="ListOfEntry"></param>
        ///<param name="filePath"></param>
        ///</summary>
        private static RouteWebSite ParsingHtmlToGetImage(string filePath)
        {
            //List<Itinerario> listOfEntry = new List<Itinerario>();

            RouteWebSite obj = new RouteWebSite();

            try
            {
                HtmlDocument doc = new HtmlDocument();

                doc.Load(@filePath);

                //Filtro non funziona
                //HtmlNode link = doc.DocumentNode.SelectNodes("//a").ToList();

                foreach (HtmlNode node in doc.DocumentNode.SelectNodes("//img").ToList())
                {
                   
                    //Console.WriteLine(">> " + node.InnerHtml);

                    foreach (HtmlAttribute a in node.Attributes)
                    {
                        if (a.Name == "src")
                        {
                            if (a.Value.ToString() != "/images/saveAsPdf.gif" && a.Value.ToString() != "/images/logo.jpg") 
                            {
                                Console.WriteLine(">> " + a.Value);
                                obj.imageUrl = a.Value;
                                obj.imageDesc = node.Attributes[1].Value.ToString();
                                return obj;
                            }
                        }
                    }
                }

                Console.WriteLine(obj.imageUrl);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message + "\r\n" + e.StackTrace);
                return null;
            }

            return null;
        }

        private void DownloadImage() { 
        }

        private static string RequestMountainToDBPedia(string url, string referer, string id)
        {
            string pathTempFile = null;

            //replace with Upper Case 4 the first Letter
            try
            {
                CrawlerHttp objApi = new CrawlerHttp();
                HttpWebResponse response = objApi.Request_QueryApi(url, referer, referer);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    pathTempFile = objApi.SaveHttpResponseBody(response, pathFolderTemp, id + ".html");
                    Console.WriteLine("\n >> Serialization Done...");
                }
                response.Close();
                return pathTempFile;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                logger.Error("Http Request -> " + url+ " : " + ex.Message);
                return null;
            }
        }

        private static int ExistRouteImage(string url, int sourceId)
        {
            var list = _context.etl_route_image.Where(a => a.image_url.Contains(url) && a.source_id == sourceId).ToList();
            return list.Count();
        }
        //
    }
}
