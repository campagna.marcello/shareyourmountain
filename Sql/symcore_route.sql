-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: symcore
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_name` varchar(200) DEFAULT NULL,
  `route_title` varchar(300) DEFAULT NULL,
  `route_area` varchar(45) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `activityExt_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `mountain_id` int(11) DEFAULT NULL,
  `mntnGroup_id` int(11) DEFAULT NULL,
  `mntnRange_id` int(11) DEFAULT NULL,
  `valley_id` int(11) DEFAULT NULL,
  `route_facing` varchar(10) DEFAULT NULL,
  `route_length` int(11) DEFAULT '0' COMMENT 'sviluppo via, convertion from char to int',
  `route_lengthDescr` varchar(250) DEFAULT NULL,
  `route_altitude` smallint(6) DEFAULT NULL COMMENT 'Max Altitude / Elevation',
  `route_height` smallint(6) DEFAULT NULL COMMENT 'Altitude / Elevation difference of wall / route',
  `route_approachAltitude` smallint(6) DEFAULT NULL COMMENT 'altitudine avvicinamento',
  `route_approachHeight` smallint(6) DEFAULT NULL COMMENT 'Dislivello Avvicinamento',
  `route_approachLength` smallint(6) DEFAULT NULL,
  `difficulty_grade` varchar(25) DEFAULT NULL,
  `route_lat` float DEFAULT NULL,
  `route_lng` float DEFAULT NULL,
  `route_pitchAsc` smallint(6) DEFAULT NULL COMMENT 'Number of pitches during the climb',
  `route_pitchDesc` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `route_status` bit(1) DEFAULT b'1',
  `route_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `route_dateMod` timestamp NULL DEFAULT NULL,
  `route_firstAscDate` datetime DEFAULT NULL COMMENT 'First Ascent Date',
  `route_firstAscClimbers` varchar(65) DEFAULT NULL COMMENT 'First Ascent Climb',
  `parking_id` int(11) DEFAULT NULL,
  `trackgps_id` int(11) DEFAULT NULL,
  `route_ranking` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_route_mountain_idx` (`mountain_id`),
  KEY `fk_route_activity_id_idx` (`activity_id`),
  KEY `fk_route_country_id_idx` (`country_id`),
  KEY `fk_route_country_mountainRange_id_idx` (`mntnRange_id`),
  KEY `fk_route_region_id_idx` (`region_id`),
  KEY `fk_route_country_mountainGroup_id_idx` (`mntnGroup_id`),
  KEY `fk_route_valley_id_idx` (`valley_id`),
  KEY `fk_route_city_id_idx` (`city_id`),
  KEY `fk_route_parking_idx` (`parking_id`),
  KEY `fk_route_trackgps_idx` (`trackgps_id`),
  KEY `fk_route_user_idx` (`user_id`),
  CONSTRAINT `fk_route_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `act_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_city_id` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_country_mountainGroup_id` FOREIGN KEY (`mntnGroup_id`) REFERENCES `mntn_mountaingroup` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_country_mountainRange_id` FOREIGN KEY (`mntnRange_id`) REFERENCES `mntn_mountainrange` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_mountain` FOREIGN KEY (`mountain_id`) REFERENCES `mountain` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_parking` FOREIGN KEY (`parking_id`) REFERENCES `parking` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_region` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_trackgps` FOREIGN KEY (`trackgps_id`) REFERENCES `trackgps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_user` FOREIGN KEY (`user_id`) REFERENCES `user_profile` (`profile_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_valley` FOREIGN KEY (`valley_id`) REFERENCES `valley` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10290 DEFAULT CHARSET=utf8 COMMENT='Table which contain generic data of routes of all activities';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-04 23:59:00
