-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: symcore
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accomodation`
--

DROP TABLE IF EXISTS `accomodation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accomodation` (
  `id` int(11) NOT NULL,
  `accmd_name` varchar(45) DEFAULT NULL,
  `accmd_type` char(1) DEFAULT NULL,
  `accmd_lat` float(10,8) DEFAULT NULL,
  `accmd_lng` float(10,8) DEFAULT NULL,
  `accmd_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `accmd_dateMod` timestamp NULL DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of all Bivacchi / Rifugi / Hotel';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `act_activity`
--

DROP TABLE IF EXISTS `act_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `act_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(45) DEFAULT NULL,
  `activity_description` varchar(45) DEFAULT NULL,
  `activity_parent_id` int(11) DEFAULT NULL,
  `activity_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activity_status` bit(1) DEFAULT b'1',
  `activity_order` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activity_parentActivity_idx` (`activity_parent_id`),
  CONSTRAINT `fk_activity_parentActivity` FOREIGN KEY (`activity_parent_id`) REFERENCES `act_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `act_difficulty`
--

DROP TABLE IF EXISTS `act_difficulty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `act_difficulty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actDifficulty_name` varchar(45) NOT NULL,
  `actDifficulty_grade` varchar(5) NOT NULL,
  `actDifficulty_scale` varchar(45) DEFAULT NULL,
  `actDifficulty_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `activity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activityDifficulty_activity_id_idx` (`activity_id`),
  CONSTRAINT `fk_activityDifficulty_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `act_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `act_translate`
--

DROP TABLE IF EXISTS `act_translate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `act_translate` (
  `id` int(11) NOT NULL,
  `actTransl_name` varchar(45) DEFAULT NULL,
  `actTransl_descr` varchar(45) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_activity_translate_id_idx` (`activity_id`),
  CONSTRAINT `fk_activity_translate_id` FOREIGN KEY (`activity_id`) REFERENCES `act_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aspnetroles`
--

DROP TABLE IF EXISTS `aspnetroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aspnetroles` (
  `Id` varchar(128) NOT NULL,
  `Name` varchar(256) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aspnetuserclaims`
--

DROP TABLE IF EXISTS `aspnetuserclaims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aspnetuserclaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` varchar(128) NOT NULL,
  `ClaimType` longtext,
  `ClaimValue` longtext,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `ApplicationUser_Claims` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aspnetuserlogins`
--

DROP TABLE IF EXISTS `aspnetuserlogins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aspnetuserlogins` (
  `LoginProvider` varchar(128) NOT NULL,
  `ProviderKey` varchar(128) NOT NULL,
  `UserId` varchar(128) NOT NULL,
  PRIMARY KEY (`LoginProvider`,`ProviderKey`,`UserId`),
  KEY `ApplicationUser_Logins` (`UserId`),
  CONSTRAINT `ApplicationUser_Logins` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aspnetuserroles`
--

DROP TABLE IF EXISTS `aspnetuserroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aspnetuserroles` (
  `UserId` varchar(128) NOT NULL,
  `RoleId` varchar(128) NOT NULL,
  PRIMARY KEY (`UserId`,`RoleId`),
  KEY `IdentityRole_Users` (`RoleId`),
  CONSTRAINT `ApplicationUser_Roles` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `IdentityRole_Users` FOREIGN KEY (`RoleId`) REFERENCES `aspnetroles` (`Id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aspnetusers`
--

DROP TABLE IF EXISTS `aspnetusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aspnetusers` (
  `Id` varchar(128) NOT NULL,
  `Email` varchar(256) DEFAULT NULL,
  `EmailConfirmed` tinyint(1) NOT NULL,
  `PasswordHash` longtext,
  `SecurityStamp` longtext,
  `PhoneNumber` longtext,
  `PhoneNumberConfirmed` tinyint(1) NOT NULL,
  `TwoFactorEnabled` tinyint(1) NOT NULL,
  `LockoutEndDateUtc` datetime DEFAULT NULL,
  `LockoutEnabled` tinyint(1) NOT NULL,
  `AccessFailedCount` int(11) NOT NULL,
  `UserName` varchar(64) NOT NULL,
  `dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Custom field to track the record data Insert ',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ass_mountaincountry`
--

DROP TABLE IF EXISTS `ass_mountaincountry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ass_mountaincountry` (
  `mntn_id` int(11) NOT NULL,
  `mntn_name` varchar(100) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `country_code` varchar(5) DEFAULT NULL,
  `dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mntn_id`,`country_id`),
  KEY `fk_ass_mountain_country_id_idx` (`country_id`),
  CONSTRAINT `fk_ass_mountain_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ass_mountain_mountain_id` FOREIGN KEY (`mntn_id`) REFERENCES `mountain` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabella di associazione delle montagne con le nazioni. Non è possibile specificare anche la regione perchè alcune montagne possono confinare su una nazione su piu regioni.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ass_mountainregion`
--

DROP TABLE IF EXISTS `ass_mountainregion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ass_mountainregion` (
  `mntn_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `region_name` varchar(45) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mntn_id`,`region_id`),
  KEY `fk_ass_mountainRegion_country_id_idx` (`country_id`),
  KEY `fk_ass_mountainRegion_region_id_idx` (`region_id`),
  CONSTRAINT `fk_ass_mountainRegion_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ass_mountainRegion_mountain_id` FOREIGN KEY (`mntn_id`) REFERENCES `mountain` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ass_mountainRegion_region_id` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `geo_city`
--

DROP TABLE IF EXISTS `geo_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geo_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(45) DEFAULT NULL,
  `city_population` varchar(45) DEFAULT NULL,
  `city_latitude` float(9,7) DEFAULT NULL,
  `city_longitude` float(9,7) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `region_code` varchar(2) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_country_id_idx` (`country_id`),
  KEY `fk_city_region_id_idx` (`region_id`),
  CONSTRAINT `fk_city_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_region_id` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=39018 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `geo_country`
--

DROP TABLE IF EXISTS `geo_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geo_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(100) DEFAULT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `geo_region`
--

DROP TABLE IF EXISTS `geo_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geo_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_name` varchar(45) NOT NULL,
  `country_id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `region_code` varchar(2) NOT NULL,
  `region_wikipedia` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `region_wikipedia_UNIQUE` (`region_wikipedia`),
  KEY `fk_region_country_id_idx` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4063 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_code` varchar(2) DEFAULT NULL,
  `language_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mntn_dbpedia`
--

DROP TABLE IF EXISTS `mntn_dbpedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mntn_dbpedia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbpedia_id` varchar(40) DEFAULT NULL,
  `dbpedia_abstract` varchar(5000) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `dbpedia_url` varchar(100) DEFAULT NULL COMMENT 'Campo derivato non necessario',
  `mountain_id` int(11) DEFAULT NULL,
  `dbpedia_wikipageId` int(11) DEFAULT NULL,
  `dbpedia_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `dbpedia_dateMod` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dbpedia_id_UNIQUE` (`dbpedia_id`),
  KEY `fk_dbpedia_mountain_idx` (`mountain_id`),
  KEY `fk_dbpedia_language_idx` (`language_id`),
  CONSTRAINT `fk_dbpedia_language` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dbpedia_mountain` FOREIGN KEY (`mountain_id`) REFERENCES `mountain` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1228 DEFAULT CHARSET=utf8 COMMENT='Dati per la costruzione del link a DBPedia.org version Italiana... ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mntn_image`
--

DROP TABLE IF EXISTS `mntn_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mntn_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(200) NOT NULL,
  `image_json` varchar(200) DEFAULT NULL COMMENT 'IMPORTANT: Check if this field has correct or necessary.',
  `mountain_id` int(11) DEFAULT NULL,
  `image_cover` bit(1) NOT NULL DEFAULT b'0',
  `image_click` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `image_descr` varchar(500) DEFAULT NULL,
  `image_filename` varchar(100) DEFAULT NULL,
  `image_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image_dateMod` timestamp NULL DEFAULT NULL,
  `image_cdn` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_image_mountain_idx` (`mountain_id`),
  CONSTRAINT `fk_image_mountain` FOREIGN KEY (`mountain_id`) REFERENCES `mountain` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mntn_mountaingroup`
--

DROP TABLE IF EXISTS `mntn_mountaingroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mntn_mountaingroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mntnGroup_name` varchar(45) DEFAULT NULL,
  `mntnGroup_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mntnRange_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mntnGroup_name_UNIQUE` (`mntnGroup_name`),
  KEY `fk_mntnGroup_mntnRange_id_idx` (`mntnRange_id`),
  CONSTRAINT `fk_mntnGroup_mntnRange_id` FOREIGN KEY (`mntnRange_id`) REFERENCES `mntn_mountainrange` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=426 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mntn_mountainrange`
--

DROP TABLE IF EXISTS `mntn_mountainrange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mntn_mountainrange` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mntnRange_name` varchar(100) DEFAULT NULL,
  `mntnRange_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mntnRange_dateMod` timestamp NULL DEFAULT NULL,
  `mntnRange_parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mntnRange_name_UNIQUE` (`mntnRange_name`),
  KEY `fk_mntnRange_mntnRange_id_idx` (`mntnRange_parentId`),
  CONSTRAINT `fk_mntnRange_mntnRange_id` FOREIGN KEY (`mntnRange_parentId`) REFERENCES `mntn_mountainrange` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COMMENT='Informazioni prelevate dalla classificazione SOIUSA. Le catene montuose registrate sono la Sezione e SottoSezione.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mntn_translate`
--

DROP TABLE IF EXISTS `mntn_translate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mntn_translate` (
  `mntn_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `mntnTransl_name` varchar(45) DEFAULT NULL,
  `mntnTransl_descr` varchar(255) DEFAULT NULL,
  `mntnGroupTransl_name` varchar(45) DEFAULT NULL,
  `mntnRangeTransl_name` varchar(45) DEFAULT NULL,
  `mntnTransl_dateIns` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mntn_id`,`language_id`),
  KEY `fk_mntnTranslation_language_id_idx` (`language_id`),
  CONSTRAINT `fk_mntnTranslation_language_id` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mntnTranslation_mntn_id` FOREIGN KEY (`mntn_id`) REFERENCES `mountain` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mountain`
--

DROP TABLE IF EXISTS `mountain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mountain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mntn_name` varchar(100) NOT NULL,
  `mntn_description` varchar(255) DEFAULT NULL,
  `mntn_latitude` float(9,7) DEFAULT NULL,
  `mntn_longitude` float(9,7) DEFAULT NULL,
  `mntn_altitude` smallint(6) DEFAULT NULL,
  `mntnRange_id` int(11) DEFAULT NULL,
  `mntnGroup_id` int(11) DEFAULT NULL,
  `mntn_imageUrl` varchar(100) DEFAULT NULL,
  `mntn_firstAscent` varchar(15) DEFAULT NULL,
  `stone_id` int(11) DEFAULT NULL,
  `mntn_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mntn_dateMod` timestamp NULL DEFAULT NULL,
  `mntn_status` bit(1) NOT NULL DEFAULT b'1' COMMENT 'Flag for enable/disable the mountain as alternative to the logical remove.',
  `mntn_ranking` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mntn_name_UNIQUE` (`mntn_name`),
  KEY `fk_mountain_group_id_idx` (`mntnGroup_id`),
  KEY `fk_mountain_range_id_idx` (`mntnRange_id`),
  KEY `fk_mountain_stone_idx` (`stone_id`),
  CONSTRAINT `fk_mountain_group_id` FOREIGN KEY (`mntnGroup_id`) REFERENCES `mntn_mountaingroup` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mountain_range_id` FOREIGN KEY (`mntnRange_id`) REFERENCES `mntn_mountainrange` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mountain_stone` FOREIGN KEY (`stone_id`) REFERENCES `stone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13042 DEFAULT CHARSET=utf8 COMMENT='Elenco delle montagne con le informazioni principali';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_statistics`
--

DROP TABLE IF EXISTS `page_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `page_type` smallint(6) NOT NULL COMMENT '1 mountain 2 route 3 camp ',
  `page_countview` int(11) DEFAULT '0',
  `page_dateMod` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_route_mountain_idx` (`page_id`),
  KEY `fk_pagestatistics_route_idx` (`page_type`)
) ENGINE=InnoDB AUTO_INCREMENT=562 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parking`
--

DROP TABLE IF EXISTS `parking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_name` varchar(45) DEFAULT NULL,
  `parking_free` bit(1) DEFAULT b'1',
  `parking_lat` float(9,7) DEFAULT NULL,
  `parking_lng` float(9,7) DEFAULT NULL,
  `parking_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parking_dateMod` timestamp NULL DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `parking_altitude` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `route_name` varchar(200) DEFAULT NULL,
  `route_title` varchar(300) DEFAULT NULL,
  `route_area` varchar(45) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `activityExt_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `mountain_id` int(11) DEFAULT NULL,
  `mntnGroup_id` int(11) DEFAULT NULL,
  `mntnRange_id` int(11) DEFAULT NULL,
  `valley_id` int(11) DEFAULT NULL,
  `route_facing` varchar(10) DEFAULT NULL,
  `route_length` int(11) DEFAULT '0' COMMENT 'sviluppo via, convertion from char to int',
  `route_lengthDescr` varchar(250) DEFAULT NULL,
  `route_altitude` smallint(6) DEFAULT NULL COMMENT 'Max Altitude / Elevation',
  `route_height` smallint(6) DEFAULT NULL COMMENT 'Altitude / Elevation difference of wall / route',
  `route_approachAltitude` smallint(6) DEFAULT NULL COMMENT 'altitudine avvicinamento',
  `route_approachHeight` smallint(6) DEFAULT NULL COMMENT 'Dislivello Avvicinamento',
  `route_approachLength` smallint(6) DEFAULT NULL,
  `difficulty_grade` varchar(25) DEFAULT NULL,
  `route_lat` float DEFAULT NULL,
  `route_lng` float DEFAULT NULL,
  `route_pitchAsc` smallint(6) DEFAULT NULL COMMENT 'Number of pitches during the climb',
  `route_pitchDesc` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `route_status` bit(1) DEFAULT b'1',
  `route_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `route_dateMod` timestamp NULL DEFAULT NULL,
  `route_firstAscDate` datetime DEFAULT NULL COMMENT 'First Ascent Date',
  `route_firstAscClimbers` varchar(65) DEFAULT NULL COMMENT 'First Ascent Climb',
  `parking_id` int(11) DEFAULT NULL,
  `trackgps_id` int(11) DEFAULT NULL,
  `route_ranking` smallint(6) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_route_mountain_idx` (`mountain_id`),
  KEY `fk_route_activity_id_idx` (`activity_id`),
  KEY `fk_route_country_id_idx` (`country_id`),
  KEY `fk_route_country_mountainRange_id_idx` (`mntnRange_id`),
  KEY `fk_route_region_id_idx` (`region_id`),
  KEY `fk_route_country_mountainGroup_id_idx` (`mntnGroup_id`),
  KEY `fk_route_valley_id_idx` (`valley_id`),
  KEY `fk_route_city_id_idx` (`city_id`),
  KEY `fk_route_parking_idx` (`parking_id`),
  KEY `fk_route_trackgps_idx` (`trackgps_id`),
  KEY `fk_route_user_idx` (`user_id`),
  CONSTRAINT `fk_route_activity_id` FOREIGN KEY (`activity_id`) REFERENCES `act_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_city_id` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_country_mountainGroup_id` FOREIGN KEY (`mntnGroup_id`) REFERENCES `mntn_mountaingroup` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_country_mountainRange_id` FOREIGN KEY (`mntnRange_id`) REFERENCES `mntn_mountainrange` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_mountain` FOREIGN KEY (`mountain_id`) REFERENCES `mountain` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_parking` FOREIGN KEY (`parking_id`) REFERENCES `parking` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_region` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_trackgps` FOREIGN KEY (`trackgps_id`) REFERENCES `trackgps` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_user` FOREIGN KEY (`user_id`) REFERENCES `user_profile` (`profile_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_valley` FOREIGN KEY (`valley_id`) REFERENCES `valley` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10290 DEFAULT CHARSET=utf8 COMMENT='Table which contain generic data of routes of all activities';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `route_detail`
--

DROP TABLE IF EXISTS `route_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_detail` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `route_image`
--

DROP TABLE IF EXISTS `route_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(200) DEFAULT NULL,
  `image_json` varchar(45) DEFAULT NULL COMMENT 'IMPORTANT: Check if this field has correct or necessary.',
  `route_id` int(11) DEFAULT NULL,
  `image_cover` bit(1) DEFAULT b'0',
  `user_id` int(11) DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `image_descr` varchar(500) DEFAULT NULL,
  `image_filename` varchar(100) DEFAULT NULL,
  `image_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `image_dateMod` timestamp NULL DEFAULT NULL,
  `image_cdn` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `image_url_UNIQUE` (`image_url`),
  KEY `fk_image_route_idx` (`route_id`),
  KEY `fk_image_source_idx` (`source_id`),
  CONSTRAINT `fk_image_route` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_image_source` FOREIGN KEY (`source_id`) REFERENCES `source` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=383 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `route_location`
--

DROP TABLE IF EXISTS `route_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_location` (
  `id` int(11) NOT NULL,
  `route_id` int(11) DEFAULT NULL,
  `loc_parking_lat` float DEFAULT NULL,
  `loc_parking_lng` float DEFAULT NULL,
  `loc_access_lat` float DEFAULT NULL,
  `loc_access_lng` float DEFAULT NULL,
  `loc_end_lat` float DEFAULT NULL,
  `loc_end_lng` float DEFAULT NULL,
  `loc_dateIns` timestamp NULL DEFAULT NULL,
  `loc_dateMod` timestamp NULL DEFAULT NULL,
  `loc_track` varchar(5000) DEFAULT NULL,
  `loc_description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_route_idx` (`route_id`),
  CONSTRAINT `fk_route_location_route` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `route_translate`
--

DROP TABLE IF EXISTS `route_translate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_translate` (
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `routeTransl_name` varchar(45) DEFAULT NULL,
  `routeTransl_title` varchar(45) DEFAULT NULL,
  `routeTransl_area` varchar(45) DEFAULT NULL,
  `mntn_name` varchar(45) DEFAULT NULL,
  `mntnGroup_name` varchar(45) DEFAULT NULL,
  `mntnRange_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`language_id`,`route_id`),
  KEY `fk_routeTranslation_language_id_idx` (`language_id`),
  KEY `fk_routeTranslation_route_id_idx` (`route_id`),
  CONSTRAINT `fk_routeTranslation_language_id` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_routeTranslation_route_id` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `route_webinfo`
--

DROP TABLE IF EXISTS `route_webinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_webinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wi_url` varchar(125) NOT NULL,
  `wi_summary` varchar(255) DEFAULT NULL,
  `wi_rate` varchar(45) DEFAULT NULL,
  `language_code` varchar(2) DEFAULT NULL,
  `route_id` int(11) NOT NULL,
  `source_id` int(11) DEFAULT NULL,
  `wi_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wi_dateMod` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT 'User Creator',
  PRIMARY KEY (`id`),
  KEY `fk_externalResource_route_id_idx` (`route_id`),
  KEY `fk_routeWebinfo_source_id_idx` (`source_id`),
  KEY `fk_routeWebinfo_user_idx` (`user_id`),
  CONSTRAINT `fk_externalResource_route_id` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_routeWebinfo_source_id` FOREIGN KEY (`source_id`) REFERENCES `source` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3214 DEFAULT CHARSET=utf8 COMMENT='fk_routeWebinfo_source_id';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `source`
--

DROP TABLE IF EXISTS `source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_name` varchar(65) NOT NULL,
  `source_url` varchar(200) DEFAULT NULL,
  `source_logo` varchar(100) DEFAULT NULL COMMENT 'Image',
  `source_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stone`
--

DROP TABLE IF EXISTS `stone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` smallint(6) NOT NULL,
  `stone_name` varchar(45) DEFAULT NULL,
  `stone_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trackgps`
--

DROP TABLE IF EXISTS `trackgps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trackgps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `track_name` varchar(45) DEFAULT NULL,
  `track_description` varchar(500) DEFAULT NULL,
  `track_url` varchar(100) DEFAULT NULL,
  `track_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `track_dateMod` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='Table with all track gps associated to 1 or more routes....working progress';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_profile` (
  `UserId` varchar(128) NOT NULL,
  `profile_firstName` varchar(50) DEFAULT NULL,
  `profile_lastName` varchar(50) DEFAULT NULL,
  `profile_website` varchar(256) DEFAULT NULL,
  `profile_bio` longtext,
  `profile_activities` varchar(500) DEFAULT NULL COMMENT 'List of all activities done in mountains',
  `country_id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL COMMENT 'This column is not part of primary ... of consequence is not used such as a foregne key',
  `profile_image` varchar(35) NOT NULL COMMENT 'filename image get by cloudinary',
  `role_id` int(11) DEFAULT NULL,
  `profile_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `profile_dateMod` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `profile_id_UNIQUE` (`profile_id`),
  KEY `fk_profile_country_idx` (`country_id`),
  KEY `fk_profile_region_idx` (`region_id`),
  KEY `fk_profile_city_idx` (`city_id`),
  KEY `fk_profile_role_idx` (`role_id`),
  CONSTRAINT `Profile_ApplicationUser` FOREIGN KEY (`UserId`) REFERENCES `aspnetusers` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_city` FOREIGN KEY (`city_id`) REFERENCES `geo_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_country` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_region` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profile_role` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for custom properties in addition to standard info managed in ASP.Identity. In this is possibile separate data & login from the Identity Framework avoiding to change code and simply patch and upgrade ot it.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) DEFAULT NULL,
  `role_desc` varchar(450) DEFAULT NULL,
  `role_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_stats`
--

DROP TABLE IF EXISTS `user_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `stats_routes` int(11) DEFAULT '0',
  `stats_mountains` int(11) DEFAULT '0',
  `stats_images` int(11) DEFAULT '0',
  `stats_reports` int(11) DEFAULT '0',
  `stats_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `stats_dateMod` timestamp NULL DEFAULT NULL,
  `stats_lastLogin` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stats_profile_idx` (`user_id`),
  CONSTRAINT `fk_stats_profile` FOREIGN KEY (`user_id`) REFERENCES `user_profile` (`profile_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_wishlist`
--

DROP TABLE IF EXISTS `user_wishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mountain_id` int(11) DEFAULT NULL,
  `route_id` int(11) DEFAULT NULL,
  `accomodation_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `wl_done` bit(1) DEFAULT b'0',
  `wl_type` smallint(6) DEFAULT NULL,
  `wl_score` smallint(6) DEFAULT NULL COMMENT 'the valuation of adventure with score from 0 to 4',
  `wl_dateIns` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `wl_dateMod` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_wishlist_user_id_idx` (`user_id`),
  KEY `fk_wishlist_mountain_idx` (`mountain_id`),
  KEY `fk_wishlist_route_idx` (`route_id`),
  CONSTRAINT `fk_wishlist_mountain` FOREIGN KEY (`mountain_id`) REFERENCES `mountain` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_wishlist_route` FOREIGN KEY (`route_id`) REFERENCES `route` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `v_community`
--

DROP TABLE IF EXISTS `v_community`;
/*!50001 DROP VIEW IF EXISTS `v_community`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_community` AS SELECT 
 1 AS `profile_id`,
 1 AS `firstName`,
 1 AS `lastName`,
 1 AS `username`,
 1 AS `website`,
 1 AS `image`,
 1 AS `country_id`,
 1 AS `region`,
 1 AS `role`,
 1 AS `stats_mountains`,
 1 AS `stats_routes`,
 1 AS `stats_images`,
 1 AS `dateIns`,
 1 AS `dateMod`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_feed_images`
--

DROP TABLE IF EXISTS `v_feed_images`;
/*!50001 DROP VIEW IF EXISTS `v_feed_images`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_feed_images` AS SELECT 
 1 AS `id`,
 1 AS `external_id`,
 1 AS `activity_id`,
 1 AS `type`,
 1 AS `title`,
 1 AS `image_url`,
 1 AS `user_id`,
 1 AS `profile_firstName`,
 1 AS `profile_LastName`,
 1 AS `days`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_feed_route`
--

DROP TABLE IF EXISTS `v_feed_route`;
/*!50001 DROP VIEW IF EXISTS `v_feed_route`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_feed_route` AS SELECT 
 1 AS `id`,
 1 AS `route_title`,
 1 AS `country_id`,
 1 AS `region_id`,
 1 AS `region`,
 1 AS `activity_id`,
 1 AS `day`,
 1 AS `status`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_feed_stats`
--

DROP TABLE IF EXISTS `v_feed_stats`;
/*!50001 DROP VIEW IF EXISTS `v_feed_stats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_feed_stats` AS SELECT 
 1 AS `type`,
 1 AS `id`,
 1 AS `title`,
 1 AS `mntn_altitude`,
 1 AS `page_countview`,
 1 AS `country_id`,
 1 AS `region`,
 1 AS `activity_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_geocity_autocomplete`
--

DROP TABLE IF EXISTS `v_geocity_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_geocity_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_geocity_autocomplete` AS SELECT 
 1 AS `id`,
 1 AS `city`,
 1 AS `regionid`,
 1 AS `country`,
 1 AS `countryid`,
 1 AS `lat`,
 1 AS `lng`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_georegion_autocomplete`
--

DROP TABLE IF EXISTS `v_georegion_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_georegion_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_georegion_autocomplete` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `country_name`,
 1 AS `country_code`,
 1 AS `country_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_mountain4map`
--

DROP TABLE IF EXISTS `v_mountain4map`;
/*!50001 DROP VIEW IF EXISTS `v_mountain4map`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_mountain4map` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `range_id`,
 1 AS `lat`,
 1 AS `lng`,
 1 AS `altitude`,
 1 AS `route`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_mountain_autocomplete`
--

DROP TABLE IF EXISTS `v_mountain_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_mountain_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_mountain_autocomplete` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `range_name`,
 1 AS `altitude`,
 1 AS `country_id`,
 1 AS `country_code`,
 1 AS `region_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_mountain_countrycode_concat`
--

DROP TABLE IF EXISTS `v_mountain_countrycode_concat`;
/*!50001 DROP VIEW IF EXISTS `v_mountain_countrycode_concat`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_mountain_countrycode_concat` AS SELECT 
 1 AS `mntn_id`,
 1 AS `country_code`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_mountain_index`
--

DROP TABLE IF EXISTS `v_mountain_index`;
/*!50001 DROP VIEW IF EXISTS `v_mountain_index`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_mountain_index` AS SELECT 
 1 AS `id`,
 1 AS `mntn_name`,
 1 AS `mntn_altitude`,
 1 AS `region_id`,
 1 AS `country_id`,
 1 AS `mntnRange_id`,
 1 AS `mntnGroup_id`,
 1 AS `mntnRange_name`,
 1 AS `mntnGroup_name`,
 1 AS `wikipedia`,
 1 AS `coordinates`,
 1 AS `count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_report_mountain4region`
--

DROP TABLE IF EXISTS `v_report_mountain4region`;
/*!50001 DROP VIEW IF EXISTS `v_report_mountain4region`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_report_mountain4region` AS SELECT 
 1 AS `region`,
 1 AS `region_id`,
 1 AS `country_id`,
 1 AS `n`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_report_route4source`
--

DROP TABLE IF EXISTS `v_report_route4source`;
/*!50001 DROP VIEW IF EXISTS `v_report_route4source`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_report_route4source` AS SELECT 
 1 AS `id`,
 1 AS `source_name`,
 1 AS `source_url`,
 1 AS `count(*)`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_report_route4sourceregion`
--

DROP TABLE IF EXISTS `v_report_route4sourceregion`;
/*!50001 DROP VIEW IF EXISTS `v_report_route4sourceregion`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_report_route4sourceregion` AS SELECT 
 1 AS `source_name`,
 1 AS `region_name`,
 1 AS `count(*)`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_route_autocomplete`
--

DROP TABLE IF EXISTS `v_route_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_route_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_route_autocomplete` AS SELECT 
 1 AS `id`,
 1 AS `route_title`,
 1 AS `route_name`,
 1 AS `country_id`,
 1 AS `region_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `valley`
--

DROP TABLE IF EXISTS `valley`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valley` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valley_name` varchar(45) DEFAULT NULL COMMENT 'Al momento la tabella è pensata per valli che non delimitano regioni. Da prime ricerche non sono riuscito ad identifcare dei casi e per semplificare lo scenario questa la struttura.',
  `country_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `valley_parentId` int(11) DEFAULT NULL,
  `valley_lat` float(9,7) DEFAULT NULL,
  `valley_lng` float(9,7) DEFAULT NULL,
  `valley_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valley_dateMod` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `valley_name_UNIQUE` (`valley_name`),
  KEY `fk_valley_region_id_idx` (`region_id`),
  KEY `fk_valley_country_id_idx` (`country_id`),
  CONSTRAINT `fk_valley_country_id` FOREIGN KEY (`country_id`) REFERENCES `geo_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_valley_region_id` FOREIGN KEY (`region_id`) REFERENCES `geo_region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=418 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'symcore'
--

--
-- Dumping routines for database 'symcore'
--
/*!50003 DROP FUNCTION IF EXISTS `SPLIT_STRING` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STRING`( s VARCHAR(1024) , del CHAR(1) , i INT) RETURNS varchar(1024) CHARSET utf8
    DETERMINISTIC
BEGIN

        DECLARE n INT ;

        -- get max number of items
        SET n = LENGTH(s) - LENGTH(REPLACE(s, del, '')) + 1;

        IF i > n THEN
            RETURN NULL ;
        ELSE
            RETURN SUBSTRING_INDEX(SUBSTRING_INDEX(s, del, i) , del , -1 ) ;        
        END IF;

    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_calculate_routeRankig` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_calculate_routeRankig`()
BEGIN
	
	DECLARE v_id         int(11) DEFAULT 0;
    DECLARE v_mntn 	     int(11) DEFAULT 0;
	DECLARE v_valley 	 int(11) DEFAULT 0;
	DECLARE v_altitude 	 int(11) DEFAULT 0;
    DECLARE v_difficulty varchar(25) DEFAULT null;
    DECLARE v_face       varchar(10) DEFAULT null;
    DECLARE v_length     varchar(45) DEFAULT null;
    DECLARE v_height     int(11) DEFAULT 0;
	DECLARE v_parking    int(11) DEFAULT 0;
	DECLARE v_city       int(11) DEFAULT 0;
	DECLARE v_trackgps   int(11) DEFAULT 0;
    DECLARE v_score      int(11) DEFAULT 0;
	DECLARE tmp_score    int(11) DEFAULT 0;
    DECLARE i     	     int(11) DEFAULT 0;
 
	 -- declare cursor for route
	 DEClARE route_cursor CURSOR FOR 
	 SELECT id,
			mountain_id,
            valley_id,
			route_altitude,
            difficulty_grade,
            route_facing,
            route_length,
            route_height,
            parking_id,
            city_id,
            trackgps_id,
			route_ranking
     FROM `symcore`.route 
     where route_status = 1;
 
 OPEN route_cursor;
 get_score: LOOP
 
 FETCH route_cursor INTO v_id,v_mntn,v_valley,v_altitude,v_difficulty,v_face,v_length,v_height,v_parking,v_city,v_trackgps,tmp_score;
    
    SET v_score = 0;

    /*Montagna*/
	IF IFNULL(v_mntn,0) > 0 THEN 
		SET v_score = v_score + 15;
	END IF;

    /*Valle*/
    IF IFNULL(v_valley,0) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;

    /*Difficoltà*/
	IF LENGTH(IFNULL(v_difficulty,'')) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;
    
    /*Lunghezza*/
    IF LENGTH(IFNULL(v_length,'')) > 0 THEN 
		SET v_score = v_score + 10;
	END IF;

    /*Esposizione*/
    IF LENGTH(IFNULL(v_face,'')) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;
    
    /*Height*/
    IF IFNULL(v_height,0) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;

    /*Città*/
    IF IFNULL(v_city,0) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;

    /*Parking*/
    IF IFNULL(v_parking,0) > 0 THEN 
		SET v_score = v_score + 10;
	END IF;
	
	/*TrackGps*/
    IF IFNULL(v_trackgps,0) > 0 THEN 
		SET v_score = v_score + 15;
	END IF;

	IF tmp_score != v_score THEN
		SELECT CONCAT(v_id,' Ranking Score Update: ',v_score); 
		UPDATE `symcore`.route
		set route_ranking = v_score, 
			route_dateMod = SYSDATE()
		where id = v_id and route_ranking != v_score;
	END IF;

 END LOOP get_score;
 CLOSE route_cursor;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_geo_nearestMountains` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_geo_nearestMountains`(
	p_latpoint float(9,7),
    p_lngpoint float(9,7),
    p_mntnID int,
    p_altitude int,
    p_route bool
)
BEGIN

	SELECT id, 
		   mntn_name as name, 
           mntn_latitude as lat, mntn_longitude as lng, mntn_altitude as altitude,
		  111.045* DEGREES(ACOS(COS(RADIANS(latpoint))
					 * COS(RADIANS(mntn_latitude))
					 * COS(RADIANS(longpoint) - RADIANS(mntn_longitude))
					 + SIN(RADIANS(latpoint))
					 * SIN(RADIANS(mntn_latitude)))) AS km,
		    (select count(*) from route where mountain_id = mountain.id) as route
	 FROM mountain
	 JOIN (
		 SELECT  p_latpoint   AS latpoint,  p_lngpoint AS longpoint
	   ) AS p ON 1=1 
	 where not mntn_latitude is null and id != p_mntnID and mntn_status = true and mntn_altitude > 600 
           and (mntn_altitude > p_altitude)
           and (p_route = false or (select count(*) from route where mountain_id = mountain.id) > 0)
	 ORDER BY km asc
	 LIMIT 100; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_community`
--

/*!50001 DROP VIEW IF EXISTS `v_community`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_community` AS select distinct `p`.`profile_id` AS `profile_id`,`p`.`profile_firstName` AS `firstName`,`p`.`profile_lastName` AS `lastName`,`users`.`UserName` AS `username`,`p`.`profile_website` AS `website`,`p`.`profile_image` AS `image`,`p`.`country_id` AS `country_id`,`geo_region`.`region_name` AS `region`,`r`.`role_name` AS `role`,ifnull(`s`.`stats_mountains`,0) AS `stats_mountains`,ifnull(`s`.`stats_routes`,0) AS `stats_routes`,ifnull(`s`.`stats_images`,0) AS `stats_images`,`p`.`profile_dateIns` AS `dateIns`,`p`.`profile_dateMod` AS `dateMod` from ((((`user_profile` `p` left join `user_stats` `s` on((`s`.`user_id` = `p`.`profile_id`))) join `aspnetusers` `users` on((`users`.`Id` = `p`.`UserId`))) left join `user_role` `r` on((`r`.`id` = `p`.`role_id`))) left join `geo_region` on((`geo_region`.`id` = `p`.`region_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_feed_images`
--

/*!50001 DROP VIEW IF EXISTS `v_feed_images`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_feed_images` AS select `route_image`.`id` AS `id`,`route_image`.`route_id` AS `external_id`,`route`.`activity_id` AS `activity_id`,'r' AS `type`,`r`.`route_title` AS `title`,`route_image`.`image_url` AS `image_url`,`route_image`.`user_id` AS `user_id`,`user_profile`.`profile_firstName` AS `profile_firstName`,`user_profile`.`profile_lastName` AS `profile_LastName`,timestampdiff(DAY,`route_image`.`image_dateIns`,now()) AS `days` from (((`route_image` join `user_profile` on((`route_image`.`user_id` = `user_profile`.`profile_id`))) join `v_route_autocomplete` `r` on((`r`.`id` = `route_image`.`route_id`))) join `route` on((`route`.`id` = `route_image`.`route_id`))) where ((`route_image`.`image_cdn` = 1) and (`route_image`.`route_id` <> 10267)) union select `mntn_image`.`id` AS `id`,`mntn_image`.`mountain_id` AS `external_id`,0 AS `activity_id`,'m' AS `type`,`m`.`mntn_name` AS `title`,`mntn_image`.`image_url` AS `image_url`,`mntn_image`.`user_id` AS `user_id`,`user_profile`.`profile_firstName` AS `profile_firstName`,`user_profile`.`profile_lastName` AS `profile_LastName`,timestampdiff(DAY,`mntn_image`.`image_dateIns`,now()) AS `image_dateIns` from ((`mntn_image` join `user_profile` on((`mntn_image`.`user_id` = `user_profile`.`profile_id`))) join `mountain` `m` on((`m`.`id` = `mntn_image`.`mountain_id`))) where (`mntn_image`.`image_cdn` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_feed_route`
--

/*!50001 DROP VIEW IF EXISTS `v_feed_route`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_feed_route` AS select `route`.`id` AS `id`,concat(`mountain`.`mntn_name`,' - ',`route`.`route_name`) AS `route_title`,`route`.`country_id` AS `country_id`,coalesce(`geo_region`.`id`,0) AS `region_id`,coalesce(`geo_region`.`region_name`,'') AS `region`,`route`.`activity_id` AS `activity_id`,timestampdiff(DAY,ifnull(`route`.`route_dateMod`,`route`.`route_dateIns`),now()) AS `day`,if(isnull(`route`.`route_dateMod`),'N','U') AS `status` from ((((`route` join `mountain` on((`route`.`mountain_id` = `mountain`.`id`))) left join `mntn_mountaingroup` on((`mountain`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) where ((`route`.`route_status` = 1) and (`route`.`mountain_id` is not null)) union select `route`.`id` AS `id`,concat(ifnull(`mntn_mountaingroup`.`mntnGroup_name`,`valley`.`valley_name`),' - ',`route`.`route_name`) AS `route_title`,`route`.`country_id` AS `country_id`,coalesce(`geo_region`.`id`,0) AS `region_id`,coalesce(`geo_region`.`region_name`,'') AS `region`,`route`.`activity_id` AS `activity_id`,timestampdiff(DAY,ifnull(`route`.`route_dateMod`,`route`.`route_dateIns`),now()) AS `day`,if(isnull(`route`.`route_dateMod`),'N','U') AS `status` from (((`route` left join `mntn_mountaingroup` on((`route`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) where ((`route`.`route_status` = 1) and isnull(`route`.`mountain_id`)) order by `id` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_feed_stats`
--

/*!50001 DROP VIEW IF EXISTS `v_feed_stats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_feed_stats` AS select 'm' AS `type`,`m`.`id` AS `id`,`m`.`mntn_name` AS `title`,`m`.`mntn_altitude` AS `mntn_altitude`,`ps`.`page_countview` AS `page_countview`,0 AS `country_id`,'' AS `region`,0 AS `activity_id` from (`page_statistics` `ps` join `mountain` `m` on((`ps`.`page_id` = `m`.`id`))) where (`ps`.`page_type` = 1) union select 'r' AS `type`,`r`.`id` AS `id`,`r`.`route_title` AS `title`,0 AS `mntn_altitude`,`ps`.`page_countview` AS `page_countview`,`r`.`country_id` AS `country_id`,`geo_region`.`region_name` AS `region`,0 AS `activity_id` from ((`page_statistics` `ps` join `v_route_autocomplete` `r` on((`ps`.`page_id` = `r`.`id`))) join `geo_region` on((`r`.`region_id` = `geo_region`.`id`))) where (`ps`.`page_type` = 2) order by `page_countview` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_geocity_autocomplete`
--

/*!50001 DROP VIEW IF EXISTS `v_geocity_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_geocity_autocomplete` AS select distinct `geo_city`.`id` AS `id`,`geo_city`.`city_name` AS `city`,`geo_city`.`region_id` AS `regionid`,`geo_city`.`country_code` AS `country`,`geo_city`.`country_id` AS `countryid`,`geo_city`.`city_latitude` AS `lat`,`geo_city`.`city_longitude` AS `lng` from `geo_city` where ((`geo_city`.`country_id` in (116,217,15)) and (`geo_city`.`region_id` is not null)) order by `geo_city`.`city_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_georegion_autocomplete`
--

/*!50001 DROP VIEW IF EXISTS `v_georegion_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_georegion_autocomplete` AS select distinct `geo_region`.`id` AS `id`,`geo_region`.`region_name` AS `name`,`geo_country`.`country_name` AS `country_name`,`geo_country`.`country_code` AS `country_code`,`geo_country`.`id` AS `country_id` from (`geo_region` join `geo_country` on((`geo_region`.`country_id` = `geo_country`.`id`))) where (`geo_country`.`id` in (116,217,65)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mountain4map`
--

/*!50001 DROP VIEW IF EXISTS `v_mountain4map`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_mountain4map` AS select `mountain`.`id` AS `id`,`mountain`.`mntn_name` AS `name`,ifnull(`mountain`.`mntnRange_id`,0) AS `range_id`,`mountain`.`mntn_latitude` AS `lat`,`mountain`.`mntn_longitude` AS `lng`,`mountain`.`mntn_altitude` AS `altitude`,(select count(0) from `route` where ((`route`.`mountain_id` = `mountain`.`id`) and (`route`.`route_status` = 1))) AS `route` from `mountain` where (`mountain`.`mntn_status` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mountain_autocomplete`
--

/*!50001 DROP VIEW IF EXISTS `v_mountain_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_mountain_autocomplete` AS select `mountain`.`id` AS `id`,`mountain`.`mntn_name` AS `name`,ifnull(`mntnrange`.`mntnRange_name`,'') AS `range_name`,`mountain`.`mntn_altitude` AS `altitude`,`mntncountry`.`country_id` AS `country_id`,`mntncountry`.`country_code` AS `country_code`,coalesce(`mntnregion`.`region_id`,0) AS `region_id` from (((`mountain` join `ass_mountaincountry` `mntncountry` on((`mntncountry`.`mntn_id` = `mountain`.`id`))) left join `ass_mountainregion` `mntnregion` on(((`mntnregion`.`mntn_id` = `mntncountry`.`mntn_id`) and (`mntnregion`.`country_id` = `mntncountry`.`country_id`)))) left join `mntn_mountainrange` `mntnrange` on((`mntnrange`.`id` = `mountain`.`mntnRange_id`))) where (`mountain`.`mntn_status` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mountain_countrycode_concat`
--

/*!50001 DROP VIEW IF EXISTS `v_mountain_countrycode_concat`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_mountain_countrycode_concat` AS select `ass_mountaincountry`.`mntn_id` AS `mntn_id`,group_concat(`ass_mountaincountry`.`country_code` separator ', ') AS `country_code` from `ass_mountaincountry` group by `ass_mountaincountry`.`mntn_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mountain_index`
--

/*!50001 DROP VIEW IF EXISTS `v_mountain_index`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_mountain_index` AS select `mountain`.`id` AS `id`,`mountain`.`mntn_name` AS `mntn_name`,`mountain`.`mntn_altitude` AS `mntn_altitude`,`ass_mountainregion`.`region_id` AS `region_id`,`ass_mountainregion`.`country_id` AS `country_id`,`mountain`.`mntnRange_id` AS `mntnRange_id`,`mountain`.`mntnGroup_id` AS `mntnGroup_id`,`mntn_mountainrange`.`mntnRange_name` AS `mntnRange_name`,`mntn_mountaingroup`.`mntnGroup_name` AS `mntnGroup_name`,if(isnull(`mntn_dbpedia`.`id`),0,1) AS `wikipedia`,if(isnull(`mountain`.`mntn_latitude`),0,1) AS `coordinates`,(select count(0) from `route` where (`route`.`mountain_id` = `mountain`.`id`)) AS `count` from (((((`mountain` left join `ass_mountainregion` on((`mountain`.`id` = `ass_mountainregion`.`mntn_id`))) left join `geo_country` on((`ass_mountainregion`.`country_id` = `geo_country`.`id`))) left join `mntn_mountainrange` on((`mountain`.`mntnRange_id` = `mntn_mountainrange`.`id`))) left join `mntn_mountaingroup` on((`mountain`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `mntn_dbpedia` on((`mountain`.`id` = `mntn_dbpedia`.`mountain_id`))) where (`mountain`.`mntn_status` = 1) group by `mountain`.`id`,`mountain`.`mntn_name`,`mountain`.`mntn_altitude`,`ass_mountainregion`.`region_id`,`ass_mountainregion`.`country_id`,`geo_country`.`country_code`,`mountain`.`mntnRange_id`,`mountain`.`mntnGroup_id`,`mntn_mountainrange`.`mntnRange_name`,`mntn_mountaingroup`.`mntnGroup_name`,`mntn_dbpedia`.`id`,`mountain`.`mntn_latitude` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_report_mountain4region`
--

/*!50001 DROP VIEW IF EXISTS `v_report_mountain4region`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_report_mountain4region` AS select `am`.`region_name` AS `region`,`am`.`region_id` AS `region_id`,`am`.`country_id` AS `country_id`,count(0) AS `n` from (`mountain` join `ass_mountainregion` `am` on((`mountain`.`id` = `am`.`mntn_id`))) group by `am`.`region_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_report_route4source`
--

/*!50001 DROP VIEW IF EXISTS `v_report_route4source`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_report_route4source` AS select `source`.`id` AS `id`,`source`.`source_name` AS `source_name`,`source`.`source_url` AS `source_url`,count(0) AS `count(*)` from ((`route` join `route_webinfo` `wi` on((`route`.`id` = `wi`.`route_id`))) join `source` on((`source`.`id` = `wi`.`source_id`))) group by `source`.`source_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_report_route4sourceregion`
--

/*!50001 DROP VIEW IF EXISTS `v_report_route4sourceregion`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_report_route4sourceregion` AS select `source`.`source_name` AS `source_name`,`geo_region`.`region_name` AS `region_name`,count(0) AS `count(*)` from (((`route` join `route_webinfo` `wi` on((`route`.`id` = `wi`.`route_id`))) join `source` on((`source`.`id` = `wi`.`source_id`))) join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) group by `source`.`source_name`,`geo_region`.`region_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_route_autocomplete`
--

/*!50001 DROP VIEW IF EXISTS `v_route_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_route_autocomplete` AS select `route`.`id` AS `id`,concat(`mountain`.`mntn_name`,' - ',`route`.`route_name`) AS `route_title`,`route`.`route_name` AS `route_name`,`route`.`country_id` AS `country_id`,coalesce(`geo_region`.`id`,0) AS `region_id` from ((((`route` join `mountain` on((`route`.`mountain_id` = `mountain`.`id`))) left join `mntn_mountaingroup` on((`mountain`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) where (`route`.`route_status` = 1) union select `route`.`id` AS `id`,concat(ifnull(`mntn_mountaingroup`.`mntnGroup_name`,`valley`.`valley_name`),' - ',`route`.`route_name`) AS `route_title`,`route`.`route_name` AS `route_name`,`route`.`country_id` AS `country_id`,coalesce(`geo_region`.`id`,0) AS `region_id` from (((`route` left join `mntn_mountaingroup` on((`route`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) where ((`route`.`route_status` = 1) and isnull(`route`.`mountain_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-25 23:57:39
