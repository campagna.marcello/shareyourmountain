SET @ORIGINAL_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @ORIGINAL_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;

UPDATE `mcampagn83_symcore`.`parking` SET `parking_name`='Funivia Piani di Bobbio (Barzio)', `parking_free`=False, `parking_lat`=45.95533, `parking_lng`=9.463837, `parking_dateIns`='2015-12-27 16:42:05', `parking_dateMod`=NULL, `country_id`=116, `region_id`=1736, `parking_altitude`=810 WHERE `id`=9;
UPDATE `mcampagn83_symcore`.`parking` SET `parking_name`='Chiesetta del Sacro Cuore (Balisio)', `parking_free`=True, `parking_lat`=45.93076, `parking_lng`=9.432735, `parking_dateIns`='2015-12-27 23:06:55', `parking_dateMod`=NULL, `country_id`=116, `region_id`=1736, `parking_altitude`=830 WHERE `id`=10;
UPDATE `mcampagn83_symcore`.`valley` SET `valley_name`='Valsassina', `valley_parentId`=NULL, `valley_lat`=45.9833, `valley_lng`=9.4, `valley_dateIns`='2015-01-26 23:26:38', `valley_dateMod`=NULL WHERE `id`=2;
UPDATE `mcampagn83_symcore`.`valley` SET `valley_name`='Valle Albigna', `valley_parentId`=NULL, `valley_lat`=46.3223, `valley_lng`=9.6597, `valley_dateIns`='2015-06-06 14:12:14', `valley_dateMod`=NULL WHERE `id`=11;
UPDATE `mcampagn83_symcore`.`valley` SET `valley_name`='Val Bregaglia', `valley_parentId`=NULL, `valley_lat`=46.3334, `valley_lng`=9.5001, `valley_dateIns`='2015-06-06 17:32:57', `valley_dateMod`=NULL WHERE `id`=12;
UPDATE `mcampagn83_symcore`.`valley` SET `country_id`=116, `region_id`=1736 WHERE `id`=2;
UPDATE `mcampagn83_symcore`.`valley` SET `country_id`=217, `region_id`=540 WHERE `id`=11;
UPDATE `mcampagn83_symcore`.`valley` SET `country_id`=217, `region_id`=540 WHERE `id`=12;

SET FOREIGN_KEY_CHECKS=@ORIGINAL_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@ORIGINAL_UNIQUE_CHECKS;