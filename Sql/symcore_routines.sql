-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: symcore
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `v_community`
--

DROP TABLE IF EXISTS `v_community`;
/*!50001 DROP VIEW IF EXISTS `v_community`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_community` AS SELECT 
 1 AS `profile_id`,
 1 AS `UserId`,
 1 AS `firstName`,
 1 AS `lastName`,
 1 AS `username`,
 1 AS `website`,
 1 AS `image`,
 1 AS `country_id`,
 1 AS `region`,
 1 AS `role`,
 1 AS `stats_mountains`,
 1 AS `stats_routes`,
 1 AS `stats_images`,
 1 AS `dateIns`,
 1 AS `dateMod`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_feed_images`
--

DROP TABLE IF EXISTS `v_feed_images`;
/*!50001 DROP VIEW IF EXISTS `v_feed_images`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_feed_images` AS SELECT 
 1 AS `id`,
 1 AS `external_id`,
 1 AS `activity_id`,
 1 AS `type`,
 1 AS `title`,
 1 AS `image_url`,
 1 AS `user_id`,
 1 AS `profile_firstName`,
 1 AS `profile_LastName`,
 1 AS `days`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_mountain4map`
--

DROP TABLE IF EXISTS `v_mountain4map`;
/*!50001 DROP VIEW IF EXISTS `v_mountain4map`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_mountain4map` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `range_id`,
 1 AS `lat`,
 1 AS `lng`,
 1 AS `altitude`,
 1 AS `route`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_mountain_index`
--

DROP TABLE IF EXISTS `v_mountain_index`;
/*!50001 DROP VIEW IF EXISTS `v_mountain_index`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_mountain_index` AS SELECT 
 1 AS `id`,
 1 AS `mntn_name`,
 1 AS `mntn_altitude`,
 1 AS `region_id`,
 1 AS `country_id`,
 1 AS `mntnRange_id`,
 1 AS `mntnGroup_id`,
 1 AS `mntnRange_name`,
 1 AS `mntnGroup_name`,
 1 AS `wikipedia`,
 1 AS `coordinates`,
 1 AS `lat`,
 1 AS `lng`,
 1 AS `count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_route_autocomplete`
--

DROP TABLE IF EXISTS `v_route_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_route_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_route_autocomplete` AS SELECT 
 1 AS `id`,
 1 AS `route_title`,
 1 AS `route_name`,
 1 AS `country_id`,
 1 AS `region_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_feed_stats`
--

DROP TABLE IF EXISTS `v_feed_stats`;
/*!50001 DROP VIEW IF EXISTS `v_feed_stats`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_feed_stats` AS SELECT 
 1 AS `type`,
 1 AS `id`,
 1 AS `title`,
 1 AS `mntn_altitude`,
 1 AS `page_countview`,
 1 AS `country_id`,
 1 AS `region`,
 1 AS `activity_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_report_route4source`
--

DROP TABLE IF EXISTS `v_report_route4source`;
/*!50001 DROP VIEW IF EXISTS `v_report_route4source`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_report_route4source` AS SELECT 
 1 AS `id`,
 1 AS `source_name`,
 1 AS `source_url`,
 1 AS `count(*)`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_georegion_autocomplete`
--

DROP TABLE IF EXISTS `v_georegion_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_georegion_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_georegion_autocomplete` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `country_name`,
 1 AS `country_code`,
 1 AS `country_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_mountain_countrycode_concat`
--

DROP TABLE IF EXISTS `v_mountain_countrycode_concat`;
/*!50001 DROP VIEW IF EXISTS `v_mountain_countrycode_concat`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_mountain_countrycode_concat` AS SELECT 
 1 AS `mntn_id`,
 1 AS `country_code`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_feed_route`
--

DROP TABLE IF EXISTS `v_feed_route`;
/*!50001 DROP VIEW IF EXISTS `v_feed_route`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_feed_route` AS SELECT 
 1 AS `id`,
 1 AS `route_title`,
 1 AS `country_id`,
 1 AS `region_id`,
 1 AS `region`,
 1 AS `activity_id`,
 1 AS `day`,
 1 AS `status`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_mountain_autocomplete`
--

DROP TABLE IF EXISTS `v_mountain_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_mountain_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_mountain_autocomplete` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `range_name`,
 1 AS `altitude`,
 1 AS `country_id`,
 1 AS `country_code`,
 1 AS `region_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_route_index`
--

DROP TABLE IF EXISTS `v_route_index`;
/*!50001 DROP VIEW IF EXISTS `v_route_index`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_route_index` AS SELECT 
 1 AS `id`,
 1 AS `route_title`,
 1 AS `route_name`,
 1 AS `mountain_id`,
 1 AS `country_id`,
 1 AS `valley_id`,
 1 AS `region_id`,
 1 AS `activity_id`,
 1 AS `activity_name`,
 1 AS `route_ranking`,
 1 AS `route_altitude`,
 1 AS `route_height`,
 1 AS `route_dateMod`,
 1 AS `trackgps_id`,
 1 AS `mntn_latitude`,
 1 AS `mntn_longitude`,
 1 AS `difficulty_grade`,
 1 AS `route_pitchAsc`,
 1 AS `route_facing`,
 1 AS `route_length`,
 1 AS `region_name`,
 1 AS `valley_name`,
 1 AS `mntn_name`,
 1 AS `country_code`,
 1 AS `city_name`,
 1 AS `route_image_url`,
 1 AS `mntn_image_url`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_sync_routetofix`
--

DROP TABLE IF EXISTS `v_sync_routetofix`;
/*!50001 DROP VIEW IF EXISTS `v_sync_routetofix`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_sync_routetofix` AS SELECT 
 1 AS `mntn_id`,
 1 AS `mntn_dateMod`,
 1 AS `mntn_rangeId`,
 1 AS `mntn_groupId`,
 1 AS `route_id`,
 1 AS `route_rangeId`,
 1 AS `route_groupId`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_report_route4sourceregion`
--

DROP TABLE IF EXISTS `v_report_route4sourceregion`;
/*!50001 DROP VIEW IF EXISTS `v_report_route4sourceregion`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_report_route4sourceregion` AS SELECT 
 1 AS `source_name`,
 1 AS `region_name`,
 1 AS `count(*)`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_geocity_autocomplete`
--

DROP TABLE IF EXISTS `v_geocity_autocomplete`;
/*!50001 DROP VIEW IF EXISTS `v_geocity_autocomplete`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_geocity_autocomplete` AS SELECT 
 1 AS `id`,
 1 AS `city`,
 1 AS `regionid`,
 1 AS `country`,
 1 AS `countryid`,
 1 AS `lat`,
 1 AS `lng`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_report_mountain4region`
--

DROP TABLE IF EXISTS `v_report_mountain4region`;
/*!50001 DROP VIEW IF EXISTS `v_report_mountain4region`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_report_mountain4region` AS SELECT 
 1 AS `region`,
 1 AS `region_id`,
 1 AS `country_id`,
 1 AS `n`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_community`
--

/*!50001 DROP VIEW IF EXISTS `v_community`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_community` AS select distinct `p`.`profile_id` AS `profile_id`,`p`.`UserId` AS `UserId`,`p`.`profile_firstName` AS `firstName`,`p`.`profile_lastName` AS `lastName`,`users`.`UserName` AS `username`,`p`.`profile_website` AS `website`,`p`.`profile_image` AS `image`,`p`.`country_id` AS `country_id`,`geo_region`.`region_name` AS `region`,`r`.`role_name` AS `role`,ifnull(`s`.`stats_mountains`,0) AS `stats_mountains`,ifnull(`s`.`stats_routes`,0) AS `stats_routes`,ifnull(`s`.`stats_images`,0) AS `stats_images`,`p`.`profile_dateIns` AS `dateIns`,`p`.`profile_dateMod` AS `dateMod` from ((((`user_profile` `p` left join `user_stats` `s` on((`s`.`user_id` = `p`.`profile_id`))) join `aspnetusers` `users` on((`users`.`Id` = `p`.`UserId`))) left join `user_role` `r` on((`r`.`id` = `p`.`role_id`))) left join `geo_region` on((`geo_region`.`id` = `p`.`region_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_feed_images`
--

/*!50001 DROP VIEW IF EXISTS `v_feed_images`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_feed_images` AS select `route_image`.`id` AS `id`,`route_image`.`route_id` AS `external_id`,`route`.`activity_id` AS `activity_id`,'r' AS `type`,`r`.`route_title` AS `title`,`route_image`.`image_url` AS `image_url`,`route_image`.`user_id` AS `user_id`,`user_profile`.`profile_firstName` AS `profile_firstName`,`user_profile`.`profile_lastName` AS `profile_LastName`,timestampdiff(DAY,`route_image`.`image_dateIns`,now()) AS `days` from (((`route_image` join `user_profile` on((`route_image`.`user_id` = `user_profile`.`profile_id`))) join `v_route_autocomplete` `r` on((`r`.`id` = `route_image`.`route_id`))) join `route` on((`route`.`id` = `route_image`.`route_id`))) where ((`route_image`.`image_cdn` = 1) and (`route_image`.`route_id` <> 10267)) union select `mntn_image`.`id` AS `id`,`mntn_image`.`mountain_id` AS `external_id`,0 AS `activity_id`,'m' AS `type`,`m`.`mntn_name` AS `title`,`mntn_image`.`image_url` AS `image_url`,`mntn_image`.`user_id` AS `user_id`,`user_profile`.`profile_firstName` AS `profile_firstName`,`user_profile`.`profile_lastName` AS `profile_LastName`,timestampdiff(DAY,`mntn_image`.`image_dateIns`,now()) AS `image_dateIns` from ((`mntn_image` join `user_profile` on((`mntn_image`.`user_id` = `user_profile`.`profile_id`))) join `mountain` `m` on((`m`.`id` = `mntn_image`.`mountain_id`))) where (`mntn_image`.`image_cdn` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mountain4map`
--

/*!50001 DROP VIEW IF EXISTS `v_mountain4map`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_mountain4map` AS select `mountain`.`id` AS `id`,`mountain`.`mntn_name` AS `name`,ifnull(`mountain`.`mntnRange_id`,0) AS `range_id`,`mountain`.`mntn_latitude` AS `lat`,`mountain`.`mntn_longitude` AS `lng`,`mountain`.`mntn_altitude` AS `altitude`,(select count(0) from `route` where ((`route`.`mountain_id` = `mountain`.`id`) and (`route`.`route_status` = 1))) AS `route` from `mountain` where (`mountain`.`mntn_status` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mountain_index`
--

/*!50001 DROP VIEW IF EXISTS `v_mountain_index`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_mountain_index` AS select `mountain`.`id` AS `id`,`mountain`.`mntn_name` AS `mntn_name`,`mountain`.`mntn_altitude` AS `mntn_altitude`,`ass_mountainregion`.`region_id` AS `region_id`,`ass_mountainregion`.`country_id` AS `country_id`,`mountain`.`mntnRange_id` AS `mntnRange_id`,`mountain`.`mntnGroup_id` AS `mntnGroup_id`,`mntn_mountainrange`.`mntnRange_name` AS `mntnRange_name`,`mntn_mountaingroup`.`mntnGroup_name` AS `mntnGroup_name`,if(isnull(`mntn_dbpedia`.`id`),0,1) AS `wikipedia`,if(isnull(`mountain`.`mntn_latitude`),0,1) AS `coordinates`,`mountain`.`mntn_latitude` AS `lat`,`mountain`.`mntn_longitude` AS `lng`,(select count(0) from `route` where (`route`.`mountain_id` = `mountain`.`id`)) AS `count` from (((((`mountain` left join `ass_mountainregion` on((`mountain`.`id` = `ass_mountainregion`.`mntn_id`))) left join `geo_country` on((`ass_mountainregion`.`country_id` = `geo_country`.`id`))) left join `mntn_mountainrange` on((`mountain`.`mntnRange_id` = `mntn_mountainrange`.`id`))) left join `mntn_mountaingroup` on((`mountain`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `mntn_dbpedia` on((`mountain`.`id` = `mntn_dbpedia`.`mountain_id`))) where (`mountain`.`mntn_status` = 1) group by `mountain`.`id`,`mountain`.`mntn_name`,`mountain`.`mntn_altitude`,`ass_mountainregion`.`region_id`,`ass_mountainregion`.`country_id`,`geo_country`.`country_code`,`mountain`.`mntnRange_id`,`mountain`.`mntnGroup_id`,`mntn_mountainrange`.`mntnRange_name`,`mntn_mountaingroup`.`mntnGroup_name`,`mntn_dbpedia`.`id`,`mountain`.`mntn_latitude` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_route_autocomplete`
--

/*!50001 DROP VIEW IF EXISTS `v_route_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_route_autocomplete` AS select `route`.`id` AS `id`,concat(`mountain`.`mntn_name`,' - ',`route`.`route_name`) AS `route_title`,`route`.`route_name` AS `route_name`,`route`.`country_id` AS `country_id`,coalesce(`geo_region`.`id`,0) AS `region_id` from ((((`route` join `mountain` on((`route`.`mountain_id` = `mountain`.`id`))) left join `mntn_mountaingroup` on((`mountain`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) where (`route`.`route_status` = 1) union select `route`.`id` AS `id`,concat(ifnull(`mntn_mountaingroup`.`mntnGroup_name`,`valley`.`valley_name`),' - ',`route`.`route_name`) AS `route_title`,`route`.`route_name` AS `route_name`,`route`.`country_id` AS `country_id`,coalesce(`geo_region`.`id`,0) AS `region_id` from (((`route` left join `mntn_mountaingroup` on((`route`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) where ((`route`.`route_status` = 1) and isnull(`route`.`mountain_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_feed_stats`
--

/*!50001 DROP VIEW IF EXISTS `v_feed_stats`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_feed_stats` AS select 'm' AS `type`,`m`.`id` AS `id`,`m`.`mntn_name` AS `title`,`m`.`mntn_altitude` AS `mntn_altitude`,`ps`.`page_countview` AS `page_countview`,0 AS `country_id`,'' AS `region`,0 AS `activity_id` from (`page_statistics` `ps` join `mountain` `m` on((`ps`.`page_id` = `m`.`id`))) where (`ps`.`page_type` = 1) union select 'r' AS `type`,`r`.`id` AS `id`,`r`.`route_title` AS `title`,0 AS `mntn_altitude`,`ps`.`page_countview` AS `page_countview`,`r`.`country_id` AS `country_id`,`geo_region`.`region_name` AS `region`,0 AS `activity_id` from ((`page_statistics` `ps` join `v_route_autocomplete` `r` on((`ps`.`page_id` = `r`.`id`))) join `geo_region` on((`r`.`region_id` = `geo_region`.`id`))) where (`ps`.`page_type` = 2) order by `page_countview` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_report_route4source`
--

/*!50001 DROP VIEW IF EXISTS `v_report_route4source`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_report_route4source` AS select `source`.`id` AS `id`,`source`.`source_name` AS `source_name`,`source`.`source_url` AS `source_url`,count(0) AS `count(*)` from ((`route` join `route_webinfo` `wi` on((`route`.`id` = `wi`.`route_id`))) join `source` on((`source`.`id` = `wi`.`source_id`))) group by `source`.`source_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_georegion_autocomplete`
--

/*!50001 DROP VIEW IF EXISTS `v_georegion_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_georegion_autocomplete` AS select distinct `geo_region`.`id` AS `id`,`geo_region`.`region_name` AS `name`,`geo_country`.`country_name` AS `country_name`,`geo_country`.`country_code` AS `country_code`,`geo_country`.`id` AS `country_id` from (`geo_region` join `geo_country` on((`geo_region`.`country_id` = `geo_country`.`id`))) where (`geo_country`.`id` in (116,217,65)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mountain_countrycode_concat`
--

/*!50001 DROP VIEW IF EXISTS `v_mountain_countrycode_concat`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_mountain_countrycode_concat` AS select `ass_mountaincountry`.`mntn_id` AS `mntn_id`,group_concat(`ass_mountaincountry`.`country_code` separator ', ') AS `country_code` from `ass_mountaincountry` group by `ass_mountaincountry`.`mntn_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_feed_route`
--

/*!50001 DROP VIEW IF EXISTS `v_feed_route`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_feed_route` AS select `route`.`id` AS `id`,concat(`mountain`.`mntn_name`,' - ',`route`.`route_name`) AS `route_title`,`route`.`country_id` AS `country_id`,coalesce(`geo_region`.`id`,0) AS `region_id`,coalesce(`geo_region`.`region_name`,'') AS `region`,`route`.`activity_id` AS `activity_id`,timestampdiff(DAY,ifnull(`route`.`route_dateMod`,`route`.`route_dateIns`),now()) AS `day`,if(isnull(`route`.`route_dateMod`),'N','U') AS `status` from ((((`route` join `mountain` on((`route`.`mountain_id` = `mountain`.`id`))) left join `mntn_mountaingroup` on((`mountain`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) where ((`route`.`route_status` = 1) and (`route`.`mountain_id` is not null)) union select `route`.`id` AS `id`,concat(ifnull(`mntn_mountaingroup`.`mntnGroup_name`,`valley`.`valley_name`),' - ',`route`.`route_name`) AS `route_title`,`route`.`country_id` AS `country_id`,coalesce(`geo_region`.`id`,0) AS `region_id`,coalesce(`geo_region`.`region_name`,'') AS `region`,`route`.`activity_id` AS `activity_id`,timestampdiff(DAY,ifnull(`route`.`route_dateMod`,`route`.`route_dateIns`),now()) AS `day`,if(isnull(`route`.`route_dateMod`),'N','U') AS `status` from (((`route` left join `mntn_mountaingroup` on((`route`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) where ((`route`.`route_status` = 1) and isnull(`route`.`mountain_id`)) order by `id` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_mountain_autocomplete`
--

/*!50001 DROP VIEW IF EXISTS `v_mountain_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_mountain_autocomplete` AS select `mountain`.`id` AS `id`,`mountain`.`mntn_name` AS `name`,ifnull(`mntnrange`.`mntnRange_name`,'') AS `range_name`,`mountain`.`mntn_altitude` AS `altitude`,`mntncountry`.`country_id` AS `country_id`,`mntncountry`.`country_code` AS `country_code`,coalesce(`mntnregion`.`region_id`,0) AS `region_id` from (((`mountain` join `ass_mountaincountry` `mntncountry` on((`mntncountry`.`mntn_id` = `mountain`.`id`))) left join `ass_mountainregion` `mntnregion` on(((`mntnregion`.`mntn_id` = `mntncountry`.`mntn_id`) and (`mntnregion`.`country_id` = `mntncountry`.`country_id`)))) left join `mntn_mountainrange` `mntnrange` on((`mntnrange`.`id` = `mountain`.`mntnRange_id`))) where (`mountain`.`mntn_status` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_route_index`
--

/*!50001 DROP VIEW IF EXISTS `v_route_index`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_route_index` AS select `route`.`id` AS `id`,concat(`mountain`.`mntn_name`,' - ',`route`.`route_name`) AS `route_title`,`route`.`route_name` AS `route_name`,`route`.`mountain_id` AS `mountain_id`,`route`.`country_id` AS `country_id`,`route`.`valley_id` AS `valley_id`,coalesce(`geo_region`.`id`,0) AS `region_id`,`route`.`activity_id` AS `activity_id`,`act_activity`.`activity_name` AS `activity_name`,`route`.`route_ranking` AS `route_ranking`,`route`.`route_altitude` AS `route_altitude`,`route`.`route_height` AS `route_height`,ifnull(`route`.`route_dateMod`,`route`.`route_dateIns`) AS `route_dateMod`,`route`.`trackgps_id` AS `trackgps_id`,`mountain`.`mntn_latitude` AS `mntn_latitude`,`mountain`.`mntn_longitude` AS `mntn_longitude`,`route`.`difficulty_grade` AS `difficulty_grade`,`route`.`route_pitchAsc` AS `route_pitchAsc`,`route`.`route_facing` AS `route_facing`,`route`.`route_length` AS `route_length`,`geo_region`.`region_name` AS `region_name`,'' AS `valley_name`,`mountain`.`mntn_name` AS `mntn_name`,`geo_country`.`country_code` AS `country_code`,`geo_city`.`city_name` AS `city_name`,(select `route_image`.`image_url` from `route_image` where (`route_image`.`route_id` = `route`.`id`) order by `route_image`.`id` desc limit 1) AS `route_image_url`,(select `mntn_image`.`image_url` from `mntn_image` where (`mntn_image`.`mountain_id` = `route`.`mountain_id`) order by `mntn_image`.`id` desc limit 1) AS `mntn_image_url` from (((((((`route` join `mountain` on((`route`.`mountain_id` = `mountain`.`id`))) left join `mntn_mountaingroup` on((`mountain`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) left join `act_activity` on((`act_activity`.`id` = `route`.`activity_id`))) left join `geo_country` on((`geo_country`.`id` = `route`.`country_id`))) left join `geo_city` on((`geo_city`.`id` = `route`.`city_id`))) where (`route`.`route_status` = 1) union select `route`.`id` AS `id`,concat(ifnull(ifnull(`mntn_mountaingroup`.`mntnGroup_name`,`valley`.`valley_name`),'ND'),' - ',`route`.`route_name`) AS `route_title`,`route`.`route_name` AS `route_name`,0 AS `mountain_id`,`route`.`country_id` AS `country_id`,`route`.`valley_id` AS `valley_id`,coalesce(`geo_region`.`id`,0) AS `region_id`,`route`.`activity_id` AS `activity_id`,`act_activity`.`activity_name` AS `activity_name`,`route`.`route_ranking` AS `route_ranking`,`route`.`route_altitude` AS `route_altitude`,`route`.`route_height` AS `route_height`,ifnull(`route`.`route_dateMod`,`route`.`route_dateIns`) AS `route_dateMod`,`route`.`trackgps_id` AS `trackgps_id`,0 AS `mntn_latitude`,0 AS `mntn_longitude`,`route`.`difficulty_grade` AS `difficulty_grade`,`route`.`route_pitchAsc` AS `route_pitchAsc`,`route`.`route_facing` AS `route_facing`,`route`.`route_length` AS `route_length`,`geo_region`.`region_name` AS `region_name`,`valley`.`valley_name` AS `valley_name`,'' AS `mntn_name`,`geo_country`.`country_code` AS `country_code`,`geo_city`.`city_name` AS `city_name`,(select `route_image`.`image_url` from `route_image` where ((`route_image`.`route_id` = `route`.`id`) and (`route_image`.`image_cdn` = 1)) order by `route_image`.`id` desc limit 1) AS `image_url`,NULL AS `mntn_image` from ((((((`route` left join `mntn_mountaingroup` on((`route`.`mntnGroup_id` = `mntn_mountaingroup`.`id`))) left join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) left join `valley` on((`valley`.`id` = `route`.`valley_id`))) left join `act_activity` on((`act_activity`.`id` = `route`.`activity_id`))) left join `geo_country` on((`geo_country`.`id` = `route`.`country_id`))) left join `geo_city` on((`geo_city`.`id` = `route`.`city_id`))) where ((`route`.`route_status` = 1) and isnull(`route`.`mountain_id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_sync_routetofix`
--

/*!50001 DROP VIEW IF EXISTS `v_sync_routetofix`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_sync_routetofix` AS select `m`.`id` AS `mntn_id`,`m`.`mntn_dateMod` AS `mntn_dateMod`,`m`.`mntnRange_id` AS `mntn_rangeId`,`m`.`mntnGroup_id` AS `mntn_groupId`,`r`.`id` AS `route_id`,`r`.`mntnRange_id` AS `route_rangeId`,`r`.`mntnGroup_id` AS `route_groupId` from (`mountain` `m` join `route` `r` on((`m`.`id` = `r`.`mountain_id`))) where ((`r`.`route_status` = 1) and (isnull(`r`.`mntnRange_id`) or isnull(`r`.`mntnGroup_id`) or (`r`.`mntnRange_id` <> `m`.`mntnRange_id`) or (`r`.`mntnGroup_id` <> `m`.`mntnGroup_id`)) and (`m`.`mntnRange_id` is not null) and (`m`.`mntnGroup_id` is not null)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_report_route4sourceregion`
--

/*!50001 DROP VIEW IF EXISTS `v_report_route4sourceregion`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `v_report_route4sourceregion` AS select `source`.`source_name` AS `source_name`,`geo_region`.`region_name` AS `region_name`,count(0) AS `count(*)` from (((`route` join `route_webinfo` `wi` on((`route`.`id` = `wi`.`route_id`))) join `source` on((`source`.`id` = `wi`.`source_id`))) join `geo_region` on((`geo_region`.`id` = `route`.`region_id`))) group by `source`.`source_name`,`geo_region`.`region_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_geocity_autocomplete`
--

/*!50001 DROP VIEW IF EXISTS `v_geocity_autocomplete`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_geocity_autocomplete` AS select distinct `geo_city`.`id` AS `id`,`geo_city`.`city_name` AS `city`,`geo_city`.`region_id` AS `regionid`,`geo_city`.`country_code` AS `country`,`geo_city`.`country_id` AS `countryid`,`geo_city`.`city_latitude` AS `lat`,`geo_city`.`city_longitude` AS `lng` from `geo_city` where ((`geo_city`.`country_id` in (116,217,15)) and (`geo_city`.`region_id` is not null)) order by `geo_city`.`city_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_report_mountain4region`
--

/*!50001 DROP VIEW IF EXISTS `v_report_mountain4region`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `v_report_mountain4region` AS select `am`.`region_name` AS `region`,`am`.`region_id` AS `region_id`,`am`.`country_id` AS `country_id`,count(0) AS `n` from (`mountain` join `ass_mountainregion` `am` on((`mountain`.`id` = `am`.`mntn_id`))) group by `am`.`region_name` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'symcore'
--

--
-- Dumping routines for database 'symcore'
--
/*!50003 DROP FUNCTION IF EXISTS `SPLIT_STRING` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE FUNCTION `SPLIT_STRING`( s VARCHAR(1024) , del CHAR(1) , i INT) RETURNS varchar(1024) CHARSET utf8
    DETERMINISTIC
BEGIN

        DECLARE n INT ;

        -- get max number of items
        SET n = LENGTH(s) - LENGTH(REPLACE(s, del, '')) + 1;

        IF i > n THEN
            RETURN NULL ;
        ELSE
            RETURN SUBSTRING_INDEX(SUBSTRING_INDEX(s, del, i) , del , -1 ) ;        
        END IF;

    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_calculate_routeRankig` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_calculate_routeRankig`()
BEGIN
	
	DECLARE v_id         int(11) DEFAULT 0;
    DECLARE v_mntn 	     int(11) DEFAULT 0;
	DECLARE v_valley 	 int(11) DEFAULT 0;
	DECLARE v_altitude 	 int(11) DEFAULT 0;
    DECLARE v_difficulty varchar(25) DEFAULT null;
    DECLARE v_face       varchar(10) DEFAULT null;
    DECLARE v_length     varchar(45) DEFAULT null;
    DECLARE v_height     int(11) DEFAULT 0;
	DECLARE v_parking    int(11) DEFAULT 0;
	DECLARE v_city       int(11) DEFAULT 0;
	DECLARE v_trackgps   int(11) DEFAULT 0;
    DECLARE v_score      int(11) DEFAULT 0;
	DECLARE tmp_score    int(11) DEFAULT 0;
    DECLARE i     	     int(11) DEFAULT 0;
 
	 -- declare cursor for route
	 DEClARE route_cursor CURSOR FOR 
	 SELECT id,
			mountain_id,
            valley_id,
			route_altitude,
            difficulty_grade,
            route_facing,
            route_length,
            route_height,
            parking_id,
            city_id,
            trackgps_id,
			route_ranking
     FROM `symcore`.route 
     where route_status = 1;
 
 OPEN route_cursor;
 get_score: LOOP
 
 FETCH route_cursor INTO v_id,v_mntn,v_valley,v_altitude,v_difficulty,v_face,v_length,v_height,v_parking,v_city,v_trackgps,tmp_score;
    
    SET v_score = 0;

    /*Montagna*/
	IF IFNULL(v_mntn,0) > 0 THEN 
		SET v_score = v_score + 15;
	END IF;

    /*Valle*/
    IF IFNULL(v_valley,0) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;

    /*Difficoltà*/
	IF LENGTH(IFNULL(v_difficulty,'')) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;
    
    /*Lunghezza*/
    IF LENGTH(IFNULL(v_length,'')) > 0 THEN 
		SET v_score = v_score + 10;
	END IF;

    /*Esposizione*/
    IF LENGTH(IFNULL(v_face,'')) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;
    
    /*Height*/
    IF IFNULL(v_height,0) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;

    /*Città*/
    IF IFNULL(v_city,0) > 0 THEN 
		SET v_score = v_score + 5;
	END IF;

    /*Parking*/
    IF IFNULL(v_parking,0) > 0 THEN 
		SET v_score = v_score + 10;
	END IF;
	
	/*TrackGps*/
    IF IFNULL(v_trackgps,0) > 0 THEN 
		SET v_score = v_score + 15;
	END IF;

	IF tmp_score != v_score THEN
		SELECT CONCAT(v_id,' Ranking Score Update: ',v_score); 
		UPDATE `symcore`.route
		set route_ranking = v_score, 
			route_dateMod = SYSDATE()
		where id = v_id and route_ranking != v_score;
	END IF;

 END LOOP get_score;
 CLOSE route_cursor;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_geo_nearestMountains` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `sp_geo_nearestMountains`(

	p_latpoint float(9,7),
    p_lngpoint float(9,7),
    p_mntnId int,
    p_altitude int,
    p_route bool,
    p_userId int)
BEGIN

declare lon1 float; 
declare lon2 float;
declare lat1 float; 
declare lat2 float;
declare p_dist int;

-- calculate lon and lat for the rectangle:
set p_dist = 40;
set lon1 = p_lngpoint-p_dist/abs(cos(radians(p_latpoint))*69);
set lon2 = p_lngpoint+p_dist/abs(cos(radians(p_latpoint))*69);
set lat1 = p_latpoint-(p_dist/69); 
set lat2 = p_latpoint+(p_dist/69);

if p_userId = 0 then 
-- run the query without ID
SELECT id, 
	   mntn_name as name, 
	   mntn_latitude as lat, 
       mntn_longitude as lng, 
       mntn_altitude as altitude,
       3956 * 2 * ASIN(SQRT(POWER(SIN((p_latpoint - dest.mntn_latitude) * pi()/180 / 2), 2) + COS(p_latpoint * pi()/180) 
                 * COS(dest.mntn_latitude * pi()/180) * POWER(SIN((p_lngpoint - dest.mntn_longitude) * pi()/180 / 2), 2) )) as km,
                  (select count(*) from route where mountain_id = dest.id) as route,
	   0 as done		
       FROM mountain as dest
       WHERE dest.mntn_longitude between lon1 and lon2 
			and dest.mntn_latitude between lat1 and lat2 
		    and not dest.mntn_latitude is null 
            and dest.id != p_mntnId 
            and dest.mntn_status = true 
            and (dest.mntn_altitude > p_altitude)
            and (p_route = false or (select count(*) from route where mountain_id = dest.id) > 0)
	   having km < p_dist
	   ORDER BY km 
	   limit 40;
else
-- run the query without ID
SELECT dest.id, 
	   mntn_name as name, 
	   mntn_latitude as lat, 
       mntn_longitude as lng, 
       mntn_altitude as altitude,
       3956 * 2 * ASIN(SQRT(POWER(SIN((p_latpoint - dest.mntn_latitude) * pi()/180 / 2), 2) + COS(p_latpoint * pi()/180) 
                 * COS(dest.mntn_latitude * pi()/180) * POWER(SIN((p_lngpoint - dest.mntn_longitude) * pi()/180 / 2), 2) )) as km,
                  (select count(*) from route where mountain_id = dest.id) as route,
	   CASE user_wishlist.wl_done
       WHEN 1 THEN true 
       ELSE false
       END as done
       FROM mountain as dest left join user_wishlist on dest.id = user_wishlist.mountain_id and wl_done = true
       WHERE dest.mntn_longitude between lon1 and lon2 
			and dest.mntn_latitude between lat1 and lat2 
		    and not dest.mntn_latitude is null 
            and dest.id != p_mntnId 
            and dest.mntn_status = true 
            and (dest.mntn_altitude > p_altitude)
            and (p_route = false or (select count(*) from route where mountain_id = dest.id) > 0)
	   having km < p_dist
	   ORDER BY km 
	   limit 40;
end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-04 23:59:06
