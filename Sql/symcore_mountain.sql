-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: symcore
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mountain`
--

DROP TABLE IF EXISTS `mountain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mountain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mntn_name` varchar(100) NOT NULL,
  `mntn_description` varchar(255) DEFAULT NULL,
  `mntn_latitude` float(9,7) DEFAULT NULL,
  `mntn_longitude` float(9,7) DEFAULT NULL,
  `mntn_altitude` smallint(6) DEFAULT NULL,
  `mntnRange_id` int(11) DEFAULT NULL,
  `mntnGroup_id` int(11) DEFAULT NULL,
  `mntn_imageUrl` varchar(100) DEFAULT NULL,
  `mntn_firstAscent` varchar(15) DEFAULT NULL,
  `stone_id` int(11) DEFAULT NULL,
  `mntn_dateIns` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mntn_dateMod` timestamp NULL DEFAULT NULL,
  `mntn_status` bit(1) NOT NULL DEFAULT b'1' COMMENT 'Flag for enable/disable the mountain as alternative to the logical remove.',
  `mntn_ranking` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_mountain_group_id_idx` (`mntnGroup_id`),
  KEY `fk_mountain_range_id_idx` (`mntnRange_id`),
  KEY `fk_mountain_stone_idx` (`stone_id`),
  CONSTRAINT `fk_mountain_group_id` FOREIGN KEY (`mntnGroup_id`) REFERENCES `mntn_mountaingroup` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mountain_range_id` FOREIGN KEY (`mntnRange_id`) REFERENCES `mntn_mountainrange` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_mountain_stone` FOREIGN KEY (`stone_id`) REFERENCES `stone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13049 DEFAULT CHARSET=utf8 COMMENT='Elenco delle montagne con le informazioni principali';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-04 23:58:42
