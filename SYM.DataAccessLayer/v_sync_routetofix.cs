//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SYM.DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_sync_routetofix
    {
        public int mntn_id { get; set; }
        public Nullable<System.DateTime> mntn_dateMod { get; set; }
        public Nullable<int> mntn_rangeId { get; set; }
        public Nullable<int> mntn_groupId { get; set; }
        public int route_id { get; set; }
        public Nullable<int> route_rangeId { get; set; }
        public Nullable<int> route_groupId { get; set; }
    }
}
