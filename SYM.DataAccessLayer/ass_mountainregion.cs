//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SYM.DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class ass_mountainregion
    {
        public int mntn_id { get; set; }
        public int region_id { get; set; }
        public string region_name { get; set; }
        public int country_id { get; set; }
        public System.DateTime dateIns { get; set; }
    
        public virtual geo_country geo_country { get; set; }
        public virtual mountain mountain { get; set; }
        public virtual geo_region geo_region { get; set; }
    }
}
