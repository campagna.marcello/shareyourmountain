//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SYM.DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class parking
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public parking()
        {
            this.route = new HashSet<route>();
        }
    
        public int id { get; set; }
        public string parking_name { get; set; }
        public Nullable<bool> parking_free { get; set; }
        public Nullable<float> parking_lat { get; set; }
        public Nullable<float> parking_lng { get; set; }
        public System.DateTime parking_dateIns { get; set; }
        public Nullable<System.DateTime> parking_dateMod { get; set; }
        public Nullable<int> country_id { get; set; }
        public Nullable<int> region_id { get; set; }
        public Nullable<int> parking_altitude { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<route> route { get; set; }
    }
}
