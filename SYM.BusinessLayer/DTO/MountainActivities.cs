﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SYM.BusinessLayer.DTO
{
    public class MountainActivity
    {
        public int? id { get; set; }
        public string activity { get; set; }
        public int number { get; set; }
    }
}
