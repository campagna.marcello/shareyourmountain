﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SYM.Common;
using SYM.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SYM.BusinessLayer.Services
{
    //Move the model in a spefic way
    public class AddressComponent
    {
        public string Short_name { get; set; }
        public string Long_name { get; set; }
        public List<string> Types { get; set; }
    }

    public class GeoInfo
    {
        public string country { get; set; }
        public string countryCode { get; set; }
        public string region { get; set; }
        public string regionCode { get; set; }
        public string ISO3166_2 { get; set; }
    }

    public class GeographicService
    {
        private Entities _context = new Entities();

        public GeoInfo GetGeoInfoByGoogle(float latitude, float longitude) {

            string url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude.ToString().Replace(',', '.') + "," + longitude.ToString().Replace(',', '.') + "&sensor=true";
            string responseBody = null;

            CrawlerHttp crawler = new CrawlerHttp();
            HttpWebResponse response = crawler.Request_QueryApi(url,"", "maps.googleapis.com");

            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), ASCIIEncoding.UTF8))
            {
                responseBody = reader.ReadToEnd();
            }

            GeoInfo result = ParsingGoogleResponse(responseBody);
            return result;
        }

        //TODO: VALIDATION ON FLOAT VALUE
        public geo_city GetCity(string name, geo_region region, float lat, float lng) {

            //Private
            geo_city city = null;
            city = GetCityByName(name, region);
            if (city == null)
            {
                city = AddCity(name, region, lat, lng);
            }
            return city;
        }

        public geo_region GetRegionByName(string name, string countryCode)
        {
            var region = _context.geo_region.Where(a => a.country_code == countryCode.ToUpper() && a.region_name.ToLower() == name.ToLower()).FirstOrDefault();
            return region;
        }

        #region PRIVATE
        private static GeoInfo ParsingGoogleResponse(string json)
        {
            JObject googleSearch = JObject.Parse(json);
            var results = googleSearch["results"].ToList();
            IList<AddressComponent> searchResults = new List<AddressComponent>();

            GeoInfo geoInfo = new GeoInfo();

            foreach (JToken result in results)
            {
                var searchResult = result["address_components"].ToString();
                List<AddressComponent> addressComponent = JsonConvert.DeserializeObject<List<AddressComponent>>(searchResult);

                foreach (var row in addressComponent)
                {
                    if (row.Types.Contains("country") && String.IsNullOrEmpty(geoInfo.country))
                    {
                       geoInfo.country = row.Long_name;
                       geoInfo.countryCode = row.Short_name;
                    }
                    else if (row.Types.Contains("administrative_area_level_1") && String.IsNullOrEmpty(geoInfo.region))
                    {
                       geoInfo.region = row.Long_name;
                       geoInfo.regionCode = row.Short_name;
                    }
                }
            }

            geoInfo.ISO3166_2 = geoInfo.countryCode + "-" + geoInfo.regionCode;

            return geoInfo;
        }

        private void GetCityByCoordinates(float latitude, float longitude) {

        }

        private geo_city AddCity(string name, geo_region region, float lat, float lng)
        {
            var city = new geo_city();
            try {
                
                city.city_latitude = lat;
                city.city_longitude = lng;
                city.city_name = name;
                city.country_code = region.country_code;
                city.country_id = region.country_id;
                city.geo_region = region;
                city.region_code = region.region_code;
                _context.geo_city.Add(city);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return city;
        }

        private geo_country GetCountryByCode(string code)
        {
            var country = _context.geo_country.Where(a => a.country_code == code).FirstOrDefault();
            return country;
        }

        private geo_city GetCityByName(string name, geo_region region)
        {
            var city = _context.geo_city.Where(a => a.city_name == name && a.region_id == region.id).FirstOrDefault();
            return city;
        }
        #endregion
    }
}
