﻿using System;
using System.Collections.Generic;
using System.Linq;
using SYM.DataAccessLayer;

namespace SYM.BusinessLayer.Services
{
    public class UserService
    {
        private Entities _context = new Entities();

        public void SetStats(int profile_id,int routes, int peaks, int images, int reports = 0)
        {
            try
            {         
                user_stats stats = _context.user_stats.Where(a => a.user_id == profile_id).FirstOrDefault();
                if (stats == null)
                {
                    //New Records
                    stats = new user_stats();
                    stats.user_id = profile_id;
                    stats.stats_mountains = peaks;
                    stats.stats_routes = routes;
                    stats.stats_images = images;
                    _context.user_stats.Add(stats);
                    _context.SaveChanges();
                }
                else {
                    //Update
                    stats.stats_mountains = peaks;
                    stats.stats_routes = routes;
                    stats.stats_images = images;
                    stats.stats_dateMod = DateTime.Now;
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public user_profile GetUserByProfileId(string profileId)
        {
            user_profile user = null;
            try
            {
                //Get Profile ID of the user Authenticate or in alternative use a parameter profileid
                if (!String.IsNullOrEmpty(profileId))
                {
                    user = _context.user_profile.Where(a => a.profile_id == Convert.ToInt32(profileId)).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return user;
        }

        public user_profile GetUserByUserId(Guid userId)
        {
            user_profile user = null;
            try
            {
                //Get Profile ID of the user Authenticate or in alternative use a parameter profileid
                if (userId != null)
                {
                    user = _context.user_profile.Find(userId.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return user;
        }
    }
}
