﻿using System;
using System.Collections.Generic;
using System.Linq;
using SYM.DataAccessLayer;

namespace SYM.BusinessLayer.Services
{
    public struct SummaryStats
    {
        public int counterMntn;
        public int counterMntnRange;
        public int counterMntnGroup;
        public int counterRoutes;
    };

    public class InfoService
    {
        private Entities _context = new Entities();

        public SummaryStats GetSummaryStats()
        {
            SummaryStats obj = new SummaryStats();
            obj.counterMntn = _context.mountain.Count();
            obj.counterMntnRange = _context.mntn_mountainrange.Count();
            obj.counterMntnGroup = _context.mntn_mountaingroup.Count();
            obj.counterRoutes = _context.route.Count();

            return obj;
        }
    }
}
