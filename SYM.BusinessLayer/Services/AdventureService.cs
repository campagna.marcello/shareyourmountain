﻿using System;
using System.Collections.Generic;
using System.Linq;
using SYM.DataAccessLayer;

namespace SYM.BusinessLayer.Services
{
    public class AdventureService
    {
        private Entities _context = new Entities();

        public bool Set(int profile, int mountain, int route, int hut, bool? done) {

            user_wishlist obj;
            obj = this.GetWishlist(profile, mountain, route, hut);
            try
            {
                if (obj == null)
                {
                    obj = new user_wishlist();
                    obj.user_id = profile;
                    if (mountain != 0) obj.mountain_id = mountain;
                    if (route != 0) obj.route_id = route;
                    if (hut != 0) obj.accomodation_id = hut;
                    obj.wl_done = done;
                    _context.user_wishlist.Add(obj);
                    _context.SaveChanges();
                }
                else if (obj.wl_done != done)
                {
                    obj.wl_done = done;
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        public bool ExistWishlist(int user, int m, int r, int h)
        {
            bool temp = false;

            try { 
                //Check if exist (mountain without route) or (route with or without mountain) 
                IEnumerable<user_wishlist> list = _context.user_wishlist.Where(a => a.user_id == user && ((a.mountain_id == m && r == 0) || (a.mountain_id == m && a.route_id == r))).ToList();
                if (list.Count() > 0)
                    temp = true;
            }
            catch
            {
                return false;
            }

            return temp;
        }

        public user_wishlist GetWishlist(int user, int m, int r, int h)
        {
            user_wishlist temp = null;

            try
            {
                //Check if exist (mountain without route) or (route with or without mountain) 
                IEnumerable<user_wishlist> list = _context.user_wishlist.Where(a => a.user_id == user && ((m == 0 || a.mountain_id == m) && (r == 0 || a.route_id == r))).ToList();
                if (list.Count() > 0)
                    temp = list.First();
            }
            catch
            {
                return null;
            }

            return temp;
        }

    }
}
