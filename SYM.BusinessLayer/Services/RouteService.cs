﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SYM.DataAccessLayer;
using SYM.BusinessLayer.DTO;

namespace SYM.BusinessLayer.Services
{
    public class RouteService
    {
        private Entities _context = new Entities();

        public int SetRanking(int id)
        {

            short score = 0;
            int scoreNew = 0;
            route r = _context.route.Find(id);

            score = r.route_ranking.GetValueOrDefault();

            //Punteggio max 100

            if (r.mountain != null)
                scoreNew = scoreNew + 10;

            if (r.valley != null)
                scoreNew = scoreNew + 10;

            if (r.difficulty_grade != null && r.difficulty_grade.Length > 1)
                scoreNew = scoreNew + 10;

            if (r.route_length != null && r.route_length > 0)
                scoreNew = scoreNew + 10;

            if (r.route_height > 0)
                scoreNew = scoreNew + 6;

            if (r.route_facing != null && r.route_facing.Length > 0)
                scoreNew = scoreNew + 3;

            if (r.geo_city != null)
                scoreNew = scoreNew + 5;

            if (r.parking != null)
                scoreNew = scoreNew + 5;

            if (r.trackgps != null)
                scoreNew = scoreNew + 10; //42

            if (r.route_firstAscClimbers != null && r.route_firstAscClimbers.Length > 0)
                scoreNew = scoreNew + 5; //42

            if(r.route_pitchAsc != null)
                scoreNew = scoreNew + 5;

            if (r.difficulty_grade != null)
                scoreNew = scoreNew + 5; 

            if (r.route_image.Count > 0)
                scoreNew = scoreNew + 3 * r.route_image.Count; //42

            if (scoreNew != score)
            {
                r.route_ranking = Convert.ToInt16(scoreNew);
                _context.SaveChanges();
            }

            return scoreNew;

        }

        public IEnumerable<v_route_index> GetRoutesSummary(string txt, int region, int country, int mountain, int valley, List<int> activityArray, int sourceid, string sort)
        {
            IEnumerable<v_route_index> routes = null;

            //First check Region Or Country and the mountain
            if (region > 0)
                routes = _context.v_route_index.Where(a => a.region_id == region).ToList();
            else if (country > 0)
                routes = _context.v_route_index.Where(a => a.country_id == country).ToList();
            else if (valley > 0)
                routes = _context.v_route_index.Where(a => a.valley_id == valley).ToList();

            //If ONLY Mountain is valorized
            if (mountain > 0 && routes != null)
                routes = routes.Where(a => a.mountain_id == mountain);
            else if (mountain > 0 && routes == null)
                routes = _context.v_route_index.Where(a => a.mountain_id == mountain).ToList();

            if (activityArray != null && activityArray.Count() > 0)
                routes = routes.Where(a => activityArray.Contains(Convert.ToInt32(a.activity_id)));

            if (!String.IsNullOrEmpty(txt) && mountain == 0)
                routes = routes.Where(a => a.route_title.ToLower().Contains(txt.ToLower().Trim()));

            //TODO: Improve this check considering all records associated

            switch (sort)
            {
                case "1":
                    routes = routes.OrderByDescending(s => s.route_ranking);
                    break;
                case "2":
                    routes = routes.OrderByDescending(s => s.route_altitude);
                    break;
                case "3":
                    routes = routes.OrderByDescending(s => s.route_dateMod);
                    break;
                default:  //Last Change or Insert
                    routes = routes.OrderByDescending(s => s.route_dateMod);
                    break;
            }

            return routes;

        }

        /// <summary>
        /// Update mountain categorization using the SUIOSA
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mntnRangeId"></param>
        /// <param name="mntnGroupId"></param>
        /// <returns></returns>
        public bool UpdateMountainData(int id, int mntnRangeId, int mntnGroupId) {

            route r = _context.route.Find(id);
            if (r == null || (mntnRangeId == 0 || mntnGroupId == 0))
            {
                return false;
            }

            r.mntnRange_id = mntnRangeId;
            r.mntnGroup_id = mntnGroupId;
            r.route_dateMod = DateTime.Now;
            _context.SaveChanges();

            return true;
        }

        public List<MountainActivity> GetMountainActivities(int mntnId) {
            List<MountainActivity> list;
            try {
                list = _context.v_route_index.Where(a => a.mountain_id == mntnId).GroupBy(a => a.activity_id)
                    .Select(cc => new MountainActivity { id = cc.FirstOrDefault().activity_id,
                                                         activity = cc.FirstOrDefault().activity_name,
                                                         number = cc.Count() }).ToList();
            }
            catch (Exception ex) {
                throw ex;
            }
            return list;
        }
    }
}
