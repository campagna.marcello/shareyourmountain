﻿using System;
using System.Collections.Generic;
using System.Linq;
using SYM.DataAccessLayer;

namespace SYM.BusinessLayer.Services
{
    public class MountainService
    {
        private Entities _context = new Entities();

        /// <summary>
        /// Set the mountain categorization comparing it with another peak
        /// </summary>
        /// <param name="mntnIdSource"></param>
        /// <param name="mntnIdToFix"></param>
        /// <returns></returns>
        public bool UpdateMissingCategorization(int mntnIdSource, int mntnIdToFix)
        {
            try
            {
                var mountainA = _context.mountain.Find(mntnIdSource);
                var objToFix = _context.mountain.Find(mntnIdToFix);

                //Check if the Mountain A is Valorized and MountainB Partial complete
                if (mountainA.mntnRange_id <= 0 && mountainA.mntnGroup_id <= 0)
                    return false;
                else if (objToFix.mntnRange_id <= 0 || objToFix.mntnGroup_id <= 0)
                    return false;

                //UpdateData
                objToFix.mntnGroup_id = mountainA.mntnGroup_id;
                objToFix.mntnRange_id = mountainA.mntnRange_id;
                objToFix.mntn_dateMod = DateTime.Now;

                //UPDATE COUNTRY AND REGION
                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public string UpdateMissingGeoData(int mountainId)
        {
            GeographicService GS = new GeographicService();
            string msgC = null,
                   msgR = null;
            string message = null;
            List<string> list = new List<string>();

            try
            {
                var mountain = _context.mountain.Find(mountainId);
                if (mountain == null)
                    return null;

                var geoInfoResponse = GS.GetGeoInfoByGoogle(mountain.mntn_latitude.GetValueOrDefault(), mountain.mntn_longitude.GetValueOrDefault());
                geo_region region = _context.geo_region.Where(r => r.country_code.Contains(geoInfoResponse.countryCode) && (r.region_name.Contains(geoInfoResponse.region) || r.region_wikipedia.Contains(geoInfoResponse.ISO3166_2))).FirstOrDefault();
                if (region == null)
                {
                    message = String.Format("Coordinates wrong - Lat {0} Lng {1}", mountain.mntn_latitude.ToString(),mountain.mntn_longitude.ToString());
                }
                else if (!CheckIfMountainHasGeoInfo(mountain, region))
                {
                    msgC = this.SetGeoCountry(mountainId, region.country_id);
                    msgR = this.SetGeoRegion(mountainId, region.id);
                    message = "AddGeoData -> " + msgC + " // " + msgR;
                }
                else
                    message = "AddGeoData -> Country & Region already created for " + mountain.mntn_name + " - " + mountain.id.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return message;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="mntnId"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public string SetGeoCountry(int mntnId, int countryId)
        {

            string msg = "";
            try
            {
                var country = _context.geo_country.Find(countryId);
                if (country == null)
                    msg = "Country Not Found";
                else if (AddCountry(mntnId, country))
                    msg = String.Format("Country {1} ADD for mountain {0}", mntnId.ToString(), country.country_code);
                else
                    msg = String.Format("Country {1} NOT ADD for mountain {0}", mntnId.ToString(), country.country_code);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return msg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mntnId"></param>
        /// <param name="regionName"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public string SetGeoRegion(int mntnId, int regionId)
        {

            string msg = "";
            try
            {
                var region = _context.geo_region.Find(regionId);
                if (region == null)
                    msg = "Region Not found";
                else if (AddRegion(mntnId, region))
                    msg = String.Format("Region {1} ADD for mountain {0}", mntnId.ToString(), region.region_name);
                else
                    msg = String.Format("Region {1} NOT ADD for mountain {0}", mntnId.ToString(), region.region_name);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return msg;
        }

        #region PRIVATE
        private bool AddRegion(int id, geo_region region)
        {

            bool result = false;
            try
            {
                ass_mountainregion obj = new ass_mountainregion();
                var checkIfExist = _context.ass_mountainregion.Where(a => a.mntn_id == id && a.region_id == region.id).ToList();

                if (checkIfExist.Count == 0)
                {
                    obj.mntn_id = id;
                    obj.country_id = region.country_id;
                    obj.region_name = region.region_name;
                    obj.region_id = region.id;
                    _context.ass_mountainregion.Add(obj);
                    _context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        private bool AddCountry(int id, geo_country country)
        {
            bool result = false;
            try
            {
                ass_mountaincountry obj = new ass_mountaincountry();
                var checkIfExist = _context.ass_mountaincountry.Where(a => a.mntn_id == id && a.country_id == country.id).ToList();

                if (checkIfExist.Count == 0)
                {
                    obj.mntn_id = id;
                    obj.country_id = country.id;
                    obj.country_code = country.country_code;
                    _context.ass_mountaincountry.Add(obj);
                    _context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Check if the mountain has the Region & Country categorization
        /// </summary>
        /// <param name="_mountain"></param>
        /// <param name="countryCode"></param>
        /// <param name="region"></param>
        /// <returns></returns>
        private bool CheckIfMountainHasGeoInfo(mountain _mountain, geo_region region)
        {

            bool result = true;

            if (_mountain.ass_mountaincountry.Count == 0 || _mountain.ass_mountainregion.Count == 0) //NOT Categorized for SURE
                return false;

            if (_mountain.ass_mountaincountry.Count > 0 && (_mountain.ass_mountaincountry.Where(a => a.country_code == region.country_code).FirstOrDefault() == null))
                result = false;
            else if (_mountain.ass_mountainregion.Count > 0 && (_mountain.ass_mountainregion.Where(a => a.region_id == region.id).FirstOrDefault() == null))
                result = false;

            return result;
        }

        #endregion
    }
}
