﻿using System;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace SYM.BusinessLayer
{
    public class CloudinaryIntegration
    {
        Account account = null;

        //Constructor
        public CloudinaryIntegration(string cloud,string key, string secret) {
            try
            {
                account = new Account(
                  cloud,
                  key,
                  secret);
            }
            catch (Exception ex) {
            }
        }

        public ImageUploadResult Upload(string folder, string tags, string pathFile)
        {

            string[] formats = new string[] { "jpg", "png", "gif" };
            string id = null;
            ImageUploadResult uploadResult;

            Cloudinary cloudinary = new Cloudinary(account);
            try
            {

                //Cloudinary Script
                var uploadParams = new ImageUploadParams()
                {
                    File = new FileDescription(@pathFile),
                    Folder = folder,
                    Format = "jpg",
                    AllowedFormats = formats,
                    Exif = true,
                    Tags = tags,
                    Metadata = true
                };
                uploadResult = cloudinary.Upload(uploadParams);
                id = uploadParams.PublicId;
                
            }
            catch (Exception ex)
            {
                return null;
            }

            return uploadResult;
        }
    }
}
