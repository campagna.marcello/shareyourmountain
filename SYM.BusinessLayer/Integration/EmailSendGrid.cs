﻿using System;
using System.Net;
using System.Net.Mail;
using SendGrid;
using System.IO;

namespace SYM.BusinessLayer
{
    /* CREDENTIALS _ TODO: crearte dedicate credentials*/
    public class EmailSendGrid
    {
        //public static readonly EmailSendGrid instance = new EmailSendGrid();
        private string sgUsername = "";
        private string sgPassword = "";

        public EmailSendGrid(string _userEncript,string _passEncript)
        {
            sgUsername = EncryptUtil.instance.Decrypt(_userEncript, true);
            sgPassword = EncryptUtil.instance.Decrypt(_passEncript, true);
        }
       
        public void Send(string emailTo, string emailFrom, string subject, string body)
        {
            //try
            //{

            SendGridMessage myMessage = new SendGridMessage();
            myMessage.AddTo(emailTo);
            myMessage.From = new MailAddress(emailFrom, "ShareYourMountain Support");
            myMessage.AddBcc("campagna.marcello@gmail.com");
            myMessage.Subject = subject;
            myMessage.Html = body;

            // Create a Web transport, using API Key //TODO CRearte New Credentials
            var credentials = new NetworkCredential(sgUsername,sgPassword);
            var transportWeb = new Web(credentials);

            // Send the email.
            transportWeb.DeliverAsync(myMessage);

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }
    }

    public class EmailUtil {

        public static string GetBody(string pathFile, string opt1 = "", string opt2 = "", string opt3 = "")
        {

            string body = "";
            //Read template file
            if (!String.IsNullOrEmpty(pathFile))
            {
                using (var sr = new StreamReader(pathFile))
                {
                    body = sr.ReadToEnd();
                }
                body = string.Format(body, opt1, opt2, opt3);
            }

            return body;
        }


    }


}