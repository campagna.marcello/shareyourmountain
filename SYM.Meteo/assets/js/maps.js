var markersArray = [];
var map;
var marker;
var infowindow;

function addMarker(latitude, longitude, text) {
  if (text === undefined) {
    text = '';
  }
  
  infowindow.close();
  marker.setVisible(false);
  marker.setPosition(new google.maps.LatLng(latitude, longitude));
  map.setCenter(new google.maps.LatLng(latitude, longitude));
  marker.setVisible(true);
  infowindow.setContent('<div><strong>'+ text +'</strong><br>');
  infowindow.open(map, marker);
  //Array utile per rimuovere tutti marker presenti.
  //markersArray.push(marker);
}

//Not Used
function clearOverlays() {
  for (var i = 0; i < markersArray.length; i++ ) {
    markersArray[i].setMap(null);
  }
  markersArray.length = 0;
}

function initMapNew(latitude, longitude) {
    
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: new google.maps.LatLng(latitude, longitude), 
    // How you would like to style the map. 
    // This is where you would paste any style found on Snazzy Maps.
    styles: [
    { featureType: 'water', stylers: [{ color: '#03C9A9' }, { visibility: 'on' }] }, 
    { featureType: 'landscape', stylers: [{ color: '#ffc04c' }] }, 
    { featureType: 'road', stylers: [{ saturation: -100 }, { lightness: 45 }] },
    { featureType: 'road.highway', stylers: [{ visibility: 'simplified' }] },
    { featureType: 'road.arterial', elementType: 'labels.icon', stylers: [{ visibility: 'on' }] }, 
    { featureType: 'administrative', elementType: 'labels.text.fill', stylers: [{ color: '#444444' }] }, 
    { featureType: 'transit', stylers: [{ visibility: 'off' }] }, 
    { featureType: 'poi', stylers: [{ visibility: 'off' }] }],
    mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
      mapTypeIds: [
        google.maps.MapTypeId.ROADMAP,
        google.maps.MapTypeId.TERRAIN
      ]
    },
    zoomControl: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.SMALL
    }
  });
  
  var input = (document.getElementById('pac-input'));
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var options = {
    types: ['(cities)']
  };

  var autocomplete = new google.maps.places.Autocomplete(input,options);
  autocomplete.bindTo('bounds', map);

  //Init InfoWindowds & Marker
  infowindow = new google.maps.InfoWindow();
  marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29)
  });

  //The Autocomplete Function
  autocomplete.addListener('place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return;
    }

    map.setCenter(place.geometry.location);
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    infowindow.setContent('<div><strong>...' + place.name + '...</strong><br>');
    infowindow.open(map, marker);
    loadWeather(place.geometry.location.A +',' + place.geometry.location.F);
  });

}