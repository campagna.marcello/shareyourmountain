var options = {
    enableHighAccuracy: false,
    timeout: 5000,
    maximumAge: 0
};

function error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
    loadWeather('Milano');
    initMapNew(45.464211, 9.191383);
};

function loadWeather(location, woeid) {
    $.simpleWeather({
        location: location,
        woeid: woeid,
        unit: 'c',
        success: function (weather) {
            var html, html1, html2, html3;

            html = '<h3 id="city" class="city_changeable" contenteditable="true">' + weather.city + ', ' + weather.region;
            /* Does your browser support geolocation? */
            if ("geolocation" in navigator) {
                html += ' <i class="fa fa-map-o"></i>';
            }
            html += '</h3>';
            $("#weather-title").html(html);

            html1 = '<h2><i class="icon icon-' + weather.code + '"></i> ' + weather.temp + '&deg;' + weather.units.temp + '</h2>';
            html1 += '<ul><li class="currently">' + weather.low + '&deg;' + weather.units.temp + ' Min ' + weather.high + '&deg;' + weather.units.temp + ' Max</li>';
            html1 += '<li class="currently">' + weather.currently + '</li>';
            html1 += '<li class="currently">' + weather.wind.speed + ' ' + weather.units.speed + ' (' + weather.wind.direction + ')</li>';
            html1 += '<li class="currently">' + weather.humidity + '% Humidity</li></ul>';
            $("#slide-1").html(html1);

            html2 = '';
            for (var i = 0; i < weather.forecast.length; i++) {
                html2 += '<div class="row forecast"><div class="col-xs-7 col-sm-2 col-md-4"><bold>' + weather.forecast[i].date.replace("2015", "") + ' ' + weather.forecast[i].day + '</bold></div><div class="col-xs-1 col-sm-9 col-md-6 forecast-descr">' + weather.forecast[i].text + '</div><div class="col-xs-5 col-sm-1 col-md-2"><i class="icon forecast icon-' + weather.forecast[i].code + '"></i></div></div>';//
            }
            $("#slide-2").html(html2);
            
            //Don't forget to include the moment.js plugin.
            //var timestamp = moment(weather.updated);
            html3 = '<p class="updated">Updated ' + weather.updated + '</p>';
            html3 += '<p><a href="https://www.yahoo.com/?ilc=401" target="_blank"> <img src="https://poweredby.yahoo.com/purple.png" width="134" height="29"/> </a>' + '</p>';
            $("#weather-bottom").html(html3);
            
            //Todo: make external the binding
            bindEventWeather();
            placesAutocomplete();
            //setCarouselHeight('#carousel-example');
                
        },
        error: function (error) {
            $("#weather").html('<p>' + error + '</p>');
        }
    });
}

function setCarouselHeight(id) {
    var slideHeight = [];
    var max;
    $(id + ' .item').each(function () {
        // add all slide heights to an array
        slideHeight.push($(this).height());
    });     
    // find the tallest item
    max = Math.max.apply(null, slideHeight);
    // set the slide's height
    $(id + ' .carousel-content').each(function () {
        $(this).css('height', max + 'px');
    });
}

function bindEventWeather() {
    //TODO:create a function or call the function after the event the successfull execution of LoadWeather
    var contents = $('.city_changeable').html();
    $('.city_changeable').blur(function () {
        if (contents != $(this).html()) {
            contents = $(this).html();
            loadWeather(contents, '');
        }
    });

    $('.city_changeable').focusout(function (e) {
        var tmp = $('.city_changeable').html();
        if (tmp.length == 0)
            $('.city_changeable').html(contents);
    });

    $('.city_changeable').hover(function (e) {
        $(this).focus();
    });

    $('.city_changeable').keypress(function (event) {
        var key = event.which;
        if (key == 13)  // the enter key code
        {
            if (contents != $(this).html()) {
                contents = $(this).html();
                loadWeather(contents, '');
            }
            return false;
        }
    });

    $('.city_changeable').click(function () {
        $('.city_changeable').empty();
    });
    
    /* Where in the world are you? */
    $('.fa-map-marker').on('click', function () {
        //TODO: Insert skip to Map Section
    });

}

function placesAutocomplete() {

    var acService = new google.maps.places.AutocompleteService(),
        placesService = new google.maps.places.PlacesService(document.createElement('div'));

    $("#city").autocomplete({
        source: function (req, resp) {
            acService.getPlacePredictions({
                input: req.term,
                types: ['(cities)']
                //componentRestrictions: {country: "ch"}
            }, function (places, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    var _places = [];
                    for (var i = 0; i < places.length; ++i) {
                        _places.push({
                            id: places[i].place_id,
                            value: places[i].description,
                            label: places[i].description
                        });
                    }
                    resp(_places);
                }
            });
        },
        select: function (e, o) {
            placesService.getDetails({
                placeId: o.item.id
            }, function (place, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    addMarker(place.geometry.location.A, place.geometry.location.F, o.item.label);
                }
            });
        },
        minLength: 4
    });
}

function smoothScroll_Sym(offset) {

    $('.smoothScroll').on('click', function (e) {
        e.preventDefault();

        var target = this.hash,
            $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - offset
        }, 1000, 'swing', function () {
            window.location.hash = '1' + target;
        });
    });

}