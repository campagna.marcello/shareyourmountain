﻿using NLog;
using SYM.DataAccessLayer;
using SYM.ModelWikipedia;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SYM.ETL_OverPassTurbo
{
    public class OSMparameters
    {
        public string region_id { get; set; }
        public string region { get; set; }
        public string country_code { get; set; }
        public int country_id { get; set; }
        public string filename { get; set; }
    }

    class Program
    {
        #region DeclareVariables
        static private string pathFolderTemp;
        static private string pathFolderLog;
        //static private string p_region = "1747"; //1733 Friuli 1736 Lombardia 1747 Veneto
        //static private string p_country = "IT";
        static private Logger logger;

        #endregion

        static void Main(string[] args)
        {
            List<OSMparameters> OSMList = new List<OSMparameters>();

            OSMList = SetOSMparameters();

            //Init
            //obj = SystemFileUtility.Instance;
            logger = LogManager.GetCurrentClassLogger();

            Console.WriteLine("*******************************************************");
            Console.WriteLine("SHAREYOURMOUNTAIN ETL - Import Mountains Opendata from OSM");
            Console.WriteLine("*******************************************************\n");

            //Setting main directories of console application
            pathFolderTemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYM.OpenStreetMap.Properties.Settings.Default.tempFolder);
            pathFolderLog = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SYM.OpenStreetMap.Properties.Settings.Default.logFolder);

            foreach (OSMparameters Osm in OSMList)
            {

                Console.WriteLine("\n>> Start Serialization xml file ..." + Osm.filename);

                //Load file and PArsing node of file xml.
                List<WikiMountain> objects = SerializeXmlWikipediaToObject(Path.Combine(pathFolderTemp, Osm.filename));
                Console.WriteLine("N° mountains to import: " + objects.Count());

                //Check list and use object if name is not empty and altitude > 600
                foreach (WikiMountain m in objects)
                {
                    Console.WriteLine(m.name);
                    etl_mountain tmp = Exist_MountainOSM(m.name,Osm.country_code,Osm.country_id);

                    if (tmp == null && m.altezza > 600)
                        addEtlMountain(m, Osm.region_id, Osm.country_code, Osm.country_id);
                    if (tmp != null && m.altezza > 600 && (m.altezza == tmp.mountain_altitude || tmp.mountain_altitude == null )) //Bug Duplicati
                        updateEtlMountain(m, Osm.region_id, Osm.country_code);
                }
            }

        }

        ///<summary>
        /// Serialize Table Html in Object List to convert it in file xml - parsingWikipediaResponse
        ///<param name="ListOfEntry"></param>
        ///<param name="filePath"></param>
        ///<param name="typeEvent">Planned or Unplanned</param>
        ///</summary>
        static public List<WikiMountain> SerializeXmlWikipediaToObject(string pathFileToLoad)
        {
            XmlDocument doc = new XmlDocument();
            List<WikiMountain> listObj = new List<WikiMountain>();

            WikiMountain obj = null;

            try
            {
                doc.Load(@pathFileToLoad);

                foreach (XmlNode node in doc.GetElementsByTagName("node"))
                {
                    try
                    {
                        //TODO: Clean String
                        obj = new WikiMountain();
                        obj.region = new List<string>();
                        obj.country = new List<string>();
                        obj.valid = true;

                        obj.latitudine = float.Parse(node.Attributes["lat"].Value, CultureInfo.InvariantCulture.NumberFormat);
                        obj.longitudine = float.Parse(node.Attributes["lon"].Value, CultureInfo.InvariantCulture.NumberFormat);

                        foreach (XmlNode tag in node.ChildNodes)
                        {
                            string t = tag.Attributes["k"].Value;
                            if (t == "ele")
                                obj.altezza = Convert.ToInt32(tag.Attributes["v"].Value);
                            else if (t == "name")
                            {
                                obj.name = tag.Attributes["v"].Value;
                                //Console.WriteLine("Mountain ->" + obj.name);
                            }
                            else if (t == "name:de")
                            {
                                obj.nameDE = tag.Attributes["v"].Value;
                                //Console.WriteLine("Mountain DE ->" + obj.name);
                            }
                            else if (t == "name:it")
                            {
                                obj.nameIT = tag.Attributes["v"].Value;
                                //Console.WriteLine("Mountain IT ->" + obj.name);
                            }
                            else if (t == "wikipedia")
                                obj.urlResource = tag.Attributes["v"].Value.Replace("it:", "").Replace("de:", "");
                        }

                        if (!String.IsNullOrEmpty(obj.name))
                        {
                            listObj.Add(obj);
                            Console.WriteLine("Mountain Read ->" + obj.name);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error("Parsing Node Xml " + e.Message);
                        continue;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + obj.name + " -> " + e.Message + "\r\n" + e.StackTrace);
                logger.Error("Parsing Xml " + e.Message);
            }

            return listObj;
        }

        //OK Add Record
        static private void addEtlMountain(WikiMountain tmp, string region, string country, int country_id)
        {

            try
            {
                EntitiesETL _context = new EntitiesETL();

                int c = _context.etl_route_temp.ToList().Count();
                string name = null;

                //if (String.IsNullOrEmpty(tmp.urlResource))
                    name = tmp.name;
                //else
                //    name = tmp.urlResource;

                etl_mountain mnt = new etl_mountain
                {
                    mountain_name = tmp.name,
                    mountain_latitude = tmp.latitudine,
                    mountain_longitude = tmp.longitudine,
                    mountain_altitude = tmp.altezza,
                    wikipedia_name = tmp.urlResource,
                    region_id_str = region,
                    country_code = country,
                    country_id = country_id,
                    region_id = Convert.ToInt32(region),
                    osm = 1,
                    mountain_description = "OSM",
                    dateIns = DateTime.Now,
                };

                _context.etl_mountain.Add(mnt);

                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Insert" + ex.Message);
                logger.Error("Record Duplicato: " + tmp.name);
            }

        }

        //OK Update MOntain if exist after check that the mounatin is the same cause the name is not unique
        static private void updateEtlMountain(WikiMountain tmp, string region, string country)
        {

            try
            {

                EntitiesETL _context = new EntitiesETL();
                etl_mountain etlMntn = _context.etl_mountain.Where(a => a.mountain_name == tmp.name.Replace("_", " ")).First();

                //etlMntn.mountain_name = tmp.name;
                etlMntn.mountain_latitude = tmp.latitudine;
                etlMntn.mountain_longitude = tmp.longitudine;
                etlMntn.mountain_altitude = tmp.altezza;
                etlMntn.osm = 1;

                if (String.IsNullOrEmpty(etlMntn.region_id_str))
                    etlMntn.region_id_str = etlMntn.region_id_str + "," + region;
                if (String.IsNullOrEmpty(etlMntn.country_code))
                    etlMntn.country_code = etlMntn.country_code + "," + country;
                
                
                etlMntn.wikipedia_name = tmp.urlResource;
                etlMntn.mountain_description = "OSM";
                etlMntn.dateMod = DateTime.Now;

                //Se esiste una montagna con altitudine valorizzata ed è diversa dall'oggetto ricevuto non salvo.

                //Bug con nome duplicato su piu' regioni......................
                //I duplicati non dovrebbero esserci sulla tabella ma sul file xml
                if (tmp.name == null || (tmp.altezza != etlMntn.mountain_altitude))
                    return;

                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Update" + ex.Message);
                logger.Error("UpdateEtlMountain into table >> " + tmp.name);
            }

        }

        ///<summary>
        /// Check if exist mountain is already downloaded in temp table
        ///<param name="name">Mounatin name in temporary table ETl</param>
        ///</summary>
        static private etl_mountain Exist_MountainOSM(string name, string country = "", int country_id = 0)
        {
            EntitiesETL _context = new EntitiesETL();
            var list = _context.etl_mountain.Where(a => a.mountain_name == name).ToList();

            if (list.Count > 0)
                return list.First();
            else
                return null;
        }

        ///<summary>
        /// Init the complete list of OSM file divise for region which contains all mountains data. This permit to creare a loop and make fast the insert/update of data in temp table.
        ///</summary>
        static private List<OSMparameters> SetOSMparameters()
        {
            List<OSMparameters> list = new List<OSMparameters>();
            //OSMparameters obj1 = new OSMparameters();
            //obj1.filename = "export-osm-peak-Veneto.xml";
            //obj1.country_code = "IT";
            //obj1.region = "Veneto";
            //obj1.region_id = "1747";
            //list.Add(obj1);

            //OSMparameters obj2 = new OSMparameters();
            //obj2.filename = "export-osm-peak-Lombardia.xml";
            //obj2.country_code = "IT";
            //obj2.region = "Lombardia";
            //obj2.region_id = "1736";
            //list.Add(obj2);

            //OSMparameters obj3 = new OSMparameters();
            //obj3.filename = "export-osm-peak-FriuliVeneziaGiulia.xml";
            //obj3.country_code = "IT";
            //obj3.region = "Friuli Venezia Giulia";
            //obj3.region_id = "1733";
            //list.Add(obj3);

            //OSMparameters obj4 = new OSMparameters();
            //obj4.filename = "export-osm-peak-Trentino-AltoAdigeSüdtirol.xml";
            //obj4.country_code = "IT";
            //obj4.region = "Trentino-Alto Adige / Südtirol";
            //obj4.region_id = "1744";

            //OSMparameters obj5 = new OSMparameters();
            //obj5.filename = "export-osm-peak-Piemonte.xml";
            //obj5.country_code = "IT";
            //obj5.country_id = 116;
            //obj5.region = "Piemonte";
            //obj5.region_id = "1739";
            //list.Add(obj5);

            //OSMparameters obj6 = new OSMparameters();
            //obj6.filename = "export-osm-peak-Ticino.xml";
            //obj6.country_code = "CH";
            //obj6.country_id = 217;
            //obj6.region = "Ticino";
            //obj6.region_id = "551";
            //list.Add(obj6);

            //OSMparameters obj7 = new OSMparameters();
            //obj7.filename = "export-osm-peak-ValDAosta.xml";
            //obj7.country_code = "IT";
            //obj7.country_id = 116;
            //obj7.region = "Valle d'Aosta";
            //obj7.region_id = "1746";
            //list.Add(obj7);

            //OSMparameters obj8 = new OSMparameters();
            //obj8.filename = "export-osm-peak-Liguria.xml";
            //obj8.country_code = "IT";
            //obj8.country_id = 116;
            //obj8.region = "Liguria";
            //obj8.region_id = "1735";
            //list.Add(obj8);

            OSMparameters obj9 = new OSMparameters();
            obj9.filename = "export-osm-peak-Emilia-Romagna.xml";
            obj9.country_code = "IT";
            obj9.country_id = 116;
            obj9.region = "Emilia-Romagna";
            obj9.region_id = "1732";
            list.Add(obj9);

            OSMparameters obj10 = new OSMparameters();
            obj10.filename = "export-osm-peak-Marche.xml";
            obj10.country_code = "IT";
            obj10.country_id = 116;
            obj10.region = "Marche";
            obj10.region_id = "1737";
            list.Add(obj10);

            OSMparameters obj11 = new OSMparameters();
            obj11.filename = "export-osm-peak-Abruzzo.xml";
            obj11.country_code = "IT";
            obj11.country_id = 116;
            obj11.region = "Abruzzo";
            obj11.region_id = "1728";
            list.Add(obj11);

            return list;

        }

    }
}
