﻿using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using SYM.BusinessLayer.Services;
using SYM.DataAccessLayer;
using SYM.SearchEngine.MvcApp.ControllersApi.PageStatistics;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace WebApi.Tests
{
    [TestFixture]
    class BusinessLogic
    {
        [Test, Order(1)]
        [Category("CheckInfo")]
        public void Should_Return_Valid_CountryCode()
        {
            GeographicService a = new GeographicService();
            GeoInfo result = a.GetGeoInfoByGoogle(float.Parse("45,87612"), float.Parse("9,394422"));

            result.countryCode.ShouldBeEquivalentTo("IT");

        }

        [Test, Order(2)]
        [Category("CheckInfo")]
        public void Should_Return_Valid_Region()
        {
            GeographicService a = new GeographicService();
            GeoInfo result = a.GetGeoInfoByGoogle(float.Parse("45,87612"), float.Parse("9,394422"));

            result.region.ShouldBeEquivalentTo("Lombardia");

        }

        //[Test, Order(3)]
        //[Category("CheckInfo")]
        //public void Should_NOT_Return_Valid_CountryCode()
        //{
        //    Geographic a = new Geographic();
        //    GeoInfo result = a.GetGeoInfoByGoogle(float.Parse("45,87612"), float.Parse("9,394422"));

        //    result.countryCode.Should().NotBeSameAs("IT");

        //}

        //[Test, Order(4)]
        //[Category("CheckInfo")]
        //public void Should_NOT_Return_Valid_Region()
        //{
        //    Geographic a = new Geographic();
        //    GeoInfo result = a.GetGeoInfoByGoogle(float.Parse("45,87612"), float.Parse("9,394422"));

        //    result.region.Equals("Lombardia").Should().;

        //}

    }
}
