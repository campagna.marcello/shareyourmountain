﻿using FluentAssertions;
using NUnit.Framework;
using SYM.WebApp.ControllersApi;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System;

namespace WebApi.Tests
{
    [TestFixture]
    public class Mountain
    {
        private List<Parameters> GetTestParameter()
        {
            var testData = new List<Parameters>();
            testData.Add(new Parameters { p_url_mntnToFix = "1874", p_regionCode = "Ticino", p_countryCode = "CH", p_url_mntnSource = "" });
            testData.Add(new Parameters { p_url_mntnToFix = "1874", p_regionCode = "CH", p_countryCode = "CAH", p_url_mntnSource = "CH2"});
            testData.Add(new Parameters { p_url_mntnToFix = "32", p_regionCode = "Lombardia", p_countryCode = "IT", p_url_mntnSource = "CH2"});

            return testData;
        }

        [Test, Order(2)]
        [Category("AddInfo")]
        public void Should_NOT_Add_Missing_GeoInfo()
        {
            //Arrange
            var controller = new MountainController();
            List<Parameters> list = GetTestParameter();

            //Act
            HttpResponseMessage result = controller.UpdateMissingGeoData(Convert.ToInt32(list[2].p_url_mntnToFix));

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NotAcceptable);

        }

        [Test, Order(2)]
        [Category("AddInfo")]
        public void Should_Add_Missing_GeoInfo()
        {
            //Arrange
            var controller = new MountainController();
            List<Parameters> list = GetTestParameter();

            //Act
            HttpResponseMessage result = controller.UpdateMissingGeoData(Convert.ToInt32(list[1].p_url_mntnToFix));

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);

        }
    }

}
