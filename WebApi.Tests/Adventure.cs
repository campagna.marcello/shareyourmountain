﻿using FluentAssertions;
using NUnit.Framework;
using SYM.SearchEngine.MvcApp.ControllersApi;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace WebApi.Tests
{
    [TestFixture]
    public class Adventure
    {

        [Test, Order(1)]
        [Category("NotRemoved")]
        public void Should_Not_Remove_Mountain()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Remove(a[0]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);

        }

        [Test, Order(2)]
        [Category("NotRemoved")]
        public void Should_Not_Remove_Route()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Remove(a[1]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);

        }

        [Test, Order(3)]
        [Category("NotRemoved")]
        public void Should_Not_Remove_WishListRoute()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Remove(a[2]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);

        }

        [Test, Order(4)]
        [Category("NotRemoved")]
        public void Should_Not_Remove_ClimbedRoute()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Remove(a[3]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);

        }

        [Test, Order(5)]
        [Category("Insert")]
        public void Should_Insert_New_Mountain()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Set(a[0]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);

        }

        [Test, Order(6)]
        [Category("Insert")]
        public void Should_Insert_New_Route()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Set(a[1]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);

        }

        [Test, Order(7)]
        [Category("Insert")]
        public void Should_Insert_New_WishListRoute()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Set(a[2]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);

        }

        [Test, Order(8)]
        [Category("Insert")]
        public void Should_Insert_New_ClimbedRoute()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Set(a[3]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);

        }

        [Test, Order(9)]
        [Category("Removed")]
        public void Should_Remove_Mountain()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Remove(a[0]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);

        }

        [Test, Order(10)]
        [Category("Removed")]
        public void Should_Remove_Route()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Remove(a[1]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);

        }

        [Test, Order(11)]
        [Category("Removed")]
        public void Should_Remove_WishListRoute()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Remove(a[2]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);

        }

        [Test, Order(12)]
        [Category("Removed")]
        public void Should_Remove_ClimbedRoute()
        {
            //Arrange
            var controller = new AdventureController();
            List<Parameters> a = GetTestProducts();

            //Act
            HttpResponseMessage result = controller.Remove(a[3]);

            //Assert
            result.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);

        }

        private List<Parameters> GetTestProducts()
        {
            var testadventures = new List<Parameters>();
            testadventures.Add(new Parameters { p_mountain = "1316", p_route = "0", p_hut = "0", p_done = "", p_type = "", p_profileid = "1" });
            testadventures.Add(new Parameters { p_mountain = "0", p_route = "394", p_hut = "0", p_done = "true", p_type = "", p_profileid = "1" });
            testadventures.Add(new Parameters { p_mountain = "1318", p_route = "6998", p_hut = "", p_done = "false", p_type = "", p_profileid = "1" });
            testadventures.Add(new Parameters { p_mountain = "1318", p_route = "10129", p_hut = "", p_done = "true", p_type = "", p_profileid = "1" });
            testadventures.Add(new Parameters { p_mountain = "1240", p_route = "82", p_hut = "", p_done = "true", p_type = "", p_profileid = "1" });

            return testadventures;
        }
    }
}
