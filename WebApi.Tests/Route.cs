﻿using FluentAssertions;
using NUnit.Framework;
using SYM.WebApp.ControllersApi;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System;
using SYM.BusinessLayer.Services;

namespace WebApi.Tests
{
    [TestFixture]
    class Route
    {
        [Test, Order(2)]
        [Category("AddInfo")]
        public void Should_Add_New_City()
        {
            //Arrange
            var geographicService = new GeographicService();

            //Act

            float lat = float.Parse("48,8333015");

            var region = geographicService.GetRegionByName("Lombardia", "IT");
            var result = geographicService.GetCity("Faloppio 2",region, float.Parse("48.8333015"), float.Parse("15.0667000"));

            //Assert
            result.ShouldBeEquivalentTo(0);

        }

        //public void Should_Not_Add_New_City()
        //{

        //}

        //public void Should_Not_Add_Existing_City()
        //{

        //}

        //public void Should_Return_Error()
        //{

        //}

    }
}
