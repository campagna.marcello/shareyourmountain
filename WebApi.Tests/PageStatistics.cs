﻿using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using SYM.DataAccessLayer;
using SYM.SearchEngine.MvcApp.ControllersApi.PageStatistics;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace WebApi.Tests
{
    [TestFixture]
    public class PageStatistics
    {
        [Test]
        public void Should_Increment_Views_After_Call_Mountain_Page()
        {
            //Arrange
            var controller = new PageStatisticsController();
            List<Parameters> a = Data_PagesMountains();
            HttpResponseMessage result;
            page_statistics stats1, stats2;

            //Act
            //1 - Get the Value first call action 
            result = controller.GetCounter(a[0]);
            stats1 = JsonConvert.DeserializeObject<page_statistics>(result.Content.ReadAsStringAsync().Result);

            //2 - Set Value
            result = controller.SetCounter(a[0]);

            //3 - Get the Value after call action
            result = controller.GetCounter(a[0]);
            stats2 = JsonConvert.DeserializeObject<page_statistics>(result.Content.ReadAsStringAsync().Result);

            //Assert counterOld < counterNew
            stats1.page_countview.Should().BeLessThan((int)stats2.page_countview);

        }

        [Test]
        public void Should_Increment_Views_After_Call_Route_Page()
        {
            //Arrange
            var controller = new PageStatisticsController();
            List<Parameters> a = Data_PagesRoutes();
            HttpResponseMessage result;
            page_statistics stats1, stats2;

            //Act
            //1 - Get the Value first call action 
            result = controller.GetCounter(a[0]);
            stats1 = JsonConvert.DeserializeObject<page_statistics>(result.Content.ReadAsStringAsync().Result);

            //2 - Set Value
            result = controller.SetCounter(a[0]);

            //3 - Get the Value after call action
            result = controller.GetCounter(a[0]);
            stats2 = JsonConvert.DeserializeObject<page_statistics>(result.Content.ReadAsStringAsync().Result);

            //Assert counterOld < counterNew
            stats1.page_countview.Should().BeLessThan((int)stats2.page_countview);

        }

        private List<Parameters> Data_PagesMountains()
        {
            var testadventures = new List<Parameters>();
            testadventures.Add(new Parameters { p_id = "1316", p_type = "m"});
            testadventures.Add(new Parameters { p_id = "1318", p_type = "m"});
            testadventures.Add(new Parameters { p_id = "1318", p_type = "m"});
            testadventures.Add(new Parameters { p_id = "1240", p_type = "m"});
            testadventures.Add(new Parameters { p_id = "0", p_type = "m" });

            return testadventures;
        }

        private List<Parameters> Data_PagesRoutes()
        {
            var testadventures = new List<Parameters>();
            testadventures.Add(new Parameters { p_id = "141", p_type = "r" });
            testadventures.Add(new Parameters { p_id = "1318", p_type = "r" });
            testadventures.Add(new Parameters { p_id = "1318", p_type = "r" });
            testadventures.Add(new Parameters { p_id = "1240", p_type = "r" });
            testadventures.Add(new Parameters { p_id = "0", p_type = "r" });

            return testadventures;
        }
    }
}

